&nbsp;
&nbsp;

<div align="center">
    <img width="350px" style="border: 0;" src="docs/img/logo/Maestro_logo_blue.svg" alt="Maestro"/>
</div>

&nbsp;
&nbsp;

Maestro is a lightweight software library for workflow management, written in Python. Maestro is especially suited for the complexity of modern bioinformatic setups, but has a generic and modular design that can be adapted to any informatic workflow. Key features are automation and decoupling of workflow steps.

## Documentation

Maestro's documentation is available at [dpipe.gitlab.io/labautomation/maestro](https://dpipe.gitlab.io/labautomation/maestro/). Alternatively, individual documentation pages can be found in the `docs/source/` directory.

## License and copyright

Maestro is licensed under the GNU General Public License v3.0

Copyright (C) 2023-2024 Maestro contributors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.
