Customization
=============

Artefact UI Customization
-------------------------

Artefacts are shown in the UI by rendering the HTML returned by the property `html` of the artefact.

By default, it will use the template `default_artefact.html`, but can be customized by overriding the `html` property of the artefact.

For example, to customize the `html` of a `TextArtefact`:

```python

from maestro.models import BaseArtefactModel

class CustomTextArtefact(BaseArtefactModel):
    def __init__(self, text: str):
        self.text = text

    @property
    def html(self):
        return f"<p>{self.text}</p>"
```

.. note::
    The `html` property should return a string of HTML, not a template.

.. note::
    Tailwind classes should not be used in the `html` property, as the UI will not have access to the Tailwind CSS classes.

