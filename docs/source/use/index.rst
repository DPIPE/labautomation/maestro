Using Maestro
=============

*Documentation for users of Maestro.*

----

If you just want to use Maestro, we recommend going through :doc:`getting_started`, :doc:`example` and :doc:`running`, while :doc:`manual` will provide guidance for task monitoring via the web user interface

.. toctree::
   :maxdepth: 1

   getting_started
   example
   running
   manual
   concepts
