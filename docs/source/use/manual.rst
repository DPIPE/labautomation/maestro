Maestro UI
==========

The Maestro user interface (UI) is primarily meant for monitoring the status of workflows. In addition, a few actions are provided in the UI, including `Action: Invalidate`_, `Action: Reorder`_ and `Action: Abort`_.

Note that the Maestro UI is not intended for *creating* or *editing* workflows. This should instead be done through the `Maestro API <concepts.html#maestro-api>`_.

The main interface of Maestro has three components:

1. A `Dashboard with workflow statuses`_ for monitoring the status of all workflows. 
2. A search box and toggles to `Filter workflows`_.
3. A list of `Workflows, steps and details`_, sorted by latest on top.

Dashboard with workflow statuses
--------------------------------

The dashboard shows the status of all workflows in the system, grouped by status. The statuses are:

.. csv-table::
    :header: "Status", "Description", "Keyboard shortcut"

    "Total", "The total number of workflows in the system.", "``T``"
    "Completed", "Successfully completed workflows.", "``C``"
    "Running", "Currently running workflows.", "``R``"
    "Queued", "Workflows that have all necessary inputs and are awaiting available resources.", "``Q``"
    "Pending", "Workflows that have been ordered but awaits some input.", "``P``"
    "Halted", "Workflows that are halted, e.g. due to failing test.", "``A``" 
    "Cancelled", "Workflows that have been manually cancelled.", "``X``" 
    "Failed", "Workflows where one or more steps have failed.", "``F``"

Filter workflows
----------------

Workflows can be filtered by status, priority or free-text. The figure below gives an overview of the different options, including keyboard shortcuts.

.. _filter_workflows_figure:

.. figure:: ../../img/filter_workflows.png
   :alt: Filtering options
   :align: center

   **Figure:** The different ways of filtering workflows in Maestro. Keyboard shortcuts are shown in red.

.. note::

    Clicking ``Total`` in the dashboard or pressing the ``T`` key will reset all filters.

Filter by priority or status
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To filter the list of workflows by priority, toggle the ``Normal`` (``N``), ``High`` (``H``) or ``Urgent`` (``U``) buttons in the top banner. Similarly, statuses can be filtered by toggling the dashboard entries. Toggles can be done either by mouse or keyboard shortcut (see :ref:`figure <filter_workflows_figure>`). Any status can be toggled, but note that ``Total`` will reset all filters, including the free-text search field.

Multiple toggles for status or priority are combined with an OR operator. If both status and priority are toggled, the resulting list will show workflows that match any of the status toggles AND any of the priority toggles.

Filter by free-text
~~~~~~~~~~~~~~~~~~~

You can also filter workflows using free-text input in the search box at the top. Press the ``S`` key or click on the search box to start searching. When multiple space-separated search terms are entered, they are implicitly combined with each other and any enabled status or priority filters with an AND operator.

.. note::

    When the search box is in focus, keyboard shortcuts for status and priority are disabled. To enable keyboard shortcuts again, press ``Tab`` or click mouse outside, or ``Esc`` to exit *and* clear the input.

Allowed inputs include:

- Workflow names (any part)
- Step names (any part)
- Metadata keywords (availability depending on the setup in your app)
- Negation of the above by prefixing with ``-``

The following inputs are also allowed, but note that their use will not be reflected in the status of corresponding toggles:

- Workflow statuses, e.g. ``status:failed``, ``status:completed,pending``
- Workflow/job priority, e.g. ``priority:normal``, ``priority:high,urgent``

.. note::

    It is recommended to use *either* the toggles *or* the free-text search for filtering by status and priority, but not both at the same time, as the results may be confusing.

Keyboard shortcuts
~~~~~~~~~~~~~~~~~~

Keyboard shortcuts are available for toggling status and priority and to focus on/clear the search box (see the :ref:`figure <filter_workflows_figure>` above for reference). Status and priority shortcuts are disabled when the search box is in focus.

Workflows, steps and details
----------------------------

Click on a workflow in the main interface to view its individual steps, and the ``Details`` button to view the details for a particular step. 

For jobs in steps that are Pending, the Details modal has separate tabs for:

- Missing inputs: Required inputs for the step that are missing
- Existing inputs: Any required inputs that have already been created, including artefact properties
- Action: Abort the step - see below for further explanation

.. tip::

    Use your mouse to hover over the PENDING status for a step in the main interface to get a quick overview of required inputs.

For other job statuses, the Details modal has separate tabs for: 

- Input artefacts: Required input for the step, including artefact properties
- Output artefacts: Produced output from the step
- Standard output logs
- Standard error logs
- Action: Abort, invalidate or reorder individual steps/artefacts - see below for further explanation

The footer of the Details modal shows Workflow name, step type and job display name (if available).

.. tip::

    Click on any input/output artefact property to copy its value to the clipboard.

Action: Invalidate
------------------

Invalidation means that outputs from a workflow, workflow step or individual output artefact are marked as invalid to prevent further use. This is useful if a step has produced erroneous results that should not be used downstream. It is also useful if a step has already been successfully completed, but you want to reorder it with different input. For workflow steps and output artefacts, an option to `Propagate invalidation`_ to downstream steps is provided.

.. warning::

   Invalidating a workflow, workflow step or artefact is a destructive action that cannot be undone. Make sure you are certain that the results are erroneous before invalidating.

Invalidate workflow
~~~~~~~~~~~~~~~~~~~

Invalidating an entire workflow will prevent further use of all output artefacts produced by the workflow, as well as all connected steps. 

This action is performed from the main interface, whereas invalidating individual steps or artefacts is done from the details view.

.. note::

    When a workflow has been invalidated, it cannot be reordered from the UI. Instead, a new order must be placed in the API.

Invalidate workflow step
~~~~~~~~~~~~~~~~~~~~~~~~

Invalidating a workflow step will prevent further use of all output artefacts produced by the step, as well as the step itself. This is useful if a step has failed and you want to reorder it without having to reorder the entire workflow.

.. note::

    The Invalidation action is only available for steps that are completed. For steps that are in progress (running/queued/pending/halted), the alternative  `Action: Abort`_ is provided (Action tab, ``Abort`` button).

Invalidate artefact
~~~~~~~~~~~~~~~~~~~

Invalidating an output artefact will prevent further use of the individual artefact, but not the step that produced it. This is useful if an artefact has been produced erroneously and should not be used downstream.

Note that there is no option to invalidate _input_ artefacts. Instead, these can be invalidated from the step or workflow that produced them.

Propagate invalidation
~~~~~~~~~~~~~~~~~~~~~~

If a step or artefact has produced erroneous results, all downstream steps and artefacts may also be affected. To ensure that all invalid results are removed from further use, choose ``Propagate`` when invalidating the step or artefact (note: not possible for input artefacts). This will invalidate all downstream steps/artefacts in addition to the one you start with.

Action: Reorder
-------------

Reordering a workflow step will attempt to create new output artefacts from the step. This is useful if a step has failed due to a transient error, and you want to retry it without having to reorder the entire workflow.

.. note::

    Note that only failed or invalidated steps may be reordered, and that reordering will also affect downstream steps.

Action: Abort
-------------

Aborting a workflow step that is pending or in progress (running, queued or halted) can be done in the Details modal (Action tab, ``Abort`` button). For pending jobs, this will prevent the step from being started, while jobs that are in progress will be stopped. This is useful if you want to stop a workflow that is no longer needed.

Conceptually, this action is the same as `Action: Invalidate`_, except there are no output artefacts to invalidate.