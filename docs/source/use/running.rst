Running Maestro in an app
=========================

[TODO]

Updating the Maestro library
-----------------------------

Maestro should be installed in the container of your app. For now, your app should point to Maestro's ``dev`` branch. When a proper Maestro release has been made, you should change this to a release tag.

To update the Maestro library with all its Python dependencies, run the following command::

    docker compose exec [your_app_name] poetry update maestro

If you need to add a new Python dependency, modify ``pyproject.toml`` and run::

    docker compose exec [your_app_name] poetry update