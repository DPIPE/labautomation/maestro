Getting started
===============

Maestro is designed to be used as a library in other applications. To setup Maestro for use in an app, follow instructions below. If you are curious you can also check the :doc:`concepts` to understand the underlying concepts and how to use them, but we recommend sticking to basics when getting started.

Basic setup
-----------

We recommend setting up your app using Docker and Docker Compose, since Maestro is build with a similar setup. To get started, follow the steps below:

1. If you haven't already, install Docker and Docker Compose (easist way is with `Docker Desktop <https://docs.docker.com/get-started/get-docker/>`_). Verify installation with::

      docker version
      docker compose version

2. Copy the `example <https://gitlab.com/DPIPE/labautomation/maestro/-/tree/dev/example>`_ directory with all its content. The :doc:`README <example>` in this folder containes further details of the contents. 

3. ``cd`` into the example folder and build the Docker image with the command::

      docker build -t example:local .

4. Once the step above succeeds, start all necessary services with Docker Compose::

      docker compose up

   You should see a log similar to below::

      ✔ Container example Recreated 0.1s
      ...
      ...
      Attaching to example, nats-example, neo4j-example[info] Migrations done filename=neo4j_migrations.py func_name=run_migrations lineno=26

5. The example should now be up and running. 

   - Open http://localhost:8000/ to access the UI. 
   
   - List all running containers::
      
      docker ps
   
   - Check logs for running containers::
      
      docker logs $CONTAINER_NAME

After this we recommend to go through :doc:`README <example>` to get more details about the how Maestro can be used.

Real world application
----------------------

For a complete example of how Maestro can be used in an app, see `Steps <https://gitlab.com/DPIPE/labautomation/steps>`_.

[TODO: Add more details]