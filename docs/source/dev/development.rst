Setup for development
=====================

This page provides information on how to set up Maestro for development. For using Maestro as a library in an app, see :doc:`../use/getting_started`.

Set up with devcontainer
------------------------

To set up for development, use the provided devcontainer in VSCode. Choose "Remote-Containers: Reopen in Container" in VSCode when prompted (or use the command palette). This will set up a development environment with all necessary dependencies.

Set up without devcontainer
---------------------------

If you don't want to use devcontainer, follow these steps:

1. Set up and activate virtualenv::

      cd maestro
      python -m venv .venv
      source .venv/bin/activate
      pip install poetry
      poetry install

2. Launch Docker Compose::

      docker compose up

3. Install `NATS CLI <https://github.com/nats-io/natscli/releases>`_

Setup Maestro as a standalone service
-------------------------------------

Maestro can be setup as a standalone service with Docker and Docker Compose:

1. Clone the Maestro repository to your local machine
2. Configure as needed:

   - Environment variables in the ``config/`` directory
   - ``docker-compose.yml`` file

3. Build using Docker Compose (from root)::

      docker compose build

4. Start the Maestro services::

      docker compose up

Alternatively, you can install Maestro locally using pip:

1. Install with pip::

      pip install src/

2. Compile tailwind CSS for UI::

      maestro-post-install

Access to services
------------------

When services are up and running after setup, you can access (with default configuration):

- Maestro UI at http://localhost:8002/
- Maestro API at http://localhost:8002/docs
- Neo4j browser at http://localhost:47474/browser/