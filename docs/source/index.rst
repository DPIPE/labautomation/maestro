.. raw:: html

   <div align="center" style="margin-bottom: 90px; margin-top: 50px;">
       <img width="400px" style="border: 0;" src="logo/Maestro_logo_blue.svg" alt="Maestro logo"/>
   </div>

Maestro is a lightweight software library for workflow management, written in Python. Maestro is especially suited for the complexity of modern bioinformatic setups, but has a generic and modular design that can be adapted to any informatic workflow. Key features are automation and decoupling of workflow steps.

The intention of Maestro is not to replace existing bioinformatic workflow languages like Nextflow, which handle all the smaller tools of a bioinformatic pipeline. Instead, Maestro can connect the higher-level parts, such as different pipelines, and integrate operational tasks that lie outside a single pipeline.

Maestro is designed to be used as a library in other applications and systems rather than on its own; see `Documentation`_ for details. A complete example of how Maestro can be used in an app can be found in `Steps <https://gitlab.com/DPIPE/labautomation/steps>`_. The figure below gives a simplified view of how Maestro can work in a bioinformatic pipeline:

.. raw:: html

   <div align="center" style="margin-bottom: 20px; margin-top: 20px;" class="only-light">
       <img width="550px" style="border: 0;" src="Maestro_bioinf_workflow.svg" alt="Maestro workflow"/>
   </div>

Documentation
=============

There are several components to Maestro. The following documentation should help you navigate all of them. If you just want to use Maestro, we recommend going through docs 1-3, while doc 4 will provide guidance for task monitoring via the web user interface. Docs 5, 6 and 7 give more technical details.

For users
---------

1. :doc:`use/getting_started`
2. :doc:`use/example`
3. :doc:`use/running`
4. :doc:`use/manual`
5. :doc:`use/concepts`

For developers
--------------

6. :doc:`dev/development`
7. :doc:`dev/modules`

Indices
-------
- :ref:`General index <genindex>`
- :ref:`Python module index <modindex>`

Support
=======

If you have any questions or need support, please feel free to create an issue in our `Gitlab repository <https://gitlab.com/DPIPE/labautomation/maestro>`_.

License and copyright
=====================

Maestro is licensed under the GNU General Public License v3.0

Copyright (C) 2023-2024 Maestro contributors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.

.. toctree::
   :hidden:

   use/index
   dev/index
   release_notes
   GitLab <https://gitlab.com/DPIPE/labautomation/maestro>
 