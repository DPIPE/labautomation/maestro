Release notes
=============

.. list-table::
   :header-rows: 1

   * - Major versions
     - Minor versions
   * - `v1.0.0`_
     - 

v1.0.0
-------------

Release date: TBD

Changes
~~~~~~~	

Initial release.