# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import datetime
import sys

# -- Path setup --------------------------------------------------------------
# Add the project directory to the Python path
sys.path.insert(0, "../../src")

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "Maestro"
copyright = f"2023-{datetime.datetime.now().year}, Maestro contributors"
author = "Maestro contributors"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx_copybutton",
    "sphinx.ext.viewcode",
    "sphinxcontrib.autodoc_pydantic",
]

# exclude_patterns = []

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "pydata_sphinx_theme"
html_logo = "../img/logo/Maestro_logo_blue.svg"
html_theme_options = {
    "logo": {
        "title_text": "Maestro documentation - Home",
    },
    # Remove dark theme option
    "navbar_end": ["navbar-icon-links"],
    # Show indices in the sidebar
    "primary_sidebar_end": ["indices.html"],
}
html_favicon = "../img/logo/favicon.ico"
html_last_updated_fmt = "%b %d, %Y"
html_extra_path = ["../img"]
# Custom CSS
html_static_path = ["_static"]
html_css_files = ["custom.css"]
# Remove dark theme option
html_context = {"default_mode": "light"}

# -- Options for autodoc -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html#module-sphinx.ext.autodoc

autodoc_default_options = {
    "ignore-module-all": True,
    "members": True,
    "undoc-members": True,
    "show-inheritance": True,
}
