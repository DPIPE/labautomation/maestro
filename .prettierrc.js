// prettier.config.js

module.exports = {
    printWidth: 100,
    tabWidth: 4,
    plugins: ["prettier-plugin-tailwindcss"],
    overrides: [
        {
            files: ["*.html"],
            options: {
                parser: "jinja-template",
            },
        },
    ],
};
