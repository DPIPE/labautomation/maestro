"""
Script for processing neo4j-sync messages from NATS

This script processes messages from the `neo4j-sync` stream and executes the write queries on the Neo4j database.

Note: This does _not_ run in the maestro image, but in the neo4j image. Thus, this does not have
access to all the packages available in Maestro. See pyproject.toml file section [tool.poetry.group.nats-neo4j-bridge.dependencies]
for the dependencies available in this image.
"""

import asyncio
import json
import logging
import os
import ssl
from typing import Any, Never

import nats
import nats.errors
from nats.aio.msg import Msg
from nats.js.api import ConsumerConfig, DeliverPolicy
from neo4j import GraphDatabase, Transaction

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("nats-bridge")

NEO4J_AUTH: tuple[str, str] = tuple(  # type: ignore[assignment]
    os.environ.get("NEO4J_AUTH", "neo4j/password").split("/", 1)
)
NATS_STREAM = os.environ.get("NATS_STREAM", "neo4j-sync")
NATS_SUBJECT = os.environ.get("NATS_SUBJECT", "transaction-write")

NATS_TLS_CA_CERT = os.environ.get("NATS_TLS_CA_CERT", None)
NATS_NKEYS_SEED = os.environ.get("NATS_NKEYS_SEED", None)
NATS_URL = os.environ.get("NATS_URL", "nats://nats:4222")

NATS_BRIDGE_READY_MARKER = os.environ.get(
    "NATS_BRIDGE_READY_MARKER", "/tmp/nats-bridge-ready"
)

# Global variable used to hold Neo4j database id
DATABASE_ID = None


def process_write_transaction(msg: Msg) -> None:
    """
    Callback for processing a message from NATS stream

    Will process all queries in the message provided that the database_id in the message does not match
    the global DATABASE_ID.

    Finally, it will update the node `__NATSBridge` in the database with the incoming message id.
    """
    data = json.loads(msg.data)
    global DATABASE_ID

    def finalize(tx: Transaction) -> None:
        "Update NATSBridge node in database with the last message id processed"
        logger.info(f"Processed message {msg.metadata.sequence.stream}")
        tx.run(
            "MERGE (n:`__NATSBridge`) SET n.message_id=$message_id",
            {"message_id": msg.metadata.sequence.stream},
        )

    def process_queries(tx: Transaction) -> None:
        "Process the queries in the message data"
        # Acquire distributed lock using Neo4j's built-in locking mechanism.
        # The n_locked property value itself is not important - the MERGE operation
        # is what provides the distributed locking by acquiring an exclusive lock
        # on the __LockNode. Setting n_locked is just a side effect of needing
        # to do a write operation to get the lock.
        tx.run(
            """
            MERGE (n:`__LockNode`)
            ON CREATE SET n.n_locked = true
            ON MATCH SET n.n_locked = true
            """
        )
        logger.debug("Acquired Neo4j lock")
        logger.debug(f"Processing queries on DATABASE_ID {DATABASE_ID}: {msg}")
        for query in data["queries"]:
            tx.run(query["query"], query["parameters"])

    with GraphDatabase.driver("bolt://localhost:7687", auth=NEO4J_AUTH) as driver:
        with driver.session() as session:
            with session.begin_transaction() as tx:
                result = tx.run("MATCH (n:`__NATSBridge`) RETURN n.message_id").single()
                assert result
                last_message_id = result.value()
                assert isinstance(last_message_id, int)
                new_message_id = int(msg.metadata.sequence.stream)
                # Check if this message directly follows the last processed message
                # we need to ensure that new messages to be processed follows last processesd/persisted message
                assert new_message_id == last_message_id + 1
                if data["database_id"] != DATABASE_ID:
                    process_queries(tx)
                finalize(tx)


async def connect_to_neo4j() -> None:
    # Try to connect with Neo4j (with retry)
    logger.info("Trying to connect to Neo4j")
    N = 0
    while True:
        N += 1
        logger.debug(f"Attempt {N} ...")

        try:
            driver = GraphDatabase.driver("bolt://localhost:7687", auth=NEO4J_AUTH)
            with driver.session() as session:
                session.run("MATCH (n) RETURN n LIMIT 1")
                pass
            break
        except BaseException as e:
            logger.debug(str(e))
            if N >= 10:
                raise
            await asyncio.sleep(3)


async def get_last_processed_message_id() -> int:
    with GraphDatabase.driver("bolt://localhost:7687", auth=NEO4J_AUTH) as driver:
        with driver.session() as session:
            query = """
                MERGE (n:`__NATSBridge`)
                ON CREATE SET n.message_id = 0
                RETURN n.message_id
            """
            result = session.run(query).single()
            assert result

            last_message_id = result.value()
            assert isinstance(last_message_id, int)

            return last_message_id


def set_database_id() -> None:
    with GraphDatabase.driver("bolt://localhost:7687", auth=NEO4J_AUTH) as driver:
        with driver.session() as session:
            global DATABASE_ID
            DATABASE_ID = (
                session.run("SHOW DATABASE maestro YIELD databaseID").single().value()  # type: ignore[union-attr]
            )


async def setup_subscription(last_message_id: int) -> None:
    async def error_cb(e: BaseException) -> Never:
        logger.error(f"Error: {e}")
        raise

    connection_options: dict[str, Any] = {
        "servers": NATS_URL.split(","),
        "nkeys_seed_str": open(NATS_NKEYS_SEED).readline().strip()
        if NATS_NKEYS_SEED
        else None,
        "error_cb": error_cb,
    }
    if NATS_TLS_CA_CERT:
        nats_ssl_context = ssl.create_default_context(ssl.Purpose.SERVER_AUTH)
        nats_ssl_context.load_verify_locations(NATS_TLS_CA_CERT)
        connection_options["tls"] = nats_ssl_context

    nc = nats.NATS()  # type: ignore[attr-defined]
    await nc.connect(**connection_options)
    js = nc.jetstream()

    # Create the stream if it does not exist
    await js.add_stream(name=NATS_STREAM, subjects=[NATS_SUBJECT])

    # Subscribe to neo4j-sync stream - read all messages from last_message_id
    # Use a pull-subscription to read messages from NATS in a controlled manner
    # Note that we do not use a callback - but rather handle messages in a loop
    # See https://docs.nats.io/jetstream/concepts/consumers
    subscription = await js.pull_subscribe(
        NATS_SUBJECT,
        stream=NATS_STREAM,
        durable=f"{DATABASE_ID}-neo4j-bridge",
        config=ConsumerConfig(
            opt_start_seq=last_message_id + 1,
            deliver_policy=DeliverPolicy.BY_START_SEQUENCE,
            max_ack_pending=1,
        ),
    )

    with open(NATS_BRIDGE_READY_MARKER, "w"):
        pass

    # Process messages non-concurrently, to avoid multiple messages being processed at the same time
    try:
        while True:
            try:
                [msg] = await subscription.fetch(timeout=1)
                try:
                    process_write_transaction(msg)
                    await msg.ack()
                except Exception as e:
                    logger.error(f"Error processing message: {msg}")
                    logger.exception(e)
                    raise
            except nats.errors.TimeoutError:
                logger.debug("No more messages to process - retrying")
                continue
            except asyncio.CancelledError:
                # Raise cancelled error to the outer try/except block
                raise
            except Exception as e:
                # TODO: Find other recoverable errors, and except them + continue
                logger.error(f"Unhandled exception: {e}")
                raise

    except asyncio.CancelledError:
        await subscription.unsubscribe()
        await nc.close()


async def main() -> None:
    await connect_to_neo4j()
    logger.info("Connected to Neo4j")

    last_message_id = await get_last_processed_message_id()
    logger.info(f"Found last message id: {last_message_id}")

    set_database_id()

    await setup_subscription(last_message_id)


if __name__ == "__main__":
    asyncio.run(main())
