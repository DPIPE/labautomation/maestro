#!/bin/bash -eu

# Test that neo4j secondary picks up where it left off after downtime
# Test this by first performing some writes while both neo4j instances are running,
# then stopping the secondary, performing some more writes to the primary, and then starting the secondary again.
# Check that the primary and secondary databases have the same number of nodes

function INFO {
    echo -e "\e[1;93mINFO: $1\e[0m"
}

function ERROR {
    echo -e "\e[1;31mERROR: $1\e[0m"
}

function SUCCESS {
    echo -e "\e[1;32mSUCCESS: $1\e[0m"
}

function wait_for_neo4j {
    RETRIES=120
    n=0
    while true; do
        n=$((n+1))
        if [[ $n -eq $RETRIES ]]; then
            ERROR "Neo4j did not start in time"
            exit 1
        fi
        set +e
        if [[ "$(docker compose ps --format '{{ .Health }}' neo4j neo4j-secondary | uniq)" == "healthy" ]]; then
            break
        else
            sleep 1
        fi
    done
}
function write {
    docker compose exec -e INDEX=$1 maestro python /app/neo4j-docker/tests/write_test_sync_node.py &>/dev/null
}

# Spin up containers
INFO "Spinning up all containers"
docker compose up -d
INFO "Waiting for neo4j to start"
wait_for_neo4j

INFO "Cleaning up databases for TestSync nodes"
# Clean databases of TestSync nodes
for container in neo4j neo4j-secondary; do
    docker compose exec -t $container cypher-shell --format plain -u neo4j -p maestro "MATCH (n:TestSync) DETACH DELETE n"
done

# Perform some initial writes
write 1
write 2
write 3
write 4

INFO "Written 4 TestSync nodes to primary while secondary is running"

# Stop secondary
INFO "Stopping secondary"
docker compose down neo4j-secondary &>/dev/null
# Check that secondary is down
curl --fail http://localhost:47475 &>/dev/null && exit 1 || true

# Perform some additional writes
write 5
write 6
write 7
write 8

INFO "Written 4 more TestSync nodes to primary while secondary is down"

# Start secondary
INFO "Starting secondary - should pick up where it left off"
docker compose up -d neo4j-secondary &>/dev/null
wait_for_neo4j

INFO "Checking that secondary has picked up where it left off"
COUNTS=()
for container in neo4j neo4j-secondary; do
    c=$(docker compose exec -t $container cypher-shell --format plain -u neo4j -p maestro "MATCH (n:TestSync) RETURN count(n)" | tail -n 1)
    COUNTS+=(${c})
done

if [[ "${COUNTS[0]}" != "8" ]]; then
    ERROR "Expected 8 TestSync nodes in primary, found ${COUNTS[0]}"
    exit 1
fi

if [[ "${COUNTS[1]}" != "8" ]]; then
    ERROR "Expected 8 TestSync nodes in secondary, found ${COUNTS[1]}"
    exit 1
fi

SUCCESS "Found 8 TestSync nodes in primary and secondary neo4j database"
exit 0



