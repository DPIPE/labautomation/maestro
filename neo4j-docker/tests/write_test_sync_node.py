# Minimalistic script to write a node to Neo4j - used in tests

import asyncio
import os

from maestro.neo4j.neo4j import Neo4jTransaction

index = os.environ.get("INDEX")


async def main():
    async with Neo4jTransaction() as neo4j_tx:
        await neo4j_tx.query("CREATE (n:TestSync {index: $index})", {"index": index})


asyncio.run(main())
