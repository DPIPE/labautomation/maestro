#!/bin/bash

# Start the original neo4j process
/startup/docker-entrypoint.sh neo4j &

# Wait for neo4j to be ready
while ! nc -z localhost 7687; do
    echo "Waiting for neo4j to start..."
    sleep 1
done

# Start the NATS subscription process
python /startup/nats_bridge.py &

# Wait for any process to exit
wait -n

# Exit with status of process that exited first
exit $?