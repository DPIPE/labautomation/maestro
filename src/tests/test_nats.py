import asyncio
from contextlib import asynccontextmanager

import pytest
import pytest_asyncio

from maestro.nats.nats import NATSSession

UPDATE_STREAM = "update_stream"
TEST_SUBJECT = "test_subject"
TEST_DURABLE = "test_durable"
TEST_DATA = b"test_data"
TEST_TIMEOUT = 0.4


@pytest_asyncio.fixture(autouse=True)
async def cleanup():
    yield
    async with NATSSession() as nats_session:
        await nats_session.delete_stream(UPDATE_STREAM)


@pytest.mark.asyncio
async def test_nats_update_stream():
    "Test that we can update a stream"
    async with NATSSession() as nats_session:
        stream = await nats_session.add_or_update_stream(
            name=UPDATE_STREAM, subjects=["test_subject"]
        )
        assert stream.config.subjects == ["test_subject"]
    async with NATSSession() as nats_session:
        stream = await nats_session.add_or_update_stream(
            name=UPDATE_STREAM, subjects=["test_subject_2"]
        )
        assert stream.config.subjects
        assert set(stream.config.subjects) == {"test_subject", "test_subject_2"}

    async with NATSSession() as nats_session:
        stream = await nats_session.add_or_update_stream(name=UPDATE_STREAM, subjects=[])
        assert stream.config.subjects
        assert set(stream.config.subjects) == {"test_subject", "test_subject_2"}


@pytest.mark.asyncio
async def test_both_ephemeral_consumers_receive_message():
    # Create stream and publish a message to it.
    async with NATSSession() as nats_session:
        await nats_session.add_or_update_stream(name=UPDATE_STREAM, subjects=[TEST_SUBJECT])
        await nats_session.publish(TEST_SUBJECT, TEST_DATA, UPDATE_STREAM)

    # Ephemeral consumer 1 receives the message.
    async with NATSSession() as nats_session:
        subscription = await nats_session.subscribe(
            UPDATE_STREAM,
            TEST_SUBJECT,
            cb=None,
            config={"ack_wait": TEST_TIMEOUT / 2},
        )
        [msg] = await subscription.fetch(timeout=TEST_TIMEOUT)
        # Message has to be acknowledged within "ack_wait" amount of time,
        # so better get to that ASAP to avoid test flakiness.
        await msg.ack()
        assert msg.data == TEST_DATA
        with pytest.raises(asyncio.TimeoutError):
            await subscription.fetch(timeout=TEST_TIMEOUT)

    # Ephemeral consumer 2 receives the message.
    async with NATSSession() as nats_session:
        subscription = await nats_session.subscribe(
            UPDATE_STREAM,
            TEST_SUBJECT,
            cb=None,
            config={"ack_wait": TEST_TIMEOUT / 2},
        )
        [msg] = await subscription.fetch(timeout=TEST_TIMEOUT)
        # Message has to be acknowledged within "ack_wait" amount of time,
        # so better get to that ASAP to avoid test flakiness.
        await msg.ack()
        assert msg.data == TEST_DATA
        with pytest.raises(asyncio.TimeoutError):
            await subscription.fetch(timeout=TEST_TIMEOUT)


@pytest.mark.asyncio
async def test_only_first_instance_of_durable_consumer_receives_message():
    """
    Verify durable consumer behavior where messages are only delivered once.
    Unlike ephemeral consumers, durable consumers maintain their position
    across multiple instances, ensuring exactly-once delivery semantics.
    """
    # Create stream and publish a message to it.
    async with NATSSession() as nats_session:
        await nats_session.add_or_update_stream(name=UPDATE_STREAM, subjects=[TEST_SUBJECT])
        await nats_session.publish(TEST_SUBJECT, TEST_DATA, UPDATE_STREAM)

    # First instance of the durable consumer receives the message.
    async with NATSSession() as nats_session:
        subscription = await nats_session.subscribe(
            UPDATE_STREAM,
            TEST_SUBJECT,
            cb=None,
            durable=TEST_DURABLE,
            config={"ack_wait": TEST_TIMEOUT / 2},
        )
        [msg] = await subscription.fetch(timeout=TEST_TIMEOUT)
        # Message has to be acknowledged within "ack_wait" amount of time,
        # so better get to that ASAP to avoid test flakiness.
        await msg.ack()
        assert msg.data == TEST_DATA
        with pytest.raises(asyncio.TimeoutError):
            await subscription.fetch(timeout=TEST_TIMEOUT)

    # Second instance of the durable consumer does not receive the message.
    async with NATSSession() as nats_session:
        subscription = await nats_session.subscribe(
            UPDATE_STREAM,
            TEST_SUBJECT,
            cb=None,
            durable=TEST_DURABLE,
            config={"ack_wait": TEST_TIMEOUT / 2},
        )
        with pytest.raises(asyncio.TimeoutError):
            await subscription.fetch(timeout=TEST_TIMEOUT)


@pytest.mark.asyncio
async def test_both_ephemeral_callback_consumers_receive_message():
    receive_queue = asyncio.Queue()

    async def cb(msg):
        # Message has to be acknowledged within "ack_wait" amount of time,
        # so better get to that ASAP to avoid test flakiness.
        await msg.ack()
        await receive_queue.put(msg)

    # Create stream and publish a message to it.
    async with NATSSession() as nats_session:
        await nats_session.add_or_update_stream(name=UPDATE_STREAM, subjects=[TEST_SUBJECT])
        await nats_session.publish(TEST_SUBJECT, TEST_DATA, UPDATE_STREAM)

    # Ephemeral consumer 1 receives the message.
    async with NATSSession() as nats_session:
        await nats_session.subscribe(
            UPDATE_STREAM,
            TEST_SUBJECT,
            cb,
            config={"ack_wait": TEST_TIMEOUT / 2},
        )
        msg = await asyncio.wait_for(receive_queue.get(), timeout=TEST_TIMEOUT)
        assert msg.data == TEST_DATA
        with pytest.raises(asyncio.TimeoutError):
            await asyncio.wait_for(receive_queue.get(), timeout=TEST_TIMEOUT)

    # Ephemeral consumer 2 receives the message.
    async with NATSSession() as nats_session:
        await nats_session.subscribe(
            UPDATE_STREAM,
            TEST_SUBJECT,
            cb,
            config={"ack_wait": TEST_TIMEOUT / 2},
        )
        msg = await asyncio.wait_for(receive_queue.get(), timeout=TEST_TIMEOUT)
        assert msg.data == TEST_DATA
        with pytest.raises(asyncio.TimeoutError):
            await asyncio.wait_for(receive_queue.get(), timeout=TEST_TIMEOUT)


@pytest.mark.asyncio
async def test_only_first_instance_of_durable_push_consumer_receives_message():
    """
    Verify durable consumer behavior where messages are only delivered once.
    Unlike ephemeral consumers, durable consumers maintain their position
    across multiple instances, ensuring exactly-once delivery semantics.
    """
    receive_queue = asyncio.Queue()

    async def cb(msg):
        await receive_queue.put(msg)

    # Create stream and publish a message to it.
    async with NATSSession() as nats_session:
        await nats_session.add_or_update_stream(name=UPDATE_STREAM, subjects=[TEST_SUBJECT])
        await nats_session.publish(TEST_SUBJECT, TEST_DATA, UPDATE_STREAM)

    # First instance of the durable consumer receives the message.
    async with NATSSession() as nats_session:
        await nats_session.subscribe(UPDATE_STREAM, TEST_SUBJECT, cb, durable=TEST_DURABLE)
        msg = await asyncio.wait_for(receive_queue.get(), timeout=TEST_TIMEOUT)
        assert msg.data == TEST_DATA
        with pytest.raises(asyncio.TimeoutError):
            await asyncio.wait_for(receive_queue.get(), timeout=TEST_TIMEOUT)

    # Second instance of the durable consumer does not receive the message.
    async with NATSSession() as nats_session:
        await nats_session.subscribe(UPDATE_STREAM, TEST_SUBJECT, cb, durable=TEST_DURABLE)
        with pytest.raises(asyncio.TimeoutError):
            await asyncio.wait_for(receive_queue.get(), timeout=TEST_TIMEOUT)


@pytest.mark.asyncio
@pytest.mark.parametrize("raise_in_callback_num", [0, 1, 2])
@pytest.mark.parametrize("raise_inside_context_manager", [False, True])
async def test_subscriber_exception_is_propagated_from_NATSSession_context_manager(
    raise_in_callback_num, raise_inside_context_manager
):
    """
    Test that an exception raised in the subscriber callback is propagated to the
    main event loop when NATSSession context manager is exited.

    If an exception is raised inside the context manager, it should be propagated as well.

    In case of exceptions inside context manager and callback, both should be wrapped in
    an ExceptionGroup.
    """

    class CallbackException(RuntimeError):
        pass

    class ContextManagerException(RuntimeError):
        pass

    future = asyncio.Future()

    async def cb(msg):
        sequence_number = int(msg.data.decode())
        if sequence_number == raise_in_callback_num:
            future.set_result(None)
            raise CallbackException(f"Callback exception {sequence_number}")
        await msg.ack()

    try:
        async with NATSSession() as nats_session:
            await nats_session.add_or_update_stream(name=UPDATE_STREAM, subjects=[TEST_SUBJECT])
            for i in range(raise_in_callback_num):
                await nats_session.publish(TEST_SUBJECT, str(i + 1).encode(), UPDATE_STREAM)
            await nats_session.subscribe(UPDATE_STREAM, TEST_SUBJECT, cb)
            if raise_in_callback_num > 0:
                await asyncio.wait_for(future, timeout=TEST_TIMEOUT)
            else:
                await asyncio.sleep(TEST_TIMEOUT)
            if raise_inside_context_manager:
                raise ContextManagerException("Exception from inside the context manager")
    except CallbackException as e:
        assert not raise_inside_context_manager
        assert str(e) == f"Callback exception {raise_in_callback_num}"
    except ContextManagerException as e:
        assert raise_in_callback_num == 0
        assert raise_inside_context_manager
        assert str(e) == "Exception from inside the context manager"
    except ExceptionGroup as eg:
        assert str(eg).startswith("multiple exceptions during NATSSession")
        assert raise_in_callback_num > 0
        expected_exceptions = {
            f"CallbackException('Callback exception {raise_in_callback_num}')",
        }
        if raise_inside_context_manager:
            expected_exceptions.add(
                "ContextManagerException('Exception from inside the context manager')"
            )
        assert set(repr(e) for e in eg.exceptions) == expected_exceptions
    else:
        assert raise_in_callback_num == 0
        assert not raise_inside_context_manager


@pytest.mark.asyncio
async def test_subscriber_exception_is_propagated_from_NATSSession_run_forever():
    async def cb(msg):
        raise RuntimeError("Test exception")

    nats_session = NATSSession()
    await nats_session.start()
    await nats_session.add_or_update_stream(name=UPDATE_STREAM, subjects=[TEST_SUBJECT])
    await nats_session.subscribe(UPDATE_STREAM, TEST_SUBJECT, cb)
    await nats_session.publish(TEST_SUBJECT, b"", UPDATE_STREAM)
    with pytest.raises(RuntimeError, match="Test exception"):
        await asyncio.wait_for(nats_session.run_forever(), timeout=TEST_TIMEOUT)


@pytest.mark.asyncio
async def test_nats_kv_store_lock_prevents_second_task_from_racing_first_task(monkeypatch):
    """
    Test that the locking mechanism in 'NATSSession' prevents the second task from
    racing the first task to append to a shared list.

    asyncio.Events are used to synchronize the two tasks to ensure that the first task
    enters the lock context before the second task is allowed to do the same.
    """

    async def append_to_list(
        appending_list: list[int],
        appending_value: int,
        proceed_event_self: asyncio.Event,
        proceed_event_other: asyncio.Event | None,
    ):
        async with NATSSession() as nats_session:
            await proceed_event_self.wait()
            async with nats_session.lock():
                if proceed_event_other is not None:
                    # Allow the other task to proceed.
                    proceed_event_other.set()
                    # In case the locking mechanism is not working correctly,
                    # give the other task time to complete first.
                    await asyncio.sleep(0.1)
                appending_list.append(appending_value)

    async def run_tasks():
        appending_list = []
        task1_proceed_event = asyncio.Event()
        task2_proceed_event = asyncio.Event()
        task1 = asyncio.create_task(
            append_to_list(appending_list, 1, task1_proceed_event, task2_proceed_event)
        )
        task2 = asyncio.create_task(append_to_list(appending_list, 2, task2_proceed_event, None))
        task1_proceed_event.set()

        await asyncio.wait_for(asyncio.gather(task1, task2), 15)
        return appending_list

    # Expect the first task to complete before the second task when lock is working correctly.
    assert await run_tasks() == [1, 2]

    # Test that the testing of the locking mechanism is working correctly by introducing
    # a non-working implementation of NATSSession.lock().
    @asynccontextmanager
    async def non_working_lock(_):
        yield

    monkeypatch.setattr(NATSSession, "lock", non_working_lock)

    # Expect the second task to complete before the first task when lock is not working.
    assert await run_tasks() == [2, 1]


@pytest.mark.asyncio
async def test_nats_kv_store_lock_allows_only_one_task_at_a_time(monkeypatch):
    """
    Test that the locking mechanism in 'NATSSession' allows only one task to enter
    the critical section at a time.
    """
    num_tasks = 100
    inside_critical_section = False
    processed_tasks = set()

    async def check_locking(i):
        nonlocal inside_critical_section
        async with NATSSession() as nats_session:
            async with nats_session.lock():
                assert i not in processed_tasks
                assert not inside_critical_section
                inside_critical_section = True
                await asyncio.sleep(0)
                inside_critical_section = False
                processed_tasks.add(i)
                return i

    async def run_tasks():
        processed_tasks.clear()
        tasks = [asyncio.create_task(check_locking(i)) for i in range(num_tasks)]
        return await asyncio.wait_for(asyncio.gather(*tasks, return_exceptions=True), 20)

    results = await run_tasks()
    assert set(results) == set(range(num_tasks))
    assert processed_tasks == set(range(num_tasks))

    # Test that the testing of the locking mechanism is working correctly by introducing
    # a non-working implementation of NATSSession.lock().
    @asynccontextmanager
    async def non_working_lock(_):
        yield

    monkeypatch.setattr(NATSSession, "lock", non_working_lock)

    # Repeat the test with a non-working lock implementation.
    results = await run_tasks()
    assert set(results) != set(range(num_tasks))
    assert processed_tasks != set(range(num_tasks))
    assert any(isinstance(result, AssertionError) for result in results)


@pytest.mark.asyncio
async def test_nats_kv_store_lock_is_released_on_exception():
    """
    Test that the locking mechanism in 'NATSSession' releases the lock after the
    first task exceeds a timeout, allowing the second task to acquire the lock.
    """

    async def timed_sleep(
        timeout: float,
        sleep_time: float,
        expect_timeout: bool,
        proceed_event_self: asyncio.Event,
        proceed_event_other: asyncio.Event | None,
    ):
        async with NATSSession() as nats_session:
            await proceed_event_self.wait()
            try:
                async with asyncio.timeout(timeout):
                    async with nats_session.lock():
                        if proceed_event_other is not None:
                            proceed_event_other.set()
                        await asyncio.sleep(sleep_time)
            except TimeoutError:
                assert expect_timeout
            else:
                assert not expect_timeout

    task1_proceed_event = asyncio.Event()
    task2_proceed_event = asyncio.Event()
    task1 = asyncio.create_task(timed_sleep(0.2, 1, True, task1_proceed_event, task2_proceed_event))
    task2 = asyncio.create_task(timed_sleep(1, 0, False, task2_proceed_event, None))
    task1_proceed_event.set()

    await asyncio.wait_for(asyncio.gather(task1, task2), 5)


@pytest.mark.asyncio
async def test_nats_concurrent_update_stream():
    """
    Test that multiple tasks can concurrently update a stream without
    overwriting each other's changes.
    """

    async def update_stream(subjects):
        async with NATSSession() as nats_session:
            await nats_session.add_or_update_stream(name=UPDATE_STREAM, subjects=subjects)

    task1 = asyncio.create_task(update_stream(["test_subject"]))
    task2 = asyncio.create_task(update_stream(["test_subject_2"]))

    await asyncio.wait_for(asyncio.gather(task1, task2), 5)

    async with NATSSession() as nats_session:
        stream = await nats_session.add_or_update_stream(name=UPDATE_STREAM, subjects=[])
        assert stream.config.subjects
        assert set(stream.config.subjects) == {"test_subject", "test_subject_2"}
