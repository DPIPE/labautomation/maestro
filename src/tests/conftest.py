import asyncio
import json
import logging
from asyncio import TaskGroup
from collections.abc import AsyncGenerator
from pathlib import Path
from typing import Literal, cast

import pytest
import pytest_asyncio
from httpx import ASGITransport, AsyncClient
from watchfiles import DefaultFilter

from maestro.api.api.config import Settings
from maestro.api.main import create_api
from maestro.config import Config
from maestro.models import BaseArtefactModel, FileArtefactModel, MaestroBase
from maestro.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction, literal_params_map
from maestro.neo4j.neo4j_utils import read_nodes
from maestro.run import shutdown_workflow_steps
from maestro.worker_queue import WorkerQueue
from maestro.workflow_step.file_watcher import FileWatcher
from maestro.workflow_step.message_watcher import MessageWatcher

Path("/tmp/tests").mkdir(exist_ok=True)


class DummyFileWatcher(FileWatcher):
    path = "/tmp/tests"
    filter = DefaultFilter()

    async def process(self, input: FileArtefactModel):
        identifier = input.path.stem
        artefact = DummyArtefact(identifier=identifier)
        return [artefact]


class DummyArtefact(BaseArtefactModel):
    """Dummy artefact for testing"""

    value: str = "dummy_value"
    identifier: str = "dummy_identifier"
    direction: Literal["input", "output"] = "input"


class DummyWorkflowStep(MessageWatcher):
    """Dummy workflow step for testing"""

    async def process(self, **kwargs):
        outputs = []
        for output_identifier in kwargs.get("outputs", []):
            outputs.append(DummyArtefact(identifier=output_identifier))
        return outputs


@pytest_asyncio.fixture()
async def neo4j_tx():
    async with Neo4jTransaction() as tx:
        yield tx


class RecordingHandler(logging.Handler):
    def __init__(self):
        super().__init__()
        self.recording: list[logging.LogRecord] = []

    def emit(self, record: logging.LogRecord):
        self.recording.append(record)


@pytest.fixture
def log_recorder():
    recording_handler = RecordingHandler()
    for logger in logging.getLogger().getChildren():
        logger.addHandler(recording_handler)
    yield recording_handler
    for logger in logging.getLogger().getChildren():
        logger.removeHandler(recording_handler)


@pytest_asyncio.fixture(autouse=True)
async def cleanup_streams():
    Config.STREAM = "test_stream"
    Config.BUCKET = "test_bucket"

    # Do not delete Config.NEO4J_SYNC_STREAM - this silently invalidates subscribers
    async with NATSSession() as nats_session:
        await nats_session.delete_stream(Config.STREAM)
        await nats_session.delete_bucket(Config.BUCKET)
    yield


@pytest_asyncio.fixture(autouse=True)
async def cleanup_neo4j():
    # The community edition of Neo4j doesn't support multiple databases.
    # Ideally, tests should be using the testdb as below and create it
    # if it doesn't exist.
    # Config.NEO4J_DATABASE = "testdb"

    # Clean up the database
    async with Neo4jTransaction() as neo4j_tx:
        # conn._driver.session().run("CREATE DATABASE testdb IF NOT EXISTS")
        await neo4j_tx.query(
            "MATCH (n) WHERE NOT n:`__Neo4jMigration` AND NOT n:`__NATSBridge` DETACH DELETE n"
        )

    yield

    await check_neo4j_synced()


async def check_neo4j_synced():
    """Check that primary and secondary Neo4j databases are in sync"""

    async def ensure_all_messages_processed(uri: str, expected_message_id: int) -> int:
        async with asyncio.timeout(5):
            while True:
                async with Neo4jTransaction(uri=uri) as tx:
                    db_node = await tx.query("MATCH (n:__NATSBridge) RETURN n")
                last_message_id = cast(int, db_node[0].value().get("message_id"))

                if last_message_id == expected_message_id:
                    return last_message_id
                await asyncio.sleep(0.1)

    async def get_all_maestro_bases(uri: str) -> list[MaestroBase]:
        "Get all MaestroBase nodes from the database"
        async with Neo4jTransaction(uri=uri) as neo4j_tx:
            db_nodes = await read_nodes(neo4j_tx, MaestroBase, {})
        return db_nodes

    async def get_all_relationships(uri: str) -> set[tuple[str, str, str, str]]:
        "Get all relationships between MaestroBase nodes from the database"
        async with Neo4jTransaction(uri=uri) as neo4j_tx:
            rels = await neo4j_tx.query(
                "MATCH (n:MaestroBase) - [r] - (m:MaestroBase) RETURN n.maestro_id as maestro_id1, type(r) as type, properties(r) as properties, m.maestro_id as maestro_id2"
            )
        return set(
            (
                rel.get("maestro_id1"),
                rel.get("type"),
                json.dumps(rel.get("properties"), sort_keys=True),
                rel.get("maestro_id2"),
            )
            for rel in rels
        )

    # Check that the last message processed by the NATS bridge is the same in both databases,
    # and that it matches the last message in the NATS stream
    # Give it some time to catch up
    async with NATSSession() as nats:
        last_message = await nats._js.get_last_msg(
            Config.NEO4J_SYNC_STREAM, Config.NEO4J_SYNC_SUBJECT
        )
        nats_last_message_id = last_message.seq
        assert nats_last_message_id is not None

    async with TaskGroup() as tg:
        tg.create_task(ensure_all_messages_processed(Config.NEO4J_URI, nats_last_message_id))
        tg.create_task(
            ensure_all_messages_processed("bolt://neo4j-secondary:7687", nats_last_message_id)
        )

    # We now know that both databases _should_ be in sync,
    # so we can check that they are by comparing the MaestroBase nodes
    # and relationships in both databases
    async with TaskGroup() as tg:
        primary_bases = tg.create_task(get_all_maestro_bases(Config.NEO4J_URI))
        secondary_bases = tg.create_task(get_all_maestro_bases("bolt://neo4j-secondary:7687"))

        primary_rels = tg.create_task(get_all_relationships(Config.NEO4J_URI))
        secondary_rels = tg.create_task(get_all_relationships("bolt://neo4j-secondary:7687"))

    assert set(primary_bases.result()) == set(
        secondary_bases.result()
    ), "Nodes are not the same in both databases"
    assert set(primary_rels.result()) == set(
        secondary_rels.result()
    ), "Relationships are not the same in both databases"


@pytest_asyncio.fixture(autouse=True)
async def cleanup_workflow_steps():
    yield
    try:
        await asyncio.wait_for(shutdown_workflow_steps(), 10)
    except asyncio.CancelledError:
        pass
    if hasattr(WorkerQueue, "loop_task"):
        loop_task = WorkerQueue.loop_task
        del WorkerQueue.loop_task
        if loop_task.cancel():
            with pytest.raises(asyncio.CancelledError):
                await asyncio.wait_for(loop_task, 10)
        else:
            try:
                await asyncio.wait_for(loop_task, 10)
            except asyncio.CancelledError:
                pass

    assert asyncio.all_tasks() == {asyncio.current_task()}


@pytest_asyncio.fixture(scope="function")
async def test_api_app() -> AsyncGenerator[AsyncClient, None]:
    Config.IS_RUNNING_IN_API = True
    settings = Settings(_env_file="test.env", _env_file_encoding="utf-8")
    yield AsyncClient(
        transport=ASGITransport(app=create_api(settings)), base_url="http://localhost"
    )
    Config.IS_RUNNING_IN_API = False


@pytest.fixture(scope="session", autouse=True)
def unset_preload_models():
    """Unset the preloaded models to avoid conflicts with the test models"""
    Config.LOAD_MODELS_FROM = []


async def fetch_unique_node(neo4j_tx: Neo4jTransaction, labels: list[str], params: dict) -> dict:
    "Utility function to fetch a node based on labels and params"
    result = await neo4j_tx.query(
        f"MATCH (n:{':'.join(labels)} {literal_params_map(params)}) RETURN n"
    )
    assert len(result) <= 1, "Expected exactly one or none node to be fetched"
    return result[0].data()["n"] if result else {}  # type: ignore[no-any-return]
