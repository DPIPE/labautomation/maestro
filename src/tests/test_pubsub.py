import asyncio

import pytest

from maestro.nats import NATSSession


def fibo(n):
    if n < 2:
        return n
    return fibo(n - 1) + fibo(n - 2)


@pytest.mark.asyncio
async def test_pubsub():
    "Test basic publish/subscribe on different subjects"
    async with NATSSession() as nats_session:
        await nats_session.add_or_update_stream("test_stream", ["fibo.even", "fibo.odd"])

        received = []
        received_odd = []
        received_even = []
        done = asyncio.Future()
        done_odd = asyncio.Future()
        done_even = asyncio.Future()
        N = 10
        N_odd = 6
        N_even = 4

        async def subscriber(msg):
            received.append(msg.data.decode())
            await msg.ack()
            # When every message has been received, set the future
            if len(received) == N:
                done.set_result(True)

        async def subscriber_odd(msg):
            received_odd.append(msg.data.decode())
            await msg.ack()
            if len(received_odd) == N_odd:
                done_odd.set_result(True)

        async def subscriber_even(msg):
            received_even.append(msg.data.decode())
            await msg.ack()
            if len(received_even) == N_even:
                done_even.set_result(True)

        await nats_session.subscribe("test_stream", "fibo.*", subscriber)
        await nats_session.subscribe("test_stream", "fibo.odd", subscriber_odd)
        await nats_session.subscribe("test_stream", "fibo.even", subscriber_even)

        # Publish odd and even numbers to different subjects
        for i in range(0, N):
            num = fibo(i)
            if num % 2 == 0:
                await nats_session.publish("fibo.even", str(num).encode())
            else:
                await nats_session.publish("fibo.odd", str(num).encode())

        await asyncio.wait_for(done, 2.0)
        await asyncio.wait_for(done_odd, 2.0)
        await asyncio.wait_for(done_even, 2.0)

        # Check that all messages were received on the correct subscribers
        assert received == ["0", "1", "1", "2", "3", "5", "8", "13", "21", "34"]
        assert received_odd == ["1", "1", "3", "5", "13", "21"]
        assert received_even == ["0", "2", "8", "34"]


@pytest.mark.asyncio
async def test_workers():
    """
    Test that acknowledging workers receive all messages,
    but that once a message is acknowledged, it is not sent to another worker
    """
    async with NATSSession() as nats_session:
        await nats_session.add_or_update_stream("test_stream", ["tasks"])

        worker1_received = []
        worker2_received = []
        N = 10
        done = asyncio.Future()

        async def worker1(msg):
            worker1_received.append(msg.data.decode())
            await msg.ack()
            if len(worker1_received) + len(worker2_received) == N:
                done.set_result(True)

        async def worker2(msg):
            worker2_received.append(msg.data.decode())
            await msg.ack()
            if len(worker1_received) + len(worker2_received) == N:
                done.set_result(True)

        await nats_session.subscribe("test_stream", "tasks", worker1, durable="workers")
        await nats_session.subscribe("test_stream", "tasks", worker2, durable="workers")

        for i in range(0, 10):
            await nats_session.publish("tasks", str(i).encode())

        await asyncio.wait_for(done, 1.0)

    # Workers should have received all items, but none of them should have received the same item
    assert set(worker1_received + worker2_received) == set(
        ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    )
    assert set(worker1_received).intersection(worker2_received) == set()


@pytest.mark.asyncio
async def test_receipt():
    """
    Test that all messages are received by the worker,
    even if messages are published before the worker is started
    """
    async with NATSSession() as nats_session:
        await nats_session.add_or_update_stream("test_stream", ["tasks"])

        worker_received = []
        done = asyncio.Future()

        async def worker(msg):
            worker_received.append(msg.data.decode())
            await msg.ack()
            if len(worker_received) == 5:
                done.set_result(True)

        # First publish a few items without a worker
        for i in range(0, 5):
            await nats_session.publish("tasks", str(i).encode())

        # When we start the worker, it should receive the 5 items
        await nats_session.subscribe(
            "test_stream", "tasks", worker, durable="workers", config={"ack_wait": 0.1}
        )

        await asyncio.wait_for(done, 1.0)
    assert worker_received == ["0", "1", "2", "3", "4"]


@pytest.mark.asyncio
async def test_non_ack_worker():
    "Test that a a message is sent to another worker if the first worker doesn't ack"
    async with NATSSession() as nats_session:
        await nats_session.add_or_update_stream("test_stream", ["tasks"])

        worker_received = []
        done = asyncio.Future()

        worker_received = []
        non_ack_worker_received = []

        async def ack_worker(msg):
            worker_received.append(msg.data.decode())
            await msg.ack()
            if len(worker_received) == 10:
                done.set_result(True)

        async def non_ack_worker(msg):
            non_ack_worker_received.append(msg.data.decode())

        # Now subscribe one acknowleding worker and one non-acknowledging worker
        await nats_session.subscribe(
            "test_stream", "tasks", ack_worker, durable="workers", config={"ack_wait": 0.1}
        )
        await nats_session.subscribe(
            "test_stream", "tasks", non_ack_worker, durable="workers", config={"ack_wait": 0.1}
        )

        for i in range(0, 10):
            await nats_session.publish("tasks", str(i).encode())

        await asyncio.wait_for(done, 2.0)

        # Worker should have received all items. The non-ack worker should have also received
        # most items, but it didn't ack.
        assert len(set(worker_received).intersection(non_ack_worker_received)) > 0
        assert set(worker_received) == set(["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"])
