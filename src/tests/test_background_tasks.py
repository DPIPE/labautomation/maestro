import asyncio
from datetime import datetime, timedelta

import pytest

import maestro.neo4j.neo4j_utils
from maestro.background_tasks import cancel_pending_jobs
from maestro.models import Job, JobStatus
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import merge_job_node, read_nodes
from tests.conftest import DummyArtefact, DummyWorkflowStep


@pytest.mark.asyncio
async def test_cancel_pending_jobs(monkeypatch):
    # since background job runs forever, we need to set timeout
    #   so tests don't forever
    background_job_timeout = 2
    # default interval for this background task in 1 hour
    #   for tests we change the interval to 1 second
    cancel_pending_jobs_interval = 1

    # test with custom timeout, create new job created_at set to now (by default)
    pending_job = Job(
        inputs={"input": (DummyArtefact, {})},
        params={"foo": "bar"},
        search_metadata=["sample_order_id: 1234"],
        workflow_step=DummyWorkflowStep,
        status=JobStatus.PENDING,
    )

    async with Neo4jTransaction() as neo4j_tx:
        await merge_job_node(neo4j_tx, pending_job)
        db_job = await read_nodes(neo4j_tx, Job, {"maestro_id": pending_job.maestro_id})
        assert len(db_job) == 1
        assert db_job[0].status == JobStatus.PENDING

    # with default pending job timeout of 1 day running cancel_pending_jobs won't do anything
    with pytest.raises(TimeoutError):
        async with asyncio.timeout(background_job_timeout):
            await cancel_pending_jobs(cancel_pending_jobs_interval)
    async with Neo4jTransaction() as neo4j_tx:
        db_job = await read_nodes(neo4j_tx, Job, {"maestro_id": pending_job.maestro_id})
        assert db_job[0].status == JobStatus.PENDING

    pending_job_to_cancel = Job(
        inputs={"input": (DummyArtefact, {})},
        params={"foo": "bar"},
        search_metadata=["sample_order_id: 1234"],
        workflow_step=DummyWorkflowStep,
        status=JobStatus.PENDING,
        pending_timeout=1,
    )
    async with Neo4jTransaction() as neo4j_tx:
        await merge_job_node(neo4j_tx, pending_job_to_cancel)
    # seting pending_timeout to 1 seconds and running cancel_pending_jobs should cancel
    #   this newly created job
    with pytest.raises(TimeoutError):
        async with asyncio.timeout(background_job_timeout):
            await cancel_pending_jobs(cancel_pending_jobs_interval)
    async with Neo4jTransaction() as neo4j_tx:
        db_job = await read_nodes(neo4j_tx, Job, {"maestro_id": pending_job_to_cancel.maestro_id})
        assert db_job[0].status == JobStatus.FAILED

    # check if default value of 1 day works as exoected to cancel pending job:
    job = Job(
        inputs={"input": (DummyArtefact, {})},
        params={"foo": "bar"},
        search_metadata=["sample_order_id: 1234"],
        workflow_step=DummyWorkflowStep,
        status=JobStatus.PENDING,
    )
    async with Neo4jTransaction() as neo4j_tx:
        # Use monkeypatch to mock datetime.now() (when called from inside
        # "maestro.neo4j.neo4j") to return a datetime value that is 1 day in the past.
        # This makes the "Job" object just old enough to be cancelled by cancel_pending_jobs().
        with monkeypatch.context() as m:

            class MockedDateTime(datetime):
                @classmethod
                def now(cls, tz=None):
                    return datetime.now(tz) - timedelta(days=1)

            m.setattr(maestro.neo4j.neo4j_utils, "datetime", MockedDateTime)
            await merge_job_node(neo4j_tx, job)

        db_jobs = await read_nodes(neo4j_tx, Job, {"maestro_id": job.maestro_id})
        assert len(db_jobs) == 1
        assert db_jobs[0].status == JobStatus.PENDING

    with pytest.raises(TimeoutError):
        async with asyncio.timeout(background_job_timeout):
            await cancel_pending_jobs(cancel_pending_jobs_interval)
    async with Neo4jTransaction() as neo4j_tx:
        db_job = await read_nodes(neo4j_tx, Job, {"maestro_id": job.maestro_id})
        assert db_job[0].status == JobStatus.FAILED


@pytest.mark.asyncio
async def test_cancel_pending_jobs_waits_for_Neo4jTransaction_task_when_cancelled(monkeypatch):
    """
    Test that cancel_pending_jobs() waits for the Neo4jTransaction task to complete
    when it is itself cancelled.
    """
    entered_query = asyncio.Event()
    left_query = False

    class Neo4jTransactionMock:
        async def __aenter__(self):
            return self

        async def __aexit__(self, *_):
            pass

        async def query(self, *_):
            entered_query.set()
            await asyncio.sleep(0.5)
            nonlocal left_query
            left_query = True

    monkeypatch.setattr("maestro.background_tasks.Neo4jTransaction", Neo4jTransactionMock)

    cancel_pending_jobs_task = asyncio.create_task(cancel_pending_jobs())
    await asyncio.wait_for(entered_query.wait(), 5)
    cancel_pending_jobs_task.cancel()
    async with asyncio.timeout(5):
        with pytest.raises(asyncio.CancelledError):
            await cancel_pending_jobs_task
    assert left_query is True


@pytest.mark.asyncio
async def test_cancel_pending_jobs_handles_Neo4jTransaction_exception_without_terminating(
    monkeypatch, log_recorder
):
    """
    Test that cancel_pending_jobs() handles exceptions raised by Neo4jTransaction.query()
    without terminating.
    """
    query_count = 0
    query_rerun = asyncio.Event()

    class Neo4jTransactionMock:
        async def __aenter__(self):
            return self

        async def __aexit__(self, *_):
            pass

        async def query(self, *_):
            nonlocal query_count
            query_count += 1
            if query_count == 1:
                raise RuntimeError("Test failure")
            elif query_count == 2:
                query_rerun.set()

    monkeypatch.setattr("maestro.background_tasks.Neo4jTransaction", Neo4jTransactionMock)

    cancel_pending_jobs_task = asyncio.create_task(cancel_pending_jobs(0.1))
    await asyncio.wait_for(query_rerun.wait(), 5)
    assert any(
        record.msg == "Error while cancelling timed out pending jobs: RuntimeError('Test failure')"
        for record in log_recorder.recording
    )
    cancel_pending_jobs_task.cancel()
    async with asyncio.timeout(5):
        with pytest.raises(asyncio.CancelledError):
            await cancel_pending_jobs_task
