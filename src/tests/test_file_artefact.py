import json
import subprocess
import tempfile
from pathlib import Path

import pytest

from maestro.models import FileArtefactModel


@pytest.mark.parametrize("test_dump", [True, False])
def test_file_artefact(test_dump: bool):
    """
    Test that size and md5sum on file artefact are equivalent to the output of the stat and md5sum commands

    Also check that the values are included in the serialized model dump
    """
    with tempfile.NamedTemporaryFile() as f:
        f.write(b"Hello world!")
        f.flush()

        expected_size = int(subprocess.check_output(["stat", "-c", "%s", f.name]).decode().strip())

        assert expected_size > 0

        file = FileArtefactModel(path=f.name)
        if test_dump:
            dumped = json.loads(file.model_dump_json())
            assert dumped["path"] == f.name
            assert dumped["size"] == expected_size
        else:
            assert file.path == Path(f.name)
            assert file.size == expected_size
