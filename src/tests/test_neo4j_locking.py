import asyncio
from enum import Enum

import pytest
from neo4j import AsyncGraphDatabase

from maestro.config import Config
from maestro.models import (
    StdErrModel,
    StdOutModel,
    WorkflowStepModelWithArtefacts,
    WorkflowStepModelWithStdOutAndStdErr,
)
from maestro.neo4j.invalidate import invalidate_workflow_steps
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import create_node, read_node
from tests.conftest import DummyArtefact


class RaceWinner(Enum):
    INVALIDATION = 0
    STEP_COMPLETION = 1


@pytest.mark.asyncio
@pytest.mark.parametrize("race_winner", RaceWinner)
async def test_workflow_step_invalidation_race_condition(race_winner):
    """
    This test sets up a race situation between two tasks that are updating a WorkflowStep node and its related nodes.
    One task invalidates the WorkflowStep node and its related nodes, while the other task adds new related STDOUT and
    STDERR nodes, to simulate completion of a workflow step.

    Two scenarios are tested, where either the invalidation task or the step completion task is allowed to proceed first.

    If the neo4j lock in invalidate_workflow_steps() in step_completion() does not work, the test is expected to fail for
    at least one of the scenarios.
    """

    # Create a WorkflowStep node with related DummyArtefact OUTPUT node.
    async def create_workflow_step():
        workflow_step = WorkflowStepModelWithArtefacts(OUTPUT=[DummyArtefact()])
        async with Neo4jTransaction() as neo4j_tx:
            await create_node(neo4j_tx, workflow_step)
        return workflow_step.maestro_id

    workflow_step_id = await create_workflow_step()

    # Events used to control execution order of the two tasks.
    invalidation_task_proceed = asyncio.Event()
    step_completion_task_proceed = asyncio.Event()

    # Invalidation task's coroutine.
    async def invalidation():
        # Wait for event allowing task to proceed.
        await invalidation_task_proceed.wait()
        async with Neo4jTransaction() as neo4j_tx:
            # Perform the invalidation.
            await invalidate_workflow_steps(neo4j_tx, [workflow_step_id])

            # Allow the other task to run.
            step_completion_task_proceed.set()

            # Allow other task to race against transaction commit in this task.
            await asyncio.sleep(0.1)

    # Workflow step completion task's coroutine.
    # This mimics what is done in the main Maestro process when a worker process has completed.
    async def step_completion():
        # Wait for event allowing task to proceed.
        await step_completion_task_proceed.wait()
        async with Neo4jTransaction() as neo4j_tx:
            # Allow the other task to run.
            invalidation_task_proceed.set()

            # Allow other task to race against this task.
            await asyncio.sleep(0)

            # Read the WorkflowStep node and update its relationships.
            workflow_step = await read_node(
                neo4j_tx, WorkflowStepModelWithStdOutAndStdErr, {"maestro_id": workflow_step_id}
            )
            assert workflow_step is not None
            workflow_step.STDOUT = StdOutModel()
            workflow_step.STDERR = StdErrModel()
            await create_node(neo4j_tx, workflow_step)
            # If the WorkflowStep node was invalidated, the related STDOUT and STDERR should be invalidated too.
            if workflow_step.invalidated_at is not None:
                await invalidate_workflow_steps(neo4j_tx, [workflow_step_id])

            # Allow other task to race against transaction commit in this task.
            await asyncio.sleep(0.1)

    # Run the two tasks concurrently.
    invalidation_task = asyncio.create_task(invalidation())
    step_completion_task = asyncio.create_task(step_completion())

    # Allow the task elected to be race winner to proceed.
    if race_winner == RaceWinner.INVALIDATION:
        invalidation_task_proceed.set()
    elif race_winner == RaceWinner.STEP_COMPLETION:
        step_completion_task_proceed.set()
    else:
        assert False

    # Wait for both tasks to complete.
    await asyncio.wait_for(asyncio.gather(invalidation_task, step_completion_task), timeout=5)

    # Inspect database and assert that the WorkflowStep node and its related nodes were all invalidated
    # irrespective of the execution order of the two tasks.
    async with Neo4jTransaction() as neo4j_tx:
        db_workflow_step = await read_node(
            neo4j_tx, WorkflowStepModelWithArtefacts, {"maestro_id": workflow_step_id}
        )
        assert db_workflow_step is not None, "db_workflow_step is None"
        assert db_workflow_step.OUTPUT, "db_workflow_step.OUTPUT is empty"
        assert db_workflow_step.OUTPUT[0].invalidated_at is not None
        assert db_workflow_step.STDOUT.invalidated_at is not None
        assert db_workflow_step.STDERR.invalidated_at is not None


class Neo4jTxOrder(Enum):
    """Order in which transactions should be executed"""

    TX_1_FIRST = "tx1"  # Transaction 1 executes before Transaction 2
    TX_2_FIRST = "tx2"  # Transaction 2 executes before Transaction 1


@pytest.mark.asyncio
@pytest.mark.parametrize("tx_order", [Neo4jTxOrder.TX_1_FIRST, Neo4jTxOrder.TX_2_FIRST])
async def test_concurrent_updates(tx_order: Neo4jTxOrder):
    """
    Test that Neo4j distributed locking mechanism works correctly for concurrent transactions.

    This test verifies:
    1. Transactions execute in the specified order
    2. Both transactions can successfully update the same node
    3. Final node value reflects the last transaction to execute

    Args:
        tx_order: Specifies which transaction should execute first
    """
    # Create the lock node before introducing concurrency. This avoids that the subsequent concurrent
    # transactions create two separate lock nodes.
    async with Neo4jTransaction():
        pass

    # Events to control transaction execution order
    tx1_proceed = asyncio.Event()
    tx2_proceed = asyncio.Event()

    async def process_tx(
        proceed_event_self: asyncio.Event,
        tx_order: Neo4jTxOrder,
        proceed_event_other: asyncio.Event | None,
    ):
        await proceed_event_self.wait()
        async with Neo4jTransaction() as neo4j_tx:
            if proceed_event_other is not None:
                # Allow the other task to attempt to start a concurrent transaction.
                proceed_event_other.set()
                # In case the concurrent transaction is not correctly waiting for lock,
                # but rather is racing with this transaction, give the concurrent
                # transcation time to win the race.
                await asyncio.sleep(0.1)
            # Verify lock state
            result = await neo4j_tx.query(
                """
                MATCH (n:__LockNode)
                RETURN n.n_locked as lock_state
                """
            )
            assert len(result) == 1, "Lock node should exist"
            lock_state = result[0]["lock_state"]

            assert lock_state is True, "Lock state should be True"

            # Update test node
            await neo4j_tx.query(
                """
                MERGE (n:TestNode {id: 'test'})
                SET n.value = $tx_order
                """,
                {"tx_order": tx_order.value},
            )

    # Control execution order
    if tx_order == Neo4jTxOrder.TX_1_FIRST:
        # Start concurrent tasks
        tx1_task = asyncio.create_task(
            process_tx(tx1_proceed, Neo4jTxOrder.TX_1_FIRST, tx2_proceed)
        )
        tx2_task = asyncio.create_task(process_tx(tx2_proceed, Neo4jTxOrder.TX_2_FIRST, None))
        tx1_proceed.set()
    else:
        # Start concurrent tasks
        tx1_task = asyncio.create_task(process_tx(tx1_proceed, Neo4jTxOrder.TX_1_FIRST, None))
        tx2_task = asyncio.create_task(
            process_tx(tx2_proceed, Neo4jTxOrder.TX_2_FIRST, tx1_proceed)
        )
        tx2_proceed.set()

    # Wait for both transactions to complete
    await asyncio.wait_for(asyncio.gather(tx1_task, tx2_task), timeout=5)

    # Verify final state
    async with Neo4jTransaction() as neo4j_tx:
        result = await neo4j_tx.query(
            """
            MATCH (n:TestNode {id: 'test'})
            RETURN n.value as value
            """
        )
        expected_value = (
            Neo4jTxOrder.TX_1_FIRST.value
            if tx_order == Neo4jTxOrder.TX_2_FIRST
            else Neo4jTxOrder.TX_2_FIRST.value
        )
        assert result[0]["value"] == expected_value, "Final value should match the last transaction"


@pytest.mark.asyncio
async def test_abrupt_session_close():
    """Test that lock node is properly reset even if session closes abruptly"""

    # First verify lock node starts unlocked
    async with Neo4jTransaction() as neo4j_tx:
        result = await neo4j_tx.query(
            """
            MATCH (n:__LockNode)
            RETURN n.n_locked as lock_state
            """
        )
        assert len(result) == 1, "Lock node should exist"
        assert result[0]["lock_state"] is True, "Lock should start as True"

    # Create transaction but don't use context manager so we can force close
    tx = Neo4jTransaction()
    tx._driver = AsyncGraphDatabase.driver(
        Config.NEO4J_URI, auth=(Config.NEO4J_USER, Config.NEO4J_PASSWORD)
    )
    tx._session = tx._driver.session()
    tx._tx = await tx._session.begin_transaction()

    # Acquire lock
    await tx.acquire_lock()

    # Force close session without proper cleanup
    await tx._session.close()
    await tx._driver.close()

    # Verify lock can be acquired again
    async with Neo4jTransaction() as neo4j_tx:
        result = await neo4j_tx.query(
            """
            MATCH (n:__LockNode)
            RETURN n.n_locked as lock_state
            """
        )
        assert result[0]["lock_state"] is True, "Lock should be released after abrupt close"
