from uuid import UUID, uuid4

import bs4
import pytest
from httpx import AsyncClient
from neo4j.time import DateTime

from maestro.models import BaseArtefactModel, Job, JobStatus, WorkflowModel, WorkflowStepModel
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import create_node, create_relationship
from tests.conftest import fetch_unique_node


class TestArtefact(BaseArtefactModel):
    foo: int
    __test__ = False


async def create_test_workflow(workflow_maestro_id: UUID) -> tuple[UUID, UUID, UUID, UUID, UUID]:
    """
    Common method to create a test workflow with two workflow steps and corresponding artefacts
    Diagram showing workflow structure:

     WORKFLOW -> JOB -> WORKFLOW_STEP1 -> OUTPUT -> WORKFLOW_STEP2 -> OUTPUT
                            ^
                            |
                          INPUT
    """
    # Create workflow, job and workflow step
    workflow = WorkflowModel(
        maestro_id=workflow_maestro_id,
        workflow_name="test_invalidate_workflow",
    )
    job = Job(status=JobStatus.COMPLETED, workflow_step="DummyWorkflowStep")
    workflow_step = WorkflowStepModel(additional_labels=["DummyWorkflowStep"])

    # Create artefacts for first workflow step
    input_artefact = TestArtefact(foo=1)
    output_artefact = TestArtefact(foo=2)

    # Create second workflow step that uses first step's output
    workflow_step2 = WorkflowStepModel()
    workflow_step2_output_artefact = TestArtefact(foo=3)

    async with Neo4jTransaction() as neo4j_tx:
        # Create nodes
        job_maestro_id = await create_node(neo4j_tx, job)
        workflow_maestro_id = await create_node(neo4j_tx, workflow)
        input_artefact_maestro_id = await create_node(neo4j_tx, input_artefact)
        workflow_step_maestro_id = await create_node(neo4j_tx, workflow_step)
        output_artefact_maestro_id = await create_node(neo4j_tx, output_artefact)
        workflow_step2_maestro_id = await create_node(neo4j_tx, workflow_step2)
        workflow_step2_output_maestro_id = await create_node(
            neo4j_tx, workflow_step2_output_artefact
        )

        # Create relationships
        await create_relationship(neo4j_tx, workflow_maestro_id, job_maestro_id, "JOB")
        await create_relationship(
            neo4j_tx, job_maestro_id, workflow_step_maestro_id, "WORKFLOW_STEP"
        )
        await create_relationship(
            neo4j_tx, input_artefact_maestro_id, workflow_step_maestro_id, "INPUT"
        )
        await create_relationship(
            neo4j_tx, workflow_step_maestro_id, output_artefact_maestro_id, "OUTPUT"
        )
        await create_relationship(
            neo4j_tx, output_artefact_maestro_id, workflow_step2_maestro_id, "INPUT"
        )
        await create_relationship(
            neo4j_tx, workflow_step2_maestro_id, workflow_step2_output_maestro_id, "OUTPUT"
        )

    return (
        input_artefact_maestro_id,
        workflow_step_maestro_id,
        output_artefact_maestro_id,
        workflow_step2_maestro_id,
        workflow_step2_output_maestro_id,
    )


async def verify_not_invalidated(*node_maestro_ids: UUID) -> None:
    """Verify that given nodes have not been invalidated"""
    async with Neo4jTransaction() as neo4j_tx:
        for node_maestro_id in node_maestro_ids:
            node = await fetch_unique_node(
                neo4j_tx, ["MaestroBase"], {"maestro_id": node_maestro_id}
            )
            assert "invalidated_at" not in node


async def verify_invalidated(*node_maestro_ids: UUID) -> None:
    """Verify that given nodes have been invalidated"""
    async with Neo4jTransaction() as neo4j_tx:
        for node_maestro_id in node_maestro_ids:
            node = await fetch_unique_node(
                neo4j_tx, ["MaestroBase"], {"maestro_id": node_maestro_id}
            )
            assert isinstance(node["invalidated_at"], DateTime)


@pytest.mark.asyncio
@pytest.mark.parametrize("workflow_maestro_id,propagate", [(uuid4(), True), (uuid4(), False)])
async def test_invalidate_workflow_endpoint(
    test_api_app: AsyncClient, workflow_maestro_id: UUID, propagate: bool
):
    # Test invalid requests
    response = await test_api_app.put("/api/v1/invalidate/workflow")
    assert response.status_code == 422

    # Test invalidating non-existent workflow
    response = await test_api_app.put(
        "/api/v1/invalidate/workflow",
        data={"maestro_id": workflow_maestro_id, "propagate": propagate},
    )
    assert response.status_code == 404
    assert response.json()["detail"] == f"WorkflowModel {workflow_maestro_id} not found"

    # Create test workflow
    (
        _input_artefact_maestro_id,
        workflow_step_maestro_id,
        output_artefact_maestro_id,
        workflow_step2_maestro_id,
        workflow_step2_output_maestro_id,
    ) = await create_test_workflow(workflow_maestro_id)

    # Verify workflow is not invalidated initially
    await verify_not_invalidated(
        workflow_maestro_id,
        workflow_step_maestro_id,
        output_artefact_maestro_id,
        workflow_step2_maestro_id,
        workflow_step2_output_maestro_id,
    )

    # Test workflow invalidation
    response = await test_api_app.put(
        "/api/v1/invalidate/workflow",
        data={"maestro_id": workflow_maestro_id, "propagate": propagate},
    )
    assert response.status_code == 200

    # Verify invalidation results
    # Check workflow, first step and output artefact are invalidated
    await verify_invalidated(
        workflow_maestro_id,
        workflow_step_maestro_id,
        output_artefact_maestro_id,
    )

    # Check propagation to second step if enabled
    if propagate:
        await verify_invalidated(workflow_step2_maestro_id, workflow_step2_output_maestro_id)
    else:
        await verify_not_invalidated(workflow_step2_maestro_id, workflow_step2_output_maestro_id)

    # Verify that we cannot invalidate workflow with same maestro_id again
    # If propagating, we should be able to invalidate again
    response = await test_api_app.put(
        "/api/v1/invalidate/workflow",
        data={"maestro_id": workflow_maestro_id, "propagate": propagate},
    )
    if propagate:
        assert response.status_code == 200
    else:
        assert response.status_code == 400
        assert (
            response.json()["detail"] == f"WorkflowModel {workflow_maestro_id} already invalidated"
        )


@pytest.mark.asyncio
@pytest.mark.parametrize("workflow_maestro_id,propagate", [(uuid4(), True), (uuid4(), False)])
async def test_invalidate_workflow_step_endpoint(
    test_api_app: AsyncClient, workflow_maestro_id: UUID, propagate: bool
):
    # Test invalid requests
    response = await test_api_app.put("/api/v1/invalidate/workflow_step")
    assert response.status_code == 422

    # Test invalidating non-existent workflow step
    response = await test_api_app.put(
        "/api/v1/invalidate/workflow_step",
        data={"maestro_id": workflow_maestro_id, "propagate": propagate},
    )
    assert response.status_code == 404
    assert response.json()["detail"] == f"WorkflowStepModel {workflow_maestro_id} not found"

    # Create test workflow step
    (
        _input_artefact_maestro_id,
        workflow_step_maestro_id,
        output_artefact_maestro_id,
        workflow_step2_maestro_id,
        workflow_step2_output_maestro_id,
    ) = await create_test_workflow(workflow_maestro_id)

    # Verify workflow step is not invalidated initially
    await verify_not_invalidated(
        workflow_step_maestro_id,
        output_artefact_maestro_id,
        workflow_step2_maestro_id,
        workflow_step2_output_maestro_id,
    )

    # Test valid request
    response = await test_api_app.put(
        "/api/v1/invalidate/workflow_step",
        data={"maestro_id": workflow_step_maestro_id, "propagate": propagate},
    )
    assert response.status_code == 200

    # Verify workflow step and output artefact were invalidated
    await verify_invalidated(workflow_step_maestro_id, output_artefact_maestro_id)

    # Check propagation to second step if enabled
    if propagate:
        await verify_invalidated(workflow_step2_maestro_id, workflow_step2_output_maestro_id)
    else:
        await verify_not_invalidated(workflow_step2_maestro_id, workflow_step2_output_maestro_id)

    # Verify that we cannot invalidate workflow step with same maestro_id again
    # If propagating, we should be able to invalidate again
    response = await test_api_app.put(
        "/api/v1/invalidate/workflow_step",
        data={"maestro_id": workflow_step_maestro_id, "propagate": propagate},
    )
    if propagate:
        assert response.status_code == 200
    else:
        assert response.status_code == 400
        assert (
            response.json()["detail"]
            == f"WorkflowStepModel {workflow_step_maestro_id} already invalidated"
        )


@pytest.mark.asyncio
@pytest.mark.parametrize("workflow_maestro_id,propagate", [(uuid4(), True), (uuid4(), False)])
async def test_invalidate_artefact_endpoint(
    test_api_app: AsyncClient, workflow_maestro_id: UUID, propagate: bool
):
    # Test invalid requests
    response = await test_api_app.put("/api/v1/invalidate/artefact")
    assert response.status_code == 422

    # Test invalidating non-existent artefact
    response = await test_api_app.put(
        "/api/v1/invalidate/artefact",
        data={"maestro_id": workflow_maestro_id, "propagate": propagate},
    )
    assert response.status_code == 404
    assert response.json()["detail"] == f"BaseArtefactModel {workflow_maestro_id} not found"

    # Create test artefact
    (
        input_artefact_maestro_id,
        workflow_step_maestro_id,
        output_artefact_maestro_id,
        workflow_step2_maestro_id,
        workflow_step2_output_maestro_id,
    ) = await create_test_workflow(workflow_maestro_id)

    # Verify artefact is not invalidated initially
    await verify_not_invalidated(
        input_artefact_maestro_id,
        workflow_step_maestro_id,
        output_artefact_maestro_id,
        workflow_step2_maestro_id,
        workflow_step2_output_maestro_id,
    )

    # Test valid request
    response = await test_api_app.put(
        "/api/v1/invalidate/artefact",
        data={"maestro_id": input_artefact_maestro_id, "propagate": propagate},
    )
    assert response.status_code == 200

    # Verify invalidation results
    await verify_invalidated(input_artefact_maestro_id)

    # Check propagation if enabled
    if propagate:
        await verify_invalidated(
            workflow_step_maestro_id,
            output_artefact_maestro_id,
            workflow_step2_maestro_id,
            workflow_step2_output_maestro_id,
        )
    else:
        await verify_not_invalidated(
            workflow_step_maestro_id,
            output_artefact_maestro_id,
            workflow_step2_maestro_id,
            workflow_step2_output_maestro_id,
        )

    # Verify that we cannot invalidate artefact with same maestro_id again
    # If propagating, we should be able to invalidate again
    response = await test_api_app.put(
        "/api/v1/invalidate/artefact",
        data={"maestro_id": input_artefact_maestro_id, "propagate": propagate},
    )
    if propagate:
        assert response.status_code == 200
    else:
        assert response.status_code == 400
        assert (
            response.json()["detail"]
            == f"BaseArtefactModel {input_artefact_maestro_id} already invalidated"
        )


@pytest.mark.asyncio
async def test_invalidate_count_endpoint(test_api_app: AsyncClient):
    """Test the /invalidate/count endpoint for different node types and propagation settings.

    This test verifies:
    1. Error handling for invalid requests
    2. Correct counting of affected nodes for workflow invalidation
    3. Correct counting of affected nodes for workflow step invalidation with/without propagation
    4. Correct counting of affected nodes for artefact invalidation with/without propagation
    """
    workflow_maestro_id = uuid4()

    # Test invalid requests
    invalid_requests = [
        ("random", "false", 422, "Invalid node type: random"),  # Invalid node type
        (
            "workflow",
            "true",
            404,
            f"Workflow {workflow_maestro_id} not found",
        ),  # Non-existent workflow
        (
            "workflow_step",
            "true",
            404,
            f"Workflow_Step {workflow_maestro_id} not found",
        ),  # Non-existent step
        (
            "artefact",
            "false",
            404,
            f"Artefact {workflow_maestro_id} not found",
        ),  # Non-existent artefact
    ]

    # Verify error responses for invalid requests
    for node_type, propagate, expected_status, expected_detail in invalid_requests:
        response = await test_api_app.get(
            f"/api/v1/invalidate/count/{node_type}/{workflow_maestro_id}/{propagate}"
        )
        assert (
            response.status_code == expected_status
        ), f"Expected {expected_status} for {node_type}"
        assert expected_detail in response.text

    # Set up test graph:
    # workflow -> step1 -> output1 -> step2 -> output2 -> step3 -> output3
    (
        _input_artefact_maestro_id,  # Not used in this test
        workflow_step_maestro_id,  # step1
        output_artefact_maestro_id,  # output1
        workflow_step2_maestro_id,  # step2
        workflow_step2_output_maestro_id,  # output2
    ) = await create_test_workflow(workflow_maestro_id)

    # Add step3 and output3 to extend the dependency chain
    async with Neo4jTransaction() as neo4j_tx:
        workflow_step3_maestro_id = await create_node(neo4j_tx, WorkflowStepModel())
        workflow_step3_output_maestro_id = await create_node(neo4j_tx, TestArtefact(foo=4))
        # Connect step3 to output2
        await create_relationship(
            neo4j_tx, workflow_step2_output_maestro_id, workflow_step3_maestro_id, "INPUT"
        )
        # Connect step3 to its output
        await create_relationship(
            neo4j_tx, workflow_step3_maestro_id, workflow_step3_output_maestro_id, "OUTPUT"
        )

    # Define test scenarios
    test_cases = [
        # Format: (node_type, node_id, propagate, expected_counts)
        # Workflow invalidation (always propagates to all nodes)
        (
            "workflow",
            workflow_maestro_id,
            "true",
            {
                "workflow_count": 1,  # The workflow itself
                "workflow_step_count": 3,  # All steps (1,2,3)
                "artefact_count": 3,  # All outputs (1,2,3)
            },
        ),
        # Workflow step invalidation - no propagation
        (
            "workflow_step",
            workflow_step_maestro_id,
            "false",
            {
                "workflow_step_count": 1,  # Only step1
                "artefact_count": 1,  # Only output1
            },
        ),
        # Workflow step invalidation - with propagation
        (
            "workflow_step",
            workflow_step_maestro_id,
            "true",
            {
                "workflow_step_count": 3,  # step1 + downstream steps (2,3)
                "artefact_count": 3,  # All outputs (1,2,3)
            },
        ),
        # Artefact invalidation - no propagation
        (
            "artefact",
            output_artefact_maestro_id,
            "false",
            {
                "workflow_step_count": 0,  # No steps affected
                "artefact_count": 1,  # Only this artefact
            },
        ),
        # Artefact invalidation - with propagation
        (
            "artefact",
            output_artefact_maestro_id,
            "true",
            {
                "workflow_step_count": 2,  # Downstream steps (2,3)
                "artefact_count": 3,  # This artefact + downstream outputs (2,3)
            },
        ),
    ]

    # Verify each test case
    for node_type, node_id, propagate, expected_counts in test_cases:
        response = await test_api_app.get(
            f"/api/v1/invalidate/count/{node_type}/{node_id}/{propagate}"
        )
        assert response.status_code == 200, f"Failed for {node_type} with propagate={propagate}"
        # Parse the HTML response
        soup = bs4.BeautifulSoup(response.text, "html.parser")
        actual_counts = {
            "workflow_count": int(soup.find(id="workflow_count").text)
            if soup.find(id="workflow_count")
            else 0,
            "workflow_step_count": int(soup.find(id="workflow_step_count").text),
            "artefact_count": int(soup.find(id="artefact_count").text),
        }

        # Remove 'workflow_count' from actual_counts if not expected
        if "workflow_count" not in expected_counts:
            actual_counts.pop("workflow_count")

        assert actual_counts == expected_counts, (
            f"Incorrect counts for {node_type} with propagate={propagate}\n"
            f"Expected: {expected_counts}\n"
            f"Got: {actual_counts}"
        )
