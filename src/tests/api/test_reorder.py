import pytest
from httpx import AsyncClient


@pytest.mark.asyncio
async def test_reorder_job_endpoint(test_api_app: AsyncClient):
    # try to reorder without any maestro_id
    response = await test_api_app.put("/api/v1/reorder/job")
    assert response.status_code == 404

    # try to reorder with maestro_id that doesn't correspond to any job
    response = await test_api_app.put("/api/v1/reorder/job/fcb3be4d-d603-40f1-9a28-49662ead9207")
    assert response.status_code == 400
