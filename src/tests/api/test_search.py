import random
import string
import uuid

import pytest

from maestro.api.queries.workflows import (
    WORKFLOW_STATUS_CTE,
    build_search_query,
    get_workflow_counts,
    get_workflows,
)
from maestro.api.schema import Search
from maestro.models import Job, JobPriorityEnum, JobStatus, WorkflowModel
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import create_node
from maestro.workflow_step.message_watcher import MessageWatcher


def test_search_model():
    search = Search.from_string()
    assert search.search_terms == []
    assert search.negate_terms == []
    assert search.statuses == set()
    assert search.priorities == set()

    search = Search.from_string("")
    assert search.search_terms == []
    assert search.negate_terms == []
    assert search.statuses == set()
    assert search.priorities == set()

    search = Search.from_string("foobar")
    assert search.search_terms == ["foobar"]
    assert search.negate_terms == []
    assert search.statuses == set()
    assert search.priorities == set()

    search = Search.from_string("foobar baz")
    assert search.search_terms == ["foobar", "baz"]
    assert search.negate_terms == []
    assert search.statuses == set()
    assert search.priorities == set()

    search = Search.from_string("-foobar")
    assert search.search_terms == []
    assert search.negate_terms == ["foobar"]
    assert search.statuses == set()
    assert search.priorities == set()

    search = Search.from_string("status:failed")
    assert search.search_terms == []
    assert search.negate_terms == []
    assert search.statuses == {JobStatus.FAILED}
    assert search.priorities == set()

    # Test case insensitivity
    search = Search.from_string("status:Failed")
    assert search.search_terms == []
    assert search.negate_terms == []
    assert search.statuses == {JobStatus.FAILED}
    assert search.priorities == set()

    search = Search.from_string("Status:failed")
    assert search.search_terms == []
    assert search.negate_terms == []
    assert search.statuses == {JobStatus.FAILED}
    assert search.priorities == set()

    search = Search.from_string("priority:normal")
    assert search.search_terms == []
    assert search.negate_terms == []
    assert search.statuses == set()
    assert search.priorities == {JobPriorityEnum.NORMAL}

    # Test case insensitivity
    search = Search.from_string("priority:Normal")
    assert search.search_terms == []
    assert search.negate_terms == []
    assert search.statuses == set()
    assert search.priorities == {JobPriorityEnum.NORMAL}

    search = Search.from_string("Priority:normal")
    assert search.search_terms == []
    assert search.negate_terms == []
    assert search.statuses == set()
    assert search.priorities == {JobPriorityEnum.NORMAL}

    search = Search.from_string("priority:normal,high")
    assert search.search_terms == []
    assert search.negate_terms == []
    assert search.statuses == set()
    assert search.priorities == {JobPriorityEnum.NORMAL, JobPriorityEnum.HIGH}

    search = Search.from_string("foobar status:completed -baz priority:urgent")
    assert search.search_terms == ["foobar"]
    assert search.negate_terms == ["baz"]
    assert search.statuses == {JobStatus.COMPLETED}
    assert search.priorities == {JobPriorityEnum.URGENT}

    search = Search.from_string("foobar status:completed,failed priority:urgent,normal -baz")
    assert search.search_terms == ["foobar"]
    assert search.negate_terms == ["baz"]
    assert search.statuses == {JobStatus.COMPLETED, JobStatus.FAILED}
    assert search.priorities == {JobPriorityEnum.NORMAL, JobPriorityEnum.URGENT}

    search = Search.from_string("foobar status:completed,failed status:pending -baz -qux")
    assert search.search_terms == ["foobar"]
    assert search.negate_terms == ["baz", "qux"]
    assert search.statuses == {JobStatus.COMPLETED, JobStatus.FAILED, JobStatus.PENDING}
    assert search.priorities == set()

    search = Search.from_string(
        "foobar priority:normal,high status:completed,failed priority:urgent status:pending -baz -qux"
    )
    assert search.search_terms == ["foobar"]
    assert search.negate_terms == ["baz", "qux"]
    assert search.statuses == {JobStatus.COMPLETED, JobStatus.FAILED, JobStatus.PENDING}
    assert search.priorities == {
        JobPriorityEnum.NORMAL,
        JobPriorityEnum.HIGH,
        JobPriorityEnum.URGENT,
    }

    search = Search.from_string("foobar qux quux")
    assert search.search_terms == ["foobar", "qux", "quux"]
    assert search.negate_terms == []
    assert search.statuses == set()
    assert search.priorities == set()

    search = Search.from_string("abc-def øæå&!¤%& -123 -456 -(){}[]")
    assert search.search_terms == ["abc-def", "øæå&!¤%&"]
    assert search.negate_terms == ["123", "456", "(){}[]"]
    assert search.statuses == set()
    assert search.priorities == set()


@pytest.mark.asyncio
async def test_build_search_query():
    base_expected_response = """
        MATCH (w:WorkflowModel)--(j:Job)
        WHERE substring(w.workflow_name, 0, 1) <> "_"
    """
    query, parameters = build_search_query(Search.from_string(""))
    assert query == base_expected_response

    workflow_id = uuid.uuid4()
    # search term
    query, parameters = build_search_query(Search.from_string(f"workfow_{workflow_id}"))
    expected_response = base_expected_response + (
        " AND (w.workflow_name =~ '(?i).*' + $search0 + '.*' "
        "OR ANY(p IN j.search_metadata WHERE p =~ '(?i).*' + $search0 + '.*') "
        "OR ANY(p IN j.display_name WHERE p =~ '(?i).*' + $search0 + '.*'))"
    )
    assert query.strip() == expected_response.strip()
    assert parameters == {"search0": f"workfow_{workflow_id}"}
    query, parameters = build_search_query(Search.from_string(f"workflow_{workflow_id} job_name"))
    expected_response = base_expected_response + (
        " AND (w.workflow_name =~ '(?i).*' + $search0 + '.*' "
        "OR ANY(p IN j.search_metadata WHERE p =~ '(?i).*' + $search0 + '.*') "
        "OR ANY(p IN j.display_name WHERE p =~ '(?i).*' + $search0 + '.*'))"
        "  AND (w.workflow_name =~ '(?i).*' + $search1 + '.*' "
        "OR ANY(p IN j.search_metadata WHERE p =~ '(?i).*' + $search1 + '.*') "
        "OR ANY(p IN j.display_name WHERE p =~ '(?i).*' + $search1 + '.*'))"
    )
    assert query.strip() == expected_response.strip()
    assert parameters == {"search0": f"workflow_{workflow_id}", "search1": "job_name"}

    # negate term
    negate_workflow_id = uuid.uuid4()
    query, parameters = build_search_query(Search.from_string(f"-workfow_{negate_workflow_id}"))
    expected_response = base_expected_response + (
        " AND NOT (w.workflow_name =~ '(?i).*' + $negate0 + '.*' "
        "OR ANY(p IN j.search_metadata WHERE p =~ '(?i).*' + $negate0 + '.*') "
        "OR ANY(p IN j.display_name WHERE p =~ '(?i).*' + $negate0 + '.*'))"
    )
    assert query.strip() == expected_response.strip()
    assert parameters == {"negate0": f"workfow_{negate_workflow_id}"}

    # status
    query, parameters = build_search_query(Search.from_string("status:completed"))
    expected_response = (
        base_expected_response
        + WORKFLOW_STATUS_CTE
        + (
            """
            WHERE workflow_status IN $statuses
            MATCH (w)--(j:Job)
        """
        )
    )
    assert query.strip() == expected_response.strip()
    assert parameters == {"statuses": {JobStatus.COMPLETED}}

    # priority
    query, parameters = build_search_query(Search.from_string("priority:normal"))
    expected_response = (
        base_expected_response + " AND ANY(priority IN j.priority WHERE priority IN $priorities)"
    )
    assert query.strip() == expected_response.strip()
    assert parameters == {"priorities": {JobPriorityEnum.NORMAL}}

    # combined
    query, parameters = build_search_query(
        Search.from_string(
            f"workflow_{workflow_id} status:completed,failed priority:urgent -workflow_{negate_workflow_id}"
        )
    )
    expected_response = (
        base_expected_response
        + (
            " AND (w.workflow_name =~ '(?i).*' + $search0 + '.*' "
            "OR ANY(p IN j.search_metadata WHERE p =~ '(?i).*' + $search0 + '.*') "
            "OR ANY(p IN j.display_name WHERE p =~ '(?i).*' + $search0 + '.*')) "
            " AND NOT (w.workflow_name =~ '(?i).*' + $negate0 + '.*' "
            "OR ANY(p IN j.search_metadata WHERE p =~ '(?i).*' + $negate0 + '.*') "
            "OR ANY(p IN j.display_name WHERE p =~ '(?i).*' + $negate0 + '.*')) "
            " AND ANY(priority IN j.priority WHERE priority IN $priorities) "
        )
        + WORKFLOW_STATUS_CTE
        + (
            """
            WHERE workflow_status IN $statuses
            MATCH (w)--(j:Job)
        """
        )
    )
    assert query.strip() == expected_response.strip()
    assert parameters == {
        "search0": f"workflow_{workflow_id}",
        "negate0": f"workflow_{negate_workflow_id}",
        "statuses": {JobStatus.COMPLETED, JobStatus.FAILED},
        "priorities": {JobPriorityEnum.URGENT},
    }


@pytest.mark.asyncio
async def test_get_workflows_search():
    class TestSearchMessageWatcher(MessageWatcher):
        async def process(self, **kwargs):
            pass

    # setup for realistic ids
    def generate_alphanumeric_id(min_length=8, max_length=20):
        length = random.randint(min_length, max_length)
        characters = string.ascii_letters + string.digits
        return "".join(random.choice(characters) for _ in range(length))

    project_id, batch_id, sample_id = (generate_alphanumeric_id() for _ in range(3))
    workflow_name_prefix = f"{project_id}_{batch_id}_{sample_id}"
    # Create some test workflows with different name, jobs, status, priority, and search_metadata
    test_workflows = [
        WorkflowModel(
            workflow_name=f"{workflow_name_prefix}_{uuid.uuid4()}",
            jobs=[
                Job(
                    inputs={},
                    params={},
                    display_name="job_1",
                    search_metadata=["job_project_id: project_1", "job_sample_id: sample_1"],
                    status=JobStatus.COMPLETED,
                    priority=JobPriorityEnum.NORMAL,
                    workflow_step=TestSearchMessageWatcher,
                ),
            ],
        ),
        WorkflowModel(
            workflow_name=f"{workflow_name_prefix}_{uuid.uuid4()}",
            jobs=[
                Job(
                    inputs={},
                    params={},
                    display_name="job_2",
                    search_metadata=["job_project_id: project_2", "job_sample_id: sample_2"],
                    status=JobStatus.RUNNING,
                    priority=JobPriorityEnum.HIGH,
                    workflow_step=TestSearchMessageWatcher,
                ),
            ],
        ),
        WorkflowModel(
            workflow_name=f"{workflow_name_prefix}_{uuid.uuid4()}",
            jobs=[
                Job(
                    inputs={},
                    params={},
                    display_name="job_3",
                    search_metadata=["job_project_id: project_3", "job_sample_id: sample_3"],
                    status=JobStatus.FAILED,
                    priority=JobPriorityEnum.URGENT,
                    workflow_step=TestSearchMessageWatcher,
                ),
            ],
        ),
    ]

    # Order the test workflows
    for workflow in test_workflows:
        async with Neo4jTransaction() as neo4j_tx:
            await create_node(neo4j_tx, workflow)

    # Helper function to check the content of the response
    async def check_response_content(
        search_string: str, expected_count: int, expected_attributes: list[dict]
    ):
        async with Neo4jTransaction() as neo4j_tx:
            results = await get_workflows(neo4j_tx, search=Search.from_string(search_string))
            assert len(results) == expected_count
            for i, workflow in enumerate(results):
                assert workflow.workflow_name.startswith(workflow_name_prefix)
                assert len(workflow.jobs) == 1
                job = next(iter(workflow.jobs))
                assert job.display_name in [attr["display_name"] for attr in expected_attributes]
                assert job.status in [attr["status"] for attr in expected_attributes]
                assert job.priority in [attr["priority"] for attr in expected_attributes]

    # Test various search scenarios with content checking
    await check_response_content(
        "job_1",
        1,
        [
            {
                "display_name": "job_1",
                "status": JobStatus.COMPLETED,
                "priority": JobPriorityEnum.NORMAL,
            }
        ],
    )
    await check_response_content(
        "status:running",
        1,
        [{"display_name": "job_2", "status": JobStatus.RUNNING, "priority": JobPriorityEnum.HIGH}],
    )
    await check_response_content(
        "priority:urgent",
        1,
        [{"display_name": "job_3", "status": JobStatus.FAILED, "priority": JobPriorityEnum.URGENT}],
    )

    # Test if search free text is case insensitive
    await check_response_content(
        "JOB_1",
        1,
        [
            {
                "display_name": "job_1",
                "status": JobStatus.COMPLETED,
                "priority": JobPriorityEnum.NORMAL,
            }
        ],
    )

    await check_response_content(
        workflow_name_prefix.upper(),
        3,
        [
            {
                "display_name": "job_1",
                "status": JobStatus.COMPLETED,
                "priority": JobPriorityEnum.NORMAL,
            },
            {
                "display_name": "job_2",
                "status": JobStatus.RUNNING,
                "priority": JobPriorityEnum.HIGH,
            },
            {
                "display_name": "job_3",
                "status": JobStatus.FAILED,
                "priority": JobPriorityEnum.URGENT,
            },
        ],
    )

    await check_response_content(
        workflow_name_prefix.lower(),
        3,
        [
            {
                "display_name": "job_1",
                "status": JobStatus.COMPLETED,
                "priority": JobPriorityEnum.NORMAL,
            },
            {
                "display_name": "job_2",
                "status": JobStatus.RUNNING,
                "priority": JobPriorityEnum.HIGH,
            },
            {
                "display_name": "job_3",
                "status": JobStatus.FAILED,
                "priority": JobPriorityEnum.URGENT,
            },
        ],
    )

    # Test search by workflow name
    await check_response_content(
        "job_1",
        1,
        [
            {
                "display_name": "job_1",
                "status": JobStatus.COMPLETED,
                "priority": JobPriorityEnum.NORMAL,
            }
        ],
    )

    # Test search by status
    await check_response_content(
        "status:running",
        1,
        [{"display_name": "job_2", "status": JobStatus.RUNNING, "priority": JobPriorityEnum.HIGH}],
    )

    # Test search by priority
    await check_response_content(
        "priority:urgent",
        1,
        [{"display_name": "job_3", "status": JobStatus.FAILED, "priority": JobPriorityEnum.URGENT}],
    )

    # Test search by metadata
    await check_response_content(
        "job_project_id: project_2",
        1,
        [{"display_name": "job_2", "status": JobStatus.RUNNING, "priority": JobPriorityEnum.HIGH}],
    )

    # Test combined search
    await check_response_content(
        "status:completed,failed priority:normal,urgent",
        2,
        [
            {
                "display_name": "job_1",
                "status": JobStatus.COMPLETED,
                "priority": JobPriorityEnum.NORMAL,
            },
            {
                "display_name": "job_3",
                "status": JobStatus.FAILED,
                "priority": JobPriorityEnum.URGENT,
            },
        ],
    )

    await check_response_content(
        "status:completed,failed priority:normal,urgent -job_1",
        1,
        [
            {
                "display_name": "job_3",
                "status": JobStatus.FAILED,
                "priority": JobPriorityEnum.URGENT,
            },
        ],
    )

    # Test negative search
    await check_response_content(
        "-job_2",
        2,
        [
            {
                "display_name": "job_1",
                "status": JobStatus.COMPLETED,
                "priority": JobPriorityEnum.NORMAL,
            },
            {
                "display_name": "job_3",
                "status": JobStatus.FAILED,
                "priority": JobPriorityEnum.URGENT,
            },
        ],
    )

    # same as above but with camel case
    await check_response_content(
        "-Job_2",
        2,
        [
            {
                "display_name": "job_1",
                "status": JobStatus.COMPLETED,
                "priority": JobPriorityEnum.NORMAL,
            },
            {
                "display_name": "job_3",
                "status": JobStatus.FAILED,
                "priority": JobPriorityEnum.URGENT,
            },
        ],
    )

    # Test search with no results
    await check_response_content("NonexistentWorkflow", 0, [])


@pytest.mark.asyncio
async def test_get_workflow_counts():
    class TestCountsMessageWatcher(MessageWatcher):
        async def process(self, **kwargs):
            pass

    # Create test workflows with different statuses
    test_workflows = []
    # Single status workflows
    statuses = {
        JobStatus.COMPLETED: 3,
        JobStatus.PENDING: 2,
        JobStatus.FAILED: 3,
        JobStatus.QUEUED: 2,
        JobStatus.RUNNING: 3,
        JobStatus.CANCELLED: 2,
    }
    # Create test workflows with different statuses:
    # - Single status workflows: Each workflow has jobs with the same status
    #   (e.g., all completed, all pending, etc.)
    # - Mixed status workflows: Each workflow has jobs with different statuses
    #   (e.g., some completed + failed, some running + pending + queued)
    for status, job_count in statuses.items():
        test_workflows.append(
            WorkflowModel(
                workflow_name=f"workflow_{uuid.uuid4()}",
                jobs=[
                    Job(
                        inputs={},
                        params={},
                        display_name=f"{status}_job_{uuid.uuid4()}",
                        status=status,
                        workflow_step=TestCountsMessageWatcher,
                    )
                    for _ in range(job_count)
                ],
            )
        )
    # Mixed status workflows
    mixed_status_combinations = [
        # failed + completed -> failed workflow
        [(JobStatus.COMPLETED, 1), (JobStatus.FAILED, 1)],
        # running + pending + queued -> running workflow
        [(JobStatus.RUNNING, 1), (JobStatus.PENDING, 1), (JobStatus.QUEUED, 1)],
        # completed + cancelled + running -> cancelled workflow
        [(JobStatus.COMPLETED, 1), (JobStatus.CANCELLED, 1), (JobStatus.RUNNING, 1)],
    ]
    for status_combo in mixed_status_combinations:
        jobs = []
        for status, count in status_combo:
            for _ in range(count):
                jobs.append(
                    Job(
                        inputs={},
                        params={},
                        display_name=f"{status}_job_{uuid.uuid4()}",
                        status=status,
                        workflow_step=TestCountsMessageWatcher,
                    )
                )
        test_workflows.append(WorkflowModel(workflow_name=f"workflow_{uuid.uuid4()}", jobs=jobs))

    async with Neo4jTransaction() as neo4j_tx:
        for workflow in test_workflows:
            await create_node(neo4j_tx, workflow)

        # Test counts with no filters
        counts = await get_workflow_counts(neo4j_tx, Search.from_string(""))
        assert counts.completed == 1
        assert counts.failed == 2
        assert counts.running == 2
        assert counts.cancelled == 2
        assert counts.queued == 1
        assert counts.pending == 1
        assert counts.all == 9

        # Test counts with status filter
        counts = await get_workflow_counts(neo4j_tx, Search.from_string("status:completed"))
        assert counts.completed == 1
        assert counts.failed == 0
        assert counts.running == 0
        assert counts.cancelled == 0
        assert counts.queued == 0
        assert counts.pending == 0
        assert counts.all == 1

        # Test counts with search term
        # Search for "completed_job" string matches workflows where:
        # - One workflow has only completed jobs -> workflow status is completed
        # - One workflow has completed + failed jobs -> workflow status is failed
        # - One workflow has completed + cancelled + running jobs -> workflow status is cancelled
        # This shows that having a completed job doesn't necessarily mean the workflow status is completed
        counts = await get_workflow_counts(neo4j_tx, Search.from_string("completed_job"))
        assert counts.completed == 1
        assert counts.failed == 1
        assert counts.running == 0
        assert counts.cancelled == 1
        assert counts.queued == 0
        assert counts.pending == 0
        assert counts.all == 3

        # Test counts with negative search
        counts = await get_workflow_counts(neo4j_tx, Search.from_string("-completed_job"))
        assert counts.completed == 0
        assert counts.failed == 2
        assert counts.running == 2
        assert counts.cancelled == 2
        assert counts.queued == 1
        assert counts.pending == 1
        assert counts.all == 8

        # Test counts with no matches
        counts = await get_workflow_counts(neo4j_tx, Search.from_string("nonexistent"))
        assert counts.completed == 0
        assert counts.failed == 0
        assert counts.running == 0
        assert counts.cancelled == 0
        assert counts.queued == 0
        assert counts.pending == 0
        assert counts.all == 0
