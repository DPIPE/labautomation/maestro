from contextlib import contextmanager
from typing import Any

import pytest
from pydantic import ValidationError

from maestro.api.queries.workflows import get_workflows
from maestro.api.schema import Search
from maestro.config import Config
from maestro.models import BaseArtefactModel, Job, WorkflowModel
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import create_node
from maestro.workflow_step.message_watcher import MessageWatcher


@contextmanager
def define_classes(artefact_class: bool, workflow_step_class: bool):
    "Utility context to temporarily define UndefinedArtefact and UndefinedWorkflowStep classes"
    if artefact_class:

        class UndefinedArtefact(BaseArtefactModel):
            value: int
    else:
        UndefinedArtefact = None  # type: ignore[assignment ,misc]

    if workflow_step_class:

        class UndefinedWorkflowStep(MessageWatcher):
            async def process(self, input: Any):
                pass
    else:
        UndefinedWorkflowStep = None  # type: ignore[assignment,misc]

    try:
        yield UndefinedArtefact, UndefinedWorkflowStep
    finally:
        del UndefinedArtefact
        del UndefinedWorkflowStep
        import gc

        gc.collect()


@pytest.mark.asyncio
async def test_config_running_api(neo4j_tx: Neo4jTransaction):
    """Test Config.IS_RUNNING_IN_API behaviour"""

    def create_workflow():
        return WorkflowModel(
            JOB={
                Job(
                    workflow_step="UndefinedWorkflowStep",
                    inputs={"input": ("UndefinedArtefact", {"value": 1})},
                )
            },
            workflow_name="test_skip_validation",
        )

    # Set Config.IS_RUNNING_IN_API to False to simulate running in a script
    Config.IS_RUNNING_IN_API = False

    # Define both "UndefinedArtefact" and "UndefinedWorkflowStep" to create the nodes in the database
    with define_classes(artefact_class=True, workflow_step_class=True):
        maestro_id = await create_node(neo4j_tx, create_workflow())

    with define_classes(artefact_class=False, workflow_step_class=True):
        # Should fail because "UndefinedArtefact" is not defined subclass of BaseArtefactModel
        # (and Config.IS_RUNNING_IN_API is False)
        with pytest.raises(ValidationError, match="Artefact class 'UndefinedArtefact' not found"):
            await get_workflows(neo4j_tx, search=Search())

    with define_classes(artefact_class=True, workflow_step_class=False):
        # Should fail because "UndefinedWorkflowStep" is not defined
        # (and Config.IS_RUNNING_IN_API is False)
        with pytest.raises(
            ValidationError, match="Workflow step 'UndefinedWorkflowStep' not found"
        ):
            await get_workflows(neo4j_tx, search=Search())

    # Set Config.IS_RUNNING_IN_API to True to simulate running in the API
    Config.IS_RUNNING_IN_API = True

    with define_classes(artefact_class=False, workflow_step_class=False):
        # Should succeed because Config.IS_RUNNING_IN_API is True
        # - "UndefinedArtefact" should fall back to BaseArtefactModel
        api_workflows = await get_workflows(neo4j_tx, search=Search())
        assert api_workflows[0].maestro_id == maestro_id
        assert len(api_workflows[0].jobs) == 1
        assert api_workflows[0].jobs.pop().inputs["input"] == (BaseArtefactModel, {"value": 1})

    with define_classes(artefact_class=True, workflow_step_class=False) as (
        UndefinedArtefact,
        UndefinedWorkflowStep,
    ):
        # - "UndefinedArtefact" is now *defined*, and it should be used
        assert UndefinedArtefact is not BaseArtefactModel
        api_workflows = await get_workflows(neo4j_tx, search=Search())
        assert api_workflows[0].maestro_id == maestro_id
        assert len(api_workflows[0].jobs) == 1
        assert api_workflows[0].jobs.pop().inputs["input"] == (UndefinedArtefact, {"value": 1})
