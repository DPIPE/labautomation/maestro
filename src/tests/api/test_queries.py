from uuid import uuid4

import pytest

from maestro.api.queries.jobs import get_job_with_inputs
from maestro.api.schema import JobWithInputs
from maestro.models import Job, JobStatus, WorkflowModel
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import create_node
from tests.conftest import DummyArtefact


@pytest.mark.asyncio
async def test_get_job_with_inputs():
    """Test the get_job_with_inputs function for various scenarios."""
    # Test job not found case
    async with Neo4jTransaction() as neo4j_tx:
        with pytest.raises(AssertionError, match="Job not found"):
            await get_job_with_inputs(neo4j_tx, uuid4())

    # Test non-pending job case
    job = Job(status=JobStatus.COMPLETED, workflow_step="DummyWorkflowStep")
    async with Neo4jTransaction() as neo4j_tx:
        job_maestro_id = await create_node(neo4j_tx, job)
        with pytest.raises(AssertionError, match="Job is not pending, cancelled or failed"):
            await get_job_with_inputs(neo4j_tx, job_maestro_id)

    # Set up test data for remaining cases
    pending_input = DummyArtefact(identifier="test_input_1")
    existing_input = DummyArtefact(identifier="test_input_2")

    pending_job = Job(
        inputs={
            "input_1": (DummyArtefact, {"identifier": pending_input.identifier}),
            "input_2": (DummyArtefact, {"identifier": existing_input.identifier}),
        },
        status=JobStatus.PENDING,
        workflow_step="DummyWorkflowStep",
    )
    workflow = WorkflowModel(workflow_name="test_workflow", jobs=[pending_job])

    async with Neo4jTransaction() as neo4j_tx:
        await create_node(neo4j_tx, workflow)

        # Test case: No inputs exist yet in the database
        result = await get_job_with_inputs(neo4j_tx, pending_job.maestro_id)
        assert isinstance(result, JobWithInputs)
        assert result.workflow_name == "test_workflow"
        assert not result.created
        expected_pending = [
            {"type": "DummyArtefact", "identifier": {"identifier": input_id}}
            for input_id in [existing_input.identifier, pending_input.identifier]
        ]
        assert sorted(
            result.formatted_pending, key=lambda x: x["identifier"]["identifier"]
        ) == sorted(expected_pending, key=lambda x: x["identifier"]["identifier"])

        # Test case: One input exists in the database
        await create_node(neo4j_tx, existing_input)
        result = await get_job_with_inputs(neo4j_tx, pending_job.maestro_id)
        assert isinstance(result, JobWithInputs)
        assert result.workflow_name == "test_workflow"
        assert result.formatted_pending == [
            {"type": "DummyArtefact", "identifier": {"identifier": pending_input.identifier}}
        ]
        assert len(result.created) == 1
        assert result.created[0].identifier == existing_input.identifier

        # Test case: All inputs exist in the database
        await create_node(neo4j_tx, pending_input)
        result = await get_job_with_inputs(neo4j_tx, pending_job.maestro_id)
        assert not result.formatted_pending
        assert len(result.created) == 2
        assert {a.identifier for a in result.created} == {
            existing_input.identifier,
            pending_input.identifier,
        }
