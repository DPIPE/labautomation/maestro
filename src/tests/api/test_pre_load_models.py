import tempfile
from pathlib import Path

from maestro.models import BaseArtefactModel
from maestro.utils import get_recursive_subclasses


def test_pre_load_models():
    """Test that pre_load_models function loads models from files"""
    with (
        tempfile.NamedTemporaryFile(dir="/tmp", suffix=".py") as f1,
        tempfile.NamedTemporaryFile(dir="/tmp", suffix=".py") as f2,
        tempfile.NamedTemporaryFile(dir="/tmp", suffix=".py") as f3,
    ):
        f1.write(
            b"\n".join(
                [
                    b"from maestro import BaseArtefactModel",
                    b"class PreLoadedModel(BaseArtefactModel):",
                    b"    pass",
                ]
            )
        )

        f2.write(
            b"\n".join(
                [
                    b"from maestro import BaseArtefactModel",
                    b"class AnotherPreLoadedModel(BaseArtefactModel):",
                    b"    pass",
                ]
            )
        )

        f3.write(
            b"\n".join(
                [
                    b"from maestro import BaseArtefactModel",
                    b"raise RuntimeError('Side effect: Will not load model')",
                    b"class NotPreLoadedModel(BaseArtefactModel):",
                    b"    pass",
                ]
            )
        )

        f1.flush()
        f2.flush()
        f3.flush()
        from maestro.api.main import pre_load_models

        # Check that the models are not loaded before calling pre_load_models
        assert "PreLoadedModel" not in [
            x.__name__ for x in get_recursive_subclasses(BaseArtefactModel)
        ]
        assert "AnotherPreLoadedModel" not in [
            x.__name__ for x in get_recursive_subclasses(BaseArtefactModel)
        ]
        assert "NotPreLoadedModel" not in [
            x.__name__ for x in get_recursive_subclasses(BaseArtefactModel)
        ]

        # Load the models
        pre_load_models([Path(f1.name), Path(f2.name), Path(f3.name)])

        # Check that the models are loaded after calling pre_load_models
        assert "PreLoadedModel" in [x.__name__ for x in get_recursive_subclasses(BaseArtefactModel)]
        assert "AnotherPreLoadedModel" in [
            x.__name__ for x in get_recursive_subclasses(BaseArtefactModel)
        ]
        assert "NotPreLoadedModel" not in [
            x.__name__ for x in get_recursive_subclasses(BaseArtefactModel)
        ]
