import asyncio
import multiprocessing
import signal
from unittest.mock import patch
from uuid import uuid4

import pytest
from httpx import AsyncClient

from maestro import run_workflow_steps
from maestro.config import Config
from maestro.models import Job, JobStatus, WorkflowModel
from maestro.nats.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import merge_job_node, read_nodes
from maestro.run import shutdown_workflow_steps
from maestro.utils import subscribe_to_job_update
from maestro.worker_queue import WorkerQueue
from maestro.workflow_step.message_watcher import MessageWatcher
from maestro.workflow_utils import order_workflow

call_count = multiprocessing.Value("i", 0)
cancel_count = multiprocessing.Value("i", 0)
bash_returncode = multiprocessing.Value("i", 0)


@pytest.mark.asyncio
async def test_cancel_job_endpoint(test_api_app: AsyncClient):
    # try to cancel a job without any maestro_id
    response = await test_api_app.put("/api/v1/cancel/job")
    assert response.status_code == 404

    # try to cancel with maestro_id that doesn't correspond to any job
    response = await test_api_app.put("/api/v1/cancel/job/fcb3be4d-d603-40f1-9a28-49662ead9207")
    assert response.status_code == 400


class BlockingStepToBeCancelled(MessageWatcher):
    async def process(self, respond_to_sigterm: bool):
        if not respond_to_sigterm:
            asyncio.get_running_loop().remove_signal_handler(signal.SIGTERM)
        with call_count.get_lock():
            call_count.value += 1
        try:
            while True:
                await asyncio.sleep(0)
        except asyncio.CancelledError:
            with cancel_count.get_lock():
                cancel_count.value += 1
            raise
        finally:
            assert respond_to_sigterm


class BlockingSubprocessStepToBeCancelled(MessageWatcher):
    async def process(self):
        try:
            bash = await asyncio.create_subprocess_exec("bash", stdin=asyncio.subprocess.PIPE)
            assert bash.stdin
            bash.stdin.write(
                b"""\
trap 'echo "bash[$$]: Got SIGTERM."; exit 1' SIGTERM
while true; do
    echo "bash[$$]: Working on something that will take forever..."
    sleep 0.05
done
"""
            )
            await asyncio.wait_for(bash.stdin.drain(), 0.1)
            await asyncio.sleep(0.1)
            with call_count.get_lock():
                call_count.value += 1
            await bash.wait()
        except asyncio.CancelledError:
            with cancel_count.get_lock():
                cancel_count.value += 1
            bash.terminate()
            bash_returncode.value = await asyncio.wait_for(bash.wait(), 1.0)
            raise
        return []


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "step_cls, params",
    [
        (BlockingStepToBeCancelled, dict(respond_to_sigterm=True)),
        (BlockingStepToBeCancelled, dict(respond_to_sigterm=False)),
        (BlockingSubprocessStepToBeCancelled, {}),
    ],
)
async def test_cancel_running_job(step_cls, params, test_api_app: AsyncClient, monkeypatch):
    if step_cls is BlockingStepToBeCancelled and not params["respond_to_sigterm"]:
        monkeypatch.setattr(Config, "GRACE_PERIOD", 0.1)
    num_jobs = 2
    call_count.value = 0
    cancel_count.value = 0
    if step_cls is BlockingSubprocessStepToBeCancelled:
        bash_returncode.value = 0

    await run_workflow_steps(
        message_watcher_classes=[step_cls], artefact_classes=[], file_watcher_classes=[]
    )
    jobs = [Job(inputs={}, params=params, workflow_step=step_cls) for _ in range(num_jobs)]
    await order_workflow(WorkflowModel(workflow_name="test_cancel_running_job", jobs=jobs))

    async with asyncio.timeout(5):
        while call_count.value < num_jobs:
            await asyncio.sleep(0.1)
    assert call_count.value == num_jobs

    assert len(WorkerQueue.running_workers) == num_jobs

    async with asyncio.timeout(5):
        response = await test_api_app.put(f"/api/v1/cancel/job/{jobs[0].maestro_id}")
        assert response.status_code == 200

    async with asyncio.timeout(5):
        while len(WorkerQueue.running_workers) > 1:
            await asyncio.sleep(0.1)
    assert len(WorkerQueue.running_workers) == num_jobs - 1

    assert cancel_count.value == (
        1 if step_cls is BlockingSubprocessStepToBeCancelled or params["respond_to_sigterm"] else 0
    )

    if step_cls is BlockingSubprocessStepToBeCancelled:
        assert bash_returncode.value == 1
        bash_returncode.value = 0

    async with Neo4jTransaction() as neo4j_tx:
        db_jobs = await read_nodes(
            neo4j_tx, Job, {"maestro_id": jobs[0].maestro_id, "status": JobStatus.CANCELLED}
        )
        assert len(db_jobs) == 1
        db_jobs = await read_nodes(
            neo4j_tx, Job, {"maestro_id": jobs[1].maestro_id, "status": JobStatus.RUNNING}
        )
        assert len(db_jobs) == 1

    async with asyncio.timeout(5):
        response = await test_api_app.put(f"/api/v1/cancel/job/{jobs[1].maestro_id}")
        assert response.status_code == 200

    async with asyncio.timeout(5):
        while len(WorkerQueue.running_workers) > 0:
            await asyncio.sleep(0.1)
    assert len(WorkerQueue.running_workers) == 0

    if step_cls is BlockingSubprocessStepToBeCancelled or params["respond_to_sigterm"]:
        assert cancel_count.value == (
            num_jobs
            if step_cls is BlockingSubprocessStepToBeCancelled or params["respond_to_sigterm"]
            else 0
        )

    if step_cls is BlockingSubprocessStepToBeCancelled:
        assert bash_returncode.value == 1

    async with Neo4jTransaction() as neo4j_tx:
        db_jobs = await read_nodes(
            neo4j_tx, Job, {"maestro_id": jobs[0].maestro_id, "status": JobStatus.CANCELLED}
        )
        assert len(db_jobs) == 1
        db_jobs = await read_nodes(
            neo4j_tx, Job, {"maestro_id": jobs[1].maestro_id, "status": JobStatus.CANCELLED}
        )
        assert len(db_jobs) == 1


@pytest.mark.asyncio
async def test_cancel_pending_job(test_api_app: AsyncClient):
    # Test cancelling a non-existent job
    non_existent_job_id = uuid4()
    response = await test_api_app.put(f"/api/v1/cancel/job/{non_existent_job_id}")
    assert response.status_code == 400
    assert "Failed to cancel job, job not found" in response.text

    # Test cancelling pending jobs
    pending_job = Job(
        status=JobStatus.PENDING,
        # using existing class to avoid creating a new one
        # we do not need any functionality from this particular class
        workflow_step=BlockingStepToBeCancelled,
    )
    async with Neo4jTransaction() as neo4j_tx:
        await merge_job_node(neo4j_tx, pending_job)

    response = await test_api_app.put(f"/api/v1/cancel/job/{pending_job.maestro_id}")
    assert response.status_code == 200

    async with Neo4jTransaction() as neo4j_tx:
        db_cancelled_jobs = await read_nodes(
            neo4j_tx, Job, {"maestro_id": pending_job.maestro_id, "status": JobStatus.CANCELLED}
        )
        assert len(db_cancelled_jobs) == 1

    # Test cancelling already cancelled job
    response = await test_api_app.put(f"/api/v1/cancel/job/{db_cancelled_jobs[0].maestro_id}")
    assert response.status_code == 400
    assert (
        "Only job with status pending, running, queued or halted can be cancelled" in response.text
    )


@pytest.mark.asyncio
async def test_cancel_queued_job(test_api_app: AsyncClient):
    # Mock the _worker_can_run_now function to always return False,
    # ensuring the job stays in the queue
    with patch(
        "maestro.worker_queue._worker_can_run_now", return_value=False
    ) as _mock_worker_can_run_now:
        async with NATSSession() as nats_session:
            # Set up the workflow steps
            await run_workflow_steps(
                message_watcher_classes=[BlockingStepToBeCancelled],
                artefact_classes=[],
                file_watcher_classes=[],
            )
            queued = asyncio.Future()
            # Create a job to be queued and eventually cancelled
            queued_job = Job(
                inputs={},
                workflow_step=BlockingStepToBeCancelled,
            )
            # Subscribe to job updates to know when it's queued
            await subscribe_to_job_update(
                nats_session, queued_job.maestro_id, JobStatus.QUEUED, queued
            )
            # Order the workflow, which should queue our job
            await order_workflow(
                WorkflowModel(workflow_name="test_cancel_queued_job", jobs=[queued_job])
            )

            # Wait for the job to be queued (with a timeout)
            await asyncio.wait_for(queued, 1.0)

            # Attempt to cancel the queued job
            response = await test_api_app.put(f"/api/v1/cancel/job/{queued_job.maestro_id}")
            assert response.status_code == 200, "Job cancellation should be successful"

            async with asyncio.timeout(5):
                while len(WorkerQueue.queue) > 0:
                    await asyncio.sleep(0.1)

            # Shut down Maestro to ensure job cancellation has completed.
            with pytest.raises(asyncio.CancelledError):
                await asyncio.wait_for(shutdown_workflow_steps(), 5)

    # Verify that the job was actually cancelled in the database
    async with Neo4jTransaction() as neo4j_tx:
        db_cancelled_jobs = await read_nodes(
            neo4j_tx, Job, {"maestro_id": queued_job.maestro_id, "status": JobStatus.CANCELLED}
        )
    assert len(db_cancelled_jobs) == 1, "There should be exactly one cancelled job in the database"
