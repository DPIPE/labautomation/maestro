import pytest
from httpx import AsyncClient


@pytest.mark.asyncio
async def test_health_check(test_api_app: AsyncClient) -> None:
    res = await test_api_app.get("/api/v1/health/_check")
    assert res.status_code == 200
