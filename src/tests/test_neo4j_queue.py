import pytest
import pytest_asyncio

from maestro.models import Job, JobStatus
from maestro.neo4j import queue
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import merge_job_node
from tests.conftest import DummyWorkflowStep


@pytest_asyncio.fixture()
async def input_jobs(neo4j_tx: Neo4jTransaction) -> dict[JobStatus, Job]:
    input_jobs = {}
    for status in JobStatus:
        input_jobs[status] = Job(workflow_step=DummyWorkflowStep, status=status)
        await merge_job_node(neo4j_tx, input_jobs[status])
    return input_jobs


@pytest.mark.asyncio
async def test_get_pending_jobs(neo4j_tx: Neo4jTransaction, input_jobs: dict[JobStatus, Job]):
    pending_jobs = await queue.get_pending_jobs(neo4j_tx)
    assert pending_jobs == [input_jobs[JobStatus.PENDING]]


@pytest.mark.asyncio
async def test_get_all_jobs(neo4j_tx: Neo4jTransaction, input_jobs: dict[str, Job]):
    jobs = await queue.get_all_jobs(neo4j_tx)
    assert all(job in jobs for job in input_jobs.values())
    assert len(jobs) == len(input_jobs)
