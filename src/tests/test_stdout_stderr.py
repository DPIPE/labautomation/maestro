import asyncio
import logging
import sys

import pytest

from maestro import run_workflow_steps
from maestro.config import Config
from maestro.models import BaseArtefactModel, Job, StdErrModel, StdOutModel, WorkflowModel
from maestro.nats.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import read_nodes
from maestro.worker_queue import WorkerQueue
from maestro.workflow_step.message_watcher import MessageWatcher
from maestro.workflow_utils import order_workflow

MAESTRO_LOG_MSG = "logs to 'maestro' logger"
MAESTRO_WORKER_LOG_MSG = "logs to 'maestro-worker' logger"


class OutErrStep(MessageWatcher):
    async def process(self):
        # Add "binary" data (actually ISO-8859-1 encoded "ÆØÅ") to test decoding
        # resilience of the "stdout" pipe reader.
        sys.stdout.buffer.write(b"prints to stdout\n\xc6\xd8\xc5")
        print("prints to stderr", file=sys.stderr)
        logging.getLogger("maestro").info(MAESTRO_LOG_MSG)
        logging.getLogger("maestro-worker").info(MAESTRO_WORKER_LOG_MSG)
        dummy_artefact = BaseArtefactModel()
        return [dummy_artefact]


@pytest.mark.asyncio
async def test_stdout_stderr(log_recorder):
    "Run a single job, and check that stdout and stderr were persisted in the database"
    async with NATSSession() as nats_session:
        await run_workflow_steps(
            message_watcher_classes=[OutErrStep],
            artefact_classes=[BaseArtefactModel],
            file_watcher_classes=[],
        )
        done = asyncio.Future()

        async def on_done(msg):
            done.set_result(True)

        await nats_session.subscribe(Config.STREAM, "artefact.BaseArtefactModel", on_done)

        # Order job
        job = Job(inputs={}, params={}, workflow_step=OutErrStep)
        await order_workflow(
            WorkflowModel(
                workflow_name="test_workflow",
                jobs=[job],
            )
        )

        await asyncio.wait_for(done, 5)

    # Wait for the worker queue to shut down so that "stdout" and "stderr" have been
    # written to the database.
    WorkerQueue.shutdown_gracefully()
    await asyncio.wait_for(WorkerQueue.loop_task, 5)

    async with Neo4jTransaction() as neo4j_tx:
        # Test that the stdout and stderr were captured, and persisted in the database
        stdout_nodes = await read_nodes(neo4j_tx, StdOutModel, {})
        assert len(stdout_nodes) == 1
        # The binary data has now been escaed in the resulting Unicode string.
        assert stdout_nodes[0].output == "prints to stdout\n\\xc6\\xd8\\xc5"

        stderr_nodes = await read_nodes(neo4j_tx, StdErrModel, {})
        assert len(stderr_nodes) == 1
        assert "prints to stderr\n" in stderr_nodes[0].output
        assert MAESTRO_WORKER_LOG_MSG in stderr_nodes[0].output
        assert any(
            record.name == "maestro"
            and record.levelname == "INFO"
            and record.msg == MAESTRO_LOG_MSG
            for record in log_recorder.recording
        )
        assert all(record.msg != MAESTRO_WORKER_LOG_MSG for record in log_recorder.recording)
