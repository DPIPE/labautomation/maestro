import asyncio
from pathlib import Path
from typing import Any
from uuid import UUID

import pytest
from pydantic import UUID4, BaseModel, ValidationError
from setproctitle import getproctitle, getthreadtitle

from maestro.config import Config
from maestro.models import BaseArtefactModel, Job, JobStatus, WorkflowModel
from maestro.nats.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import create_node, read_node, read_nodes
from maestro.run import run_workflow_steps
from maestro.utils import subscribe_to_job_update
from maestro.workflow_step.message_watcher import MessageWatcher
from maestro.workflow_utils import order_workflow
from tests.conftest import (
    DummyArtefact,
    DummyFileWatcher,
    DummyWorkflowStep,
)


def test_input_validation():
    """
    Test that the Job model validates inputs correctly

    - Allows string input for artefact class
    - Allows artefact class input
    - Raises ValidationError if artefact class is not a subclass of BaseArtefactModel
    - Raises ValidationError if artefact class does not have the required properties

    """
    j1 = Job(
        workflow_step=DummyWorkflowStep,
        inputs={"input": (DummyArtefact, {"value": 1})},
    )

    j2 = Job(
        workflow_step="DummyWorkflowStep",
        inputs={"input": ("DummyArtefact", {"value": 1})},
    )

    assert j1.inputs == j2.inputs

    with pytest.raises(ValidationError):
        Job(
            workflow_step="DummyWorkflowStep",
            inputs={"input": ("NotAnArtefact", {"value": 1})},
        )

    with pytest.raises(ValidationError):
        Job(
            workflow_step="DummyWorkflowStep",
            inputs={"input": (DummyArtefact, {"not_some_property": 1})},
        )

    with pytest.raises(ValidationError):
        Job(
            workflow_step="DummyWorkflowStep",
            inputs={"input": (DummyArtefact, {"value": 1, "not_some_property": 1})},
        )


def test_input_validation_nested():
    """
    Test that we do not allow to filter inputs that are nested artefacts

    TODO: Add support for this in the future
    """

    class NestedArtefact(BaseArtefactModel):
        nested_value: DummyArtefact

    with pytest.raises(ValidationError, match="Property 'nested_value' is a nested field"):
        Job(
            workflow_step="DummyWorkflowStep",
            inputs={"input": (NestedArtefact, {"nested_value": {"value": 1}})},
        )

    with pytest.raises(ValidationError, match="Property 'nested_value' is a nested field"):
        Job(
            workflow_step="DummyWorkflowStep",
            inputs={"input": ("NestedArtefact", {"nested_value": DummyArtefact(value="1")})},
        )


def test_input_validation_multiple_props():
    "Test that the Job model validates multiple properties correctly - order should not matter"

    class MultiplePropsArtefact(BaseArtefactModel):
        value: int
        value2: int
        nested_value: DummyArtefact

    class MultiplePropsWorkflowStep(MessageWatcher):
        async def process(self, input: MultiplePropsArtefact):
            pass

    j1 = Job(
        workflow_step=MultiplePropsWorkflowStep,
        inputs={"input": (MultiplePropsArtefact, {"value": 1, "value2": 2})},
    )

    j2 = Job(
        workflow_step=MultiplePropsWorkflowStep,
        inputs={"input": (MultiplePropsArtefact, {"value2": 2, "value": 1})},
    )

    assert j1.inputs == j2.inputs

    # Test that we don't have to provide all properties
    Job(
        workflow_step=MultiplePropsWorkflowStep,
        inputs={"input": (MultiplePropsArtefact, {"value": 1})},
    )

    Job(
        workflow_step=MultiplePropsWorkflowStep,
        inputs={"input": (MultiplePropsArtefact, {"value2": 2})},
    )

    # Test that we can *not* provide nested artefacts
    with pytest.raises(ValidationError, match="Property 'nested_value' is a nested field"):
        Job(
            workflow_step=MultiplePropsWorkflowStep,
            inputs={
                "input": (
                    MultiplePropsArtefact,
                    {"value": 1, "nested_value": DummyArtefact(value="1")},
                )
            },
        )


def test_workflow_step_validation():
    """
    Test that the Job model validates the workflow_step correctly

    - Allows string input for workflow_step
    - Allows workflow_step class input
    - Raises ValidationError if workflow_step class is not a subclass of WorkflowStepBase

    """
    j1 = Job(
        workflow_step="DummyWorkflowStep",
        inputs={"input": (DummyArtefact, {"value": 1})},
    )

    j2 = Job(
        workflow_step=DummyWorkflowStep,
        inputs={"input": (DummyArtefact, {"value": 1})},
    )

    j3 = Job(
        workflow_step=DummyWorkflowStep,
    )

    assert j1.workflow_step == j2.workflow_step
    assert (
        j1.workflow_step_class
        == j2.workflow_step_class
        == j3.workflow_step_class
        == DummyWorkflowStep
    )

    # Raises ValidationError if workflow_step does not match a WorkflowStep class
    with pytest.raises(ValidationError, match="Workflow step 'NotAWorkflowStep' not found"):
        Job(
            workflow_step="NotAWorkflowStep",
            inputs={"input": (DummyArtefact, {"value": 1})},
        )

    with pytest.raises(
        ValidationError, match="Workflow step '<class 'tests.conftest.DummyArtefact'>' not found"
    ):
        Job(
            workflow_step=DummyArtefact,
            inputs={"input": (DummyArtefact, {"value": 1})},
        )

    with pytest.raises(
        ValidationError, match="Property 'non_existing_prop' not part of artefact class"
    ):
        Job(
            workflow_step=DummyWorkflowStep,
            inputs={"input": (DummyArtefact, {"non_existing_prop": 1})},
        )


def test_process_params_validation_single_required():
    """
    Create a simple process method with a single required parameter, and
    test that the Job model validates against the process method correctly
    """

    class WorkflowStepRequiredParam(MessageWatcher):
        async def process(self, input: DummyArtefact):
            pass

    # Successful validation
    Job(
        workflow_step=WorkflowStepRequiredParam,
        inputs={"input": (DummyArtefact, {"value": 1})},
    )

    # Input can also be passed as param
    Job(
        workflow_step=WorkflowStepRequiredParam,
        params={"input": DummyArtefact(value="1")},
    )

    # Test that additional params are not allowed
    with pytest.raises(
        ValidationError,
        match=r"Unknown arguments: \['not_allowed'\] for WorkflowStepRequiredParam.process",
    ):
        Job(
            workflow_step=WorkflowStepRequiredParam,
            inputs={"input": (DummyArtefact, {"value": 1})},
            params={"not_allowed": 1},
        )

    # Test that additional inputs are not allowed
    with pytest.raises(
        ValidationError,
        match=r"Unknown arguments: \['not_allowed'\] for WorkflowStepRequiredParam.process",
    ):
        Job(
            workflow_step=WorkflowStepRequiredParam,
            inputs={
                "input": (DummyArtefact, {"value": 1}),
                "not_allowed": (DummyArtefact, {"value": 2}),
            },
        )

    # Test that missing required inputs are not allowed
    with pytest.raises(
        ValidationError,
        match=r"Missing required arguments: \['input'\] for WorkflowStepRequiredParam.process",
    ):
        Job(
            workflow_step=WorkflowStepRequiredParam,
        )


def test_process_params_validation_single_optional():
    """
    Create a simple process method with a single optional parameter, and
    test that the Job model validates against the process method correctly
    """

    class WorkflowStepOptionalParam(MessageWatcher):
        async def process(self, input: DummyArtefact | None = None):
            pass

    # Successful validation
    Job(
        workflow_step=WorkflowStepOptionalParam,
        inputs={"input": (DummyArtefact, {"value": 1})},
    )

    # This should also validate, since input is optional
    Job(
        workflow_step=WorkflowStepOptionalParam,
    )

    # Test that additional params are not allowed
    with pytest.raises(
        ValidationError,
        match=r"Unknown arguments: \['not_allowed'\] for WorkflowStepOptionalParam.process",
    ):
        Job(
            workflow_step=WorkflowStepOptionalParam,
            inputs={"input": (DummyArtefact, {"value": 1})},
            params={"not_allowed": 1},
        )

    # Test that additional inputs are not allowed
    with pytest.raises(
        ValidationError,
        match=r"Unknown arguments: \['not_allowed'\] for WorkflowStepOptionalParam.process",
    ):
        Job(
            workflow_step=WorkflowStepOptionalParam,
            inputs={
                "input": (DummyArtefact, {"value": 1}),
                "not_allowed": (DummyArtefact, {"value": 2}),
            },
        )


def test_process_params_validation_single_required_with_kwargs():
    """
    Create a simple process method with a single required parameter and **kwargs, and
    test that the Job model validates against the process method correctly
    """

    class WorkflowStepWithKwargs(MessageWatcher):
        async def process(self, input: DummyArtefact, **kwargs):
            pass

    # Successful validation
    Job(
        workflow_step=WorkflowStepWithKwargs,
        inputs={"input": (DummyArtefact, {"value": 1})},
    )

    # Test that additional params are allowed
    Job(
        workflow_step=WorkflowStepWithKwargs,
        inputs={"input": (DummyArtefact, {"value": 1})},
        params={"allowed": 1},
    )

    # Test that additional inputs are not allowed
    Job(
        workflow_step=WorkflowStepWithKwargs,
        inputs={
            "input": (DummyArtefact, {"value": 1}),
            "allowed": (DummyArtefact, {"value": 2}),
        },
    )

    # Test that missing required inputs are not allowed
    with pytest.raises(
        ValidationError,
        match=r"Missing required arguments: \['input'\] for WorkflowStepWithKwargs.process",
    ):
        Job(
            workflow_step=WorkflowStepWithKwargs,
        )


def test_process_params_validation_mixed():
    """
    Test that the Job model validates against a process method with a mix of required and optional parameters
    """

    class SecondDummyArtefact(BaseArtefactModel):
        value: int

    class WorkflowStepMixed(MessageWatcher):
        async def process(
            self,
            required_artefact: DummyArtefact,
            required_arg: int,
            optional_artefact: SecondDummyArtefact | None = None,
            optional_arg: int = 1,
            **kwargs,
        ):
            pass

    # Successful validation
    Job(
        workflow_step=WorkflowStepMixed,
        inputs={
            "required_artefact": (DummyArtefact, {"value": 1}),
        },
        params={
            "required_arg": 1,
        },
    )

    # Successful validation with optional artefact as input
    Job(
        workflow_step=WorkflowStepMixed,
        inputs={
            "required_artefact": (DummyArtefact, {"value": 1}),
            "optional_artefact": (SecondDummyArtefact, {"value": 2}),
        },
        params={
            "required_arg": 1,
        },
    )

    # Successful validation with optional arg as param
    Job(
        workflow_step=WorkflowStepMixed,
        inputs={
            "required_artefact": (DummyArtefact, {"value": 1}),
        },
        params={
            "required_arg": 1,
            "optional_arg": 2,
        },
    )

    # Successful validation with optional artefact as param
    Job(
        workflow_step=WorkflowStepMixed,
        inputs={
            "required_artefact": (DummyArtefact, {"value": 1}),
        },
        params={
            "required_arg": 1,
            "optional_artefact": SecondDummyArtefact(value=2),
        },
    )

    # Successful validation with all artefacts and args as params
    Job(
        workflow_step=WorkflowStepMixed,
        inputs={},
        params={
            "required_artefact": DummyArtefact(value="1"),
            "required_arg": 1,
            "optional_artefact": SecondDummyArtefact(value=2),
            "optional_arg": 2,
        },
    )

    # Test that additional params are allowed
    Job(
        workflow_step=WorkflowStepMixed,
        inputs={
            "required_artefact": (DummyArtefact, {"value": 1}),
        },
        params={
            "required_arg": 1,
            "allowed": 1,
        },
    )

    # Unsuccessful validation - missing required arg
    with pytest.raises(
        ValidationError,
        match=r"Missing required arguments: \['required_arg'\] for WorkflowStepMixed.process",
    ):
        Job(
            workflow_step=WorkflowStepMixed,
            inputs={
                "required_artefact": (DummyArtefact, {"value": 1}),
            },
        )

    # Unsuccessful validation - missing required artefact
    with pytest.raises(
        ValidationError,
        match=r"Missing required arguments: \['required_artefact'\] for WorkflowStepMixed.process",
    ):
        Job(
            workflow_step=WorkflowStepMixed,
            params={
                "required_arg": 1,
            },
        )

    # Unsuccessful validation - missing required artefact and arg
    with pytest.raises(
        ValidationError,
        match=r"Missing required arguments: \['required_arg', 'required_artefact'\] for WorkflowStepMixed.process",
    ):
        Job(
            workflow_step=WorkflowStepMixed,
        )

    # Unsuccessful validation - missing required artefact and arg
    with pytest.raises(
        ValidationError,
        match=r"Missing required arguments: \['required_arg', 'required_artefact'\] for WorkflowStepMixed.process",
    ):
        Job(
            workflow_step=WorkflowStepMixed,
            params={
                "optional_artefact": SecondDummyArtefact(value=2),
                "optional_arg": 2,
            },
        )


def test_process_params_validation_inheritance():
    """
    Test that subclasses of a required artefact can be passed as inputs
    Test that superclasses of a required artefact cannot be passed as inputs
    """

    class WorkflowStepWithSubclass(MessageWatcher):
        async def process(self, input: DummyArtefact):
            pass

    class DummyArtefactSubclass(DummyArtefact): ...

    # Successful validation
    Job(
        workflow_step=WorkflowStepWithSubclass,
        inputs={"input": (DummyArtefactSubclass, {"value": 1})},
    )

    # Raises ValidationError if input is not a subclass of the required artefact
    with pytest.raises(
        ValidationError,
        match="Artefact class '<class 'maestro.models.BaseArtefactModel'>' is not valid argument for input",
    ):
        Job(
            workflow_step=WorkflowStepWithSubclass,
            inputs={"input": (BaseArtefactModel, {})},
        )


def test_process_params_duplicated_keys():
    """
    Test that the Job model raises a ValidationError if the same key is used for both an input and a param
    """

    class WorkflowStepDuplicate(MessageWatcher):
        async def process(self, input: DummyArtefact):
            pass

    with pytest.raises(
        ValidationError,
        match=r"Keys duplicated in both inputs and params: \['input'\]",
    ):
        Job(
            workflow_step=WorkflowStepDuplicate,
            inputs={"input": (DummyArtefact, {"value": 1})},
            params={"input": DummyArtefact(value="1")},
        )


@pytest.mark.asyncio
async def test_process_params_coercion(neo4j_tx: Neo4jTransaction):
    # Test that the Job model can coerce the input parameters to the correct type
    # The correct type is defined by the process method signature

    class SomePydanticModel(BaseModel):
        a: str
        b: int

    class CoercingWorkflowStep(MessageWatcher):
        async def process(
            self,
            uuid: UUID4,
            uuid_native: UUID,
            integer: int,
            model: SomePydanticModel,
            any: Any,
            not_coerced: Any,
        ):
            pass

    uuid = "583321ed-3874-479a-8a95-2eece18210fe"
    job = Job(
        workflow_step=CoercingWorkflowStep,
        params={
            "uuid": uuid,  # Pass a string
            "uuid_native": uuid,  # Pass a string
            "integer": "1",
            "model": {"a": "abc", "b": "45"},
            "any": {"a": "abc", "b": "45"},
            "not_coerced": UUID4(uuid),
        },
    )

    # Ensure that coercion happens in validators
    assert job.params["uuid"] == UUID4(uuid)  # Coerced to UUID4
    assert job.params["uuid_native"] == UUID(uuid)  # Coerced to UUID
    assert job.params["integer"] == 1  # Coerced to int
    assert job.params["model"] == SomePydanticModel(a="abc", b=45)  # Coerced to SomePydanticModel
    assert job.params["any"] == {"a": "abc", "b": "45"}  # Not coerced
    assert job.params["not_coerced"] == UUID4(uuid)

    # Ensure that we can create the node in the database, and that coercion happens when reading the node back
    await create_node(neo4j_tx, job)
    db_job = await read_node(neo4j_tx, Job, {"maestro_id": job.maestro_id})
    assert db_job
    assert db_job.params["uuid"] == UUID4(uuid)
    assert db_job.params["uuid_native"] == UUID(uuid)
    assert db_job.params["integer"] == 1
    assert db_job.params["model"] == SomePydanticModel(a="abc", b=45)
    assert db_job.params["any"] == {"a": "abc", "b": "45"}

    # Since we have no concept of what the type is from the signature, we're
    # unable to coerce the type to the original type
    assert db_job.params["not_coerced"] == uuid  # <-- Was originally a UUID4, but is now a string


@pytest.mark.asyncio
async def test_jobs_different_instance_id(monkeypatch):
    """Test that jobs are never queued and completed when their instance_id are different from
    this Maestro instance."""

    # Create a job with a different instance_id, should not be queued and completed
    identifier = "input_file"
    # Store original instance ID and create a different one for testing
    original_instance_id = Config.INSTANCE_ID
    other_instance_id = f"{original_instance_id}_other"

    # Temporarily change instance ID for this test
    monkeypatch.setattr(Config, "INSTANCE_ID", other_instance_id)

    other_instance_job = Job(
        instance_id=other_instance_id,
        workflow_step=DummyWorkflowStep,
        inputs={"input": (DummyArtefact, {"identifier": identifier})},
    )
    workflow = WorkflowModel(
        workflow_name="other_instance_workflow",
        jobs=[other_instance_job],
    )

    await run_workflow_steps(file_watcher_classes=[DummyFileWatcher])
    await order_workflow(workflow)
    # Wait for the workflow job to exist in the database.
    async with asyncio.timeout(5):
        while True:
            async with Neo4jTransaction() as neo4j_tx:
                db_jobs = await read_nodes(
                    neo4j_tx,
                    Job,
                    {"workflow_step": DummyWorkflowStep.__name__, "instance_id": other_instance_id},
                )
                if db_jobs:
                    break
    assert len(db_jobs) == 1
    assert db_jobs[0].status == JobStatus.PENDING
    assert db_jobs[0].revision == 1
    Path(f"/tmp/tests/{identifier}").touch()

    # Restore original instance ID for this test
    monkeypatch.setattr(Config, "INSTANCE_ID", original_instance_id)

    # Create a job with default instance_id, should be queued and completed
    completed_future = asyncio.Future()
    this_instance_job = Job(
        workflow_step=DummyWorkflowStep,
        inputs={"input": (DummyArtefact, {"identifier": identifier})},
    )
    workflow = WorkflowModel(
        workflow_name="this_instance_workflow",
        jobs=[this_instance_job],
    )

    # Wait for the job to be completed
    async with NATSSession() as nats_session:
        await subscribe_to_job_update(
            nats_session, this_instance_job.maestro_id, JobStatus.COMPLETED, completed_future
        )
        await order_workflow(workflow)
        await asyncio.wait_for(completed_future, 10)

    async with Neo4jTransaction() as neo4j_tx:
        db_jobs = await read_nodes(
            neo4j_tx,
            Job,
            {"workflow_step": DummyWorkflowStep.__name__, "instance_id": Config.INSTANCE_ID},
        )
        assert len(db_jobs) == 1
        assert db_jobs[0].status == JobStatus.COMPLETED
        assert db_jobs[0].revision == 4  # pending -> queued -> running -> completed

    # Since its subsquently defined job is completed, we can safely assume
    # that the initial job has not been (and will not be) queued
    async with Neo4jTransaction() as neo4j_tx:
        db_jobs = await read_nodes(
            neo4j_tx,
            Job,
            {"workflow_step": DummyWorkflowStep.__name__, "instance_id": other_instance_id},
        )
        assert len(db_jobs) == 1
        assert db_jobs[0].status == JobStatus.PENDING
        assert db_jobs[0].revision == 1


@pytest.mark.asyncio
async def test_process_and_thread_names():
    class ProcessAndThreadNameFetcherOutput(BaseArtefactModel):
        process_name: str
        thread_name: str

    class ProcessAndThreadNameFetcher(MessageWatcher):
        async def process(self):
            return [
                ProcessAndThreadNameFetcherOutput(
                    process_name=getproctitle(), thread_name=getthreadtitle()
                )
            ]

    await run_workflow_steps(
        message_watcher_classes=[ProcessAndThreadNameFetcher],
        artefact_classes=[ProcessAndThreadNameFetcherOutput],
        file_watcher_classes=[],
    )

    job = Job(
        workflow_step=ProcessAndThreadNameFetcher,
        display_name="ProcessAndThreadNameFetcher:ExtraInformation",
    )

    async with NATSSession() as nats_session:
        completed_future = asyncio.Future()
        await subscribe_to_job_update(
            nats_session, job.maestro_id, JobStatus.COMPLETED, completed_future
        )
        await order_workflow(WorkflowModel(workflow_name="workflow", jobs=[job]))
        await asyncio.wait_for(completed_future, 5.0)

    async with Neo4jTransaction() as neo4j_tx:
        output = await read_node(
            neo4j_tx,
            ProcessAndThreadNameFetcherOutput,
            {},
        )
        assert output.process_name == f"[{job.name_and_id}]"
        assert output.thread_name == f"[{str(job.maestro_id)[:13]}]"
