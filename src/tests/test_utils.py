import asyncio
from pathlib import Path
from typing import Any

import pytest

from maestro import run_workflow_steps
from maestro.models import Job, JobStatus, WorkflowModel, WorkflowStepModel
from maestro.nats.nats import NATSSession
from maestro.neo4j.invalidate import invalidate_workflow_steps
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import create_node, read_node, read_nodes
from maestro.publishers import publish_cancel
from maestro.utils import (
    get_optional_args,
    get_recursive_subclasses,
    get_required_args,
    has_kwargs,
    subscribe_to_job_update,
)
from maestro.workflow_step.message_watcher import MessageWatcher
from maestro.workflow_utils import cancel_pending_job, order_workflow, reorder_job
from tests.conftest import (
    DummyArtefact,
    DummyFileWatcher,
    DummyWorkflowStep,
)


class FailingWorkflowStep(MessageWatcher):
    async def process(cls, input: Any = None, **kwargs):
        raise ValueError("Fail for reorder test")


class EternalWorkflowStep(MessageWatcher):
    async def process(cls, input: Any = None, **kwargs):
        await asyncio.sleep(float("inf"))


@pytest.mark.asyncio
async def test_reorder_invalidated_workflow_step():
    """
    Test reordering an invalidated workflow step.

    This function creates two Job instances with a DummyWorkflowStep type, sets up a WorkflowModel with these jobs,
    runs the workflow steps, orders the workflow, touches a file, and then attempts to reorder the workflow step.

    Also tests when maestro_id not corresponding to a Workflow Step is used to reun a workflow
    and trying to reorder a Non-invalidated workflow step or workflow step with job that is neither failed or cancelled
    Raises:
    AssertionError: If the reorder fails and the workflow step is not found
    ValueError: If the reorder fails due to invalidation or job status

    """

    input_id = "input1"
    output_id = "output_1_1"
    completed_future = asyncio.Future()
    job_1 = Job(
        workflow_step=DummyWorkflowStep,
        inputs={"input": (DummyArtefact, {"identifier": input_id})},
        params={"outputs": [output_id, "output_1_2"]},
    )
    job_2 = Job(
        workflow_step=DummyWorkflowStep,
        inputs={"input": (DummyArtefact, {"identifier": output_id})},
        params={"outputs": ["output_2_1"]},
    )

    workflow = WorkflowModel(
        workflow_name="test_reorder_1",
        jobs=[job_1, job_2],
    )

    await run_workflow_steps(
        artefact_classes=[DummyArtefact],
        message_watcher_classes=[DummyWorkflowStep],
        file_watcher_classes=[DummyFileWatcher],
    )

    # Order workflow and wait for completion
    async with NATSSession() as nats_session:
        await subscribe_to_job_update(
            nats_session, job_2.maestro_id, JobStatus.COMPLETED, completed_future
        )
        await order_workflow(workflow)
        Path(f"/tmp/tests/{input_id}").touch()
        await asyncio.wait_for(completed_future, timeout=3.0)

    job_maestro_id = job_1.maestro_id
    async with Neo4jTransaction() as neo4j_tx:
        db_workflow_step = await neo4j_tx.query(
            """
            MATCH (j:Job {maestro_id: $job_maestro_id})-[:WORKFLOW_STEP]->(ws:WorkflowStepModel)
            RETURN ws.maestro_id as maestro_id
            """,
            {"job_maestro_id": job_maestro_id},
        )
        workflow_step_maestro_id = db_workflow_step[0]["maestro_id"]

    with pytest.raises(
        ValueError,
        match="To reorder a job either job must have status cancelled or failed or connected workflow step must be invalidated",
    ):
        await reorder_job(job_maestro_id)

    async with Neo4jTransaction() as neo4j_tx:
        await invalidate_workflow_steps(neo4j_tx, [workflow_step_maestro_id])

        db_invalidated_workflow_step = await read_node(
            neo4j_tx, WorkflowStepModel, {"maestro_id": workflow_step_maestro_id}
        )
        assert db_invalidated_workflow_step
        assert db_invalidated_workflow_step.invalidated_at is not None
        db_jobs = await read_nodes(neo4j_tx, Job, {})
        # 2 jobs for DummyWorkflowStep and 1 for DummyFileWatcher
        assert len(db_jobs) == 3

    # try reorder after workflow step has been invalidated
    await reorder_job(job_maestro_id)
    await asyncio.sleep(1)
    async with Neo4jTransaction() as neo4j_tx:
        db_jobs = await read_nodes(neo4j_tx, Job, {})
        # 2 old and 1 new job for DummyWorkflowStep and 1 for DummyFileWatcher
        assert len(db_jobs) == 4


@pytest.mark.asyncio
async def test_reorder_failed_workflow_step():
    """
    Test the reorder of a failed workflow step by creating a job with a failing workflow step,
    ordering the workflow and then reordering the failed workflow step.
    """
    failed_future = asyncio.Future()
    input_id = "input1"
    job = Job(
        workflow_step=FailingWorkflowStep,
        inputs={"input": (DummyArtefact, {"identifier": input_id})},
        params={"outputs": ["output_1_1"]},
    )

    workflow = WorkflowModel(
        workflow_name="test_failed_reorder_1",
        jobs=[job],
    )

    await run_workflow_steps(
        artefact_classes=[DummyArtefact],
        message_watcher_classes=[FailingWorkflowStep],
        file_watcher_classes=[DummyFileWatcher],
    )

    # Order workflow and wait for job to fail
    async with NATSSession() as nats_session:
        await subscribe_to_job_update(nats_session, job.maestro_id, JobStatus.FAILED, failed_future)
        await order_workflow(workflow)
        Path(f"/tmp/tests/{input_id}").touch()
        await asyncio.wait_for(failed_future, timeout=3.0)

    async with Neo4jTransaction() as neo4j_tx:
        db_jobs = await read_nodes(neo4j_tx, Job, {})
        # 2 jobs for DummyWorkflowStep and 1 for DummyFileWatcher
        assert len(db_jobs) == 2
        db_failed_jobs = await read_nodes(neo4j_tx, Job, {"status": JobStatus.FAILED})
        assert len(db_failed_jobs) == 1

    await reorder_job(db_failed_jobs[0].maestro_id)
    await asyncio.sleep(1)
    async with Neo4jTransaction() as neo4j_tx:
        db_jobs = await read_nodes(neo4j_tx, Job, {})
        assert len(db_jobs) == 3
        db_failed_jobs = await read_nodes(neo4j_tx, Job, {"status": JobStatus.FAILED})
        assert len(db_failed_jobs) == 2


@pytest.mark.asyncio
async def test_reorder_cancelled_job_without_workflow_step():
    """
    Test reordering a cancelled job that does not have a workflow step
    """
    pending_future = asyncio.Future()
    cancelled_future = asyncio.Future()
    input_id = "input1"
    job = Job(
        workflow_step=EternalWorkflowStep,
        inputs={"input": (DummyArtefact, {"identifier": input_id})},
        params={"outputs": ["output_1_1"]},
    )

    workflow = WorkflowModel(
        workflow_name="test_failed_reorder_1",
        jobs=[job],
    )

    await run_workflow_steps(
        artefact_classes=[DummyArtefact],
        message_watcher_classes=[EternalWorkflowStep],
        file_watcher_classes=[DummyFileWatcher],
    )

    # Order workflow, but cancel before it gets started
    async with NATSSession() as nats_session:
        await subscribe_to_job_update(
            nats_session, job.maestro_id, JobStatus.PENDING, pending_future
        )
        await subscribe_to_job_update(
            nats_session, job.maestro_id, JobStatus.CANCELLED, cancelled_future
        )

        await order_workflow(workflow)
        await asyncio.wait_for(pending_future, timeout=3.0)
        await cancel_pending_job(job.maestro_id)
        await asyncio.wait_for(cancelled_future, timeout=3.0)

    await reorder_job(job.maestro_id)


@pytest.mark.asyncio
async def test_reorder_cancelled_job_with_workflow_step():
    running_future = asyncio.Future()
    cancelled_future = asyncio.Future()
    input_id = "input1"
    job = Job(
        workflow_step=EternalWorkflowStep,
        inputs={"input": (DummyArtefact, {"identifier": input_id})},
        params={"outputs": ["output_1_1"]},
    )

    workflow = WorkflowModel(
        workflow_name="test_failed_reorder_1",
        jobs=[job],
    )

    await run_workflow_steps(
        artefact_classes=[DummyArtefact],
        message_watcher_classes=[EternalWorkflowStep],
        file_watcher_classes=[DummyFileWatcher],
    )

    # Order workflow, but cancel after it gets started
    async with NATSSession() as nats_session:
        await subscribe_to_job_update(
            nats_session, job.maestro_id, JobStatus.RUNNING, running_future
        )
        await subscribe_to_job_update(
            nats_session, job.maestro_id, JobStatus.CANCELLED, cancelled_future
        )

        await order_workflow(workflow)
        Path(f"/tmp/tests/{input_id}").touch()
        await asyncio.wait_for(running_future, timeout=3.0)
        await publish_cancel(job.maestro_id)
        await asyncio.wait_for(cancelled_future, timeout=3.0)

    await reorder_job(job.maestro_id)


@pytest.mark.asyncio
async def test_cancel_pending_job():
    """
    Test cancellation of a pending job identified with job maestro_id
    check function throws error when expected
    """
    pending_future = asyncio.Future()
    job = Job(
        workflow_step=FailingWorkflowStep,
        inputs={"input": (DummyArtefact, {})},
        params={"outputs": ["output_1_1"]},
    )

    workflow = WorkflowModel(
        workflow_name="test_cancel_pending_job",
        jobs=[job],
    )

    await run_workflow_steps(
        artefact_classes=[DummyArtefact],
        message_watcher_classes=[FailingWorkflowStep],
        file_watcher_classes=[DummyFileWatcher],
    )

    async with NATSSession() as nats_session:
        await subscribe_to_job_update(
            nats_session, job.maestro_id, JobStatus.PENDING, pending_future
        )
        await order_workflow(workflow)
        await asyncio.wait_for(pending_future, timeout=3.0)

    job_maestro_id = job.maestro_id
    async with Neo4jTransaction() as neo4j_tx:
        db_jobs = await read_nodes(neo4j_tx, Job, {})
        # 1 DummyWorkflowStep job
        assert len(db_jobs) == 1
        db_cancelled_jobs = await read_nodes(neo4j_tx, Job, {"status": JobStatus.CANCELLED})
        assert len(db_cancelled_jobs) == 0

    await cancel_pending_job(job_maestro_id)
    await asyncio.sleep(1)
    async with Neo4jTransaction() as neo4j_tx:
        db_jobs = await read_nodes(neo4j_tx, Job, {})
        assert len(db_jobs) == 1
        db_cancelled_jobs = await read_nodes(neo4j_tx, Job, {"status": JobStatus.CANCELLED})
        assert len(db_cancelled_jobs) == 1
        assert db_cancelled_jobs[0].maestro_id == job_maestro_id


def test_get_recursive_subclasses():
    "Test get_recursive_subclasses function"

    class A:
        pass

    class B(A):
        pass

    class C(B):
        pass

    class D:
        pass

    class E(A, D):
        pass

    assert get_recursive_subclasses(A) == [B, C, E]
    assert get_recursive_subclasses(B) == [C]
    assert get_recursive_subclasses(C) == []
    assert get_recursive_subclasses(D) == [E]
    assert get_recursive_subclasses(E) == []


def test_get_required_args():
    "Test get_required_args function"

    assert tuple(get_required_args(lambda x: ...)) == (("x", Any),)
    assert tuple(get_required_args(lambda x, y: ...)) == (("x", Any), ("y", Any))
    assert tuple(get_required_args(lambda x, y=1: ...)) == (("x", Any),)
    assert tuple(get_required_args(lambda x, y=1, z=2: ...)) == (("x", Any),)
    assert tuple(get_required_args(lambda x, y=1, z=2, **kwargs: ...)) == (("x", Any),)

    def func(a: int, b: int | str, c=1, d=2):
        pass

    assert tuple(get_required_args(func)) == (("a", int), ("b", int | str))

    def func(a: int, b: int | str, c=1, d=2, **kwargs):
        pass

    assert tuple(get_required_args(func)) == (("a", int), ("b", int | str))


def test_get_optional_args():
    "Test get_optional_args function"

    assert dict(get_optional_args(lambda x: ...)) == {}
    assert dict(get_optional_args(lambda x, y: ...)) == {}
    assert dict(get_optional_args(lambda x, y=1: ...)) == {"y": Any}
    assert dict(get_optional_args(lambda x, y=1, z=2: ...)) == {"y": Any, "z": Any}
    assert dict(get_optional_args(lambda x, y=1, z=2, **kwargs: ...)) == {"y": Any, "z": Any}

    def func(a: int, b: int | str, c: int = 1, d: int | str = 2):
        pass

    assert dict(get_optional_args(func)) == {"c": int, "d": int | str}

    def func(a: int, b: int | str, c: int = 1, d: int | str = 2, **kwargs):
        pass

    assert dict(get_optional_args(func)) == {"c": int, "d": int | str}


def test_has_kwargs():
    "Test has_kwargs function"

    assert has_kwargs(lambda x: ...) is False
    assert has_kwargs(lambda x, y: ...) is False
    assert has_kwargs(lambda x, y=1: ...) is False
    assert has_kwargs(lambda x, y=1, z=2: ...) is False
    assert has_kwargs(lambda x, y=1, z=2, **kwargs: ...) is True

    def func(a: int, b: int | str, c=1, d=2):
        pass

    assert has_kwargs(func) is False

    def func(a: int, b: int | str, c=1, d=2, **kwargs):
        pass

    assert has_kwargs(func) is True


@pytest.mark.asyncio
async def test_allow_adding_new_jobs_to_existing_workflow():
    """Ensure that new jobs can be added to an existing workflow and that existing jobs are not
    reordered."""
    async with NATSSession() as nats_session:
        partly_completed = asyncio.Future()
        completed = asyncio.Future()

        await run_workflow_steps(file_watcher_classes=[])

        input_id = "input_1"
        output_id_1 = "output_1"
        output_id_2 = "output_2"

        # Original jobs
        job_1 = Job(
            workflow_step=DummyWorkflowStep,
            inputs={"input": (DummyArtefact, {"identifier": input_id})},
            params={"outputs": [output_id_1]},
            priority=1,
        )
        job_2 = Job(
            workflow_step=DummyWorkflowStep,
            inputs={"input": (DummyArtefact, {"identifier": output_id_1})},
            params={"outputs": [output_id_2]},
            priority=2,
        )

        # New job added later
        new_job = Job(
            workflow_step=DummyWorkflowStep,
            inputs={"input": (DummyArtefact, {"identifier": output_id_2})},
            priority=3,
        )

        workflow = WorkflowModel(
            workflow_name="reorder_workflow",
            jobs=[
                job_1,
                job_2,
            ],
        )

        async with Neo4jTransaction() as neo4j_tx:
            await create_node(neo4j_tx, DummyArtefact(identifier=input_id))

        await subscribe_to_job_update(
            nats_session, job_2.maestro_id, JobStatus.COMPLETED, partly_completed
        )
        await subscribe_to_job_update(
            nats_session, new_job.maestro_id, JobStatus.COMPLETED, completed
        )

        await order_workflow(workflow)

        await asyncio.wait_for(partly_completed, timeout=5.0)

        async with Neo4jTransaction() as neo4j_tx:
            db_workflow = await read_node(
                neo4j_tx, WorkflowModel, {"maestro_id": workflow.maestro_id}
            )
            assert db_workflow and len(db_workflow.jobs) == 2, "Expected 2 jobs in workflow"

        # Add new job to workflow
        workflow.jobs.add(new_job)

        # Reorder workflow. This should not update revision of existing jobs,
        # as they are removed from workflow_order.jobs in on_order_received
        await order_workflow(workflow)

        await asyncio.wait_for(completed, timeout=5.0)

        async with Neo4jTransaction() as neo4j_tx:
            updated_db_workflow = await read_node(
                neo4j_tx, WorkflowModel, {"maestro_id": db_workflow.maestro_id}
            )
            assert len(updated_db_workflow.jobs) == 3, "Expected 3 jobs in updated workflow"

            assert db_workflow.jobs.issubset(
                updated_db_workflow.jobs
            ), "Initial jobs should be in updated workflow jobs"
