import asyncio

import pytest

from maestro import run_workflow_steps
from maestro.models import Job, JobStatus, StdErrModel, StdOutModel, WorkflowModel
from maestro.nats.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import read_nodes
from maestro.utils import subscribe_to_job_update
from maestro.worker_queue import WorkerQueue
from maestro.workflow_step.message_watcher import MessageWatcher
from maestro.workflow_utils import order_workflow


class FailingStep(MessageWatcher):
    async def process(cls, **kwargs):
        raise Exception("This is a test exception")


class RunningTimeoutStep(MessageWatcher):
    async def process(cls, **kwargs):
        await asyncio.sleep(10)


@pytest.mark.asyncio
async def test_failing_job_in_queue():
    "Test that a failing job moves from pending -> running -> failed in the queue"
    async with NATSSession() as nats_session:
        await run_workflow_steps(
            artefact_classes=[],
            message_watcher_classes=[FailingStep],
            file_watcher_classes=[],
        )

        # Subscribe to job updates
        running = asyncio.Future()
        failed = asyncio.Future()

        job = Job(inputs={}, params={}, workflow_step=FailingStep)
        await subscribe_to_job_update(nats_session, job.maestro_id, JobStatus.RUNNING, running)
        await subscribe_to_job_update(nats_session, job.maestro_id, JobStatus.FAILED, failed)

        # Submit a job
        await order_workflow(
            WorkflowModel(
                workflow_name="test_workflow",
                jobs=[job],
            )
        )

        # Job list should move from running -> failed
        await asyncio.wait_for(running, 5)
        await asyncio.wait_for(failed, 5)

        async with Neo4jTransaction() as neo4j_tx:
            # Test that job list is correct
            jobs = await read_nodes(neo4j_tx, Job, {"workflow_step": FailingStep.__name__})

        assert len(jobs) == 1
        assert jobs[0].status == JobStatus.FAILED
        assert jobs[0].maestro_id == job.maestro_id

        # Ensure that the worker queue is flushed
        WorkerQueue.shutdown_gracefully()
        await asyncio.wait_for(WorkerQueue.loop_task, 5)

    async with Neo4jTransaction() as neo4j_tx:
        # Test that the stdout and stderr were captured, and persisted in the database
        stdout_nodes = await read_nodes(neo4j_tx, StdOutModel, {})
        assert len(stdout_nodes) == 1
        assert stdout_nodes[0].output == ""

        stderr_nodes = await read_nodes(neo4j_tx, StdErrModel, {})
        assert len(stderr_nodes) == 1
        assert "Exception: This is a test exception" in stderr_nodes[0].output

        job_nodes = await read_nodes(neo4j_tx, Job, {})
        assert len(job_nodes) == 1
        assert job_nodes[0].status == JobStatus.FAILED


@pytest.mark.asyncio
async def test_timeout_job():
    "Test that a job taking more time than expected retuns timeout error"
    async with NATSSession() as nats_session:
        await run_workflow_steps(
            artefact_classes=[],
            message_watcher_classes=[RunningTimeoutStep],
            file_watcher_classes=[],
        )

        # Subscribe to job updates
        running = asyncio.Future()
        failed = asyncio.Future()

        # setup timeout job
        job = Job(inputs={}, params={}, workflow_step=RunningTimeoutStep, running_timeout=2)

        # subscribe to job update, we want to know when job has started running
        await subscribe_to_job_update(nats_session, job.maestro_id, JobStatus.RUNNING, running)
        await subscribe_to_job_update(nats_session, job.maestro_id, JobStatus.FAILED, failed)

        # Submit a job
        await order_workflow(
            WorkflowModel(
                workflow_name="test_workflow",
                jobs=[job],
            )
        )

        await asyncio.wait_for(running, 1.0)
        await asyncio.wait_for(failed, job.running_timeout + 1.0)

        # Ensure that the worker queue is flushed
        WorkerQueue.shutdown_gracefully()
        await asyncio.wait_for(WorkerQueue.loop_task, 5)

    # check if the job has status failed
    async with Neo4jTransaction() as neo4j_tx:
        job_nodes = await read_nodes(neo4j_tx, Job, {})
        assert len(job_nodes) == 1
        assert job_nodes[0].status == JobStatus.FAILED
