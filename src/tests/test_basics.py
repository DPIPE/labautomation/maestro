import asyncio
import os
import signal

import pytest

from maestro import run_workflow_steps


@pytest.mark.asyncio
async def test_immediate_shutdown():
    """
    Test that immediate shutdown is clean.
    """
    await run_workflow_steps(file_watcher_classes=[])


@pytest.mark.asyncio
async def test_sigusr1_handler(log_recorder):
    def has_task_info(log_recording):
        return (
            any(
                record.msg == "Received SIGUSR1. Logging task information..."
                for record in log_recording
            )
            and any(record.msg.startswith("Number of tasks: ") for record in log_recording)
            and any(record.msg.startswith("  Task  1: ") for record in log_recording)
        )

    await run_workflow_steps(file_watcher_classes=[])
    os.kill(os.getpid(), signal.SIGUSR1)
    async with asyncio.timeout(5):
        while not has_task_info(log_recorder.recording):
            await asyncio.sleep(0.1)
