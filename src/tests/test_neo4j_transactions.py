import pytest
from neo4j import AsyncGraphDatabase
from neo4j.exceptions import CypherSyntaxError

from maestro.config import Config
from maestro.neo4j.neo4j import Neo4jTransaction


@pytest.mark.asyncio
async def test_neo4j_out_of_context():
    "Test that we can't run queries if not run as a context"
    neo4j_tx = Neo4jTransaction()
    with pytest.raises(
        AssertionError, match="No transaction - make sure you run this in a context manager"
    ):
        await neo4j_tx.query("MATCH (n) RETURN n")


@pytest.mark.asyncio
async def test_neo4j_transactions():
    "Test that autocommit transactions work as intended"
    async with Neo4jTransaction() as neo4j_tx:
        await neo4j_tx.query("CREATE (n:Test)")

        # The node has been created in this transaction, so should be available here
        result = await neo4j_tx.query("MATCH (n:Test) RETURN n")
        assert len(result) == 1

        # This has not yet been commited, so should not be available in another transaction
        # (Cannot use context manager here, because the locking mechanism will result in a deadlock.)
        neo4j_tx2 = Neo4jTransaction()
        async with AsyncGraphDatabase.driver(
            Config.NEO4J_URI, auth=(Config.NEO4J_USER, Config.NEO4J_PASSWORD)
        ) as neo4j_tx2._driver:
            async with neo4j_tx2._driver.session() as neo4j_tx2._session:
                async with await neo4j_tx2._session.begin_transaction() as neo4j_tx2._tx:
                    result = await neo4j_tx2.query("MATCH (n:Test) RETURN n")
                    assert len(result) == 0

    # Neo4jTransaction defaults to autocommit, so it should be available after the context is exited
    async with Neo4jTransaction() as neo4j_tx3:
        result = await neo4j_tx3.query("MATCH (n:Test) RETURN n")
        assert len(result) == 1


@pytest.mark.asyncio
async def test_neo4j_transactions_no_autocommit():
    "Test that autocommit transactions work as intended"

    # Create a node in a transaction, should be rolled back
    async with Neo4jTransaction(autocommit=False) as neo4j_tx:
        await neo4j_tx.query("CREATE (n:Test)")

    # Check that node is not created
    async with Neo4jTransaction(autocommit=False) as neo4j_tx:
        result = await neo4j_tx.query("MATCH (n:Test) RETURN n")
        assert len(result) == 0

    # Create node, and explicitly commit
    async with Neo4jTransaction(autocommit=False) as neo4j_tx:
        await neo4j_tx.query("CREATE (n:Test)")
        await neo4j_tx.commit()

    # Now, the node should be available
    async with Neo4jTransaction(autocommit=False) as neo4j_tx:
        result = await neo4j_tx.query("MATCH (n:Test) RETURN n")
        assert len(result) == 1


@pytest.mark.asyncio
async def test_neo4j_transactions_rollback():
    "Test that transactions can be rolled back"
    async with Neo4jTransaction(autocommit=True) as neo4j_tx:
        await neo4j_tx.query("CREATE (n:Test)")
        await neo4j_tx.rollback()

    async with Neo4jTransaction() as neo4j_tx:
        result = await neo4j_tx.query("MATCH (n:Test) RETURN n")
        assert len(result) == 0


@pytest.mark.asyncio
async def test_error_handling():
    "Test that transactions that raise errors are rolled back"

    with pytest.raises(CypherSyntaxError):
        async with Neo4jTransaction(autocommit=True) as neo4j_tx:
            await neo4j_tx.query("CREATE (n:Test) RETURN n")
            await neo4j_tx.query("NOT_SUPPORTED_QUERY")

    with pytest.raises(ZeroDivisionError):
        async with Neo4jTransaction(autocommit=True) as neo4j_tx:
            await neo4j_tx.query("CREATE (n:Test) RETURN n")
            1 / 0

    try:
        async with Neo4jTransaction(autocommit=True) as neo4j_tx:
            await neo4j_tx.query("CREATE (n:Test) RETURN n")
            1 / 0
    except ZeroDivisionError:
        pass

    async with Neo4jTransaction() as neo4j_tx:
        result = await neo4j_tx.query("MATCH (n:Test) RETURN n")
        assert len(result) == 0

    # Exception caught within the context manager should not prevent the session from being commited
    async with Neo4jTransaction(autocommit=True) as neo4j_tx:
        try:
            await neo4j_tx.query("CREATE (n:Test) RETURN n")
            1 / 0
        except ZeroDivisionError:
            pass

    async with Neo4jTransaction() as neo4j_tx:
        result = await neo4j_tx.query("MATCH (n:Test) RETURN n")
        assert len(result) == 1
