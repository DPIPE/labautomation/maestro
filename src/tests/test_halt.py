import asyncio
from typing import Literal, override
from uuid import UUID, uuid4

import pytest
from httpx import AsyncClient

from maestro.models import BaseArtefactModel, Job, JobStatus, WorkflowModel, WorkflowStepModel
from maestro.nats.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import create_node, fetch_node_by_rel, read_node
from maestro.run import run_workflow_steps
from maestro.utils import subscribe_to_job_update
from maestro.workflow_step.message_watcher import MessageWatcher
from maestro.workflow_utils import order_workflow


class Artefact(BaseArtefactModel):
    identifier: UUID


class ArtefactA(Artefact):
    pass


class ArtefactB(Artefact):
    pass


class ArtefactC(Artefact):
    pass


class DummyStep(MessageWatcher):
    @override
    async def process(self, input_artefact: ArtefactA) -> list[BaseArtefactModel]:
        output_artefact = ArtefactB(identifier=input_artefact.identifier)
        return [output_artefact]


class HaltedStep(MessageWatcher):
    @override
    async def process(self, input_artefact: ArtefactB) -> list[BaseArtefactModel]:
        output_artefact = ArtefactC(identifier=input_artefact.identifier)

        # Halt the job
        await self.halt()

        return [output_artefact]


@pytest.mark.asyncio
@pytest.mark.parametrize("decision", ["resume", "cancel"])
async def test_workflow_step_halting(
    test_api_app: AsyncClient, decision: Literal["resume", "cancel"]
):
    """Check that workflowstep.halt() temporarily stops execution of a workflow step until
    the job is either resumed or cancelled."""

    async with NATSSession() as nats_session:
        halted_future = asyncio.Future()
        final_future = asyncio.Future()

        await run_workflow_steps(file_watcher_classes=[])

        artefact_id = uuid4()
        job = Job(
            inputs={"input_artefact": (ArtefactB, {"identifier": artefact_id})},
            params={},
            workflow_step=HaltedStep,
        )
        workflow = WorkflowModel(
            workflow_name="halted_workflow",
            jobs=[job],
        )

        await subscribe_to_job_update(nats_session, job.maestro_id, JobStatus.HALTED, halted_future)

        if decision == "resume":
            final_status = JobStatus.COMPLETED
        else:
            final_status = JobStatus.CANCELLED

        await subscribe_to_job_update(nats_session, job.maestro_id, final_status, final_future)

        async with Neo4jTransaction() as neo4j_tx:
            input_artefact = ArtefactB(identifier=artefact_id)
            await create_node(neo4j_tx, input_artefact)

        await order_workflow(workflow)

        await asyncio.wait_for(halted_future, timeout=5.0)

        # Job should be halted, output artefact should not be avilable
        async with Neo4jTransaction() as neo4j_tx:
            output_artefact = await read_node(neo4j_tx, ArtefactC, {"identifier": artefact_id})
            assert output_artefact is None, "Output artefact should not be created as job is halted"

        # Resume or cancel job by calling API endpoint
        if decision == "resume":
            endpoint = f"/api/v1/resume/job/{job.maestro_id}"
        else:
            endpoint = f"/api/v1/cancel/job/{job.maestro_id}"

        response = await test_api_app.put(endpoint)
        assert response.status_code == 200, f"Failed to {decision} job: {response.text}"

        await asyncio.wait_for(final_future, timeout=5.0)

        # Job is completed, output artefact should be available
        async with Neo4jTransaction() as neo4j_tx:
            output_artefact = await read_node(neo4j_tx, ArtefactC, {"identifier": artefact_id})
            db_job = await read_node(neo4j_tx, Job, {"maestro_id": job.maestro_id})

            if decision == "resume":
                assert (
                    db_job.status == JobStatus.COMPLETED
                ), "Job should be completed after resuming"
                assert (
                    output_artefact is not None
                ), "Output artefact should be created after job is resumed and completed"
                assert output_artefact.identifier == artefact_id
            else:
                assert (
                    db_job.status == JobStatus.CANCELLED
                ), "Job should be cancelled after cancelling"
                assert (
                    output_artefact is None
                ), "Output artefact should not be created when job is cancelled"


@pytest.mark.asyncio
@pytest.mark.parametrize("propagate", [True, False])
async def test_invalidation_behavior_of_halted_steps(test_api_app: AsyncClient, propagate: bool):
    """Check that invalidation of a preceding step to a halted step correctly invalidates the
    halted step when propagating."""

    async with NATSSession() as nats_session:
        halted_future = asyncio.Future()
        cancelled_or_completed_future = asyncio.Future()

        # Start maestro and define workflow
        await run_workflow_steps(file_watcher_classes=[])

        artefact_id = uuid4()
        job_a = Job(
            inputs={"input_artefact": (ArtefactA, {"identifier": artefact_id})},
            params={},
            workflow_step=DummyStep,
        )
        job_b = Job(
            inputs={"input_artefact": (ArtefactB, {"identifier": artefact_id})},
            params={},
            workflow_step=HaltedStep,
        )

        workflow = WorkflowModel(
            workflow_name="halted_workflow",
            jobs=[job_a, job_b],
        )

        await subscribe_to_job_update(
            nats_session, job_b.maestro_id, JobStatus.HALTED, halted_future
        )

        if propagate:
            status = JobStatus.CANCELLED
        else:
            status = JobStatus.COMPLETED

        await subscribe_to_job_update(
            nats_session, job_b.maestro_id, status, cancelled_or_completed_future
        )

        async with Neo4jTransaction() as neo4j_tx:
            input_artefact = ArtefactA(identifier=artefact_id)
            await create_node(neo4j_tx, input_artefact)

        # Order workflow and wait for job B to halt
        await order_workflow(workflow)

        await asyncio.wait_for(halted_future, timeout=5.0)

        # Invalidate job A with or without propagation
        async with Neo4jTransaction() as neo4j_tx:
            workflow_step = await fetch_node_by_rel(
                neo4j_tx, Job, {"maestro_id": job_a.maestro_id}, WorkflowStepModel
            )
            assert workflow_step is not None, "Failed to fetch workflow step"

        response = await test_api_app.put(
            "/api/v1/invalidate/workflow_step",
            data={"maestro_id": workflow_step.maestro_id, "propagate": propagate},
        )
        assert response.status_code == 200, f"Failed to invalidate job A: {response.text}"

        if propagate:
            # Wait for cancellation
            await asyncio.wait_for(cancelled_or_completed_future, timeout=5.0)

            # Job B should be cancelled and no output artefact should be created
            async with Neo4jTransaction() as neo4j_tx:
                db_job_b = await read_node(neo4j_tx, Job, {"maestro_id": job_b.maestro_id})
                assert (
                    db_job_b.status == JobStatus.CANCELLED
                ), "Job B should be cancelled after invalidating job A"

                artefact_c = await read_node(neo4j_tx, ArtefactC, {"identifier": artefact_id})
                assert (
                    artefact_c is None
                ), "Output artefact should not be created when job is cancelled"
        else:
            # Job B should still be halted, resuming it should complete the job
            response = await test_api_app.put(f"/api/v1/resume/job/{job_b.maestro_id}")
            assert response.status_code == 200, f"Failed to resume job: {response.text}"

            # Wait for completion
            await asyncio.wait_for(cancelled_or_completed_future, timeout=5.0)

            # Job B should be completed and output artefact should be created
            async with Neo4jTransaction() as neo4j_tx:
                db_job_b = await read_node(neo4j_tx, Job, {"maestro_id": job_b.maestro_id})
                assert (
                    db_job_b.status == JobStatus.COMPLETED
                ), "Job B should be completed after resuming"

                artefact_c = await read_node(neo4j_tx, ArtefactC, {"identifier": artefact_id})
                assert (
                    artefact_c is not None
                ), "Output artefact should be created after job is resumed and completed"
                assert artefact_c.identifier == artefact_id


@pytest.mark.asyncio
async def test_resume_api_endpoint(test_api_app: AsyncClient):
    """Check that the endpoint can only be used to resume halted and existing jobs."""
    job = Job(
        workflow_step=HaltedStep,
    )

    async with Neo4jTransaction() as neo4j_tx:
        await create_node(neo4j_tx, job)

    response = await test_api_app.put(f"/api/v1/resume/job/{job.maestro_id}")
    assert (
        response.status_code == 400
    ), f"Should not be able to resume unhalted job: {response.text}"

    response = await test_api_app.put(f"/api/v1/resume/job/{uuid4()}")
    assert (
        response.status_code == 400
    ), f"Should not be able to resume non-existent job: {response.text}"


@pytest.mark.asyncio
async def test_halt_job_not_set_raises_error():
    """Check that halting a job without setting the `_job` attribute is not allowed."""
    workflow_step = HaltedStep()

    with pytest.raises(ValueError, match=f"Failed to halt, job not set for {workflow_step}"):
        await workflow_step.halt()


@pytest.mark.asyncio
async def test_halt_non_running_job_raises_error():
    """Check that halting a non-running job is not allowed."""
    job = Job(
        inputs={"input_artefact": (ArtefactB, {"identifier": uuid4()})},
        params={},
        workflow_step=HaltedStep,
    )
    workflow_step = job.workflow_step_class()
    workflow_step._job = job

    with pytest.raises(
        ValueError, match=f"Failed to halt, job with maestro_id {job.maestro_id} is not running"
    ):
        await workflow_step.halt()
