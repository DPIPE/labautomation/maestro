import asyncio
import multiprocessing
from collections.abc import Callable
from pathlib import Path
from typing import cast
from uuid import UUID

import pytest
from pydantic import Field

from maestro.config import Config
from maestro.models import (
    Job,
    JobStatus,
    MaestroBase,
    StdErrModel,
    StdOutModel,
    WorkflowModel,
    WorkflowStepModelWithArtefacts,
)
from maestro.nats.nats import NATSSession
from maestro.neo4j.invalidate import invalidate_workflow_steps
from maestro.neo4j.neo4j import Neo4jTransaction, literal_params_map
from maestro.neo4j.neo4j_utils import (
    create_node,
    create_relationship,
    fetch_node_by_rel,
    filter_duplicate_jobs,
    get_valid_existing_job,
    merge_job_node,
    read_node,
    read_nodes,
)
from maestro.run import run_workflow_steps
from maestro.utils import subscribe_to_job_update
from maestro.workflow_utils import order_workflow, order_workflow_with_job_filtering
from tests.conftest import (
    DummyArtefact,
    DummyFileWatcher,
    DummyWorkflowStep,
)


def test_literal_params_map():
    d = {"int": 1, "float": 3.2, "str": "foo", "bool": True}

    assert literal_params_map(d) == '{int: 1, float: 3.2, str: "foo", bool: True}'


@pytest.mark.asyncio
async def test_create_node(neo4j_tx):
    "Test creating a node with a single property"

    class TestBaseModel(MaestroBase):
        foo: int

    node = TestBaseModel(foo=1)

    await create_node(neo4j_tx, node)

    records = await neo4j_tx.query("MATCH (n:TestBaseModel) RETURN n as node")
    assert len(records) == 1
    assert records[0]["node"]["maestro_id"] == str(node.maestro_id)
    assert records[0]["node"]["foo"] == node.foo


@pytest.mark.asyncio
async def test_create_nested_node(neo4j_tx):
    "Test creating a node with a nested property"

    class TestDeepNestedModel(MaestroBase):
        dabla: int

    class TestNestedModel(MaestroBase):
        foo: int
        deep_nested: TestDeepNestedModel

    class TestBaseModel(MaestroBase):
        bar: int
        nested: TestNestedModel

    deep_nested = TestDeepNestedModel(dabla=1)
    nested = TestNestedModel(foo=1, deep_nested=deep_nested)
    model = TestBaseModel(bar=2, nested=nested)

    created_maestro_id = await create_node(neo4j_tx, model)
    assert created_maestro_id == model.maestro_id
    records = await neo4j_tx.query(
        "MATCH (n:TestBaseModel) -[r1:nested]->(n2:TestNestedModel)-[r2:`deep_nested`]->(n3:TestDeepNestedModel) RETURN n as node, n2 as nested, n3 as deep_nested"
    )
    assert len(records) == 1
    assert records[0]["node"]["maestro_id"] == str(model.maestro_id)
    assert records[0]["node"]["bar"] == model.bar
    assert records[0]["nested"]["maestro_id"] == str(nested.maestro_id)
    assert records[0]["nested"]["foo"] == nested.foo
    assert records[0]["deep_nested"]["dabla"] == nested.deep_nested.dabla
    assert records[0]["deep_nested"]["maestro_id"] == str(nested.deep_nested.maestro_id)


@pytest.mark.asyncio
async def test_create_nested_node_with_alias(neo4j_tx):
    "Test creating a node with a nested property"

    class TestDeepNestedModel(MaestroBase):
        dabla: int

    class TestNestedModel(MaestroBase):
        foo: int
        deep_nested: TestDeepNestedModel = Field(..., alias="DEEP_NESTED_ALIAS")

    class TestBaseModel(MaestroBase):
        bar: int
        nested: TestNestedModel = Field(..., alias="NESTED_ALIAS")

    deep_nested = TestDeepNestedModel(dabla=1)
    nested = TestNestedModel(foo=1, deep_nested=deep_nested)
    model = TestBaseModel(bar=2, nested=nested)

    created_maestro_id = await create_node(neo4j_tx, model)
    assert created_maestro_id == model.maestro_id
    records = await neo4j_tx.query(
        "MATCH (n:TestBaseModel) -[r1:`NESTED_ALIAS`]->(n2:TestNestedModel)-[r2:`DEEP_NESTED_ALIAS`]->(n3:TestDeepNestedModel) RETURN n as node, n2 as nested, n3 as deep_nested"
    )
    assert len(records) == 1
    assert records[0]["node"]["maestro_id"] == str(model.maestro_id)
    assert records[0]["node"]["bar"] == model.bar
    assert records[0]["nested"]["maestro_id"] == str(nested.maestro_id)
    assert records[0]["nested"]["foo"] == nested.foo
    assert records[0]["deep_nested"]["dabla"] == nested.deep_nested.dabla
    assert records[0]["deep_nested"]["maestro_id"] == str(nested.deep_nested.maestro_id)


@pytest.mark.asyncio
async def test_create_nested_iterable(neo4j_tx):
    "Test creating a node with nested iterable properties (tuple + list)"

    class TestNestedModel(MaestroBase):
        foo: int

    class TestBaseModel(MaestroBase):
        bar: int
        nested_list: list[TestNestedModel]
        nested_tuple: tuple[TestNestedModel, TestNestedModel]

    nested1 = TestNestedModel(foo=1)
    nested2 = TestNestedModel(foo=2)
    model = TestBaseModel(
        bar=3,
        nested_list=[nested1, nested2],
        nested_tuple=(nested1, nested2),
    )

    await create_node(neo4j_tx, model)
    list_records = await neo4j_tx.query(
        "MATCH (n:TestBaseModel) -[r:nested_list]->(n2:TestNestedModel) RETURN n as node, r.relationship_index as index, n2 as nested ORDER BY index"
    )
    assert len(list_records) == 2
    assert list_records[0]["node"]["maestro_id"] == str(model.maestro_id)
    assert list_records[0]["node"]["bar"] == model.bar
    assert list_records[0]["nested"]["foo"] == nested1.foo
    assert list_records[1]["nested"]["foo"] == nested2.foo
    assert list_records[0]["nested"]["maestro_id"] == str(nested1.maestro_id)
    assert list_records[1]["nested"]["maestro_id"] == str(nested2.maestro_id)

    tuple_records = await neo4j_tx.query(
        "MATCH (n:TestBaseModel) -[r:nested_tuple]->(n2:TestNestedModel) RETURN n as node, r.relationship_index as index, n2 as nested ORDER BY index"
    )
    assert len(tuple_records) == 2
    assert tuple_records[0]["node"]["maestro_id"] == str(model.maestro_id)
    assert tuple_records[0]["node"]["bar"] == model.bar
    assert tuple_records[0]["nested"]["foo"] == nested1.foo
    assert tuple_records[1]["nested"]["foo"] == nested2.foo
    assert tuple_records[0]["nested"]["maestro_id"] == str(nested1.maestro_id)
    assert tuple_records[1]["nested"]["maestro_id"] == str(nested2.maestro_id)


@pytest.mark.asyncio
async def test_append_to_node(neo4j_tx):
    "Test that we can add more relationships to an existing node"

    class TestNestedModel(MaestroBase):
        foo: int

    class TestBaseModel(MaestroBase):
        nested_list: list[TestNestedModel]

    no_nested = TestBaseModel(nested_list=[])
    # First, create a node without any nested nodes
    await create_node(neo4j_tx, no_nested)

    # Calling create_node with same maestro_id should not create a new node,
    # but relationships should be added (even if the nested nodes are already connected)
    nested_models = [TestNestedModel(foo=i) for i in range(10)]
    for i in range(10):
        nested = no_nested.model_copy(update={"nested_list": [nested_models[i]]})
        assert nested.maestro_id == no_nested.maestro_id
        await create_node(neo4j_tx, nested)

        nested = no_nested.model_copy(update={"nested_list": [nested_models[i]]})
        assert nested.maestro_id == no_nested.maestro_id
        await create_node(neo4j_tx, nested)

    # When read back, we should get the full list of nested nodes
    db_nodes = await read_nodes(neo4j_tx, TestBaseModel, {})
    assert len(db_nodes) == 1
    db_node = db_nodes[0]

    assert len(db_node.nested_list) == 20
    for i in range(10):
        # All nodes are added twice
        assert db_node.nested_list[2 * i].foo == i
        assert db_node.nested_list[2 * i + 1].foo == i

    # There are only 10 unique TestNestedModel nodes
    assert len(set(db_node.nested_list)) == 10
    db_nested_models = await read_nodes(neo4j_tx, TestNestedModel, {})
    assert len(db_nested_models) == 10

    # However, they have duplicated relationships
    db_relationships = await neo4j_tx.query("MATCH ()-[r:`nested_list`]->() RETURN r")
    assert len(db_relationships) == 20
    relationship_indices = [r.value()["relationship_index"] for r in db_relationships]
    assert set(relationship_indices) == set(range(20))


@pytest.mark.asyncio
async def test_set_append(neo4j_tx):
    """Test that adding the same nested set multiple times does not create duplicate relationships"""

    class TestNestedModel(MaestroBase):
        foo: int

    class TestBaseModel(MaestroBase):
        nested_set: set[TestNestedModel]

    no_nested = TestBaseModel(nested_set=set())
    # First, create a node without any nested nodes
    await create_node(neo4j_tx, no_nested)

    # Calling create_node with same maestro_id should not create a new node,
    # but relationships should be added _IF_ the nested nodes are not already connected
    nested_models = [TestNestedModel(foo=i) for i in range(10)]

    for i in range(10):
        nested = no_nested.model_copy(update={"nested_set": {nested_models[i]}})
        assert nested.maestro_id == no_nested.maestro_id
        await create_node(neo4j_tx, nested)

        # Doing it twice should change nothing
        nested = no_nested.model_copy(update={"nested_set": {nested_models[i]}})
        assert nested.maestro_id == no_nested.maestro_id
        await create_node(neo4j_tx, nested)

    # When read back, we should get the full list of nested nodes
    db_nodes = await read_nodes(neo4j_tx, TestBaseModel, {})
    assert len(db_nodes) == 1
    db_node = db_nodes[0]

    assert len(db_node.nested_set) == 10

    # No duplicated relationships
    db_relationships = await neo4j_tx.query("MATCH ()-[r:`nested_set`]->() RETURN r")
    assert len(db_relationships) == 10


@pytest.mark.asyncio
async def test_read_node(neo4j_tx):
    "Test that we can read back a node from the database into a MaestroBase object"

    class TestModel(MaestroBase):
        id: int | None = None
        test_str: str = ""
        test_int: int = 0
        test_float: float = 0.0
        test_bool: bool = False

    model = TestModel(test_str="foo", test_int=1, test_float=3.2, test_bool=True)
    await create_node(neo4j_tx, model)
    read_model1 = await read_node(neo4j_tx, TestModel, model.model_dump_primitive())
    read_model2 = await read_node(neo4j_tx, TestModel, {"maestro_id": model.maestro_id})
    assert model == read_model1
    assert model == read_model2


@pytest.mark.asyncio
async def test_read_nested_node(neo4j_tx):
    "Test that we can read back a node with (deeply) nested properties from the database into a MaestroBase object"

    class TestNestedModel(MaestroBase):
        dabla: int

    class TestDeepNestedModel(MaestroBase):
        foo: int
        deep_nested: TestNestedModel

    class TestBaseModel(MaestroBase):
        bar: int
        nested_simple: TestNestedModel
        nested_deep: TestDeepNestedModel
        nested_list: list[TestDeepNestedModel | TestNestedModel]
        nested_tuple: tuple[TestNestedModel, TestDeepNestedModel]

    simple_nested = TestNestedModel(dabla=1)
    deep_nested_1 = TestDeepNestedModel(foo=1, deep_nested=simple_nested)
    deep_nested_2 = TestDeepNestedModel(foo=2, deep_nested=simple_nested)
    model = TestBaseModel(
        bar=3,
        nested_simple=simple_nested,
        nested_deep=deep_nested_1,
        nested_list=[deep_nested_1, simple_nested],
        nested_tuple=(simple_nested, deep_nested_2),
    )

    await create_node(neo4j_tx, model)
    read_model = await read_node(neo4j_tx, TestBaseModel, {"maestro_id": model.maestro_id})
    assert model == read_model


@pytest.mark.asyncio
async def test_read_nested_node_with_alias(neo4j_tx):
    "Test that we can read back a node with (deeply) nested properties from the database into a MaestroBase object (using alias)"

    class TestDeepNestedModel(MaestroBase):
        dabla: int

    class TestNestedModel(MaestroBase):
        foo: int
        deep_nested: TestDeepNestedModel = Field(..., alias="DEEP_NESTED_ALIAS")

    class TestBaseModel(MaestroBase):
        bar: int
        nested_list: list[TestNestedModel] = Field(..., alias="NESTED_LIST_ALIAS")
        nested_tuple: tuple[TestNestedModel, TestNestedModel] = Field(
            ..., alias="NESTED_TUPLE_ALIAS"
        )

    deep_nested = TestDeepNestedModel(dabla=1)
    nested1 = TestNestedModel(foo=1, deep_nested=deep_nested)
    nested2 = TestNestedModel(foo=2, deep_nested=deep_nested)
    model = TestBaseModel(
        bar=3,
        nested_list=[nested1, nested2],
        nested_tuple=(nested1, nested2),
    )

    await create_node(neo4j_tx, model)
    read_model = await read_node(neo4j_tx, TestBaseModel, {"maestro_id": model.maestro_id})
    assert model == read_model


@pytest.mark.asyncio
async def test_read_node_with_additional_labels(neo4j_tx: Neo4jTransaction):
    "Test that we can read back a node with additional labels from the database into a MaestroBase object"

    class TestModel(MaestroBase):
        pass

    model = TestModel(
        additional_labels=["FirstLabel", "SecondLabel", "ThirdLabel", "FourthLabel", "FifthLabel"]
    )
    await create_node(neo4j_tx, model)
    read_model = await read_node(neo4j_tx, TestModel, model.model_dump_primitive())
    assert model == read_model
    assert model.type == read_model.type == "FifthLabel"
    assert read_model.additional_labels == [
        "FirstLabel",
        "SecondLabel",
        "ThirdLabel",
        "FourthLabel",
        "FifthLabel",
    ]


@pytest.mark.asyncio
async def test_read_nested_iterable(neo4j_tx: Neo4jTransaction):
    "Test that we can read back a node with nested iterable properties (tuple + list) from the database into a MaestroBase object"

    class TestNestedModel1(MaestroBase):
        foo: int

    class TestNestedModel2(MaestroBase):
        bar: int

    class TestBaseModel(MaestroBase):
        bar: int
        nested_tuple1: tuple[TestNestedModel1, TestNestedModel1]
        nested_tuple2: tuple[TestNestedModel1, ...]
        nested_list: list[TestNestedModel1]
        nested_set: set[TestNestedModel1]
        nested_tuple_multiple: tuple[TestNestedModel1, TestNestedModel2]
        nested_list_multiple: list[TestNestedModel1 | TestNestedModel2]
        nested_set_multiple: set[TestNestedModel1 | TestNestedModel2]

    # Create list of length 10, and tuple of length 10
    nested_tuple1 = (TestNestedModel1(foo=i) for i in range(2))
    nested_tuple2 = (TestNestedModel1(foo=i) for i in range(2, 12))
    nested_list = [TestNestedModel1(foo=i) for i in range(12, 22)]
    nested_set = {TestNestedModel1(foo=i) for i in range(22, 32)}
    nested_tuple_multiple = (
        TestNestedModel1(foo=32),
        TestNestedModel2(bar=33),
    )
    nested_list_multiple = []
    for i in range(24, 34, 2):
        nested_list_multiple.extend(
            [
                TestNestedModel1(foo=i),
                TestNestedModel2(bar=i + 1),
            ]
        )

    nested_set_multiple = set()
    for i in range(34, 44, 2):
        nested_set_multiple |= set(
            (
                TestNestedModel1(foo=i),
                TestNestedModel2(bar=i + 1),
            ),
        )

    model = TestBaseModel(
        bar=42,
        nested_tuple1=nested_tuple1,
        nested_tuple2=nested_tuple2,
        nested_list=nested_list,
        nested_set=nested_set,
        nested_tuple_multiple=nested_tuple_multiple,
        nested_list_multiple=nested_list_multiple,
        nested_set_multiple=nested_set_multiple,
    )
    await create_node(neo4j_tx, model)
    read_model = await read_node(neo4j_tx, TestBaseModel, model.model_dump_primitive())
    assert model == read_model


@pytest.mark.asyncio
async def test_read_nested_iterable_with_subclass(neo4j_tx: Neo4jTransaction):
    """Test that correct subclass is inferred from node labels when reading iterable properties"""

    class BaseNested(MaestroBase):
        pass

    class SubNested(BaseNested):
        foo: int

    class SubSubNested(SubNested):
        bar: int

    class BaseModel(MaestroBase):
        nested1: BaseNested
        nested2: BaseNested
        nested_list: list[BaseNested]
        nested_tuple: tuple[BaseNested, BaseNested]

    model = BaseModel(
        nested1=SubNested(foo=1),
        nested2=SubSubNested(foo=2, bar=1),
        nested_list=[SubNested(foo=3), SubSubNested(foo=4, bar=2)],
        nested_tuple=(SubNested(foo=5), SubSubNested(foo=6, bar=3)),
    )

    await create_node(neo4j_tx, model)
    read_model = await read_node(neo4j_tx, BaseModel, model.model_dump_primitive())
    assert model == read_model
    assert isinstance(read_model.nested1, SubNested)
    assert isinstance(read_model.nested2, SubSubNested)
    assert isinstance(read_model.nested_list[0], SubNested)
    assert isinstance(read_model.nested_list[1], SubSubNested)
    assert isinstance(read_model.nested_tuple[0], SubNested)
    assert isinstance(read_model.nested_tuple[1], SubSubNested)


@pytest.mark.asyncio
async def test_read_and_create_node_with_collections(neo4j_tx: Neo4jTransaction):
    """Test creating a node with collections properties, and reading it back

    Tests serialization and deserialization of list, tuple, and dict properties of MaestroBase
    """

    class TestModel(MaestroBase):
        list1: list[int]
        list2: list[str]
        tuple1: tuple[float, float]
        tuple2: tuple[str, str]
        dict1: dict[str, str]
        dict2: dict[str, int]
        dict3: dict[str, list[int]]
        dict4: dict[str, tuple[int, int]]
        dict5: dict[int, str]

    model = TestModel(
        list1=[1, 2, 3],
        list2=["foo", "bar"],
        tuple1=(1.1, 2.2),
        tuple2=("foo", "bar"),
        dict1={"foo": "bar"},
        dict2={"foo": 1},
        dict3={"foo": [1, 2, 3]},
        dict4={"foo": (1, 2)},
        dict5={1: "foo"},
    )
    await create_node(neo4j_tx, model)
    read_model = await read_node(neo4j_tx, TestModel, model.model_dump_primitive())
    assert model == read_model


@pytest.mark.asyncio
async def test_merge_job_node(neo4j_tx: Neo4jTransaction):
    "Test that we can merge (update) job nodes in the database"
    test_job = Job(
        workflow_step="DummyWorkflowStep",
        inputs={"input": ("DummyArtefact", {})},
        params={"sequencer_name": "NovaSeq"},
        status=JobStatus.PENDING,
        search_metadata=["project_id: wf_1234"],
    )
    await merge_job_node(neo4j_tx, test_job)
    jobs = await read_nodes(neo4j_tx, Job, {})
    assert len(jobs) == 1
    db_job = jobs[0]
    assert db_job.maestro_id == test_job.maestro_id
    assert db_job.status == test_job.status
    assert db_job.search_metadata == test_job.search_metadata
    assert db_job.updated_at == test_job.updated_at
    assert db_job.created_at == test_job.created_at
    assert db_job.revision == 1
    assert test_job.updated_at == test_job.created_at

    # Update test_job
    test_job.status = JobStatus.COMPLETED
    await merge_job_node(neo4j_tx, test_job)

    jobs = await read_nodes(neo4j_tx, Job, {})
    assert len(jobs) == 1
    db_job = jobs[0]
    assert db_job.maestro_id == test_job.maestro_id
    assert db_job.status == test_job.status
    assert db_job.updated_at > db_job.created_at  # type: ignore[operator]
    assert db_job.revision == 2
    assert db_job.created_at == test_job.created_at


@pytest.mark.asyncio
async def test_neo4j_merge_job_node_conflict():
    """
    Test that a conflict is correctly resolved when trying to merge the same job node from
    multiple processes. Only one process should succeed, and only one message should be published.
    """
    job = Job(workflow_step="DummyWorkflowStep")

    async with NATSSession() as nats_session:
        last_message = await nats_session._js.get_last_msg(
            Config.NEO4J_SYNC_STREAM, Config.NEO4J_SYNC_SUBJECT
        )
        last_message_seq_before = cast(int, last_message.seq)

    def fork():
        async def _fork():
            import os
            import sys

            sys.stderr = open(os.devnull, "w")
            async with Neo4jTransaction(uri="bolt://neo4j:7687") as neo4j_tx_primary:
                await merge_job_node(neo4j_tx_primary, job)

        asyncio.run(_fork())

    processes = [
        multiprocessing.get_context("fork").Process(target=fork, name=f"Process-{i}")
        for i in range(10)
    ]

    for p in processes:
        p.start()

    for p in processes:
        p.join()

    # Only one process should have succeeded - check exitcode
    assert sum(p.exitcode == 0 for p in processes) == 1

    async with NATSSession() as nats_session:
        last_message = await nats_session._js.get_last_msg(
            Config.NEO4J_SYNC_STREAM, Config.NEO4J_SYNC_SUBJECT
        )
        last_message_seq_after = cast(int, last_message.seq)

    # Only one message should have been sent
    assert last_message_seq_after == last_message_seq_before + 1


@pytest.mark.asyncio
async def test_fetch_node_by_rel(neo4j_tx: Neo4jTransaction) -> None:
    test_job = Job(
        workflow_step="DummyWorkflowStep",
        inputs={"input": ("DummyArtefact", {})},
        params={"sequencer_name": "NovaSeq"},
        status=JobStatus.PENDING,
        search_metadata=["project_id: wf_1234"],
    )
    workflow_artefact = WorkflowModel(
        workflow_name="unique_wf_name",
    )
    query_params = {"maestro_id": test_job.maestro_id}

    await merge_job_node(neo4j_tx, test_job)
    empty_workflow_node = await fetch_node_by_rel(neo4j_tx, Job, query_params, WorkflowModel)
    assert empty_workflow_node is None

    workflow_db_maestro_id = await create_node(neo4j_tx, workflow_artefact)
    unconnected_workflow_node = await fetch_node_by_rel(neo4j_tx, Job, query_params, WorkflowModel)
    assert unconnected_workflow_node is None

    await create_relationship(neo4j_tx, workflow_db_maestro_id, test_job.maestro_id, "JOB")
    workflow_node = await fetch_node_by_rel(neo4j_tx, Job, query_params, WorkflowModel)
    assert workflow_node.maestro_id == workflow_artefact.maestro_id
    assert workflow_node.workflow_name == workflow_artefact.workflow_name


@pytest.mark.asyncio
async def test_relationship_list_behavior(neo4j_tx: Neo4jTransaction) -> None:
    original_workflow_step_model = WorkflowStepModelWithArtefacts(
        INPUT=[DummyArtefact()],
        OUTPUT=[DummyArtefact()],
        STDOUT=StdOutModel(),
        STDERR=StdErrModel(),
    )
    await create_node(neo4j_tx, original_workflow_step_model)
    reloaded_workflow_step_1 = await read_node(neo4j_tx, WorkflowStepModelWithArtefacts, {})
    assert reloaded_workflow_step_1 is not None
    assert len(reloaded_workflow_step_1.INPUT) == 1
    assert len(reloaded_workflow_step_1.OUTPUT) == 1

    # The INPUT and OUTPUT relationships become duplicated at this point in the datbase.
    # This is by design.
    await create_node(neo4j_tx, reloaded_workflow_step_1)
    reloaded_workflow_step_2 = await read_node(neo4j_tx, WorkflowStepModelWithArtefacts, {})
    assert reloaded_workflow_step_2 is not None
    assert len(reloaded_workflow_step_2.INPUT) == 2
    assert len(reloaded_workflow_step_2.OUTPUT) == 2

    # To avoid duplication, the INPUT and OUTPUT lists of "reloaded_workflow_step_2"
    # would need to be cleared before being stored.
    reloaded_workflow_step_2.INPUT.clear()
    reloaded_workflow_step_2.OUTPUT.clear()
    await create_node(neo4j_tx, reloaded_workflow_step_2)
    reloaded_workflow_step_3 = await read_node(neo4j_tx, WorkflowStepModelWithArtefacts, {})
    assert reloaded_workflow_step_3 is not None
    assert len(reloaded_workflow_step_3.INPUT) == 2
    assert len(reloaded_workflow_step_3.OUTPUT) == 2


@pytest.mark.asyncio
async def test_get_existing_job(neo4j_tx: Neo4jTransaction):
    initial_job = Job(
        workflow_step="DummyWorkflowStep",
        inputs={"input": ("DummyArtefact", {})},
        params={"sequencer_name": "NovaSeq"},
        status=JobStatus.PENDING,
        search_metadata=["project_id: wf_1234"],
    )

    existing_job = await get_valid_existing_job(neo4j_tx, initial_job)
    assert existing_job is None

    await merge_job_node(neo4j_tx, initial_job)

    existing_job = await get_valid_existing_job(neo4j_tx, initial_job)

    assert existing_job == initial_job

    duplicated_job = Job(
        workflow_step="DummyWorkflowStep",
        inputs={"input": ("DummyArtefact", {})},
        params={"sequencer_name": "NovaSeq"},
        status=JobStatus.PENDING,
        search_metadata=["project_id: wf_1234"],
    )

    existing_job = await get_valid_existing_job(neo4j_tx, duplicated_job)
    assert existing_job == initial_job

    # Update the status of the initial job
    # status of completed, running, pending, queued jobs should still be considered as the same job
    initial_job.status = JobStatus.COMPLETED
    await merge_job_node(neo4j_tx, initial_job)
    existing_job = await get_valid_existing_job(neo4j_tx, duplicated_job)
    assert existing_job == initial_job

    initial_job.status = JobStatus.RUNNING
    await merge_job_node(neo4j_tx, initial_job)
    existing_job = await get_valid_existing_job(neo4j_tx, duplicated_job)
    assert existing_job == initial_job

    initial_job.status = JobStatus.PENDING
    await merge_job_node(neo4j_tx, initial_job)
    existing_job = await get_valid_existing_job(neo4j_tx, duplicated_job)
    assert existing_job == initial_job

    initial_job.status = JobStatus.QUEUED
    await merge_job_node(neo4j_tx, initial_job)
    existing_job = await get_valid_existing_job(neo4j_tx, duplicated_job)
    assert existing_job == initial_job

    # status of failed, cancelled jobs should be considered as different jobs
    initial_job.status = JobStatus.FAILED
    await merge_job_node(neo4j_tx, initial_job)
    existing_job = await get_valid_existing_job(neo4j_tx, duplicated_job)
    assert existing_job is None

    initial_job.status = JobStatus.CANCELLED
    await merge_job_node(neo4j_tx, initial_job)
    existing_job = await get_valid_existing_job(neo4j_tx, duplicated_job)
    assert existing_job is None

    # if tied to a workflow step that is invalidated,
    # it should be considered as different job
    initial_job.status = JobStatus.COMPLETED
    await merge_job_node(neo4j_tx, initial_job)
    await neo4j_tx.commit()

    async with Neo4jTransaction() as neo4j_tx:
        existing_job = await get_valid_existing_job(neo4j_tx, duplicated_job)
        assert existing_job == initial_job

        workflow_step = WorkflowStepModelWithArtefacts(STDERR=StdErrModel(), STDOUT=StdOutModel())
        await create_node(neo4j_tx, workflow_step)
        await create_relationship(
            neo4j_tx, existing_job.maestro_id, workflow_step.maestro_id, "WORKFLOW_STEP"
        )
        existing_job = await get_valid_existing_job(neo4j_tx, duplicated_job)
        assert existing_job == initial_job

        await invalidate_workflow_steps(neo4j_tx, [workflow_step.maestro_id])

        existing_job = await get_valid_existing_job(neo4j_tx, duplicated_job)
        assert existing_job is None


def create_workflow(input_id: str, name: str, n_jobs: int = 3) -> tuple[WorkflowModel, list[Job]]:
    output_id_1 = "output_1_1"
    output_id_2 = "output_2_1"
    job_1 = Job(
        workflow_step=DummyWorkflowStep,
        inputs={"input": (DummyArtefact, {"identifier": input_id})},
        params={"outputs": [output_id_1, "output_1_2"]},
    )
    job_2 = Job(
        workflow_step=DummyWorkflowStep,
        inputs={"input": (DummyArtefact, {"identifier": output_id_1})},
        params={"outputs": [output_id_2, "output_2_2"]},
    )
    job_3 = Job(
        workflow_step=DummyWorkflowStep,
        inputs={"input": (DummyArtefact, {"identifier": output_id_2})},
        params={"outputs": ["output_3_1"]},
    )
    jobs = [job_1, job_2, job_3][:n_jobs]
    workflow = WorkflowModel(
        workflow_name=name,
        jobs=set(jobs),
    )
    return workflow, jobs


@pytest.mark.asyncio
async def test_filter_duplicate_jobs():
    """Test that we can filter out existing and valid jobs from workflow orders."""

    completed_future = asyncio.Future()
    input_id = "input_1"
    input_file = Path(f"/tmp/tests/{input_id}")
    first_workflow, jobs = create_workflow(input_id, "first_workflow")

    # No duplicate jobs should be found
    async with Neo4jTransaction() as neo4j_tx:
        non_existing_jobs, existing_db_jobs = await filter_duplicate_jobs(
            neo4j_tx, first_workflow.jobs
        )
        assert non_existing_jobs == first_workflow.jobs
        assert not existing_db_jobs

        first_workflow.jobs = non_existing_jobs

    await run_workflow_steps(file_watcher_classes=[DummyFileWatcher])

    async with NATSSession() as nats_session:
        await subscribe_to_job_update(
            nats_session, jobs[2].maestro_id, JobStatus.COMPLETED, completed_future
        )
        await order_workflow(first_workflow)
        input_file.touch()
        await asyncio.wait_for(completed_future, timeout=5.0)

    # Re-create the same workflow
    second_workflow, _ = create_workflow(input_id, "second_workflow")

    # All jobs have valid duplicates in the database
    async with Neo4jTransaction() as neo4j_tx:
        non_existing_jobs, existing_db_jobs = await filter_duplicate_jobs(
            neo4j_tx, second_workflow.jobs
        )
        assert not non_existing_jobs

        # The existing db jobs have the same hashes
        assert set(j.hash for j in existing_db_jobs) == set(j.hash for j in second_workflow.jobs)

        # But they are not the same objects
        assert existing_db_jobs != second_workflow.jobs

        second_workflow.jobs = set()

    await order_workflow(second_workflow)

    # Workflow should end up an isolated node
    async with Neo4jTransaction() as neo4j_tx:
        neighbor = await fetch_node_by_rel(
            neo4j_tx, WorkflowModel, {"maestro_id": second_workflow.maestro_id}, Job
        )
        assert neighbor is None


@pytest.mark.asyncio
@pytest.mark.parametrize("func", [order_workflow_with_job_filtering, order_workflow])
async def test_order_workflow_with_and_without_job_filtering(func: Callable):
    """Test that we can correctly order a workflow with and without job filtering"""
    first_future = asyncio.Future()
    input_id = "input_1"

    # Create input artefact
    async with Neo4jTransaction() as neo4j_tx:
        await create_node(neo4j_tx, DummyArtefact(identifier=input_id))

    # Create workflow with only one job (first one)
    first_workflow, first_jobs = create_workflow(input_id, "first_workflow", n_jobs=1)
    await run_workflow_steps()

    # Run the first workflow
    async with NATSSession() as nats_session:
        await subscribe_to_job_update(
            nats_session, first_jobs[0].maestro_id, JobStatus.COMPLETED, first_future
        )
        await order_workflow(first_workflow)
        await asyncio.wait_for(first_future, timeout=5.0)

    # Create the same workflow with all jobs
    second_future = asyncio.Future()
    second_workflow, second_jobs = create_workflow(input_id, "second_workflow")

    # Run the second workflow
    async with NATSSession() as nats_session:
        await subscribe_to_job_update(
            nats_session, second_jobs[-1].maestro_id, JobStatus.COMPLETED, second_future
        )
        await func(second_workflow)
        await asyncio.wait_for(second_future, timeout=5.0)

    async with Neo4jTransaction() as neo4j_tx:
        first_db_workflow = await read_node(
            neo4j_tx, WorkflowModel, {"maestro_id": first_workflow.maestro_id}
        )
        assert (
            len(first_db_workflow.jobs) == 1
        ), f"First workflow should have 1 job, but got {len(first_db_workflow.jobs)}"

        second_db_workflow = await read_node(
            neo4j_tx, WorkflowModel, {"maestro_id": second_workflow.maestro_id}
        )

        if func == order_workflow_with_job_filtering:
            assert {job.maestro_id for job in second_db_workflow.jobs} == {
                job.maestro_id for job in second_jobs[1:]
            }, "Second workflow should have the last two jobs only"
        elif func == order_workflow:
            assert {job.maestro_id for job in second_db_workflow.jobs} == {
                job.maestro_id for job in second_jobs
            }, "Second workflow should have all 3 jobs"


@pytest.mark.asyncio
async def test_read_nodes_with_missing_subclass(neo4j_tx: Neo4jTransaction):
    """Test that we can read nodes with missing subclasses, and that it will fall back to the superclass"""

    class TestNestedModelRead(MaestroBase):
        foo: int

    class TestBaseModelRead(MaestroBase):
        __node_label__ = "TestBaseModel"
        nested: TestNestedModelRead

    async def write_mode() -> UUID:
        class TestNestedModelWrite(TestNestedModelRead):
            pass

        # Remove TestNestedModelWrite from scope
        class TestBaseModelWrite(MaestroBase):
            __node_label__ = "TestBaseModel"
            bar: int
            nested: TestNestedModelWrite

        nested = TestNestedModelWrite(foo=1)
        model = TestBaseModelWrite(bar=2, nested=nested)

        await create_node(neo4j_tx, model)

        # Reading back now, should fall back to the subclass, since it is in scope
        nodes = await read_nodes(neo4j_tx, TestBaseModelRead, {"maestro_id": model.maestro_id})
        assert len(nodes) == 1
        read_model = nodes[0]
        assert read_model.nested.__class__ == TestNestedModelWrite

        return model.maestro_id

    maestro_id = await write_mode()

    # Ensure that the TestNestedModelWrite is garbage collected
    import gc

    gc.collect()

    # TestNestedModelWrite is now out of scope, so it should fall back to TestNestedModelRead
    nodes = await read_nodes(neo4j_tx, TestBaseModelRead, {"maestro_id": maestro_id})
    assert len(nodes) == 1
    read_model = nodes[0]
    assert read_model.nested.__class__ == TestNestedModelRead

    # Try to read the node back, this time using a nested model that is not a subclass of TestNestedModelRead
    # This should fail, since there is no subclass to fall back to that matches the labels of the node
    class NestedNonSubclass(MaestroBase):
        foo: int

    class TestBaseModelReadFail(MaestroBase):
        __node_label__ = "TestBaseModel"
        nested: NestedNonSubclass

    with pytest.raises(
        ValueError,
        match="Unable to determine subclass to use for nested model for field 'nested' of 'TestBaseModelReadFail'",
    ):
        await read_nodes(neo4j_tx, TestBaseModelReadFail, {"maestro_id": maestro_id})
