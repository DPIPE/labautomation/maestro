import asyncio
import functools
import multiprocessing
import multiprocessing.sharedctypes
import os
import pathlib
import signal
import sys
from typing import Any

import pytest

from maestro import run_workflow_steps
from maestro.config import Config
from maestro.models import (
    BaseArtefactModel,
    Job,
    JobStatus,
    StdErrModel,
    StdOutModel,
    WorkflowModel,
    WorkflowStepModelWithArtefacts,
)
from maestro.nats.nats import NATSSession
from maestro.neo4j.invalidate import invalidate_artefacts
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import fetch_node_by_rel, merge_job_node, read_nodes
from maestro.utils import subscribe_to_job_update
from maestro.worker_queue import WorkerQueue
from maestro.workflow_step.message_watcher import MessageWatcher
from maestro.workflow_utils import order_workflow

call_count: Any = multiprocessing.Value("i", 0)
current_running: Any = multiprocessing.Value("i", 0)
max_current_running: Any = multiprocessing.Value("i", 0)
cancel_count: Any = multiprocessing.Value("i", 0)


class WorkerStep(MessageWatcher):
    async def process(self, num_jobs: int, sleep_time: float, sequence_number: int):
        concurrency_limit = (
            min(Config.MAX_WORKERS, num_jobs)
            if self.concurrency_limit is None
            else min(Config.MAX_WORKERS, num_jobs, self.concurrency_limit)
        )
        # Increment currently running count.
        with current_running.get_lock():
            current_running.value += 1
            assert current_running.value <= concurrency_limit
            # Update max running count.
            if current_running.value > max_current_running.value:
                max_current_running.value = current_running.value
        # Jobs are expected to be processed in order and in parallel, with
        # at most Config.MAX_WORKERS running at the same time.
        assert sequence_number < call_count.value + Config.MAX_WORKERS
        # Pretend to do some work
        await asyncio.sleep(sleep_time)
        # Wait for the other workers to start before we finish, to reach the
        # maximum number of workers running at the same time.
        try:
            async with asyncio.timeout(10.0):
                while max_current_running.value < concurrency_limit:
                    await asyncio.sleep(0.1)
        except TimeoutError:
            assert False, "Other workers did not start in time"
        # Decrement currently running count.
        with current_running.get_lock():
            assert current_running.value <= concurrency_limit
            current_running.value -= 1
        # Increment the call count.
        with call_count.get_lock():
            call_count.value += 1
        return []


def expected_total_time(num_jobs: int, concurrency_limit: int, job_duration: float) -> float:
    """
    Estimate the total time taken to process "num_jobs" jobs.

    Each job takes "job_duration" second to complete, and we expect that the
    total time taken depends on the number of workers ("concurrency_limit").
    This evaluates to 1 * job_duration for 1-3 jobs, 2 * job_duration for 4-6 jobs,
    3 * job_duration for 7-9 jobs, when concurrency_limit = 3.
    Additionally, "overhead" time is added per job and a "margin".
    """
    overhead = 1.0
    margin = 1.0
    return (1 + (num_jobs - 1) // concurrency_limit) * (job_duration + overhead) + margin


@pytest.mark.asyncio
@pytest.mark.parametrize("num_jobs", [3, 4, 5, 6, 10])
@pytest.mark.parametrize("concurrency_limit", [None, 2, 4])
async def test_workers(num_jobs: int, concurrency_limit: int | None, monkeypatch):
    """
    Test that the worker scheduling works as expected.

    We create a number of jobs, and check that they are processed in parallel,
    determined by Config.MAX_WORKERS.
    """

    monkeypatch.setattr(Config, "MAX_WORKERS", 3)
    if concurrency_limit is None:
        concurrency_limit = Config.MAX_WORKERS
    else:
        monkeypatch.setattr(WorkerStep, "concurrency_limit", concurrency_limit)
        concurrency_limit = min(Config.MAX_WORKERS, concurrency_limit)
    sleep_time = 0.1

    # Since we're using multiprocessing, we need to use a shared variable to
    # keep track of the number of times the process was called.
    call_count.value = 0
    current_running.value = 0
    max_current_running.value = 0

    # Start the WorkerStep
    await run_workflow_steps(
        message_watcher_classes=[WorkerStep], artefact_classes=[], file_watcher_classes=[]
    )

    for i in range(num_jobs):
        await order_workflow(
            WorkflowModel(
                workflow_name=f"test_workers_{i}",
                jobs={
                    Job(
                        inputs={},
                        params=dict(num_jobs=num_jobs, sleep_time=sleep_time, sequence_number=i),
                        workflow_step=WorkerStep,
                    )
                },
            )
        )

    # Wait for the processes to finish
    async with asyncio.timeout(expected_total_time(num_jobs, concurrency_limit, sleep_time)):
        while call_count.value != num_jobs:
            await asyncio.sleep(0.1)
            assert current_running.value <= min(concurrency_limit, num_jobs)
            assert max_current_running.value <= min(concurrency_limit, num_jobs)

    # Check that all jobs were processed
    assert call_count.value == num_jobs
    assert current_running.value == 0
    assert max_current_running.value == min(concurrency_limit, num_jobs)

    # Wait for worker queue to shut down before inspecting database.
    # This avoids race between database updates happening from workers after
    # leaving WorkerStep.process() and the database checking happening below.
    WorkerQueue.shutdown_gracefully()
    await asyncio.wait_for(WorkerQueue.loop_task, 10)

    async with Neo4jTransaction() as neo4j_tx:
        db_jobs = await read_nodes(neo4j_tx, Job, {"status": JobStatus.COMPLETED})
        assert len(db_jobs) == num_jobs


class CrashingStep(MessageWatcher):
    async def process(self, sleep_time: float):
        await asyncio.sleep(sleep_time)
        try:
            print("stdout: Going to crash...")
            print("stderr: Going to crash...", file=sys.stderr)
            # Cause abrupt process termination, wihout normal exit processing.
            os._exit(1)
        finally:
            # This is never reached.
            assert False


@pytest.mark.asyncio
async def test_crashing_workers_do_not_dos_attack(monkeypatch):
    # See above test for explanation.
    monkeypatch.setattr(Config, "MAX_WORKERS", 3)
    num_crashing_jobs = Config.MAX_WORKERS * 2
    num_successful_jobs = Config.MAX_WORKERS
    num_jobs = num_crashing_jobs + num_successful_jobs
    sleep_time = 0.1

    # See above test for explanation.
    call_count.value = 0
    current_running.value = 0
    max_current_running.value = 0

    await run_workflow_steps(
        message_watcher_classes=[WorkerStep, CrashingStep],
        artefact_classes=[],
        file_watcher_classes=[],
    )

    # Add CrashingStep jobs first, so they get processed first.
    jobs = [
        Job(inputs={}, params=dict(sleep_time=sleep_time), workflow_step=CrashingStep)
        for _ in range(num_crashing_jobs)
    ] + [
        Job(
            inputs={},
            params=dict(num_jobs=num_jobs, sleep_time=sleep_time, sequence_number=i),
            workflow_step=WorkerStep,
        )
        for i in range(num_successful_jobs)
    ]
    await order_workflow(
        WorkflowModel(workflow_name="test_crashing_workers_do_not_dos_attack", jobs=jobs)
    )

    async with asyncio.timeout(expected_total_time(num_jobs, Config.MAX_WORKERS, sleep_time)):
        while call_count.value != num_successful_jobs:
            await asyncio.sleep(0.1)
            assert current_running.value <= min(Config.MAX_WORKERS, num_jobs)
            assert max_current_running.value <= min(Config.MAX_WORKERS, num_jobs)

    assert call_count.value == num_successful_jobs
    assert current_running.value == 0
    assert max_current_running.value == min(Config.MAX_WORKERS, num_jobs)

    async with asyncio.timeout(5):
        while len(WorkerQueue.queue) > 0:
            await asyncio.sleep(0.1)

    WorkerQueue.shutdown_gracefully()
    await asyncio.wait_for(WorkerQueue.loop_task, 5)

    async with Neo4jTransaction() as neo4j_tx:
        db_jobs = await read_nodes(neo4j_tx, Job, {"status": JobStatus.COMPLETED})
        assert len(db_jobs) == num_successful_jobs
        db_jobs = await read_nodes(neo4j_tx, Job, {"status": JobStatus.FAILED})
        assert len(db_jobs) == num_crashing_jobs
        db_stdouts = await read_nodes(neo4j_tx, StdOutModel, {})
        assert len(db_stdouts) == num_jobs
        assert (
            len([s for s in db_stdouts if "stdout: Going to crash...\n" in s.output])
            == num_crashing_jobs
        )
        db_stderrs = await read_nodes(neo4j_tx, StdErrModel, {})
        assert len(db_stderrs) == num_jobs
        assert (
            len([s for s in db_stderrs if "stderr: Going to crash...\n" in s.output])
            == num_crashing_jobs
        )


class BlockingStep(MessageWatcher):
    async def process(self, respond_to_sigterm: bool):
        if not respond_to_sigterm:
            asyncio.get_running_loop().remove_signal_handler(signal.SIGTERM)
        with call_count.get_lock():
            call_count.value += 1
        try:
            while True:
                await asyncio.sleep(0)
        except asyncio.CancelledError:
            with cancel_count.get_lock():
                cancel_count.value += 1
            raise
        finally:
            assert respond_to_sigterm


@pytest.mark.asyncio
@pytest.mark.parametrize("respond_to_sigterm", [True, False])
async def test_cancel_blocking_workers(respond_to_sigterm, monkeypatch):
    monkeypatch.setattr(Config, "MAX_WORKERS", 3)
    if not respond_to_sigterm:
        monkeypatch.setattr(Config, "GRACE_PERIOD", 0.1)

    call_count.value = 0
    cancel_count.value = 0

    task = await run_workflow_steps(
        message_watcher_classes=[BlockingStep],
        artefact_classes=[],
        file_watcher_classes=[],
    )

    jobs = [
        Job(
            inputs={},
            params=dict(respond_to_sigterm=respond_to_sigterm),
            workflow_step=BlockingStep,
        )
        for _ in range(Config.MAX_WORKERS)
    ]
    await order_workflow(WorkflowModel(workflow_name="test_cancel_blocking_workers", jobs=jobs))

    async with asyncio.timeout(1.0):
        while call_count.value != Config.MAX_WORKERS:
            await asyncio.sleep(0.1)

    with pytest.raises(asyncio.CancelledError):
        task.cancel()
        await asyncio.wait_for(
            task, timeout=5.0 + (0 if respond_to_sigterm else Config.GRACE_PERIOD)
        )

    assert cancel_count.value == (Config.MAX_WORKERS if respond_to_sigterm else 0)

    async with Neo4jTransaction() as neo4j_tx:
        db_jobs = await read_nodes(neo4j_tx, Job, {"status": JobStatus.CANCELLED})
        assert len(db_jobs) == Config.MAX_WORKERS


class OrphaningGrandchildrenStep(MessageWatcher):
    """
    This workflow step spawns two grandchild processes and then intentionally
    terminates abruptly. The grandchildren are consequently orphaned causing them
    to be adopted by another process.
    """

    # The elements are automatically initialized to 0.
    grandchild_pids = multiprocessing.Array("i", 3)
    terminated_normally = multiprocessing.Value("i", 0)
    received_sigterm = multiprocessing.Value("i", 0)

    async def grandchild(self, i):
        current_task = asyncio.current_task()
        assert current_task is not None
        asyncio.get_running_loop().add_signal_handler(signal.SIGTERM, current_task.cancel)
        # The first grandchild termintes on its own halfway into the grace period.
        if i == 0:
            await asyncio.sleep(0.5)
            # This will not be reached if the first grandchild was terminated by SIGTERM or SIGKILL.
            with self.terminated_normally.get_lock():
                self.terminated_normally.value += 1
        else:
            while True:
                try:
                    await asyncio.sleep(1)
                except asyncio.CancelledError:
                    with self.received_sigterm.get_lock():
                        self.received_sigterm.value += 1
                    # The second grandchild termintes after receiving SIGTERM while the third
                    # grandchild keeps running and will need to be killed by SIGKILL.
                    if i == 1:
                        break

    async def process(self):
        for i in range(len(self.grandchild_pids)):
            grandchild = multiprocessing.get_context("fork").Process(
                target=functools.partial(asyncio.run, self.grandchild(i))
            )
            grandchild.start()
            self.grandchild_pids[i] = grandchild.pid
        # Cause abrupt termination.
        os._exit(1)
        # This point is never reached.


@pytest.mark.asyncio
async def test_orphaned_grandchildren_are_terminated():
    """
    Test that orphaned grandchild processes get adopted and then killed by the
    main Maestro process after a workflow step abruptly terminates.
    """

    await run_workflow_steps(
        message_watcher_classes=[OrphaningGrandchildrenStep],
        artefact_classes=[],
        file_watcher_classes=[],
    )

    async def order_workflow_and_expect_orphans_to_be_reaped():
        # Clear the grandchild PIDs array in case another test has already used it.
        for i in range(len(OrphaningGrandchildrenStep.grandchild_pids)):
            OrphaningGrandchildrenStep.grandchild_pids[i] = 0
        OrphaningGrandchildrenStep.terminated_normally.value = 0
        OrphaningGrandchildrenStep.received_sigterm.value = 0

        # Order a workflow consisting of a single step that will leave behind
        # orphaned grandchild processes.
        await order_workflow(
            WorkflowModel(
                workflow_name="test_orphaned_grandchildren_are_terminated",
                jobs=[Job(workflow_step=OrphaningGrandchildrenStep)],
            )
        )

        # Check that the grandchildren were started.
        async with asyncio.timeout(5):
            while any(pid == 0 for pid in iter(OrphaningGrandchildrenStep.grandchild_pids)):
                await asyncio.sleep(0.1)

        # Check that the grandchildren were terminated.
        async with asyncio.timeout(5):
            # Use existence of "/proc/<pid>" to check if a process is still running.
            while any(
                pathlib.Path(f"/proc/{pid}").exists()
                for pid in iter(OrphaningGrandchildrenStep.grandchild_pids)
            ):
                await asyncio.sleep(0.1)

        assert OrphaningGrandchildrenStep.terminated_normally.value == 1
        assert OrphaningGrandchildrenStep.received_sigterm.value == 2

    await order_workflow_and_expect_orphans_to_be_reaped()

    # Repeat test to check that subreaper continues to work.
    await order_workflow_and_expect_orphans_to_be_reaped()

    WorkerQueue.shutdown_gracefully()
    await asyncio.wait_for(WorkerQueue.loop_task, 5)


class ChainedArtefact(BaseArtefactModel):
    sequence_number: int


class ChainedStep(MessageWatcher):
    started_count = multiprocessing.Value("i", 0)
    can_finish_count = multiprocessing.Value("i", 0)
    cancelled_count = multiprocessing.Value("i", 0)

    async def process(self, input: ChainedArtefact | None = None):
        with self.started_count.get_lock():
            self.started_count.value += 1
        try:
            while True:
                await asyncio.sleep(0.1)
                with self.can_finish_count.get_lock():
                    if self.can_finish_count.value > 0:
                        self.can_finish_count.value -= 1
                        break
        except asyncio.CancelledError:
            with self.cancelled_count.get_lock():
                self.cancelled_count.value += 1
            raise
        return [ChainedArtefact(sequence_number=0 if input is None else input.sequence_number + 1)]


@pytest.mark.asyncio
@pytest.mark.parametrize("termination_signal", ["SIGTERM", "SIGINT"])
async def test_restarted_run_workflow_steps_runs_waiting_job(termination_signal):
    """
    Test that Maestro automatically runs waiting job that was postposed
    due to a preceding graceful shutdown.
    """
    async with NATSSession() as nats_session:
        ChainedStep.started_count.value = 0
        ChainedStep.can_finish_count.value = 0

        task = await run_workflow_steps(
            message_watcher_classes=[ChainedStep],
            artefact_classes=[ChainedArtefact],
            file_watcher_classes=[],
        )

        # setup future in order to subscribe to job status updating to "queued"
        job_queued = asyncio.Future()
        second_job = Job(
            workflow_step=ChainedStep,
            inputs={"input": (ChainedArtefact, {})},
        )
        await subscribe_to_job_update(
            nats_session, second_job.maestro_id, JobStatus.QUEUED, job_queued
        )
        # Start workflow consisting of two jobs where one depends on the other.
        await order_workflow(
            WorkflowModel(
                workflow_name="test_restarted_run_workflow_steps_runs_waiting_job",
                jobs={
                    Job(
                        workflow_step=ChainedStep,
                        params={},
                    ),
                    second_job,
                },
            )
        )

        # Wait for the first job to start.
        async with asyncio.timeout(5):
            while ChainedStep.started_count.value < 1:
                await asyncio.sleep(0.1)
        assert ChainedStep.started_count.value == 1

        # Request graceful shutdown of the worker queue.
        os.kill(os.getpid(), getattr(signal, termination_signal))

        # Allow the first job to finish.
        with ChainedStep.can_finish_count.get_lock():
            ChainedStep.can_finish_count.value += 1

        # Wait until second job is queued.
        await asyncio.wait_for(job_queued, 5)

    # Wait for Maestro to shut down completely.
    await asyncio.wait_for(task, 5)

    # Check that the second job was not started.
    assert ChainedStep.started_count.value == 1

    # Restart Maestro.
    await run_workflow_steps(
        message_watcher_classes=[ChainedStep],
        artefact_classes=[ChainedArtefact],
        file_watcher_classes=[],
    )

    # Expect that the second job is automatically queued and started.
    async with asyncio.timeout(5):
        while ChainedStep.started_count.value < 2:
            await asyncio.sleep(0.1)
    assert ChainedStep.started_count.value == 2

    # Allow the second job to finish.
    with ChainedStep.can_finish_count.get_lock():
        ChainedStep.can_finish_count.value += 1

    # Gracefully shutdown the worker queue to prevent cleanup fixture from
    # cancelling the second job while it is finishing up.
    WorkerQueue.shutdown_gracefully()
    await asyncio.wait_for(WorkerQueue.loop_task, 5)


@pytest.mark.asyncio
async def test_restarted_run_workflow_steps_sets_job_to_pending_if_input_has_been_invalidated():
    """
    Test that Maestro automatically sets a job back to "pending" if its input has
    been invalidated after it entered "running" state during preceding Maestro run.
    """
    ChainedStep.started_count.value = 0
    ChainedStep.can_finish_count.value = 0

    task = await run_workflow_steps(
        message_watcher_classes=[ChainedStep],
        artefact_classes=[ChainedArtefact],
        file_watcher_classes=[],
    )

    first_job = Job(
        workflow_step=ChainedStep,
        params={},
    )
    second_job = Job(
        workflow_step=ChainedStep,
        inputs={"input": (ChainedArtefact, {})},
    )
    async with NATSSession() as nats_session:
        job_queued = asyncio.Future()
        await subscribe_to_job_update(
            nats_session, second_job.maestro_id, JobStatus.QUEUED, job_queued
        )
        # Start workflow consisting of two jobs where one depends on the other.
        await order_workflow(
            WorkflowModel(
                workflow_name="chained_jobs",
                jobs={first_job, second_job},
            )
        )

        # Wait for the first job to start.
        async with asyncio.timeout(5):
            while ChainedStep.started_count.value < 1:
                await asyncio.sleep(0.1)
        assert ChainedStep.started_count.value == 1

        # Request graceful shutdown of the worker queue.
        os.kill(os.getpid(), signal.SIGTERM)

        # Allow the first job to finish.
        with ChainedStep.can_finish_count.get_lock():
            ChainedStep.can_finish_count.value += 1

        # Wait until second job is queued.
        await asyncio.wait_for(job_queued, 5)

    # Wait for Maestro to shut down completely.
    await asyncio.wait_for(task, 5)

    # Check that the second job was not started.
    assert ChainedStep.started_count.value == 1

    async with Neo4jTransaction() as neo4j_tx:
        # Invalidate the first job's output artefact.
        output_artefact = (
            await fetch_node_by_rel(
                neo4j_tx, Job, {"maestro_id": first_job.maestro_id}, WorkflowStepModelWithArtefacts
            )
        ).OUTPUT[0]
        await invalidate_artefacts(neo4j_tx, [output_artefact.maestro_id], propagate=True)

        # Set second job's status to "running" to simulate that it was running, rather
        # than "queued", when the system was shut down.
        # This adds coverage of the "n.status IN ['queued', 'running']" condition used
        # to re-queue jobs upon Maestro restart.
        second_job_db = (await read_nodes(neo4j_tx, Job, {"maestro_id": second_job.maestro_id}))[0]
        assert second_job_db.status == JobStatus.QUEUED
        second_job_db.status = JobStatus.RUNNING
        await merge_job_node(neo4j_tx, second_job_db)

    async with NATSSession() as nats_session:
        job_pending = asyncio.Future()
        await subscribe_to_job_update(
            nats_session, second_job.maestro_id, JobStatus.PENDING, job_pending
        )

        # Restart Maestro.
        await run_workflow_steps(
            message_watcher_classes=[ChainedStep],
            artefact_classes=[ChainedArtefact],
            file_watcher_classes=[],
        )

        # Wait until second job is set to "pending".
        await asyncio.wait_for(job_pending, 5)


class SequentialStep(MessageWatcher):
    """
    A workflow step that can only run one at a time and stores the provided
    sequence number for inspection by the test case.

    The step will wait for an indication to finish, given by the test case.
    """

    concurrency_limit = 1
    current_sequence_number = multiprocessing.Value("i", 0)
    can_finish_count = multiprocessing.Value("i", 0)

    async def process(self, sequence_number: int):
        self.current_sequence_number.value = sequence_number
        while True:
            await asyncio.sleep(0.1)
            with self.can_finish_count.get_lock():
                if self.can_finish_count.value > 0:
                    self.can_finish_count.value -= 1
                    self.current_sequence_number.value = 0
                    return []


@pytest.mark.asyncio
async def test_job_priority():
    """
    Order jobs with varying priorities and check that they are processed in the correct order.
    """
    # Clear the shared variables in case another test has already used them.
    SequentialStep.current_sequence_number.value = 0
    SequentialStep.can_finish_count.value = 0

    await run_workflow_steps(
        message_watcher_classes=[SequentialStep],
        artefact_classes=[],
        file_watcher_classes=[],
    )

    first_sequence_number = 1

    # Order a low-priority job first.
    await order_workflow(
        WorkflowModel(
            workflow_name="low_priority",
            jobs={
                Job(workflow_step=SequentialStep, params={"sequence_number": first_sequence_number})
            },
        )
    )

    # Wait for the low-priority job to have started.
    async with asyncio.timeout(5):
        while SequentialStep.current_sequence_number.value != first_sequence_number:
            await asyncio.sleep(0.1)

    assert SequentialStep.current_sequence_number.value == first_sequence_number

    # Order jobs of various priorities in "arbitrary" order. Within each priority level,
    # the expected ordering is "stable", i.e. jobs should be processed in the order
    # they were ordered.
    ordering_sequence = (
        ("another_low_priority", 1, 6),
        ("medium_priority", 2, 4),
        ("high_priority", 3, 2),
        ("another_medium_priority", 2, 5),
        ("another_high_priority", 3, 3),
    )
    for workflow_name, priority, sequence_number in ordering_sequence:
        await order_workflow(
            WorkflowModel(
                workflow_name=workflow_name,
                jobs={
                    Job(
                        workflow_step=SequentialStep,
                        priority=priority,
                        params={"sequence_number": sequence_number},
                    )
                },
            )
        )

    # Allow the first job to finish.
    with SequentialStep.can_finish_count.get_lock():
        SequentialStep.can_finish_count.value += 1

    # Check that all remaining jobs are processed in the correct order.
    for i in range(2, 2 + len(ordering_sequence)):
        # Wait for next job to start.
        async with asyncio.timeout(5):
            while SequentialStep.current_sequence_number.value != i:
                await asyncio.sleep(0.1)

        assert SequentialStep.current_sequence_number.value == i

        # Allow job to finish.
        with SequentialStep.can_finish_count.get_lock():
            SequentialStep.can_finish_count.value += 1

    # Gracefully shut down the worker queue to prevent cleanup fixture from
    # cancelling last job while it is finishing up.
    WorkerQueue.shutdown_gracefully()
    await asyncio.wait_for(WorkerQueue.loop_task, 5)
