import asyncio

import pytest

from maestro import subscribers
from maestro.config import Config
from maestro.models import Job, JobStatus, WorkflowModel
from maestro.nats.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import create_node, read_node
from maestro.utils import subscribe_to_job_update
from maestro.worker_queue import WorkerQueue
from tests.conftest import DummyArtefact, DummyWorkflowStep


@pytest.mark.asyncio
async def test_on_order_received(monkeypatch):
    job_pending = asyncio.Future()
    job_running = asyncio.Future()
    job_completed = asyncio.Future()
    job_other_instance = asyncio.Future()

    WorkerQueue.setup()
    WorkerQueue.run()

    # Store original instance ID and create a different one for testing
    original_instance_id = Config.INSTANCE_ID
    other_instance_id = "other_instance"

    async with NATSSession() as nats_session:
        await nats_session.add_or_update_stream(
            Config.STREAM,
            ["dummy_subject", "artefact.*", "workflow_step.DummyWorkflowStep.*"],
        )
        await nats_session.subscribe(
            Config.STREAM,
            "dummy_subject",
            subscribers.on_order_received,
        )
        # This should go from "pending" -> "queued" -> "running" -> "completed", as there are no unresolved inputs
        running_job = Job(workflow_step=DummyWorkflowStep)  # Uses default instance_id
        await subscribe_to_job_update(
            nats_session, running_job.maestro_id, JobStatus.PENDING, job_pending
        )
        await subscribe_to_job_update(
            nats_session, running_job.maestro_id, JobStatus.RUNNING, job_running
        )
        await subscribe_to_job_update(
            nats_session, running_job.maestro_id, JobStatus.COMPLETED, job_completed
        )

        # Create a job for a different instance that should be filtered out
        other_instance_job = Job(workflow_step=DummyWorkflowStep, instance_id=other_instance_id)

        await subscribe_to_job_update(
            nats_session, other_instance_job.maestro_id, JobStatus.QUEUED, job_other_instance
        )

        running_workflow = WorkflowModel(
            workflow_name=f"{running_job.workflow_step}",
            jobs=[running_job, other_instance_job],
        )
        async with Neo4jTransaction() as neo4j_tx:
            await create_node(neo4j_tx, running_workflow)
        await nats_session.publish(
            "dummy_subject", running_workflow.model_dump_json().encode(), Config.STREAM
        )

        await asyncio.wait_for(asyncio.gather(job_pending, job_running, job_completed), 5)

        # Verify the other instance job was not processed
        async with Neo4jTransaction() as neo4j_tx:
            workflow = await read_node(
                neo4j_tx, WorkflowModel, {"maestro_id": running_workflow.maestro_id}
            )
            assert len(workflow.jobs) == 2
            assert {(job.status, job.instance_id) for job in workflow.jobs} == {
                (
                    JobStatus.COMPLETED,
                    "default",
                ),
                (
                    JobStatus.PENDING,
                    "other_instance",
                ),
            }

        # Change instance ID to other_instance
        monkeypatch.setattr(Config, "INSTANCE_ID", other_instance_id)

        # Publish workflow again with new instance ID
        await nats_session.publish(
            "dummy_subject", running_workflow.model_dump_json().encode(), Config.STREAM
        )

        await asyncio.wait_for(job_other_instance, 5)

        # Verify the other instance job is now processed
        async with Neo4jTransaction() as neo4j_tx:
            workflow = await read_node(
                neo4j_tx, WorkflowModel, {"maestro_id": running_workflow.maestro_id}
            )
            assert len(workflow.jobs) == 2
            jobs = list(workflow.jobs)
            instance_ids = {jobs[0].instance_id, jobs[1].instance_id}
            assert instance_ids == {original_instance_id, other_instance_id}

        # Reset instance ID to original
        monkeypatch.setattr(Config, "INSTANCE_ID", original_instance_id)

        # This should go into "pending", as the input artefact does not exist
        pending_job = Job(workflow_step=DummyWorkflowStep, inputs={"input": ("DummyArtefact", {})})
        job_pending = asyncio.Future()
        await subscribe_to_job_update(
            nats_session, pending_job.maestro_id, JobStatus.PENDING, job_pending
        )
        pending_workflow = WorkflowModel(
            workflow_name=f"{pending_job.workflow_step}",
            jobs=[pending_job],
        )
        async with Neo4jTransaction() as neo4j_tx:
            await create_node(neo4j_tx, pending_workflow)

        await nats_session.publish(
            "dummy_subject", pending_workflow.model_dump_json().encode(), Config.STREAM
        )

        # Wait for pending job received
        await asyncio.wait_for(job_pending, 5)

    WorkerQueue.shutdown_gracefully()
    await asyncio.wait_for(WorkerQueue.loop_task, 5)


@pytest.mark.asyncio
async def test_on_order_received_waits_for_workflow_synchronization(monkeypatch):
    job_pending = asyncio.Future()
    job_running = asyncio.Future()
    job_completed = asyncio.Future()

    WorkerQueue.setup()
    WorkerQueue.run()

    async with NATSSession() as nats_session:
        await nats_session.add_or_update_stream(
            Config.STREAM,
            ["dummy_subject", "artefact.*", "workflow_step.DummyWorkflowStep.*"],
        )
        await nats_session.subscribe(
            Config.STREAM,
            "dummy_subject",
            subscribers.on_order_received,
        )
        # This should go from "pending" -> "queued" -> "running" -> "completed", as there are no unresolved inputs
        running_job = Job(workflow_step=DummyWorkflowStep)  # Uses default instance_id
        await subscribe_to_job_update(
            nats_session, running_job.maestro_id, JobStatus.PENDING, job_pending
        )
        await subscribe_to_job_update(
            nats_session, running_job.maestro_id, JobStatus.RUNNING, job_running
        )
        await subscribe_to_job_update(
            nats_session, running_job.maestro_id, JobStatus.COMPLETED, job_completed
        )

        running_workflow = WorkflowModel(
            workflow_name=f"{running_job.workflow_step}",
            jobs=[running_job],
        )
        await nats_session.publish(
            "dummy_subject", running_workflow.model_dump_json().encode(), Config.STREAM
        )

        # Check that the job does not get started.
        with pytest.raises(asyncio.TimeoutError):
            await asyncio.wait_for(asyncio.shield(job_pending), 0.2)
        with pytest.raises(asyncio.TimeoutError):
            await asyncio.wait_for(asyncio.shield(job_running), 0.2)
        with pytest.raises(asyncio.TimeoutError):
            await asyncio.wait_for(asyncio.shield(job_completed), 0.2)

        # Add the workflow to the database. This simulates a delay in the workflow synchronization which can
        # happen if the workflow was created on a different instance and the database is not yet up-to-date.
        async with Neo4jTransaction() as neo4j_tx:
            await create_node(neo4j_tx, running_workflow)

        await asyncio.wait_for(asyncio.gather(job_pending, job_running, job_completed), 5)

    WorkerQueue.shutdown_gracefully()
    await asyncio.wait_for(WorkerQueue.loop_task, 5)


@pytest.mark.asyncio
async def test_on_input_received():
    pending_job_received = asyncio.Future()
    running_job_received = asyncio.Future()
    completed_job_received = asyncio.Future()

    WorkerQueue.setup()
    WorkerQueue.run()

    async with NATSSession() as nats_session:
        await nats_session.add_or_update_stream(
            Config.STREAM, ["dummy_subject", "workflow_step.DummyWorkflowStep.*"]
        )
        await nats_session.subscribe(
            Config.STREAM,
            "dummy_subject",
            subscribers.on_input_received,
            config={"deliver_policy": "new"},
        )

        # Create a job with a required input artefact, and publish it,
        # Leave it in JobStatus.PENDING status
        pending_job = Job(
            workflow_step=DummyWorkflowStep,
            inputs={"input": ("DummyArtefact", {"identifier": "dummy_identifier"})},
            status=JobStatus.PENDING,
        )
        await subscribe_to_job_update(
            nats_session, pending_job.maestro_id, JobStatus.PENDING, pending_job_received
        )
        await subscribe_to_job_update(
            nats_session, pending_job.maestro_id, JobStatus.RUNNING, running_job_received
        )
        await subscribe_to_job_update(
            nats_session, pending_job.maestro_id, JobStatus.COMPLETED, completed_job_received
        )
        workflow = WorkflowModel(
            workflow_name=f"{pending_job.workflow_step}",
            jobs=[pending_job],
        )

        # Create workflow in database
        async with Neo4jTransaction() as neo4j_tx:
            await create_node(neo4j_tx, workflow)

        await asyncio.wait_for(pending_job_received, 5)

        # Create and publish a non-matching artefact. This should not set running on the job,
        # since the job is still waiting for the correct artefact
        non_matching_artefact = DummyArtefact(identifier="non_matching_identifier")
        async with Neo4jTransaction() as neo4j_tx:
            await create_node(neo4j_tx, non_matching_artefact)
        await nats_session.publish(
            "dummy_subject", str(non_matching_artefact.maestro_id).encode(), Config.STREAM
        )

        with pytest.raises(asyncio.TimeoutError):
            await asyncio.wait_for(asyncio.shield(running_job_received), 0.1)

        # Create and publish a matching artefact. This should set status in running
        matching_artefact = DummyArtefact(identifier="dummy_identifier")
        async with Neo4jTransaction() as neo4j_tx:
            await create_node(neo4j_tx, matching_artefact)
        await nats_session.publish(
            "dummy_subject", str(matching_artefact.maestro_id).encode(), Config.STREAM
        )
        await asyncio.wait_for(running_job_received, 5)
        await asyncio.wait_for(completed_job_received, 5)

    WorkerQueue.shutdown_gracefully()
    await asyncio.wait_for(WorkerQueue.loop_task, 5)


@pytest.mark.asyncio
async def test_on_input_received_waits_for_artefact_synchronization():
    running_job_received = asyncio.Future()
    completed_job_received = asyncio.Future()

    WorkerQueue.setup()
    WorkerQueue.run()

    async with NATSSession() as nats_session:
        await nats_session.add_or_update_stream(
            Config.STREAM, ["dummy_subject", "workflow_step.DummyWorkflowStep.*"]
        )
        await nats_session.subscribe(
            Config.STREAM,
            "dummy_subject",
            subscribers.on_input_received,
            config={"deliver_policy": "new"},
        )

        # Create a job with a required input artefact, and publish it,
        pending_job = Job(
            workflow_step=DummyWorkflowStep,
            inputs={"input": ("DummyArtefact", {"identifier": "dummy_identifier"})},
        )
        await subscribe_to_job_update(
            nats_session, pending_job.maestro_id, JobStatus.RUNNING, running_job_received
        )
        await subscribe_to_job_update(
            nats_session, pending_job.maestro_id, JobStatus.COMPLETED, completed_job_received
        )

        # Create workflow in database.
        workflow = WorkflowModel(
            workflow_name=f"{pending_job.workflow_step}",
            jobs=[pending_job],
        )
        async with Neo4jTransaction() as neo4j_tx:
            await create_node(neo4j_tx, workflow)

        # Publish a matching artefact, but do not add it to the database yet.
        matching_artefact = DummyArtefact(identifier="dummy_identifier")
        await nats_session.publish(
            "dummy_subject", str(matching_artefact.maestro_id).encode(), Config.STREAM
        )

        # Check that the job does not get started.
        with pytest.raises(asyncio.TimeoutError):
            await asyncio.wait_for(asyncio.shield(running_job_received), 0.5)

        # Add the artefact to the database. This simulates a delay in the artefact synchronization which can
        # happen if the artefact was created on a different instance and the database is not yet up-to-date.
        async with Neo4jTransaction() as neo4j_tx:
            await create_node(neo4j_tx, matching_artefact)

        # Check that the job gets started and completed.
        await asyncio.wait_for(running_job_received, 5)
        await asyncio.wait_for(completed_job_received, 5)

    WorkerQueue.shutdown_gracefully()
    await asyncio.wait_for(WorkerQueue.loop_task, 5)
