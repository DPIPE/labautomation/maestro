"""

Test that:

- neo4j transactions that write are also published to NATS upon commit
- neo4j transactions that only read are not published to NATS
- neo4j transactions that fail are not published to NATS
- neo4j transactions that are rolled back are not published to NATS

"""

import pytest

from maestro.config import Config
from maestro.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction, TransactionWriteQueries

WRITE_QUERY = "CREATE (n:Test {key: $value})"
WRITE_PARAMS = {"value": "value"}
READ_QUERY = "MATCH (n) RETURN n LIMIT 1"


@pytest.mark.asyncio
async def test_neo4j_commit_publish():
    "Test that neo4j transactions that write are also published to NATS upon commit"

    async with NATSSession() as nats_session:
        await nats_session.add_or_update_stream(
            Config.NEO4J_SYNC_STREAM, [Config.NEO4J_SYNC_SUBJECT]
        )
        subscriber = await nats_session.subscribe(
            Config.NEO4J_SYNC_STREAM,
            Config.NEO4J_SYNC_SUBJECT,
            cb=None,
            config={"deliver_policy": "new"},
        )

        # Run a transaction that both reads and writes
        async with Neo4jTransaction() as neo4j_tx:
            await neo4j_tx.query(READ_QUERY)
            await neo4j_tx.query(
                WRITE_QUERY,
                WRITE_PARAMS,
            )
            await neo4j_tx.query(READ_QUERY)

        # Get the published message
        [msg] = await subscriber.fetch(timeout=1.0)

        # Only one message should be published
        with pytest.raises(TimeoutError):
            await subscriber.fetch(timeout=1.0)

        # Check the message data, and that only the write query is included
        data = TransactionWriteQueries.model_validate_json(msg.data)
        assert len(data.queries) == 1
        assert data.queries[0].query == WRITE_QUERY
        assert data.queries[0].parameters == WRITE_PARAMS


@pytest.mark.asyncio
async def test_neo4j_commit_publish_multiple():
    "Test that multiple neo4j transactions that write are also published to NATS upon commit"

    async with NATSSession() as nats_session:
        await nats_session.add_or_update_stream(
            Config.NEO4J_SYNC_STREAM, [Config.NEO4J_SYNC_SUBJECT]
        )
        subscriber = await nats_session.subscribe(
            Config.NEO4J_SYNC_STREAM,
            Config.NEO4J_SYNC_SUBJECT,
            cb=None,
            config={"deliver_policy": "new"},
        )

        # Run multiple transactions that all contain multiple write queries
        write_queries = [
            "CREATE (n:Test1 {key: $value})",
            "MERGE (n:TestMerge1 {key: $value}) ON CREATE SET n.created = true ON MATCH SET n.matched = true",
            "CREATE (n:Test2 {key: $value})",
            "MERGE (n:TestMerge2 {key: $value}) ON CREATE SET n.created = true ON MATCH SET n.matched = true",
        ]
        write_params = [
            {"value": "value1"},
            {"value": "value2"},
            {"value": "value3"},
            {"value": "value4"},
        ]

        async with Neo4jTransaction() as neo4j_tx:
            await neo4j_tx.query(
                write_queries[0],
                write_params[0],
            )
            await neo4j_tx.query(
                write_queries[1],
                write_params[1],
            )

        async with Neo4jTransaction() as neo4j_tx:
            await neo4j_tx.query(
                write_queries[2],
                write_params[2],
            )
            await neo4j_tx.query(
                write_queries[3],
                write_params[3],
            )

        # Get the published messages
        [msg1, msg2] = await subscriber.fetch(2, timeout=1.0)

        # Check that index is incremented correctly
        assert msg2.metadata.sequence.stream == msg1.metadata.sequence.stream + 1

        # Only two messages should be published
        with pytest.raises(TimeoutError):
            await subscriber.fetch(timeout=1.0)

        # Check the message data, and that the write queries are included in the correct messages
        data1 = TransactionWriteQueries.model_validate_json(msg1.data)
        assert len(data1.queries) == 2
        assert data1.queries[0].query == write_queries[0]
        assert data1.queries[0].parameters == write_params[0]
        assert data1.queries[1].query == write_queries[1]
        assert data1.queries[1].parameters == write_params[1]

        data2 = TransactionWriteQueries.model_validate_json(msg2.data)
        assert len(data2.queries) == 2
        assert data2.queries[0].query == write_queries[2]
        assert data2.queries[0].parameters == write_params[2]
        assert data2.queries[1].query == write_queries[3]
        assert data2.queries[1].parameters == write_params[3]


@pytest.mark.asyncio
async def test_neo4j_no_publish(neo4j_tx: Neo4jTransaction):
    "Test that neo4j transactions that only read are not published to NATS"

    async with NATSSession() as nats_session:
        await nats_session.add_or_update_stream(
            Config.NEO4J_SYNC_STREAM, [Config.NEO4J_SYNC_SUBJECT]
        )
        subscriber = await nats_session.subscribe(
            Config.NEO4J_SYNC_STREAM,
            Config.NEO4J_SYNC_SUBJECT,
            cb=None,
            config={"deliver_policy": "new"},
        )

        # Run a transaction that only reads
        await neo4j_tx.query(READ_QUERY)

        # No message should be published
        with pytest.raises(TimeoutError):
            await subscriber.fetch(timeout=1.0)


@pytest.mark.asyncio
async def test_neo4j_fail_no_publish():
    "Test that neo4j transactions that fail are not published to NATS"

    async with NATSSession() as nats_session:
        await nats_session.add_or_update_stream(
            Config.NEO4J_SYNC_STREAM, [Config.NEO4J_SYNC_SUBJECT]
        )
        subscriber = await nats_session.subscribe(
            Config.NEO4J_SYNC_STREAM,
            Config.NEO4J_SYNC_SUBJECT,
            cb=None,
            config={"deliver_policy": "new"},
        )

        # Run a transaction that writes, but subsequently fails
        try:
            async with Neo4jTransaction() as neo4j_tx:
                await neo4j_tx.query(
                    WRITE_QUERY,
                    WRITE_PARAMS,
                )
                await neo4j_tx.query("RETURN 1/0")
        except Exception:
            pass

        # No message should be published
        with pytest.raises(TimeoutError):
            await subscriber.fetch(timeout=1.0)


@pytest.mark.asyncio
async def test_neo4j_rollback_no_publish(neo4j_tx: Neo4jTransaction):
    "Test that neo4j transactions that are rolled back are not published to NATS"

    async with NATSSession() as nats_session:
        await nats_session.add_or_update_stream(
            Config.NEO4J_SYNC_STREAM, [Config.NEO4J_SYNC_SUBJECT]
        )
        subscriber = await nats_session.subscribe(
            Config.NEO4J_SYNC_STREAM,
            Config.NEO4J_SYNC_SUBJECT,
            cb=None,
            config={"deliver_policy": "new"},
        )

        # Run a transaction that writes, but is rolled back
        await neo4j_tx.query(
            WRITE_QUERY,
            WRITE_PARAMS,
        )
        await neo4j_tx.rollback()

        # No message should be published
        with pytest.raises(TimeoutError):
            await subscriber.fetch(timeout=1.0)
