import asyncio

import pytest

from maestro import run_workflow_steps
from maestro.config import Config
from maestro.models import Job, JobStatus, WorkflowModel, WorkflowStepModelWithArtefacts
from maestro.neo4j.invalidate import invalidate_artefacts, invalidate_workflows
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import fetch_node_by_rel, read_node
from maestro.worker_queue import WorkerQueue
from maestro.workflow_utils import order_workflow
from tests.test_worker_queue import ChainedArtefact, ChainedStep


@pytest.mark.asyncio
@pytest.mark.parametrize("propagate_invalidation", [True, False])
async def test_invalidating_input_artefact_of_running_job_pipeline(
    propagate_invalidation, monkeypatch
):
    """
    Invalidate the input artefact of a running job pipeline and check that the jobs either get
    cancelled upon propagating invalidation or completes as normal upon non-propagating invalidation.
    """
    # Clear the shared variables in case another test has already used them.
    ChainedStep.started_count.value = 0
    ChainedStep.can_finish_count.value = 0
    ChainedStep.cancelled_count.value = 0

    # Limit the number of running workers to 1 to force one job to remain in queued state.
    monkeypatch.setattr(Config, "MAX_WORKERS", 1)

    maestro_main_task = await run_workflow_steps(
        message_watcher_classes=[ChainedStep],
        artefact_classes=[ChainedArtefact],
        file_watcher_classes=[],
    )

    # Start workflow consisting of four jobs with interdependencies.
    jobs = [
        # This job will run to completion and generate an output artefact.
        Job(
            workflow_step=ChainedStep,
            params={},
        ),
        # After the above job has completed, one job of the following two
        # jobs will be allowed to run while the other will be queued due to
        # MAX_WORKERS limit specified above.
        Job(
            workflow_step=ChainedStep,
            inputs={"input": (ChainedArtefact, {"sequence_number": 0})},
        ),
        Job(
            workflow_step=ChainedStep,
            inputs={"input": (ChainedArtefact, {"sequence_number": 0})},
        ),
        # The following job will be pending until one of the preceding jobs has completed,
        # which will only happen for the non-propagating invalidation case. The job will
        # remain pending for the propagating invalidation case.
        Job(
            workflow_step=ChainedStep,
            inputs={"input": (ChainedArtefact, {"sequence_number": 1})},
        ),
    ]
    await order_workflow(WorkflowModel(workflow_name="job_chain", jobs=jobs))

    async def wait_for_start_of_job_number(i):
        async with asyncio.timeout(5):
            while ChainedStep.started_count.value < i:
                await asyncio.sleep(0.1)
        assert ChainedStep.started_count.value == i

    async def allow_completion_of_job_number(i):
        with ChainedStep.can_finish_count.get_lock():
            ChainedStep.can_finish_count.value += 1

    # Wait for the first job to finish.
    await wait_for_start_of_job_number(1)
    await allow_completion_of_job_number(1)

    # Wait for the second job to start.
    await wait_for_start_of_job_number(2)

    # Check that the four jobs are in different states.
    async with Neo4jTransaction() as neo4j_tx:
        assert {
            status
            async for status in (
                (await read_node(neo4j_tx, Job, {"maestro_id": job.maestro_id})).status
                for job in jobs
            )
        } == {JobStatus.COMPLETED, JobStatus.PENDING, JobStatus.QUEUED, JobStatus.RUNNING}

    # Invalidate the first job's output artefact WHILE the second job is running.
    async with Neo4jTransaction() as neo4j_tx:
        output_artefact = (
            await fetch_node_by_rel(
                neo4j_tx, Job, {"maestro_id": jobs[0].maestro_id}, WorkflowStepModelWithArtefacts
            )
        ).OUTPUT[0]
        await invalidate_artefacts(
            neo4j_tx, [output_artefact.maestro_id], propagate=propagate_invalidation
        )

    if propagate_invalidation:
        # Expected side-effect of propagating invalidation is that the second and third jobs get
        # cancelled, so calling allow_completion_of_job_number() is not necessary in that case.
        # Wait for queued job to be cancelled.
        async with asyncio.timeout(5):
            while len(WorkerQueue.queue) > 0:
                await asyncio.sleep(0.1)
    else:
        # For non-propagating invalidation, the subsequent jobs will be scheduled to run as normal
        # so need to be allowed to complete.
        await allow_completion_of_job_number(2)
        await wait_for_start_of_job_number(3)
        await allow_completion_of_job_number(3)
        await wait_for_start_of_job_number(4)
        await allow_completion_of_job_number(4)

    # Gracefully shut down the worker queue to make sure the following database inspection
    # happens after the last job has had all its information written to the database.
    WorkerQueue.shutdown_gracefully()
    await asyncio.wait_for(maestro_main_task, 5)

    if propagate_invalidation:
        # Check that the running job was cancelled. The queued job may have had time to start
        # after the originally running was cancelled, so accept a cancelled count of 1 or 2.
        assert ChainedStep.cancelled_count.value in (1, 2)

        # Check that the two middle jobs were cancelled.
        async with Neo4jTransaction() as neo4j_tx:
            assert {
                status
                async for status in (
                    (await read_node(neo4j_tx, Job, {"maestro_id": job.maestro_id})).status
                    for job in jobs[1:3]
                )
            } == {JobStatus.CANCELLED}

        # Check that the last job remain pending.
        async with Neo4jTransaction() as neo4j_tx:
            assert (
                await read_node(neo4j_tx, Job, {"maestro_id": jobs[3].maestro_id})
            ).status == JobStatus.PENDING
    else:
        # Check that no running job got cancelled.
        assert ChainedStep.cancelled_count.value == 0

        # Check that all jobs completed.
        async with Neo4jTransaction() as neo4j_tx:
            assert {
                status
                async for status in (
                    (await read_node(neo4j_tx, Job, {"maestro_id": job.maestro_id})).status
                    for job in jobs
                )
            } == {JobStatus.COMPLETED}

    async with Neo4jTransaction() as neo4j_tx:
        for j in range(1, 3 if propagate_invalidation else 4):
            db_workflowstep = await fetch_node_by_rel(
                neo4j_tx, Job, {"maestro_id": jobs[j].maestro_id}, WorkflowStepModelWithArtefacts
            )
            if propagate_invalidation:
                # Check that the subsequent jobs did not produce any output artefact.
                assert len(db_workflowstep.OUTPUT) == 0
            else:
                # Check that the subsequent jobs completed as normal.
                assert len(db_workflowstep.OUTPUT) == 1
                assert db_workflowstep.OUTPUT[0].invalidated_at is None


@pytest.mark.asyncio
async def test_invalidating_workflow_cancels_its_jobs(monkeypatch):
    """
    Invalidate a workflow with one pending, one queued, and one running job and check that they all get cancelled.
    """
    # Clear the shared variables in case another test has already used them.
    ChainedStep.started_count.value = 0
    ChainedStep.cancelled_count.value = 0

    # Limit the number of running workers to 1 to force one job to remain in queued state.
    monkeypatch.setattr(Config, "MAX_WORKERS", 1)

    maestro_main_task = await run_workflow_steps(
        message_watcher_classes=[ChainedStep],
        artefact_classes=[ChainedArtefact],
        file_watcher_classes=[],
    )

    # Order workflow with three jobs, all of which will end up being cancelled due to
    # workflow invalidation.
    workflow = WorkflowModel(
        workflow_name="job_chain",
        jobs=[
            # One job of the following two jobs will be allowed to run while the other
            # will be queued due to MAX_WORKERS limit specified above.
            Job(
                workflow_step=ChainedStep,
                params={},
            ),
            Job(
                workflow_step=ChainedStep,
                params={},
            ),
            # The following job will remain pending due to lack of input artefact, until
            # it is eventually cancelled as a consequence of workflow invalidation.
            Job(
                workflow_step=ChainedStep,
                inputs={"input": (ChainedArtefact, {"sequence_number": 0})},
            ),
        ],
    )
    await order_workflow(workflow)

    # Wait for one job to start running.
    async with asyncio.timeout(5):
        while ChainedStep.started_count.value < 1:
            await asyncio.sleep(0.1)
    assert ChainedStep.started_count.value == 1

    # Check that the three jobs are in different states.
    async with Neo4jTransaction() as neo4j_tx:
        assert {
            status
            async for status in (
                (await read_node(neo4j_tx, Job, {"maestro_id": job.maestro_id})).status
                for job in workflow.jobs
            )
        } == {JobStatus.PENDING, JobStatus.QUEUED, JobStatus.RUNNING}

    # Invalidate the workflow WHILE jobs are in different states.
    async with Neo4jTransaction() as neo4j_tx:
        await invalidate_workflows(neo4j_tx, [workflow.maestro_id])

    # Wait for queued job to be cancelled.
    async with asyncio.timeout(5):
        while len(WorkerQueue.queue) > 0:
            await asyncio.sleep(0.1)

    # Gracefully shut down the worker queue to make sure the running job cancellation has completed.
    WorkerQueue.shutdown_gracefully()
    await asyncio.wait_for(maestro_main_task, 5)

    # Check that the running job got cancelled.
    assert ChainedStep.cancelled_count.value == 1

    # Check that the three jobs are all reported as cancelled in the database.
    async with Neo4jTransaction() as neo4j_tx:
        assert {
            status
            async for status in (
                (await read_node(neo4j_tx, Job, {"maestro_id": job.maestro_id})).status
                for job in workflow.jobs
            )
        } == {JobStatus.CANCELLED}
