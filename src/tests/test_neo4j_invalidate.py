import asyncio

import pytest
from neo4j.time import DateTime

from maestro import run_workflow_steps
from maestro.models import (
    BaseArtefactModel,
    Job,
    JobStatus,
    MaestroBase,
    WorkflowModel,
    WorkflowStepModel,
)
from maestro.nats.nats import NATSSession
from maestro.neo4j.invalidate import (
    invalidate_artefacts,
    invalidate_workflow_steps,
    invalidate_workflows,
)
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import create_node, create_relationship
from maestro.utils import subscribe_to_job_update
from maestro.workflow_utils import order_workflow
from tests.conftest import (
    DummyArtefact,
    DummyWorkflowStep,
    fetch_unique_node,
)


@pytest.mark.asyncio
async def test_invalidate_artefact(neo4j_tx: Neo4jTransaction):
    test_artefact = DummyArtefact(invalidated_at=None)
    maestro_id = await create_node(neo4j_tx, test_artefact)
    fetched = await fetch_unique_node(neo4j_tx, ["DummyArtefact"], {"maestro_id": maestro_id})
    assert "invalidated_at" not in fetched
    await invalidate_artefacts(neo4j_tx, [test_artefact.maestro_id])
    fetched = await fetch_unique_node(neo4j_tx, ["DummyArtefact"], {"maestro_id": maestro_id})
    assert isinstance(fetched["invalidated_at"], DateTime)


@pytest.mark.asyncio
async def test_invalidate_nested_artefact(neo4j_tx: Neo4jTransaction) -> None:
    """
    Test invalidating an artefact with nested artefacts

    Add another non-artefact-node in between to test that only directly related artefacts

    N1 - A1 - N2
          |
          B
          |
          A2

    A1 is the root artefact, with N1 and N2 as nested artefacts
    A2 is not directly related to the artefact, and should not be invalidated
    """

    class TestNestedModel(BaseArtefactModel):
        foo: int

    class TestBaseModel(BaseArtefactModel):
        bar: int
        nested1: TestNestedModel
        nested2: TestNestedModel

        model_config = {
            "arbitrary_types_allowed": True,
        }

    class NonArtefact(MaestroBase):
        pass

    nested1 = TestNestedModel(foo=1)
    nested2 = TestNestedModel(foo=2)
    base_model = TestBaseModel(bar=-1, nested1=nested1, nested2=nested2)
    base_maestro_id = await create_node(neo4j_tx, base_model)

    non_artefact = NonArtefact()
    non_artefact_maestro_id = await create_node(neo4j_tx, non_artefact)
    await create_relationship(neo4j_tx, non_artefact_maestro_id, base_maestro_id, "NON_ARTEFACT")
    non_related_artefact = TestNestedModel(foo=3)
    non_related_artefact_maestro_id = await create_node(neo4j_tx, non_related_artefact)
    await create_relationship(
        neo4j_tx,
        non_related_artefact_maestro_id,
        non_artefact_maestro_id,
        "NON_RELATED_ARTEFACT",
    )

    fetched = await fetch_unique_node(neo4j_tx, ["TestBaseModel"], {"maestro_id": base_maestro_id})
    assert "invalidated_at" not in fetched

    # Invalidate artefact
    await invalidate_artefacts(neo4j_tx, [base_model.maestro_id])

    # Check that artefact, and nested artefacts are invalidated
    fetched = await fetch_unique_node(neo4j_tx, ["TestBaseModel"], {"maestro_id": base_maestro_id})
    assert isinstance(fetched["invalidated_at"], DateTime)
    fetched_nested1 = await fetch_unique_node(
        neo4j_tx, ["TestNestedModel"], {"maestro_id": nested1.maestro_id}
    )
    assert isinstance(fetched_nested1["invalidated_at"], DateTime)
    fetched_nested2 = await fetch_unique_node(
        neo4j_tx, ["TestNestedModel"], {"maestro_id": nested2.maestro_id}
    )
    assert isinstance(fetched_nested2["invalidated_at"], DateTime)

    # Check that non-related artefact is not invalidated
    fetched_non_related = await fetch_unique_node(
        neo4j_tx, ["TestNestedModel"], {"maestro_id": non_related_artefact_maestro_id}
    )
    assert "invalidated_at" not in fetched_non_related
    fetched_non_artefact = await fetch_unique_node(neo4j_tx, ["NonArtefact"], {})
    assert "invalidated_at" not in fetched_non_artefact


@pytest.mark.asyncio
@pytest.mark.parametrize("propagate", [True, False])
async def test_invalidate_artefact_propagate(neo4j_tx: Neo4jTransaction, propagate: bool):
    """
    Test that invalidating an artefact with propagate=True invalidates all workflow steps that use the artefact
    as input (and vice versa if propagate=False)
    """

    class TestModel(BaseArtefactModel):
        foo: int

    artefact = TestModel(foo=1)
    artefact_maestro_id = await create_node(neo4j_tx, artefact)
    workflow_step = WorkflowStepModel(additional_labels=["DummyWorkflowStep"])
    workflow_step_maestro_id = await create_node(neo4j_tx, workflow_step)

    await create_relationship(neo4j_tx, artefact_maestro_id, workflow_step_maestro_id, "INPUT")

    fetched = await fetch_unique_node(neo4j_tx, ["TestModel"], {"maestro_id": artefact_maestro_id})
    assert "invalidated_at" not in fetched
    fetched_workflow_step = await fetch_unique_node(
        neo4j_tx, ["DummyWorkflowStep"], {"maestro_id": workflow_step_maestro_id}
    )
    assert "invalidated_at" not in fetched_workflow_step
    await invalidate_artefacts(neo4j_tx, [artefact.maestro_id], propagate=propagate)

    fetched = await fetch_unique_node(neo4j_tx, ["TestModel"], {"maestro_id": artefact_maestro_id})
    assert isinstance(fetched["invalidated_at"], DateTime)
    fetched_workflow_step = await fetch_unique_node(
        neo4j_tx, ["DummyWorkflowStep"], {"maestro_id": workflow_step_maestro_id}
    )
    if propagate:
        assert fetched_workflow_step["invalidated_at"]
    else:
        assert "invalidated_at" not in fetched_workflow_step


@pytest.mark.asyncio
@pytest.mark.parametrize("propagate", [True, False])
async def test_invalidate_artefact_invalidates_workflow_step_when_all_outputs_invalidated(
    neo4j_tx: Neo4jTransaction, propagate: bool
):
    """
    Test that invalidating an artefact also invalidates workflow step
    if all the other outputs are already invalidated
    """
    # Create workflow step with multiple output artefacts
    workflow_step = WorkflowStepModel(additional_labels=["DummyWorkflowStep"])
    workflow_step_maestro_id = await create_node(neo4j_tx, workflow_step)

    class TestModel(BaseArtefactModel):
        foo: int

    # Create input artefact
    artefact_input = TestModel(foo=1)
    artefact_input_maestro_id = await create_node(neo4j_tx, artefact_input)

    # Create output artefacts
    artefact_output1 = TestModel(foo=2)
    artefact_output1_maestro_id = await create_node(neo4j_tx, artefact_output1)
    artefact_output2 = TestModel(foo=3)
    artefact_output2_maestro_id = await create_node(neo4j_tx, artefact_output2)
    artefact_output3 = TestModel(foo=4)
    artefact_output3_maestro_id = await create_node(neo4j_tx, artefact_output3)

    # Connect artefacts to workflow step
    await create_relationship(
        neo4j_tx, artefact_input_maestro_id, workflow_step_maestro_id, "INPUT"
    )
    await create_relationship(
        neo4j_tx, workflow_step_maestro_id, artefact_output1_maestro_id, "OUTPUT"
    )
    await create_relationship(
        neo4j_tx, workflow_step_maestro_id, artefact_output2_maestro_id, "OUTPUT"
    )
    await create_relationship(
        neo4j_tx, workflow_step_maestro_id, artefact_output3_maestro_id, "OUTPUT"
    )
    # Create a second workflow step that uses output2 as input
    workflow_step2 = WorkflowStepModel(additional_labels=["DummyWorkflowStep"])
    workflow_step2_maestro_id = await create_node(neo4j_tx, workflow_step2)

    # Create output artefact for second workflow step
    artefact_output4 = TestModel(foo=5)
    artefact_output4_maestro_id = await create_node(neo4j_tx, artefact_output4)

    # Connect artefacts to second workflow step
    await create_relationship(
        neo4j_tx, artefact_output2_maestro_id, workflow_step2_maestro_id, "INPUT"
    )
    await create_relationship(
        neo4j_tx, workflow_step2_maestro_id, artefact_output4_maestro_id, "OUTPUT"
    )

    # Create another artefact and workflow step
    artefact_input2 = TestModel(foo=6)
    artefact_input2_maestro_id = await create_node(neo4j_tx, artefact_input2)
    artefact_output5 = TestModel(foo=7)
    artefact_output5_maestro_id = await create_node(neo4j_tx, artefact_output5)
    workflow_step3 = WorkflowStepModel(additional_labels=["DummyWorkflowStep"])
    workflow_step3_maestro_id = await create_node(neo4j_tx, workflow_step3)
    # Connect artefact to workflow step
    await create_relationship(
        neo4j_tx, artefact_input2_maestro_id, workflow_step3_maestro_id, "INPUT"
    )
    await create_relationship(
        neo4j_tx, workflow_step3_maestro_id, artefact_output5_maestro_id, "OUTPUT"
    )

    # Initially nothing should be invalidated
    db_workflow_step = await fetch_unique_node(
        neo4j_tx, ["DummyWorkflowStep"], {"maestro_id": workflow_step_maestro_id}
    )
    assert "invalidated_at" not in db_workflow_step

    for maestro_id in [
        artefact_output1_maestro_id,
        artefact_output2_maestro_id,
        artefact_output3_maestro_id,
        artefact_output4_maestro_id,
    ]:
        db_artefact = await fetch_unique_node(neo4j_tx, ["TestModel"], {"maestro_id": maestro_id})
        assert "invalidated_at" not in db_artefact

    # Verify that invalidating the input artefact does not invalidate the workflow step
    await invalidate_artefacts(neo4j_tx, [artefact_input_maestro_id])
    db_workflow_step = await fetch_unique_node(
        neo4j_tx, ["DummyWorkflowStep"], {"maestro_id": workflow_step_maestro_id}
    )
    assert "invalidated_at" not in db_workflow_step

    # Invalidate first artefact
    await invalidate_artefacts(neo4j_tx, [artefact_output1_maestro_id], propagate=propagate)

    # First artefact should be invalidated
    db_artefact1 = await fetch_unique_node(
        neo4j_tx, ["TestModel"], {"maestro_id": artefact_output1_maestro_id}
    )
    assert isinstance(db_artefact1["invalidated_at"], DateTime)
    # The workflow step should not be invalidated yet
    db_workflow_step = await fetch_unique_node(
        neo4j_tx, ["DummyWorkflowStep"], {"maestro_id": workflow_step_maestro_id}
    )
    assert "invalidated_at" not in db_workflow_step

    # Invalidate second artefact
    await invalidate_artefacts(neo4j_tx, [artefact_output2_maestro_id], propagate=propagate)

    # Second artefact should be invalidated
    db_artefact2 = await fetch_unique_node(
        neo4j_tx, ["TestModel"], {"maestro_id": artefact_output2_maestro_id}
    )
    assert isinstance(db_artefact2["invalidated_at"], DateTime)

    # Workflow step should not be invalidated yet
    db_workflow_step = await fetch_unique_node(
        neo4j_tx, ["DummyWorkflowStep"], {"maestro_id": workflow_step_maestro_id}
    )
    assert "invalidated_at" not in db_workflow_step

    # Check that the other artefacts and workflow steps are not invalidated
    for maestro_id in [artefact_input2_maestro_id, artefact_output5_maestro_id]:
        db_artefact = await fetch_unique_node(neo4j_tx, ["TestModel"], {"maestro_id": maestro_id})
        assert "invalidated_at" not in db_artefact

    db_workflow_step3 = await fetch_unique_node(
        neo4j_tx, ["DummyWorkflowStep"], {"maestro_id": workflow_step3_maestro_id}
    )
    assert "invalidated_at" not in db_workflow_step3

    # If propagate is True, check that workflow step 2 and its output are invalidated
    if propagate:
        # Second workflow step should be invalidated since its input is invalidated
        db_workflow_step2 = await fetch_unique_node(
            neo4j_tx, ["DummyWorkflowStep"], {"maestro_id": workflow_step2_maestro_id}
        )
        assert isinstance(db_workflow_step2["invalidated_at"], DateTime)

        # Output of second workflow step should be invalidated
        db_artefact4 = await fetch_unique_node(
            neo4j_tx, ["TestModel"], {"maestro_id": artefact_output4_maestro_id}
        )
        assert isinstance(db_artefact4["invalidated_at"], DateTime)


@pytest.mark.asyncio
async def test_invalidate_workflow_steps(neo4j_tx: Neo4jTransaction):
    """
    Test that invalidating a workflow step with artefacts invalidates the produced artefacts as well
    """

    class TestModel(BaseArtefactModel):
        foo: int

    # Generate input and output artefacts
    artefact_output1 = TestModel(foo=1)
    artefact_output1_maestro_id = await create_node(neo4j_tx, artefact_output1)
    artefact_output2 = TestModel(foo=2)
    artefact_output2_maestro_id = await create_node(neo4j_tx, artefact_output2)
    artefact_input1 = TestModel(foo=3)
    artefact_input1_maestro_id = await create_node(neo4j_tx, artefact_input1)
    artefact_input2 = TestModel(foo=4)
    artefact_input2_maestro_id = await create_node(neo4j_tx, artefact_input2)

    # Create workflow step, and add relationship to artefacts
    workflow_step = WorkflowStepModel(additional_labels=["DummyWorkflowStep"])
    workflow_step_maestro_id = await create_node(neo4j_tx, workflow_step)

    await create_relationship(
        neo4j_tx, workflow_step_maestro_id, artefact_output1_maestro_id, "TEST_OUTPUT"
    )
    await create_relationship(
        neo4j_tx, workflow_step_maestro_id, artefact_output2_maestro_id, "TEST_OUTPUT"
    )
    await create_relationship(
        neo4j_tx, artefact_input1_maestro_id, workflow_step_maestro_id, "TEST_INPUT"
    )
    await create_relationship(
        neo4j_tx, artefact_input2_maestro_id, workflow_step_maestro_id, "TEST_INPUT"
    )

    # Check that artefacts are not invalidated
    for maestro_id in [artefact_output1_maestro_id, artefact_output2_maestro_id]:
        fetched = await fetch_unique_node(neo4j_tx, ["TestModel"], {"maestro_id": maestro_id})
        assert "invalidated_at" not in fetched

    for maestro_id in [artefact_input1_maestro_id, artefact_input2_maestro_id]:
        fetched = await fetch_unique_node(neo4j_tx, ["TestModel"], {"maestro_id": maestro_id})
        assert "invalidated_at" not in fetched

    fetched_workflow_step = await fetch_unique_node(
        neo4j_tx, ["DummyWorkflowStep"], {"maestro_id": workflow_step_maestro_id}
    )
    assert "invalidated_at" not in fetched_workflow_step

    # Invalidate workflow step
    await invalidate_workflow_steps(neo4j_tx, [workflow_step.maestro_id])

    # Check that output artefacts are invalidated, and that input artefacts are not
    for maestro_id in [artefact_output1_maestro_id, artefact_output2_maestro_id]:
        fetched = await fetch_unique_node(neo4j_tx, ["TestModel"], {"maestro_id": maestro_id})
        assert isinstance(fetched["invalidated_at"], DateTime)

    for maestro_id in [artefact_input1_maestro_id, artefact_input2_maestro_id]:
        fetched = await fetch_unique_node(neo4j_tx, ["TestModel"], {"maestro_id": maestro_id})
        assert "invalidated_at" not in fetched

    fetched_workflow_step = await fetch_unique_node(
        neo4j_tx, ["DummyWorkflowStep"], {"maestro_id": workflow_step_maestro_id}
    )
    assert isinstance(fetched_workflow_step["invalidated_at"], DateTime)


@pytest.mark.asyncio
async def test_invalidate_workflow():
    async def add_dummy_workflow(suffix: str) -> WorkflowModel:
        input_id_1 = f"input1_{suffix}"
        input_id_2 = f"input2_{suffix}"
        output_id_1 = f"output1_1_{suffix}"

        workflow = WorkflowModel(
            workflow_name="dummy_workflow",
            jobs=[
                Job(
                    workflow_step=DummyWorkflowStep,
                    inputs={"input": ("DummyArtefact", {"identifier": input_id_1})},
                    params={"outputs": [output_id_1, "output1_2"]},
                ),
                Job(
                    workflow_step=DummyWorkflowStep,
                    inputs={"input": ("DummyArtefact", {"identifier": input_id_2})},
                    params={"outputs": ["output2_1", "output2_2"]},
                ),
                Job(
                    workflow_step=DummyWorkflowStep,
                    inputs={"input": ("DummyArtefact", {"identifier": output_id_1})},
                    params={"outputs": ["output3_1"]},
                ),
            ],
        )
        input_artefact1 = DummyArtefact(identifier=input_id_1)
        input_artefact2 = DummyArtefact(identifier=input_id_2)

        async with Neo4jTransaction() as neo4j_tx:
            await create_node(neo4j_tx, input_artefact1)
            await create_node(neo4j_tx, input_artefact2)

        return workflow

    await run_workflow_steps(
        artefact_classes=[DummyArtefact],
        message_watcher_classes=[DummyWorkflowStep],
        file_watcher_classes=[],
    )

    workflow_invalidated = await add_dummy_workflow("invalidated")
    workflow_non_invalidated = await add_dummy_workflow("non_invalidated")

    # Make sure all jobs are completed
    async with NATSSession() as nats_session:
        job_futures = []
        for job in workflow_invalidated.jobs | workflow_non_invalidated.jobs:
            job_future = asyncio.Future()
            await subscribe_to_job_update(
                nats_session, job.maestro_id, JobStatus.COMPLETED, job_future
            )
            job_futures.append(asyncio.wait_for(job_future, 10))

        await order_workflow(workflow_invalidated)
        await order_workflow(workflow_non_invalidated)

        # Wait for all job futures concurrently
        await asyncio.gather(*job_futures)

    # Invalidate the workflow
    async with Neo4jTransaction() as neo4j_tx:
        await invalidate_workflows(neo4j_tx, [workflow_invalidated.maestro_id])

        # Get all nodes generated from the workflow - except Job nodes which are not invalidated
        # Do not include nodes connected with INPUT relationships as they are not tied to the workflow
        query = 'MATCH (wf:WorkflowModel {maestro_id: $maestro_id})-[:JOB|WORKFLOW_STEP|OUTPUT|STDERR|STDOUT*]->(leaf) WHERE NOT "Job" IN labels(leaf) RETURN wf,leaf;'

        # Check that all nodes in the invalidated workflow are invalidated
        result = await neo4j_tx.query(
            query,
            {"maestro_id": workflow_invalidated.maestro_id},
        )
        assert len(result) > 0

        for leaf in result:
            assert isinstance(leaf["wf"]["invalidated_at"], DateTime)
            assert isinstance(leaf["leaf"]["invalidated_at"], DateTime)

        # Check that no nodes in the non-invalidated workflow are invalidated
        result = await neo4j_tx.query(
            query,
            {"maestro_id": workflow_non_invalidated.maestro_id},
        )
        assert len(result) > 0

        for leaf in result:
            assert "invalidated_at" not in leaf["wf"]
            assert "invalidated_at" not in leaf["leaf"]
