import asyncio
import sys
import time
from pathlib import Path

import pytest

from maestro import run_workflow_steps
from maestro.models import Job, JobStatus, WorkflowStepModelWithArtefacts
from maestro.nats.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import read_nodes

# Add demo to sys.path
sys.path.append(str(Path(__file__).parent.parent.parent / "demo"))

from demo import (
    FileWatcherSetup,
    InputB,
    StepA,
    StepB,
    StepC,
    create_artefact,
    identifier,
    order_steps,
    reset_all,
)


# Skip sleeping in demo to speed up test execution
@pytest.fixture(autouse=True)
def no_sleep():
    original_sleep = time.sleep
    time.sleep = lambda x: None
    yield
    time.sleep = original_sleep


@pytest.mark.asyncio
async def test_demo():
    await reset_all()
    path = Path("/tmp/maestro-demo")
    path.mkdir(exist_ok=True)
    file = path / f"{identifier}"
    if file.exists():
        file.unlink()
    await run_workflow_steps(file_watcher_classes=[FileWatcherSetup])
    await order_steps([StepA, StepB, StepC], identifier)
    future = asyncio.Future()

    async def all_done(msg):
        await asyncio.sleep(0.5)
        future.set_result(True)

    async with NATSSession() as nats_session:
        await nats_session.subscribe("test_stream", "artefact.OutputC", all_done)

        # Touch input A file - FileWatcher will order all jobs
        file.touch()

        # Create input B
        await create_artefact(InputB, identifier)
        await asyncio.wait_for(future, timeout=25)

        async with Neo4jTransaction() as neo4j_tx:
            jobs_step_a = await read_nodes(neo4j_tx, Job, {"workflow_step": "StepA"})
            jobs_step_b = await read_nodes(neo4j_tx, Job, {"workflow_step": "StepB"})
            jobs_step_c = await read_nodes(neo4j_tx, Job, {"workflow_step": "StepC"})

        assert len(jobs_step_a) == 1
        assert len(jobs_step_b) == 1
        assert len(jobs_step_c) == 1

        assert jobs_step_a[0].status == JobStatus.COMPLETED
        assert jobs_step_b[0].status == JobStatus.COMPLETED
        assert jobs_step_c[0].status == JobStatus.COMPLETED

    # Check that all nodes with relationships exist in the database
    """
    (FileWatcher) -> [InputA] -> (StepA) -> [OutputA] -> (StepB) -> [OutputB] -> (StepC) -> [OutputC]
                                                            |                            -> [OutputD]
                                                        [InputB]
    """

    async with Neo4jTransaction() as neo4j_tx:
        workflow_steps = await read_nodes(neo4j_tx, WorkflowStepModelWithArtefacts, {})

        # We should have 4 workflow steps
        assert len(workflow_steps) == 4

        # Will raise StopIteration if doesn't exist
        file_watcher = next(step for step in workflow_steps if step.type == "FileWatcherSetup")
        step_a = next(step for step in workflow_steps if step.type == "StepA")
        step_b = next(step for step in workflow_steps if step.type == "StepB")
        step_c = next(step for step in workflow_steps if step.type == "StepC")

        # All steps whould have stderr and stdout
        assert all(step.STDERR is not None for step in workflow_steps)
        assert all(step.STDOUT is not None for step in workflow_steps)

        # We should have 5 other artefacts, corresponding to the 2 inputs and 3 outputs
        assert {"InputA"} == {x.type for x in file_watcher.OUTPUT}
        assert {"InputA"} == {x.type for x in step_a.INPUT}
        assert {"OutputA"} == {x.type for x in step_a.OUTPUT}
        assert {"InputB", "OutputA"} == {x.type for x in step_b.INPUT}
        assert {"OutputB"} == {x.type for x in step_b.OUTPUT}
        assert {"OutputB"} == {x.type for x in step_b.OUTPUT}
        assert {"OutputC", "OutputD"} == {x.type for x in step_c.OUTPUT}

        jobs = await read_nodes(neo4j_tx, Job, {})
        assert len(jobs) == 4
        assert all(j.status == JobStatus.COMPLETED for j in jobs)
