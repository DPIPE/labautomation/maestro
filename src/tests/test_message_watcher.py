import asyncio
import re

import nats.aio.msg
import pytest

from maestro import run_workflow_steps
from maestro.config import Config
from maestro.exceptions import SetupError
from maestro.models import (
    BaseArtefactModel,
    Job,
    JobStatus,
    WorkflowModel,
    WorkflowStepModelWithArtefacts,
)
from maestro.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import create_node, read_nodes
from maestro.utils import subscribe_to_job_update
from maestro.worker_queue import WorkerQueue
from maestro.workflow_step.message_watcher import MessageWatcher
from maestro.workflow_step.workflow_step import WorkflowStepBase
from maestro.workflow_utils import order_workflow


class ArtefactWithIdentifier(BaseArtefactModel):
    identifier: str


class TestInput(ArtefactWithIdentifier):
    """Input artefact for testing"""

    # Do not collect this class for pytest
    __test__ = False
    another_id: str | None = None


class TestNestedInput(ArtefactWithIdentifier):
    """Input artefact with nested artefact"""

    # Do not collect this class for pytest
    __test__ = False
    nested_artefact: TestInput | None = None
    nested_artefacts_list: list[TestInput]


class TestNestedInputList(ArtefactWithIdentifier):
    """Input artefact with nested artefact"""

    # Do not collect this class for pytest
    __test__ = False
    nested_input_list: list[TestNestedInput]


class TestOutput(ArtefactWithIdentifier):
    """Output artefact for testing"""

    # Do not collect this class for pytest
    __test__ = False


class TestStep(MessageWatcher):
    """Workflow step for testing"""

    # Do not collect this class for pytest
    __test__ = False

    foo: str

    async def process(self, input: TestInput, **kwargs):
        # Test that the input is a TestInput artefact
        # with the right sample id
        assert input.identifier == "person"

        output = TestOutput(identifier=input.identifier)
        return [output]


class TestNestedStep(MessageWatcher):
    """Workflow step for testing nested artefact input"""

    # Do not collect this class for pytest
    __test__ = False

    async def process(self, input: TestNestedInputList, **kwargs):
        # Test that the input is a TestInput artefact
        # with the right sample id
        assert input.identifier == "person"
        assert len(input.nested_input_list) == 2
        assert input.nested_input_list[0].identifier == "nested_artefact_1"
        assert input.nested_input_list[1].identifier == "nested_artefact_2"
        assert input.nested_input_list[0].nested_artefact
        assert input.nested_input_list[0].nested_artefact.identifier == "artefact_1"
        assert input.nested_input_list[0].nested_artefacts_list[0].identifier == "artefact_2"
        assert input.nested_input_list[0].nested_artefacts_list[1].identifier == "artefact_3"
        assert input.nested_input_list[1].nested_artefacts_list[0].identifier == "artefact_1"
        output = TestOutput(identifier=input.identifier)
        return [output]


@pytest.mark.asyncio
async def test_message_watcher_job_queue():
    """
    Create a message watcher that adds a job to the queue, then adds an input
    triggering the message watcher to run.
    """

    async with NATSSession() as nats_session:
        # Listen for the process to finish
        on_process_done_called = asyncio.Future()
        pending_future = asyncio.Future()
        output = {}

        async def on_process_done(msg: nats.aio.msg.Msg):
            """
            Callback for when the process is done

            Set future to true and update output
            """
            output["subject"] = msg.subject
            on_process_done_called.set_result(True)

        # Run the workflow step continuously in the background
        await run_workflow_steps(
            message_watcher_classes=[TestStep],
            artefact_classes=[TestInput, TestOutput],
            file_watcher_classes=[],
        )

        # Add subscription to the output artefact
        await nats_session.subscribe(Config.STREAM, "artefact.TestOutput", on_process_done)

        # Make sure the job list is empty
        async with Neo4jTransaction() as neo4j_tx:
            jobs = await read_nodes(neo4j_tx, Job, {})
            assert jobs == []

        job = Job(
            inputs={"input": ("TestInput", {"identifier": "person", "another_id": "person_id"})},
            params={"foo": "bar"},
            workflow_step=TestStep,
        )
        await subscribe_to_job_update(
            nats_session, job.maestro_id, JobStatus.PENDING, pending_future
        )
        await order_workflow(
            WorkflowModel(
                workflow_name="test_workflow",
                jobs=[job],
            )
        )

        # After the job is added, the job list should contain the job
        await asyncio.wait_for(pending_future, timeout=3.0)

        async with Neo4jTransaction() as neo4j_tx:
            jobs = await read_nodes(neo4j_tx, Job, {"workflow_step": TestStep.__name__})
        assert len(jobs) == 1
        assert jobs[0].status == JobStatus.PENDING
        assert jobs[0].inputs == {
            "input": (TestInput, {"identifier": "person", "another_id": "person_id"})
        }
        assert jobs[0].params == {"foo": "bar"}

        # Add the input artefact
        artefact = TestInput(identifier="person", another_id="person_id")
        async with Neo4jTransaction() as neo4j_tx:
            await create_node(neo4j_tx, artefact)

        await nats_session.publish(f"artefact.{artefact.type}", str(artefact.maestro_id).encode())
        await asyncio.wait_for(on_process_done_called, timeout=2.0)

        WorkerQueue.shutdown_gracefully()
        await asyncio.wait_for(WorkerQueue.loop_task, 1)

        # Make sure the job is completed
        async with Neo4jTransaction() as neo4j_tx:
            jobs = await read_nodes(neo4j_tx, Job, {"workflow_step": TestStep.__name__})
        assert len(jobs) == 1
        assert jobs[0].status == JobStatus.COMPLETED
        assert jobs[0].inputs == {
            "input": (TestInput, {"identifier": "person", "another_id": "person_id"})
        }
        assert jobs[0].params == {"foo": "bar"}

    # Check that the output is correct
    assert output != {}
    assert output["subject"] == "artefact.TestOutput"

    # Check that the process, input and output artefacts are in the database
    # and that the input and output artefacts are connected to the process
    async with Neo4jTransaction() as neo4j_tx:
        nodes = await read_nodes(neo4j_tx, WorkflowStepModelWithArtefacts, {})
        assert len(nodes) == 1
        node = nodes[0]
        assert node.foo == "bar"  # type: ignore[attr-defined]
        assert len(node.INPUT) == 1
        assert len(node.OUTPUT) == 1
        assert node.INPUT[0].identifier == "person"  # type: ignore[attr-defined]
        assert node.OUTPUT[0].identifier == "person"  # type: ignore[attr-defined]


def test_message_watcher_setup_errors():
    with pytest.raises(
        SetupError, match="MessageWatcher subclasses must implement a process method"
    ):

        class TestMessageWatcherIncomplete(MessageWatcher):
            pass

    with pytest.raises(
        SetupError,
        match=re.escape(
            "A MessageWatcher's process method must be defined with `async def process(self, ...)`"
        ),
    ):

        class TestMessageWatcherInvalid(MessageWatcher):
            def process(self, *args, **kwargs):
                pass

    # Correct implementation shouldn't raise any errors
    class TestMessageWatcherValid(MessageWatcher):
        async def process(self):
            pass

    assert "TestMessageWatcherValid" in WorkflowStepBase._registered_subclasses
    assert (
        WorkflowStepBase._registered_subclasses["TestMessageWatcherValid"]
        is TestMessageWatcherValid
    )


@pytest.mark.asyncio
async def test_message_watcher_step_with_nested_artefacts():
    async with NATSSession() as nats_session:
        # Listen for the process to finish
        on_process_done_called = asyncio.Future()
        pending_future = asyncio.Future()
        output = {}

        async def on_process_done(msg: nats.aio.msg.Msg):
            """
            Callback for when the process is done

            Set future to true and update output
            """
            output["subject"] = msg.subject
            on_process_done_called.set_result(True)

        # Run the workflow step continuously in the background
        await run_workflow_steps(
            message_watcher_classes=[TestNestedStep],
            artefact_classes=[TestNestedInputList, TestOutput],
            file_watcher_classes=[],
        )

        # Add subscription to the output artefact
        await nats_session.subscribe(Config.STREAM, "artefact.TestOutput", on_process_done)

        job = Job(
            inputs={"input": ("TestNestedInputList", {"identifier": "person"})},
            workflow_step=TestNestedStep,
        )
        await subscribe_to_job_update(
            nats_session, job.maestro_id, JobStatus.PENDING, pending_future
        )
        await order_workflow(
            WorkflowModel(
                workflow_name="test_workflow",
                jobs=[job],
            )
        )
        await asyncio.wait_for(pending_future, timeout=3.0)

        # Add the input artefact
        input_1 = TestInput(identifier="artefact_1")
        input_2 = TestInput(identifier="artefact_2")
        input_3 = TestInput(identifier="artefact_3")
        nested_artefact_1 = TestNestedInput(
            identifier="nested_artefact_1",
            nested_artefact=input_1,
            nested_artefacts_list=[input_2, input_3],
        )
        nested_artefact_2 = TestNestedInput(
            identifier="nested_artefact_2",
            nested_artefacts_list=[input_1],
        )
        artefact = TestNestedInputList(
            identifier="person",
            nested_input_list=[nested_artefact_1, nested_artefact_2],
        )
        async with Neo4jTransaction() as neo4j_tx:
            await create_node(neo4j_tx, artefact)
        await nats_session.publish(f"artefact.{artefact.type}", str(artefact.maestro_id).encode())
        await asyncio.wait_for(on_process_done_called, timeout=2.0)

        WorkerQueue.shutdown_gracefully()
        await asyncio.wait_for(WorkerQueue.loop_task, 1)

    assert output != {}
    assert output["subject"] == "artefact.TestOutput"
