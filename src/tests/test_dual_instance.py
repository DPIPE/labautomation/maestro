import asyncio
import json
import multiprocessing
import multiprocessing.synchronize
import os
import shutil
import subprocess
import sys
import tempfile
from pathlib import Path
from typing import Literal

import pytest
import pytest_asyncio
from pydantic import computed_field
from pydantic.fields import FieldInfo
from setproctitle import setproctitle

from maestro.config import Config
from maestro.models import (
    FileArtefactModel,
    Job,
    JobStatus,
    WorkflowModel,
    WorkflowStepModelWithArtefacts,
)
from maestro.nats.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import read_nodes
from maestro.run import run_workflow_steps
from maestro.workflow_step.file_watcher import FileWatcher
from maestro.workflow_step.message_watcher import MessageWatcher
from maestro.workflow_utils import order_workflow

# Create temporary directories - these will be deleted when it goes out of scope
tmpdir_local = tempfile.TemporaryDirectory()
tmpdir_remote = tempfile.TemporaryDirectory()

STORAGE_LOCAL = Path(tmpdir_local.name)
STORAGE_REMOTE = Path(tmpdir_remote.name)


class InfrastructureAwareFileArtefact(FileArtefactModel):
    INSTANCE_ID: Literal["local", "remote"]

    @computed_field
    @property
    def filename(self) -> str:
        return self.path.name


class RemoteFileArtefact(InfrastructureAwareFileArtefact):
    INSTANCE_ID: Literal["remote"] = "remote"


class LocalFileArtefact(InfrastructureAwareFileArtefact):
    INSTANCE_ID: Literal["local"] = "local"


class LocalFileWatcher(FileWatcher):
    """Watches a local directory, and triggers a workflow to move the file to remote, and process it there"""

    path = STORAGE_LOCAL

    async def process(self, input: FileArtefactModel):
        local_file = LocalFileArtefact(path=input.path)
        # Order jobs
        workflow = WorkflowModel(
            workflow_name=input.path.stem,
            jobs=[
                Job(
                    workflow_step=MoveToRemote,
                    inputs={
                        "input": (
                            LocalFileArtefact,
                            {"path": local_file.path},
                        )
                    },
                ),
                Job(
                    workflow_step=ProcessRemote,
                    instance_id="remote",
                    inputs={
                        "input": (
                            RemoteFileArtefact,
                            {
                                "filename": local_file.filename,
                            },
                        )
                    },
                ),
            ],
        )

        await order_workflow(workflow)

        return [local_file]


class MoveToRemote(MessageWatcher):
    """Move a local file to a remote directory, returning a new RemoteFileArtefact"""

    async def process(self, input: LocalFileArtefact):
        new_path = STORAGE_REMOTE / input.path.name
        shutil.move(input.path, new_path)
        return [RemoteFileArtefact(path=new_path)]


class ProcessRemote(MessageWatcher):
    """Process a remote file, returning a new RemoteFileArtefact"""

    async def process(self, input: RemoteFileArtefact):
        assert input.INSTANCE_ID == "remote"
        processed_path = input.path.with_suffix(".processed")
        processed_path.touch()
        return [RemoteFileArtefact(path=processed_path)]


async def run_workflow_steps_in_subprocess(
    message_watcher_classes: list, file_watcher_classes: list, neo4j_uri: str, instance_id: str
):
    """Wrapper to run workflow steps in a separate process, with updated config values for neo4j_uri and instance_id"""

    async def main(
        message_watcher_classes: list,
        file_watcher_classes: list,
        started_flag: multiprocessing.synchronize.Event,
    ):
        task = await run_workflow_steps(
            message_watcher_classes=message_watcher_classes,
            file_watcher_classes=file_watcher_classes,
        )
        # Need to give it some time, to ensure that file watchers are started
        await asyncio.sleep(0)
        started_flag.set()
        await task

    def start(
        message_watcher_classes: list,
        file_watcher_classes: list,
        neo4j_uri: str,
        instance_id: str,
        started_flag: multiprocessing.synchronize.Event,
    ):
        """Start the workflow steps in a subprocess"""
        # Pipe "stdout" and "stderr" from this process to a "sed" child process to prefix the instance ID on each output line.
        sed = subprocess.Popen(["sed", "-u", f"s/^/{instance_id}: /"], stdin=subprocess.PIPE)
        os.dup2(sed.stdin.fileno(), sys.stdout.fileno())
        os.dup2(sed.stdin.fileno(), sys.stderr.fileno())
        # Close the original write-end of "sed" "stdin" pipe now that it has been connected to our "stdout" and "stderr".
        sed.stdin.close()

        # Set the process title to the instance ID. This is useful when observing the system via tools like "ps" and "top".
        setproctitle(instance_id)

        Config.NEO4J_URI = neo4j_uri
        Config.INSTANCE_ID = instance_id

        # Update instance id default value
        # Since this is inherited from the parent process, the default value has already been set
        Job.model_fields["instance_id"] = FieldInfo.merge_field_infos(
            Job.model_fields["instance_id"], FieldInfo(default=Config.INSTANCE_ID)
        )
        Job.model_rebuild(force=True)
        assert Job.model_fields["instance_id"].default == Config.INSTANCE_ID != "default"
        asyncio.run(main(message_watcher_classes, file_watcher_classes, started_flag))

    started_flag = multiprocessing.Event()
    process = multiprocessing.get_context("fork").Process(
        target=start,
        kwargs={
            "message_watcher_classes": message_watcher_classes,
            "file_watcher_classes": file_watcher_classes,
            "neo4j_uri": neo4j_uri,
            "instance_id": instance_id,
            "started_flag": started_flag,
        },
        name=f"Maestro Instance '{instance_id}'",
    )
    print(f"Starting instance '{instance_id}'")
    process.start()

    # Ensure that the process has started within 5 seconds
    async with asyncio.timeout(5):
        while True:
            if started_flag.is_set():
                break
            await asyncio.sleep(0.1)
    assert process.is_alive()
    print(f"Instance '{instance_id}' started (PID: {process.pid})")
    return process


@pytest_asyncio.fixture(autouse=True)
async def local_run_workflow_steps():
    """
    Start up a process to run the workflow steps with INSTANCE_ID set to 'local'

    Runs one file watcher and one message watcher - the LocalFileWatcher and MoveToRemote steps
    """
    process = await run_workflow_steps_in_subprocess(
        message_watcher_classes=[MoveToRemote],
        file_watcher_classes=[LocalFileWatcher],
        instance_id="local",
        neo4j_uri=Config.NEO4J_URI,
    )
    yield process

    process.terminate()
    process.join()


@pytest_asyncio.fixture(autouse=True)
async def remote_run_workflow_steps():
    """
    Start up a process to run the workflow steps with INSTANCE_ID set to 'remote'

    Only runs a single message watcher - the ProcessRemote step
    """
    process = await run_workflow_steps_in_subprocess(
        message_watcher_classes=[ProcessRemote],
        file_watcher_classes=[],
        instance_id="remote",
        neo4j_uri="bolt://neo4j-secondary:7687",
    )
    yield process

    process.terminate()
    process.join()


@pytest.mark.asyncio
@pytest.mark.parametrize("workflow_count", [1, 10])
async def test_dual_instance(workflow_count):
    """
    Test that we can run a number of workflows with two instances ('local' and 'remote'), where the 'local' instance
    watches a local directory, and moves files to a 'remote' directory, where the 'remote' instance processes
    the artefacts

    Note: The 'remote' instance does not use a file watcher, as it is only processing artefacts that have been
    produced by the 'local' instance's MoveToRemote step
    """
    MARGIN = 10
    TIMEOUT_PER_JOB = 1
    LOCAL_JOBS_PER_WORKFLOW = 2
    REMOTE_JOBS_PER_WORKFLOW = 1
    JOBS_PER_WORKFLOW = LOCAL_JOBS_PER_WORKFLOW + REMOTE_JOBS_PER_WORKFLOW
    TOTAL_TIMEOUT = workflow_count * JOBS_PER_WORKFLOW * TIMEOUT_PER_JOB + MARGIN
    NEO4J_CONSISTENCY_RETRY_DELAY = 1

    for i in range(workflow_count):
        Path(STORAGE_LOCAL / f"test-{i}.txt").touch()

    final_step_done = asyncio.Future()

    # Wait for all ProcessRemote steps to be done.
    async with NATSSession() as nats:
        done = set()

        async def on_message(msg):
            workflow_step_maestro_id = json.loads(msg.data.decode())["maestro_id"]
            done.add(workflow_step_maestro_id)
            if len(done) == workflow_count:
                final_step_done.set_result(None)
            await msg.ack()

        await nats.subscribe(Config.STREAM, "workflow_step.ProcessRemote.done", on_message)
        await asyncio.wait_for(final_step_done, TOTAL_TIMEOUT)
        assert len(done) == workflow_count

    # Check that the file exists where they should
    for i in range(workflow_count):
        assert not (STORAGE_LOCAL / f"test-{i}.txt").exists(), "Local file should have been moved"
        assert (STORAGE_REMOTE / f"test-{i}.txt").exists(), "Remote file should have been moved"
        assert (
            STORAGE_REMOTE / f"test-{i}.processed"
        ).exists(), "Remote file should have been processed"

    expected_file_watcher_outputs = set(
        (
            STORAGE_LOCAL / f"test-{i}.txt",
            "local",
        )
        for i in range(workflow_count)
    )
    expected_move_outputs = set(
        (
            STORAGE_REMOTE / f"test-{i}.txt",
            "remote",
        )
        for i in range(workflow_count)
    )
    expected_process_outputs = set(
        (
            STORAGE_REMOTE / f"test-{i}.processed",
            "remote",
        )
        for i in range(workflow_count)
    )

    async with asyncio.timeout(TOTAL_TIMEOUT):
        while True:
            try:
                # Check consistency in Neo4j
                for uri in ["bolt://neo4j:7687", "bolt://neo4j-secondary:7687"]:
                    async with Neo4jTransaction(uri=uri) as tx:
                        # Check that the jobs were in fact executed on the correct instance
                        local_jobs = await read_nodes(tx, Job, {"instance_id": "local"})
                        remote_jobs = await read_nodes(tx, Job, {"instance_id": "remote"})

                        assert (
                            len(local_jobs) == LOCAL_JOBS_PER_WORKFLOW * workflow_count
                        ), f"Local instance should have {LOCAL_JOBS_PER_WORKFLOW * workflow_count} jobs ({uri})"
                        assert (
                            len(remote_jobs) == REMOTE_JOBS_PER_WORKFLOW * workflow_count
                        ), f"Remote instance should have {REMOTE_JOBS_PER_WORKFLOW * workflow_count} job ({uri})"

                        assert all(
                            job.status == JobStatus.COMPLETED for job in local_jobs + remote_jobs
                        ), f"All jobs should be completed  ({uri})"

                        # Get workflow steps
                        workflow_steps = await read_nodes(tx, WorkflowStepModelWithArtefacts, {})
                        assert (
                            len(workflow_steps) == JOBS_PER_WORKFLOW * workflow_count
                        ), f"There should be {JOBS_PER_WORKFLOW * workflow_count} workflow steps ({uri})"

                        # Check the different workflow steps' inputs and outputs
                        file_watcher_outputs = set()
                        for file_watcher_workflow_step in (
                            step for step in workflow_steps if "LocalFileWatcher" in step.labels
                        ):
                            assert (
                                len(file_watcher_workflow_step.OUTPUT) == 1
                            ), f"File watcher should have 1 output ({uri})"
                            file_watcher_outputs.add(file_watcher_workflow_step.OUTPUT[0])

                        assert (
                            len(file_watcher_outputs) == workflow_count
                        ), f"File watcher should have {workflow_count} outputs ({uri})"
                        actual_file_watcher_outputs = set(
                            tuple(output.model_dump(include={"path", "INSTANCE_ID"}).values())
                            for output in file_watcher_outputs
                        )
                        assert (
                            actual_file_watcher_outputs == expected_file_watcher_outputs
                        ), f"Incorrect file watcher outputs {uri})"

                        move_outputs = set()
                        for move_workflow_step in (
                            step for step in workflow_steps if "MoveToRemote" in step.labels
                        ):
                            assert (
                                len(move_workflow_step.INPUT) == 1
                            ), f"Move to remote should have 1 input ({uri})"
                            assert (
                                move_workflow_step.INPUT[0] in file_watcher_outputs
                            ), f"Move to remote input should be a file watcher output ({uri})"
                            file_watcher_outputs.remove(move_workflow_step.INPUT[0])
                            assert (
                                len(move_workflow_step.OUTPUT) == 1
                            ), f"Move to remote should have 1 output ({uri})"
                            move_outputs.add(move_workflow_step.OUTPUT[0])

                        assert (
                            file_watcher_outputs == set()
                        ), f"File watcher outputs should now be empty ({uri})"

                        assert (
                            len(move_outputs) == workflow_count
                        ), f"Move to remote should have {workflow_count} outputs ({uri})"
                        actual_move_outputs = set(
                            tuple(output.model_dump(include={"path", "INSTANCE_ID"}).values())
                            for output in move_outputs
                        )
                        assert (
                            actual_move_outputs == expected_move_outputs
                        ), f"Incorrect move to remote outputs {uri})"

                        process_outputs = set()
                        for process_workflow_step in (
                            step for step in workflow_steps if "ProcessRemote" in step.labels
                        ):
                            assert (
                                len(process_workflow_step.INPUT) == 1
                            ), f"Process remote should have 1 input ({uri})"
                            assert (
                                process_workflow_step.INPUT[0] in move_outputs
                            ), f"Process remote input should be a move to remote output ({uri})"
                            move_outputs.remove(process_workflow_step.INPUT[0])
                            assert (
                                len(process_workflow_step.OUTPUT) == 1
                            ), f"Process remote should have 1 output ({uri})"
                            process_outputs.add(process_workflow_step.OUTPUT[0])

                        assert (
                            move_outputs == set()
                        ), f"Move to remote outputs should now be empty ({uri})"

                        assert (
                            len(process_outputs) == workflow_count
                        ), f"Process remote should have {workflow_count} outputs ({uri})"
                        actual_process_outputs = set(
                            tuple(output.model_dump(include={"path", "INSTANCE_ID"}).values())
                            for output in process_outputs
                        )
                        assert (
                            actual_process_outputs == expected_process_outputs
                        ), f"Incorrect process remote outputs {uri})"

            except AssertionError as e:
                print(e)
                print(f"Retrying in {NEO4J_CONSISTENCY_RETRY_DELAY} second(s)...")
                await asyncio.sleep(NEO4J_CONSISTENCY_RETRY_DELAY)
            else:
                break
