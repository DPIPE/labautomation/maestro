import random
import time

from maestro.models import BaseArtefactModel
from maestro.workflow_step.message_watcher import MessageWatcher


class PedigreeMemberArtefact(BaseArtefactModel):
    member: str = ""


class SampleArtefact(PedigreeMemberArtefact):
    """Sample artefact for testing"""

    sex: str = "M"


class SequenceReadArtefact(PedigreeMemberArtefact):
    """Sequence read artefact for testing"""

    fastq_files: str = ""


class SequenceQCArtefact(PedigreeMemberArtefact):
    """Sequence QC artefact for testing"""

    quality: float = 0.0


class VariantCallingArtefact(PedigreeMemberArtefact):
    """Variant calling artefact for testing"""

    vcf_file: str = ""


class VariantCallingCollection(BaseArtefactModel):
    family_id: str = ""
    artefacts: list[VariantCallingArtefact]


class JointVariantCallingArtefact(BaseArtefactModel):
    """Joint variant calling artefact for testing"""

    vcf_file: str = ""


class Sequencing(MessageWatcher):
    """Process for testing"""

    sequencer_name: str = "NovaSeq"

    async def process(self, sample: SampleArtefact, **kwargs):
        s_read = SequenceReadArtefact(member=sample.member)
        s_read.fastq_files = f"{sample.maestro_id}.fastq"

        s_qc = SequenceQCArtefact(member=sample.member)
        s_qc.quality = random.random()
        time.sleep(1.0)
        return [s_read, s_qc]


class VariantCalling(MessageWatcher):
    """Process for testing"""

    caller_name: str = "GATK"

    async def process(self, s_read: SequenceReadArtefact, **kwargs):
        v_call = VariantCallingArtefact(member=s_read.member)
        v_call.vcf_file = f"{s_read.maestro_id}.vcf"

        time.sleep(1.0)
        return [v_call]


class JointVariantCalling(MessageWatcher):
    """Process for testing"""

    caller_name: str = "FamilyPlanner"

    async def process(
        self,
        input: VariantCallingCollection,
        pedigree: dict[str, str],
    ):
        proband_vc = next(vc for vc in input.artefacts if vc.member == pedigree["proband"])

        j_v_call = JointVariantCallingArtefact(member=proband_vc.member)
        j_v_call.vcf_file = f"{proband_vc.maestro_id}.vcf"

        time.sleep(1.0)
        return [j_v_call]


class VariantCallingCollector(MessageWatcher):
    async def process(self, family_id: str, **kwargs):
        assert all(isinstance(artefact, VariantCallingArtefact) for artefact in kwargs.values())
        return [VariantCallingCollection(family_id=family_id, artefacts=list(kwargs.values()))]
