import asyncio

import pytest

from maestro import run_workflow_steps
from maestro.models import Job, JobStatus, WorkflowModel
from maestro.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import create_node, read_nodes
from maestro.utils import subscribe_to_job_update
from maestro.worker_queue import WorkerQueue
from maestro.workflow_utils import order_workflow
from tests.artefacts_processes import (
    JointVariantCalling,
    JointVariantCallingArtefact,
    SampleArtefact,
    SequenceQCArtefact,
    SequenceReadArtefact,
    Sequencing,
    VariantCalling,
    VariantCallingArtefact,
    VariantCallingCollection,
    VariantCallingCollector,
)


@pytest.mark.asyncio
async def test_pipeline():
    """
    Create a process that adds a job to the queue, then adds an input
    triggering the process to run.
    """

    await run_workflow_steps(
        artefact_classes=[
            JointVariantCallingArtefact,
            SampleArtefact,
            SequenceQCArtefact,
            SequenceReadArtefact,
            VariantCallingArtefact,
            VariantCallingCollection,
        ],
        message_watcher_classes=[
            JointVariantCalling,
            Sequencing,
            VariantCalling,
            VariantCallingCollector,
        ],
        file_watcher_classes=[],
    )

    async with NATSSession() as nats_session:
        all_done = asyncio.Future()
        # Listen for the process to finish

        async def on_all_done(msg):
            all_done.set_result(True)

        await nats_session.subscribe(
            "test_stream", "artefact.JointVariantCallingArtefact", on_all_done
        )

        # Place sequencing orders
        sequencing_jobs = [
            Job(
                inputs={"sample": (SampleArtefact, {"member": "father"})},
                params={"sequencer_name": "NovaSeq"},
                status=JobStatus.PENDING,
                workflow_step=Sequencing,
            ),
            Job(
                inputs={"sample": (SampleArtefact, {"member": "mother"})},
                params={"sequencer_name": "NovaSeq"},
                status=JobStatus.PENDING,
                workflow_step=Sequencing,
            ),
            Job(
                inputs={"sample": (SampleArtefact, {"member": "proband"})},
                params={"sequencer_name": "NovaSeq"},
                status=JobStatus.PENDING,
                workflow_step=Sequencing,
            ),
        ]
        for job in sequencing_jobs:
            await order_workflow(
                WorkflowModel(
                    workflow_name="test_workflow",
                    jobs=[job],
                )
            )

        # Place variant calling orders
        vc_jobs = [
            Job(
                inputs={"s_read": (SequenceReadArtefact, {"member": "father"})},
                params={"caller_name": "GATK"},
                status=JobStatus.PENDING,
                workflow_step=VariantCalling,
            ),
            Job(
                inputs={"s_read": (SequenceReadArtefact, {"member": "mother"})},
                params={"caller_name": "GATK"},
                status=JobStatus.PENDING,
                workflow_step=VariantCalling,
            ),
            Job(
                inputs={"s_read": (SequenceReadArtefact, {"member": "proband"})},
                params={"caller_name": "GATK"},
                status=JobStatus.PENDING,
                workflow_step=VariantCalling,
            ),
        ]
        for job in vc_jobs:
            await order_workflow(
                WorkflowModel(
                    workflow_name="test_workflow",
                    jobs=[job],
                )
            )

        # Place collect variant calling order
        collect_vc_job = Job(
            inputs={
                "father": (VariantCallingArtefact, {"member": "father"}),
                "mother": (VariantCallingArtefact, {"member": "mother"}),
                "proband": (VariantCallingArtefact, {"member": "proband"}),
            },
            params={"family_id": "family_id"},
            status=JobStatus.PENDING,
            workflow_step=VariantCallingCollector,
        )

        await order_workflow(
            WorkflowModel(
                workflow_name="test_workflow",
                jobs=[collect_vc_job],
            )
        )

        # Place joint variant calling order
        pending_future = asyncio.Future()
        joint_vc_job = Job(
            inputs={
                "input": (VariantCallingCollection, {"family_id": "family_id"}),
            },
            params={
                "pedigree": {
                    "father": "father",
                    "mother": "mother",
                    "proband": "proband",
                }
            },
            status=JobStatus.PENDING,
            workflow_step=JointVariantCalling,
        )
        await subscribe_to_job_update(
            nats_session, joint_vc_job.maestro_id, JobStatus.PENDING, pending_future
        )

        await order_workflow(
            WorkflowModel(
                workflow_name="test_workflow",
                jobs=[joint_vc_job],
            )
        )

        # Check that everything is pending
        await asyncio.wait_for(pending_future, timeout=3)  # wait for last step

        async with Neo4jTransaction() as neo4j_tx:
            sequencing_job_queue = await read_nodes(
                neo4j_tx, Job, {"workflow_step": Sequencing.__name__}
            )
            assert len(sequencing_job_queue) == len(sequencing_jobs)
            assert {j.maestro_id for j in sequencing_job_queue} == {
                j.maestro_id for j in sequencing_jobs
            }
            assert all(j.status == JobStatus.PENDING for j in sequencing_job_queue)

            vc_job_queue = await read_nodes(
                neo4j_tx, Job, {"workflow_step": VariantCalling.__name__}
            )

            assert len(vc_job_queue) == len(vc_jobs)
            assert {j.maestro_id for j in vc_job_queue} == {j.maestro_id for j in vc_jobs}
            assert all(j.status == JobStatus.PENDING for j in vc_job_queue)

            joint_vc_job_queue = await read_nodes(
                neo4j_tx, Job, {"workflow_step": JointVariantCalling.__name__}
            )

            assert len(joint_vc_job_queue) == 1
            assert joint_vc_job_queue[0].maestro_id == joint_vc_job.maestro_id
            assert joint_vc_job_queue[0].status == JobStatus.PENDING

        # Add the input artefact
        async with Neo4jTransaction() as neo4j_tx:
            father_sample = SampleArtefact(member="father", sex="M")
            await create_node(neo4j_tx, father_sample)
            mother_sample = SampleArtefact(member="mother", sex="F")
            await create_node(neo4j_tx, mother_sample)
            proband_sample = SampleArtefact(member="proband", sex="F")
            await create_node(neo4j_tx, proband_sample)

        await nats_session.publish(
            f"artefact.{father_sample.type}", str(father_sample.maestro_id).encode()
        )
        await nats_session.publish(
            f"artefact.{mother_sample.type}", str(mother_sample.maestro_id).encode()
        )
        await nats_session.publish(
            f"artefact.{proband_sample.type}", str(proband_sample.maestro_id).encode()
        )

        await asyncio.wait_for(all_done, timeout=10.0)
        WorkerQueue.shutdown_gracefully()
        await asyncio.wait_for(WorkerQueue.loop_task, 10)

        # Check that all jobs are completed
        async with Neo4jTransaction() as neo4j_tx:
            sequencing_job_queue = await read_nodes(
                neo4j_tx, Job, {"workflow_step": Sequencing.__name__}
            )

            assert len(sequencing_job_queue) == len(sequencing_jobs)
            assert {j.maestro_id for j in sequencing_job_queue} == {
                j.maestro_id for j in sequencing_jobs
            }
            assert all(j.status == JobStatus.COMPLETED for j in sequencing_job_queue)

            vc_job_queue = await read_nodes(
                neo4j_tx, Job, {"workflow_step": VariantCalling.__name__}
            )

            assert len(vc_job_queue) == len(vc_jobs)
            assert {j.maestro_id for j in vc_job_queue} == {j.maestro_id for j in vc_jobs}
            assert all(j.status == JobStatus.COMPLETED for j in vc_job_queue)

            collect_vc_job_queue = await read_nodes(
                neo4j_tx, Job, {"workflow_step": VariantCallingCollector.__name__}
            )
            assert len(collect_vc_job_queue) == 1
            assert collect_vc_job_queue[0].maestro_id == collect_vc_job.maestro_id
            assert collect_vc_job_queue[0].status == JobStatus.COMPLETED

            joint_vc_job_queue = await read_nodes(
                neo4j_tx, Job, {"workflow_step": JointVariantCalling.__name__}
            )

            assert len(joint_vc_job_queue) == 1
            assert joint_vc_job_queue[0].maestro_id == joint_vc_job.maestro_id
            assert joint_vc_job_queue[0].status == JobStatus.COMPLETED
