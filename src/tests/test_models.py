from pydantic import Field

from maestro.models import MaestroBase


def test_maestro_base():
    class Test(MaestroBase):
        pass

    test_obj = Test()
    assert test_obj.type == "Test"
    assert test_obj.labels == ["Test", "MaestroBase"]

    assert test_obj.get_nested_iterables() == {}
    assert test_obj.get_nested() == {}
    assert test_obj.model_dump_primitive() == {
        "maestro_id": test_obj.maestro_id,
        "type": "Test",
        "invalidated_at": None,
        "additional_labels": "[]",
    }

    class TestSubclass(Test):
        pass

    test_subclass_obj = TestSubclass()
    assert test_subclass_obj.type == "TestSubclass"
    assert test_subclass_obj.labels == ["TestSubclass", "Test", "MaestroBase"]

    assert test_subclass_obj.get_nested_iterables() == {}
    assert test_subclass_obj.get_nested() == {}
    assert test_subclass_obj.model_dump_primitive() == {
        "maestro_id": test_subclass_obj.maestro_id,
        "type": "TestSubclass",
        "invalidated_at": None,
        "additional_labels": "[]",
    }


def test_maestro_base_additional_labels():
    class Test(MaestroBase):
        pass

    test_obj = Test(additional_labels=["TestAdditional"])
    assert test_obj.type == "TestAdditional"
    assert test_obj.labels == ["Test", "MaestroBase", "TestAdditional"]

    assert test_obj.get_nested_iterables() == {}
    assert test_obj.get_nested() == {}
    assert test_obj.model_dump_primitive() == {
        "maestro_id": test_obj.maestro_id,
        "type": "TestAdditional",
        "invalidated_at": None,
        "additional_labels": "['TestAdditional']",
    }


def test_nested_maestro_base():
    class Test(MaestroBase):
        pass

    test_obj = Test()
    assert test_obj.type == "Test"
    assert test_obj.labels == ["Test", "MaestroBase"]

    class TestNested(MaestroBase):
        test: Test
        optional_test1: Test | None = None
        optional_test2: Test | None = None

    test_nested_obj = TestNested(test=test_obj)
    assert test_nested_obj.type == "TestNested"
    assert test_nested_obj.labels == ["TestNested", "MaestroBase"]

    assert set(TestNested.nested_fields()) == {
        ("test", Test),
        ("optional_test1", Test),
        ("optional_test2", Test),
    }
    assert test_nested_obj.get_nested_iterables() == {}
    assert test_nested_obj.get_nested() == {"test": test_obj}
    assert test_nested_obj.model_dump_primitive() == {
        "maestro_id": test_nested_obj.maestro_id,
        "type": "TestNested",
        "invalidated_at": None,
        "additional_labels": "[]",
    }


def test_nested_maestro_base_iterable():
    class Test(MaestroBase):
        pass

    test_obj = Test()
    assert test_obj.type == "Test"
    assert test_obj.labels == ["Test", "MaestroBase"]

    class TestNested(MaestroBase):
        tests: list[Test]

    test_nested_obj = TestNested(tests=[test_obj])
    assert test_nested_obj.type == "TestNested"
    assert test_nested_obj.labels == ["TestNested", "MaestroBase"]

    assert test_nested_obj.get_nested_iterables() == {"tests": [test_obj]}
    assert test_nested_obj.get_nested() == {}
    assert test_nested_obj.model_dump_primitive() == {
        "maestro_id": test_nested_obj.maestro_id,
        "type": "TestNested",
        "invalidated_at": None,
        "additional_labels": "[]",
    }


def test_base_artefact():
    from maestro.models import BaseArtefactModel

    class Test(BaseArtefactModel):
        pass

    test_obj = Test()
    assert test_obj.type == "Test"
    assert test_obj.labels == ["Test", "BaseArtefactModel", "MaestroBase"]

    assert test_obj.get_nested_iterables() == {}
    assert test_obj.get_nested() == {}
    assert test_obj.model_dump_primitive() == {
        "maestro_id": test_obj.maestro_id,
        "type": "Test",
        "invalidated_at": None,
        "additional_labels": "[]",
    }

    class TestSubclass(Test):
        pass

    test_subclass_obj = TestSubclass()
    assert test_subclass_obj.type == "TestSubclass"


def test_nested_fields():
    class Test(MaestroBase):
        pass

    class Test2(MaestroBase):
        pass

    class Subclass(Test):
        pass

    class TestNested(MaestroBase):
        test: Test
        optional_test1: Test | None
        optional_test2: Test | None
        tuple_test1: tuple[Test, Test]
        tuple_test2: tuple[Test, ...]
        list_test: list[Test]
        set_test: set[Test]
        optional_tuple_test1: tuple[Test, Test] | None
        optional_tuple_test2: tuple[Test, ...] | None
        optional_list_test: list[Test] | None
        optional_set_test: set[Test] | None
        alias_field: Test = Field(alias="test_alias")
        test_multiple_optional: Test | Test2
        test_multiple_tuple: tuple[Test, Test2]
        test_multiple_list: list[Test | Test2]
        test_multiple_set: set[Test | Test2]
        test_subclass: Subclass

    assert set(TestNested.nested_fields()) == {
        ("test", Test),
        ("optional_test1", Test),
        ("optional_test2", Test),
        ("tuple_test1", Test),
        ("tuple_test2", Test),
        ("list_test", Test),
        ("set_test", Test),
        ("optional_tuple_test1", Test),
        ("optional_tuple_test2", Test),
        ("optional_list_test", Test),
        ("optional_set_test", Test),
        ("test_alias", Test),
        ("test_multiple_optional", Test),
        ("test_multiple_optional", Test2),
        ("test_multiple_tuple", Test),
        ("test_multiple_tuple", Test2),
        ("test_multiple_list", Test),
        ("test_multiple_list", Test2),
        ("test_multiple_set", Test),
        ("test_multiple_set", Test2),
        ("test_subclass", Subclass),
    }
