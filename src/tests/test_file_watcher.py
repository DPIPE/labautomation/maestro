import asyncio
import gc
import multiprocessing
import os
import re
import tempfile
from pathlib import Path
from uuid import uuid4

import pytest
from watchfiles import Change

from maestro import run_workflow_steps
from maestro.exceptions import SetupError
from maestro.models import (
    FileArtefactModel,
    Job,
    JobPriorityEnum,
    JobStatus,
    MaestroBase,
    WorkflowModel,
    WorkflowStepModelWithArtefacts,
)
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import read_node, read_nodes
from maestro.worker_queue import WorkerQueue
from maestro.workflow_step.file_watcher import AddedFilesFilter, FileWatcher
from maestro.workflow_step.message_watcher import MessageWatcher
from maestro.workflow_step.workflow_step import WorkflowStepBase


class TracingAddedFilesFilter(AddedFilesFilter):
    def __init__(self):
        super().__init__()
        self.calls = []

    def __call__(self, change, path):
        result = super().__call__(change, path)
        self.calls.append((change, path, result))
        return result


class TestFileOutput(FileArtefactModel):
    """Output artefact for testing"""

    # Do not collect this class for pytest
    __test__ = False


class TestFileWatcher(FileWatcher):
    # Do not collect this class for pytest
    __test__ = False

    path = "/tmp/"  # Overridden in tests
    pattern = ".*.txt"
    filter = TracingAddedFilesFilter()
    call_count = multiprocessing.Value("i", 0)

    async def process(self, input: FileArtefactModel):
        path = input.path
        output = TestFileOutput(path=path)
        with self.call_count.get_lock():
            self.call_count.value += 1
        return [output]


@pytest.fixture(autouse=True)
def reset_calls():
    TestFileWatcher.call_count.value = 0
    TestFileWatcher.filter.calls.clear()
    yield


@pytest.mark.asyncio
async def test_file_watcher():
    with tempfile.TemporaryDirectory(dir=Path(__file__).parent) as watch_dir:
        TestFileWatcher.path = watch_dir
        TestFileWatcher.call_count.value = 0

        file = Path(watch_dir) / "test.txt"
        non_matching_file = Path(watch_dir) / "test.jpg"
        file.touch()
        one_second_task = asyncio.create_task(asyncio.sleep(1))
        await run_workflow_steps(file_watcher_classes=[TestFileWatcher])
        # Allow TestFileWatcher.run() to reach awatch().
        await asyncio.sleep(0)
        assert TestFileWatcher.filter.calls == []

        assert TestFileWatcher.priority == JobPriorityEnum.NORMAL
        # Ensure that second call to file.touch() is at least one second after the first call.
        # This is to ensure that "watchfiles" always reports "Change.modified".
        await one_second_task
        # Should not trigger when file is already there (on Change.modified)
        file.touch()
        async with asyncio.timeout(5):
            while len(TestFileWatcher.filter.calls) < 1:
                await asyncio.sleep(0.1)
        assert TestFileWatcher.filter.calls == [(Change.modified, str(file), False)]
        TestFileWatcher.filter.calls.clear()
        async with Neo4jTransaction() as neo4j_tx:
            all_jobs = await read_nodes(neo4j_tx, Job, {})
            assert all_jobs == []

        # Should not trigger when file is not matching pattern
        non_matching_file.touch()
        one_second_task = asyncio.create_task(asyncio.sleep(1))
        async with asyncio.timeout(5):
            while len(TestFileWatcher.filter.calls) < 2:
                await asyncio.sleep(0.1)
        assert set(TestFileWatcher.filter.calls) == {
            (Change.added, str(non_matching_file), True),
            (Change.modified, str(non_matching_file.parent), False),
        }
        TestFileWatcher.filter.calls.clear()

        async with Neo4jTransaction() as neo4j_tx:
            all_jobs = await read_nodes(neo4j_tx, Job, {})
            assert all_jobs == []

        # Ensure that call to file.unlink() below is at least one second after the
        # call to non_matching_file.touch() above. This is to ensure that "watchfiles"
        # always reports "Change.modified" for the parent directory so that the
        # testing on TestFileWatcher.filter.calls below is deterministic.
        await one_second_task
        # Should not trigger when file is removed (on Change.deleted)
        file.unlink()
        async with asyncio.timeout(5):
            while len(TestFileWatcher.filter.calls) < 2:
                await asyncio.sleep(0.1)
        assert set(TestFileWatcher.filter.calls) == {
            (Change.deleted, str(file), False),
            (Change.modified, str(file.parent), False),
        }
        TestFileWatcher.filter.calls.clear()
        async with Neo4jTransaction() as neo4j_tx:
            all_jobs = await read_nodes(neo4j_tx, Job, {})
            assert all_jobs == []

            records = await read_nodes(neo4j_tx, MaestroBase, {})
            assert len(records) == 0

        # Update file watcher priority and test if job gets the same value
        TestFileWatcher.priority = JobPriorityEnum.HIGH
        # Should trigger when file is added
        assert not file.exists()
        assert TestFileWatcher.call_count.value == 0
        file.touch()
        async with asyncio.timeout(5):
            while TestFileWatcher.call_count.value < 1:
                await asyncio.sleep(0.1)
        # Wait for worker queue to gracefully shut down before inspecting database.
        # This avoids race between database updates happening from workers after
        # leaving TestFileWatcher.process() and the database checking happening below.
        WorkerQueue.shutdown_gracefully()
        await asyncio.wait_for(WorkerQueue.loop_task, 5)
        assert TestFileWatcher.call_count.value == 1

        async with Neo4jTransaction() as neo4j_tx:
            file_jobs = await read_nodes(neo4j_tx, Job, {"workflow_step": TestFileWatcher.__name__})
            assert len(file_jobs) == 1
            assert file_jobs[0].status == JobStatus.COMPLETED
            assert file_jobs[0].priority == JobPriorityEnum.HIGH

            # 1 workflow, 1 job, 1 workflow_step
            # 1 output artefact, 1 stdout, 1 stderr
            all_nodes = await read_nodes(neo4j_tx, MaestroBase, {})
            assert len(all_nodes) == 6

            jobs = await read_nodes(neo4j_tx, Job, {})
            assert len(jobs) == 1
            assert jobs[0].status == JobStatus.COMPLETED
            assert jobs[0].workflow_step == "TestFileWatcher"

            workflow_steps = await read_nodes(neo4j_tx, WorkflowStepModelWithArtefacts, {})
            assert len(workflow_steps) == 1
            assert len(workflow_steps[0].INPUT) == 0
            assert len(workflow_steps[0].OUTPUT) == 1
            assert workflow_steps[0].OUTPUT[0].type == "TestFileOutput"
            assert workflow_steps[0].STDERR is not None
            assert workflow_steps[0].STDOUT is not None


@pytest.mark.asyncio
async def test_file_watcher_copy_folder():
    number_of_files = 4
    with (
        tempfile.TemporaryDirectory(dir=Path(__file__).parent) as watch_dir,
        tempfile.TemporaryDirectory(dir=Path(__file__).parent) as tmp_dir,
    ):
        TestFileWatcher.path = watch_dir
        TestFileWatcher.call_count.value = 0
        TestFileWatcher.priority = JobPriorityEnum.URGENT

        await run_workflow_steps(file_watcher_classes=[TestFileWatcher])

        # Allow TestFileWatcher.run() to reach awatch() before creating and copying files.
        await asyncio.sleep(0)

        # Create another folder, and copy it to the watched folder
        files = [Path(tmp_dir) / f"test_{i}.txt" for i in range(number_of_files)]
        for file in files:
            file.touch()
        os.system(f"cp -r {tmp_dir} {watch_dir}/")

        async with asyncio.timeout(5):
            while TestFileWatcher.call_count.value < number_of_files:
                await asyncio.sleep(0.1)

        # Wait for worker queue to be idle before inspecting database.
        # This avoids race between database updates happening from workers after
        # leaving TestFileWatcher.process() and the database checking happening below.
        async with asyncio.timeout(5):
            while len(WorkerQueue.running_workers) > 0:
                await asyncio.sleep(0.1)
        assert TestFileWatcher.call_count.value == number_of_files

        async with Neo4jTransaction() as neo4j_tx:
            file_jobs = await read_nodes(neo4j_tx, Job, {"workflow_step": TestFileWatcher.__name__})
            assert len(file_jobs) == number_of_files
            assert all(job.status == JobStatus.COMPLETED for job in file_jobs)

        # Copy the folder again - should trigger the file watcher again
        # and create new jobs
        # Note that we need to sleep after removing the files, otherwise the file watcher
        # will not detect the changes as new files, but modifications to existing files
        os.system(f"rm -r {watch_dir}/*")
        assert not list(Path(watch_dir).iterdir()), "Folder is not empty?"

        await asyncio.sleep(0.5)
        os.system(f"cp -r {tmp_dir} {watch_dir}/")

        async with asyncio.timeout(5):
            while TestFileWatcher.call_count.value < 2 * number_of_files:
                await asyncio.sleep(0.1)

        WorkerQueue.shutdown_gracefully()
        await asyncio.wait_for(WorkerQueue.loop_task, 5)
        assert TestFileWatcher.call_count.value == 2 * number_of_files

        async with Neo4jTransaction() as neo4j_tx:
            file_jobs = await read_nodes(neo4j_tx, Job, {"workflow_step": TestFileWatcher.__name__})
            assert len(file_jobs) == 2 * number_of_files
            assert all(job.priority == JobPriorityEnum.URGENT for job in file_jobs)
            assert all(job.status == JobStatus.COMPLETED for job in file_jobs)


def test_file_watcher_setup_errors():
    with pytest.raises(SetupError, match="FileWatcher subclass must define path"):

        class TestFileWatcherWithoutPath(FileWatcher):
            pass

    with pytest.raises(SetupError, match="FileWatcher path must be str or Path"):

        class TestFileWatcherInvalidPath(FileWatcher):
            path = 123  # type: ignore[assignment]

    with pytest.raises(SetupError, match="FileWatcher path does not exist: /does/not/exist"):

        class TestFileWatcherIncorrectPath(FileWatcher):
            path = "/does/not/exist"

    with pytest.raises(SetupError, match="Invalid regex pattern:"):

        class TestFileWatcherInvaldRegex(FileWatcher):
            path = "/tmp"
            pattern = "["

    with pytest.raises(SetupError, match="FileWatcher subclasses must implement a process method"):

        class TestFileWatcherIncomplete(FileWatcher):
            path = "/tmp"
            pattern = ".*"

    with pytest.raises(
        SetupError,
        match=re.escape(
            "A FileWatcher's process method must be defined with `async def process(self, ...)`"
        ),
    ):

        class TestFileWatcherIncorrectMethod(FileWatcher):
            path = "/tmp"
            pattern = ".*"

            def process(self, input: FileArtefactModel):
                pass

    with pytest.raises(
        SetupError,
        match="WorkflowStep subclass with type 'TestFileWatcherDuplicated' already exists. Try renaming the class.",
    ):

        class TestFileWatcherDuplicated(FileWatcher):
            path = "/tmp"
            pattern = ".*"

            async def process(self, input: FileArtefactModel):
                pass

        class TestFileWatcherDuplicated(FileWatcher):  # noqa: F811
            path = "/tmp"
            pattern = ".*"

            async def process(self, input: FileArtefactModel):
                pass

    with pytest.raises(
        SetupError,
        match="TestFileWatcherInvalidPriority priority must be 1, 2 or 3, not 4",
    ):

        class TestFileWatcherInvalidPriority(FileWatcher):
            path = "/tmp"
            pattern = ".*"
            priority = 4

            async def process(self, input):
                pass

    with pytest.raises(
        SetupError,
        match="TestFileAndMessageWatcher should inherit WorkflowStep through only one ascendant",
    ):

        class TestFileAndMessageWatcher(FileWatcher, MessageWatcher):
            path = "/tmp"
            pattern = ".*"

            async def process(self, input):
                pass

    # Need to garbage collection here. Otherwise, TestFileWatcherValid will be erronously
    # registered as a MessageWatcher subclass (for whatever reason)
    gc.collect()

    # Correct implementation shouldn't raise any errors
    class TestFileWatcherValid(FileWatcher):
        path = "/tmp"

        async def process(self, input):
            pass

    # Correct implementation shouldn't raise any errors
    class TestFileWatcherValid2(FileWatcher):
        path = "/tmp"
        pattern = ".*"

        async def process(self, input):
            pass

    assert "TestFileWatcherValid" in WorkflowStepBase._registered_subclasses
    assert WorkflowStepBase._registered_subclasses["TestFileWatcherValid"] is TestFileWatcherValid
    assert "TestFileWatcherValid2" in WorkflowStepBase._registered_subclasses
    assert WorkflowStepBase._registered_subclasses["TestFileWatcherValid2"] is TestFileWatcherValid2


@pytest.mark.asyncio
async def test_workflow_id_and_name(monkeypatch):
    uuid = uuid4()
    monkeypatch.setattr(
        TestFileWatcher,
        "workflow_id_and_name",
        classmethod(lambda cls, path: (uuid, "test_workflow")),
    )

    with tempfile.TemporaryDirectory(dir=Path(__file__).parent) as watch_dir:
        monkeypatch.setattr(TestFileWatcher, "path", watch_dir)
        await run_workflow_steps(file_watcher_classes=[TestFileWatcher], message_watcher_classes=[])
        # Allow TestFileWatcher.run() to reach awatch().
        await asyncio.sleep(0)
        file = Path(watch_dir) / "test.txt"
        file.touch()
        async with asyncio.timeout(5):
            while TestFileWatcher.call_count.value < 1:
                await asyncio.sleep(0.1)

        async with Neo4jTransaction() as neo4j_tx:
            workflow = await read_node(neo4j_tx, WorkflowModel, {"maestro_id": str(uuid)})
            assert workflow
            assert workflow.workflow_name == "test_workflow"
            assert len(workflow.jobs) == 1
            assert workflow.jobs.pop().workflow_step == "TestFileWatcher"
