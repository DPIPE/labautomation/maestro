import asyncio

import pytest

from maestro.models import Job, MaestroBase
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import (
    create_node,
    create_relationship,
    merge_job_node,
    read_nodes,
)


async def has_secondary():
    try:
        async with Neo4jTransaction(uri="bolt://neo4j-secondary:7687"):
            return True
    except Exception:
        return False


# Skip tests if no secondary neo4j instance is found
skip_decorator = pytest.mark.skipif(
    not asyncio.run(has_secondary()), reason="No secondary neo4j instance found"
)


class TestNode(MaestroBase):
    __test__ = False
    name: str


@pytest.mark.asyncio
@skip_decorator
async def test_neo4j_sync_simple_query():
    "Test that neo4j-secondary receives the clear text write as neo4j"
    async with Neo4jTransaction(uri="bolt://neo4j:7687") as neo4j_tx_primary:
        await neo4j_tx_primary.query("CREATE (n:TestNode {name: 'test'})")

    query = "MATCH (n:TestNode {name: 'test'}) RETURN n"
    async with Neo4jTransaction(uri="bolt://neo4j:7687") as neo4j_tx_primary:
        result_primary = await neo4j_tx_primary.query(query)
        assert len(result_primary) == 1

    # Sleep to give secondary time to pick up the write
    await asyncio.sleep(0.5)
    async with Neo4jTransaction(uri="bolt://neo4j-secondary:7687") as neo4j_tx_secondary:
        result_secondary = await neo4j_tx_secondary.query(query)
        assert len(result_secondary) == 1


@pytest.mark.asyncio
@skip_decorator
async def test_neo4j_sync_create_node():
    "Test that utility query create_node works as expected"

    async with Neo4jTransaction(uri="bolt://neo4j:7687") as neo4j_tx_primary:
        maestro_id = await create_node(neo4j_tx_primary, TestNode(name="test"))

    query = "MATCH (n:TestNode {name: 'test', maestro_id: $maestro_id}) RETURN n"
    params = {"maestro_id": maestro_id}

    async with Neo4jTransaction(uri="bolt://neo4j:7687") as neo4j_tx_primary:
        result_primary = await neo4j_tx_primary.query(query, params)
        assert len(result_primary) == 1

    # Sleep to give secondary time to pick up the write
    await asyncio.sleep(0.5)
    async with Neo4jTransaction(uri="bolt://neo4j-secondary:7687") as neo4j_tx_secondary:
        result_secondary = await neo4j_tx_secondary.query(query, params)
        assert len(result_secondary) == 1


@pytest.mark.asyncio
@skip_decorator
async def test_neo4j_sync_create_relationship():
    "Test that utility query create_relationship works as expected"
    async with Neo4jTransaction(uri="bolt://neo4j:7687") as neo4j_tx_primary:
        maestro_id_1 = await create_node(neo4j_tx_primary, TestNode(name="test"))
        maestro_id_2 = await create_node(neo4j_tx_primary, TestNode(name="test2"))
        await create_relationship(neo4j_tx_primary, maestro_id_1, maestro_id_2, "REL")

    query_nodes = "MATCH (n:TestNode) RETURN n"
    query_relationship = "MATCH (n:TestNode {name: 'test', maestro_id: $maestro_id_1})-[r:REL]->(m:TestNode {name: 'test2', maestro_id: $maestro_id_2}) RETURN r"
    params = {"maestro_id_1": maestro_id_1, "maestro_id_2": maestro_id_2}

    async with Neo4jTransaction(uri="bolt://neo4j:7687") as neo4j_tx_primary:
        nodes_primary = await neo4j_tx_primary.query(query_nodes)
        assert len(nodes_primary) == 2
        result_primary = await neo4j_tx_primary.query(query_relationship, params)
        assert len(result_primary) == 1

    # Sleep to give secondary time to pick up the write
    await asyncio.sleep(0.5)
    async with Neo4jTransaction(uri="bolt://neo4j-secondary:7687") as neo4j_tx_secondary:
        nodes_secondary = await neo4j_tx_secondary.query(query_nodes)
        assert len(nodes_secondary) == 2

        result_secondary = await neo4j_tx_secondary.query(query_relationship, params)
        assert len(result_secondary) == 1

    assert (
        set(n.value().get("name") for n in nodes_secondary)
        == set(n.value().get("name") for n in nodes_primary)
        == {"test", "test2"}
    )


@pytest.mark.asyncio
@skip_decorator
async def test_neo4j_sync_merge_job():
    "Test that a merge job is correctly replicated"
    job = Job(workflow_step="DummyWorkflowStep")
    async with Neo4jTransaction(uri="bolt://neo4j:7687") as neo4j_tx_primary:
        await merge_job_node(neo4j_tx_primary, job)

    async with Neo4jTransaction(uri="bolt://neo4j:7687") as neo4j_tx_primary:
        jobs_primary = await read_nodes(
            neo4j_tx_primary, Job, {"workflow_step": "DummyWorkflowStep"}
        )
        assert len(jobs_primary) == 1

    assert job == jobs_primary[0]

    # # Sleep to give secondary time to pick up the write
    await asyncio.sleep(0.5)
    async with Neo4jTransaction(uri="bolt://neo4j-secondary:7687") as neo4j_tx_secondary:
        jobs_secondary = await read_nodes(
            neo4j_tx_secondary, Job, {"workflow_step": "DummyWorkflowStep"}
        )
        assert len(jobs_secondary) == 1

    assert jobs_secondary[0] == job
