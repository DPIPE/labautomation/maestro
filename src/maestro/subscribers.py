import asyncio
import json
import logging
import uuid

from nats.aio.msg import Msg

from maestro.config import Config
from maestro.exceptions import InputNotFound
from maestro.models import BaseArtefactModel, Job, JobStatus, WorkflowModel
from maestro.neo4j import queue
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import (
    MultipleNodesFoundError,
    fetch_inputs,
    merge_job_node,
    read_node,
)
from maestro.worker import Worker
from maestro.worker_queue import WorkerQueue

logger = logging.getLogger("maestro")


async def on_order_received(msg: Msg) -> None:
    """
    Subscriber to order.workflow_step.* NATS messages
    """
    payload = json.loads(msg.data.decode())
    workflow_order = WorkflowModel(**payload)
    # Filter jobs to only keep those matching this instance
    workflow_order.jobs = {
        job for job in workflow_order.jobs if job.instance_id == Config.INSTANCE_ID
    }
    logger.info(
        f"Ordered workflow {workflow_order.maestro_id} "
        f"(workflow_name: {workflow_order.workflow_name}, "
        f"search_metadata: {workflow_order.search_metadata}, "
        f"#jobs: {len(workflow_order.jobs)})"
    )

    # Wait for the jobs to be available in the database. This is necessary if order_workflow() was called
    # on a different instance and this instance's database is not yet up-to-date.
    while True:
        async with Neo4jTransaction() as neo4j_tx:
            for job in workflow_order.jobs:
                if await read_node(neo4j_tx, Job, {"maestro_id": job.maestro_id}) is None:
                    break
            else:
                break
        logger.info(
            f"Waiting for jobs in workflow {workflow_order.maestro_id} to be available in the database..."
        )
        await asyncio.sleep(0.5)

    # Find jobs to move to running
    ready_workers: list[Worker] = []
    async with Neo4jTransaction() as neo4j_tx:
        for job in workflow_order.jobs:
            db_job = await read_node(neo4j_tx, Job, {"maestro_id": job.maestro_id})
            assert db_job is not None
            job = db_job

            if job.status != JobStatus.PENDING:
                continue

            try:
                input_artefacts = await fetch_inputs(neo4j_tx, job.inputs)
            except MultipleNodesFoundError:
                logger.exception(f"Error fetching input for {job}")
            except InputNotFound:
                pass
            else:
                job.status = JobStatus.QUEUED
                await merge_job_node(neo4j_tx, job)
                ready_workers.append(await Worker.create(neo4j_tx, job, input_artefacts))

    # Loop over jobs outside of the transaction, since doing it inside might cause conditions where
    # we attempt to modify the job before it has been commited to the database
    for worker in ready_workers:
        logger.debug(f"All input artefacts found for {worker.job}. Processing directly...")
        WorkerQueue.add_to_queue(worker)

    await msg.ack()


async def on_input_received(msg: Msg) -> None:
    """
    Subscriber to artefact.* NATS messages
    """
    maestro_id = msg.data.decode()
    logger.debug(f"Received {msg.subject}(maestro_id='{maestro_id}')")

    # Wait for the artefact to be available in the database. This is necessary if the artefact was created
    # on a different instance and this instance's database is not yet up-to-date.
    while True:
        async with Neo4jTransaction() as neo4j_tx:
            if await read_node(neo4j_tx, BaseArtefactModel, {"maestro_id": maestro_id}) is not None:
                break
        logger.info(f"Waiting for artefact {maestro_id} to be available in the database...")
        await asyncio.sleep(0.5)

    # Find jobs, if any, that now has all inputs and can be moved to running
    ready_workers: list[Worker] = []
    async with Neo4jTransaction() as neo4j_tx:
        pending_jobs = await queue.get_pending_jobs(neo4j_tx)
        for job in pending_jobs:
            # Skip jobs that are not for this instance
            if job.instance_id != Config.INSTANCE_ID:
                continue

            try:
                input_artefacts = await fetch_inputs(neo4j_tx, job.inputs)
            except InputNotFound:
                logger.debug(
                    f"{job.workflow_step}: Not all input artefacts found for job. Leaving job pending..."
                )
            else:
                job.status = JobStatus.QUEUED
                await merge_job_node(neo4j_tx, job)
                ready_workers.append(await Worker.create(neo4j_tx, job, input_artefacts))

    # Loop over jobs outside of the transaction, since doing it inside might cause conditions where
    # we attempt to modify the job before it has been commited to the database
    for worker in ready_workers:
        logger.debug(f"All input artefacts found for {worker.job}. Processing...")
        WorkerQueue.add_to_queue(worker)

    await msg.ack()


async def on_cancel_received(msg: Msg) -> None:
    maestro_id = uuid.UUID(msg.data.decode())
    await WorkerQueue.cancel(maestro_id)
    await msg.ack()
