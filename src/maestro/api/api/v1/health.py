from fastapi import APIRouter, Response

router = APIRouter()


@router.get("/_check")
async def health_check() -> Response:
    return Response(status_code=200, content="Status: OK")
