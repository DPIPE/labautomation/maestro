from fastapi import APIRouter, Depends

from maestro.api.api.config import Settings, settings
from maestro.api.api.v1 import cancel, health, invalidate, reorder, resume

api_v1_router = APIRouter()


@api_v1_router.get("/")
def home(settings: Settings = Depends(settings)) -> dict:
    return {
        "uri": settings.PROJECT_NAME,
        "api": settings.PROJECT_URI,
        "project": settings.API_STR,
    }


api_v1_router.include_router(health.router, prefix="/health", tags=["health"])
api_v1_router.include_router(invalidate.router, prefix="/invalidate", tags=["invalidate"])
api_v1_router.include_router(reorder.router, prefix="/reorder", tags=["reorder"])
api_v1_router.include_router(cancel.router, prefix="/cancel", tags=["cancel"])
api_v1_router.include_router(resume.router, prefix="/resume", tags=["resume"])
