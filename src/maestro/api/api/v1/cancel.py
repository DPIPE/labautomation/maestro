from fastapi import APIRouter, HTTPException, Request
from fastapi.responses import HTMLResponse
from pydantic import UUID4

from maestro.api.api import templates
from maestro.models import Job, JobStatus
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import read_nodes
from maestro.publishers import publish_cancel
from maestro.workflow_utils import cancel_pending_job

router = APIRouter()


@router.put("/job/{maestro_id}", status_code=200)
async def cancel_job_endpoint(
    maestro_id: UUID4,
    request: Request,
) -> HTMLResponse:
    try:
        async with Neo4jTransaction() as neo4j_tx:
            db_jobs = await read_nodes(neo4j_tx, Job, {"maestro_id": maestro_id})

        assert len(db_jobs) > 0, "Failed to cancel job, job not found"
        db_job = db_jobs[0]

        if db_job.status == JobStatus.PENDING:
            await cancel_pending_job(maestro_id)
        elif db_job.status.is_processing():
            await publish_cancel(maestro_id)
        else:
            raise ValueError(
                "Only job with status pending, running, queued or halted can be cancelled"
            )

        return templates.TemplateResponse(request, "confirm.html", {"button": "abort"})
    # consider common excpetion catcher/wrapper for all requests
    except (AssertionError, ValueError) as e:
        raise HTTPException(status_code=400, detail=f"{e}")
