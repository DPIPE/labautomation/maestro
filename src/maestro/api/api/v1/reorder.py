from fastapi import APIRouter, HTTPException, Request
from fastapi.responses import HTMLResponse
from pydantic import UUID4

from maestro.api.api import templates
from maestro.workflow_utils import reorder_job

router = APIRouter()


@router.put("/job/{maestro_id}", status_code=200)
async def reorder_job_endpoint(
    maestro_id: UUID4,
    request: Request,
) -> HTMLResponse:
    try:
        await reorder_job(maestro_id)
        return templates.TemplateResponse(request, "confirm.html", {"button": "reorder"})
    # consider common excpetion catcher/wrapper for all requests
    except ValueError as e:
        raise HTTPException(status_code=400, detail=f"{e}")
