from collections.abc import Callable, Coroutine
from typing import Annotated, Any

from fastapi import APIRouter, Depends, Form, HTTPException, Request
from fastapi.responses import HTMLResponse
from pydantic import UUID4

from maestro.api.api import templates
from maestro.models import Job, WorkflowStepModel
from maestro.neo4j.invalidate import (
    invalidate_artefacts,
    invalidate_workflow_steps,
    invalidate_workflows,
)
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import fetch_node_by_rel

router = APIRouter()


def valid_node_exists(node_type: str) -> Callable[[UUID4, bool], Coroutine[Any, Any, UUID4]]:
    async def valid_node_exists_wrapper(
        maestro_id: Annotated[UUID4, Form()],
        propagate: Annotated[bool, Form()],
    ) -> UUID4:
        """
        Verify node of given type exists and is not invalidated,
        raise 400 if node is already invalidated and propagate is False
        """
        async with Neo4jTransaction() as neo4j_tx:
            exists = await neo4j_tx.query(
                f"""
                MATCH (n:{node_type} {{maestro_id: $maestro_id}})
                RETURN count(n) > 0 as exists, n.invalidated_at as invalidated_at
                """,
                {"maestro_id": maestro_id},
            )
            if not exists or not exists[0]["exists"]:
                raise HTTPException(status_code=404, detail=f"{node_type} {maestro_id} not found")
            # If the node is invalidated, but we are not propagating, raise an error
            elif exists[0]["invalidated_at"] and not propagate:
                raise HTTPException(
                    status_code=400, detail=f"{node_type} {maestro_id} already invalidated"
                )
        return maestro_id

    return valid_node_exists_wrapper


@router.get("/count/{node_type}/{maestro_id}/{propagate}", status_code=200)
async def get_invalidation_count(
    request: Request,
    node_type: str,
    maestro_id: UUID4,
    propagate: bool = False,
) -> HTMLResponse:
    """
    Get count of nodes that would be invalidated based on node type and propagation setting.

    Args:
        node_type: Type of node to count invalidations for ('workflow', 'workflow_step', or 'artefact')
        maestro_id: UUID of the node
        propagate: Whether to include downstream nodes that would be invalidated

    Returns:
        Dictionary containing counts of affected nodes by type

    Raises:
        HTTPException: If node not found (404) or invalid node type (422)
    """
    if node_type not in ["workflow", "workflow_step", "artefact"]:
        raise HTTPException(status_code=422, detail=f"Invalid node type: {node_type}")

    async with Neo4jTransaction() as neo4j_tx:
        if node_type == "workflow":
            query = """
                MATCH (w:WorkflowModel {maestro_id: $maestro_id})
                WHERE w IS NOT NULL
                WITH w, 1 as workflow_count

                OPTIONAL MATCH (w)-[*]-(ws:WorkflowStepModel)-[*]->(a:BaseArtefactModel)
                WHERE ws.invalidated_at IS NULL AND a.invalidated_at IS NULL
                WITH workflow_count, COLLECT(DISTINCT ws) as workflow_steps, COLLECT(DISTINCT a) as artefacts

                RETURN
                    workflow_count,
                    SIZE(workflow_steps) as workflow_step_count,
                    SIZE(artefacts) as artefact_count
            """
        elif node_type == "workflow_step":
            query = """
                MATCH (ws:WorkflowStepModel {maestro_id: $maestro_id})
                WHERE ws IS NOT NULL
                WITH ws, 1 as workflow_step_count

                OPTIONAL MATCH (ws)-[:OUTPUT]->(a:BaseArtefactModel)
                WHERE a.invalidated_at IS NULL
                WITH workflow_step_count, COLLECT(DISTINCT a) as direct_artefacts

                OPTIONAL MATCH (a)-[*]->(dep_ws:WorkflowStepModel)-[*]->(dep_a:BaseArtefactModel)
                WHERE $propagate AND a IN direct_artefacts
                    AND dep_ws.invalidated_at IS NULL
                    AND dep_a.invalidated_at IS NULL

                WITH workflow_step_count, direct_artefacts, COLLECT(DISTINCT dep_ws) as dep_workflow_steps, COLLECT(DISTINCT dep_a) as dep_artefacts

                RETURN
                    workflow_step_count + SIZE(dep_workflow_steps) as workflow_step_count,
                    SIZE(direct_artefacts) + SIZE(dep_artefacts) as artefact_count
            """
        else:  # artefact
            query = """
                MATCH (a:BaseArtefactModel {maestro_id: $maestro_id})
                WHERE a IS NOT NULL
                WITH a, 1 as main_artefact_count

                OPTIONAL MATCH (a)-[*]->(dep_ws:WorkflowStepModel)-[*]->(dep_a:BaseArtefactModel)
                WHERE $propagate
                    AND dep_ws.invalidated_at IS NULL
                    AND dep_a.invalidated_at IS NULL

                WITH main_artefact_count, COLLECT(DISTINCT dep_ws) as dep_workflow_steps, COLLECT(DISTINCT dep_a) as dep_artefacts

                RETURN
                    SIZE(dep_workflow_steps) as workflow_step_count,
                    main_artefact_count + SIZE(dep_artefacts) as artefact_count
            """

        result = await neo4j_tx.query(query, {"maestro_id": maestro_id, "propagate": propagate})

    if not result:
        raise HTTPException(status_code=404, detail=f"{node_type.title()} {maestro_id} not found")
    response = {
        "workflow_step_count": result[0]["workflow_step_count"],
        "artefact_count": result[0]["artefact_count"],
    }
    if "workflow_count" in dict(result[0]):
        response["workflow_count"] = result[0]["workflow_count"]

    return templates.TemplateResponse(request, "invalidate_warning.html", {"count": response})


@router.put("/artefact", status_code=200)
async def invalidate_artefact(
    maestro_id: Annotated[UUID4, Depends(valid_node_exists("BaseArtefactModel"))],
    propagate: Annotated[bool, Form()],
    request: Request,
) -> HTMLResponse:
    async with Neo4jTransaction() as neo4j_tx:
        await invalidate_artefacts(neo4j_tx, [maestro_id], propagate)
    return templates.TemplateResponse(
        request,
        "confirm.html",
        {
            "type": "artefact",
            "button": "invalidate",
            "maestro_id": maestro_id,
        },
    )


@router.put("/workflow_step", status_code=200)
async def invalidate_workflow_step(
    maestro_id: Annotated[UUID4, Depends(valid_node_exists("WorkflowStepModel"))],
    propagate: Annotated[bool, Form()],
    request: Request,
) -> HTMLResponse:
    async with Neo4jTransaction() as neo4j_tx:
        await invalidate_workflow_steps(neo4j_tx, [maestro_id], propagate)
        job = await fetch_node_by_rel(neo4j_tx, WorkflowStepModel, {"maestro_id": maestro_id}, Job)
    assert job is not None, f"Job not found for workflow step {maestro_id}"
    return templates.TemplateResponse(
        request,
        "confirm.html",
        {
            "type": "workflow_step",
            "button": "invalidate",
            "maestro_id": maestro_id,
            "job_maestro_id": job.maestro_id,
        },
    )


@router.put("/workflow", status_code=200)
async def invalidate_workflow(
    maestro_id: Annotated[UUID4, Depends(valid_node_exists("WorkflowModel"))],
    propagate: Annotated[bool, Form()],
    request: Request,
) -> HTMLResponse:
    async with Neo4jTransaction() as neo4j_tx:
        await invalidate_workflows(neo4j_tx, [maestro_id], propagate)
    return templates.TemplateResponse(
        request,
        "confirm.html",
        {
            "type": "workflow",
            "button": "invalidate",
            "maestro_id": maestro_id,
        },
    )
