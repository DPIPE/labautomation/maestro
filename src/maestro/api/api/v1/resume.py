from fastapi import APIRouter, HTTPException, Request
from fastapi.responses import HTMLResponse
from pydantic import UUID4

from maestro.api.api import templates
from maestro.models import Job, JobStatus
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import read_node
from maestro.workflow_utils import resume_halted_job

router = APIRouter()


@router.put("/job/{maestro_id}", status_code=200)
async def resume_halted_job_endpoint(
    maestro_id: UUID4,
    request: Request,
) -> HTMLResponse:
    try:
        async with Neo4jTransaction() as neo4j_tx:
            db_job = await read_node(neo4j_tx, Job, {"maestro_id": maestro_id})

        assert db_job is not None, f"Failed to resume job, job {maestro_id} not found"

        if db_job.status == JobStatus.HALTED:
            await resume_halted_job(maestro_id)
        else:
            raise ValueError(
                f"Only job with status halted can be resumed, job {maestro_id} has status {db_job.status}"
            )

        return templates.TemplateResponse(request, "confirm.html", {"button": "resume"})
    except (AssertionError, ValueError) as e:
        raise HTTPException(status_code=400, detail=f"{e}")
