from collections.abc import Callable
from datetime import datetime
from pathlib import Path
from typing import TypeVar

from jinja2 import Environment, FileSystemLoader, StrictUndefined
from fastapi.templating import Jinja2Templates

# Get path to templates directory relative to this file
templates_dir = Path(__file__).parent.parent.parent / "templates"

# Create Jinja environment with strict undefined variable handling
jinja_env = Environment(loader=FileSystemLoader(templates_dir), undefined=StrictUndefined)

# Initialize FastAPI templates with our custom environment
templates = Jinja2Templates(env=jinja_env)

T = TypeVar("T", bound=Callable)


# Decorators to add custom functions and filters to Jinja2
def template_function(func: T) -> T:
    templates.env.globals[func.__name__] = func
    return func


def template_filter(func: T) -> T:
    templates.env.filters[func.__name__] = func
    return func


# Custom filters
@template_filter
def dateformat(input: datetime) -> str:
    """Show datetime as 2024-05-29 07:29:26"""
    return input.strftime("%Y-%m-%d %H:%M:%S")


# Custom functions
@template_function
def now() -> str:
    return dateformat(datetime.now())
