import os

from fastapi import Request
from pydantic import AnyHttpUrl
from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    API_STR: str = "/api/v1"
    PROJECT_NAME: str = "Maestro API"
    PROJECT_URI: AnyHttpUrl | None = AnyHttpUrl("http://localhost")
    UI_URI: AnyHttpUrl | None = AnyHttpUrl("http://localhost")

    CORS_ORIGIN: set[str] = set("http://localhost")

    model_config = SettingsConfigDict(
        env_file=os.getenv("MAESTRO_API_DOTENV"), env_file_encoding="utf-8"
    )


def settings(request: Request) -> Settings:
    # setup for custom settings to be used later
    settings = Settings()
    return settings
