"""
Functions to download and compile static files after installation.
"""

import hashlib
import logging
import subprocess
from dataclasses import dataclass
from pathlib import Path
from urllib import request

from maestro.config import Config

MAESTRO_ROOT = Path(__file__).parent.parent
STATIC_FOLDER = MAESTRO_ROOT / "static"

TAILWIND_VERSION = "v3.3.3"
logger = logging.getLogger("maestro")


def md5_from_file(file: Path) -> str:
    with file.open("rb") as f:
        return hashlib.md5(f.read()).hexdigest()


def compile_tailwind() -> None:
    """
    Compile tailwind css.

    If production is True, the tailwind binary will be run with the --minify flag.
    If production is False, the tailwind binary will be run with the --watch=always flag.

    On inital run, the tailwind binary will be downloaded from the tailwindlabs/tailwindcss releases.
    """

    # Check if tailwind binary exists, is the correct version, and is not corrupt
    tailwind_binary = Path(f"/tmp/tailwind_{TAILWIND_VERSION}")
    try:
        output = subprocess.check_output([str(tailwind_binary), "--help"])
        if f"tailwindcss {TAILWIND_VERSION}" not in output.decode():
            logger.error("Tailwindcss binary is not the correct version")
            tailwind_binary.unlink()
        logger.info(f"Tailwindcss binary found: {tailwind_binary}")
    except FileNotFoundError:
        logger.info(f"Tailwindcss binary not found: {tailwind_binary}")
    except (subprocess.CalledProcessError, PermissionError):
        logger.error("Corrupt tailwindcss binary found")
        tailwind_binary.unlink()

    # Download tailwindcss binary if not exists
    if not tailwind_binary.exists():
        logger.info(f"Downloading tailwindcss binary to {tailwind_binary}")
        tailwind_binary_url = f"https://github.com/tailwindlabs/tailwindcss/releases/download/{TAILWIND_VERSION}/tailwindcss-linux-x64"
        response = request.urlopen(tailwind_binary_url)
        with tailwind_binary.open("wb") as f:
            f.write(response.read())
        # Make executable
        tailwind_binary.chmod(0o775)

    # Run tailwindcss binary
    tailwind_config = MAESTRO_ROOT / "tailwind.config.js"
    tailwind_output = STATIC_FOLDER / "css/tailwind.css"
    tailwind_input = STATIC_FOLDER / "css/input.css"
    assert tailwind_config.exists()
    tailwind_cli_args = [
        str(tailwind_binary),
        "-c",
        str(tailwind_config),
        "-i",
        str(tailwind_input),
        "-o",
        str(tailwind_output),
    ]
    if Config.ENV == "development":
        tailwind_cli_args.append(
            "--watch=always"
        )  # Note: --watch is insufficient: https://github.com/tailwindlabs/tailwindcss/issues/12954
    else:
        tailwind_cli_args.append("--minify")

    tailwind_process = subprocess.Popen(tailwind_cli_args, stdin=subprocess.PIPE)
    if Config.ENV == "development":
        logger.info(f"Building tailwindcss for development: {' '.join(tailwind_cli_args)}")
    else:
        logger.info(f"Building tailwindcss for production: {' '.join(tailwind_cli_args)}")
        tailwind_process.wait(30)
        tailwind_binary.unlink()


def download_static_files() -> None:
    @dataclass
    class StaticFile:
        file: Path
        url: str
        md5sum: str

    # Download static js files, if not already downloaded
    js_files = [
        StaticFile(
            file=STATIC_FOLDER / "js/htmx.min.js",
            url="https://unpkg.com/htmx.org@1.9.5",
            md5sum="df6c57fe3e87e94a65e93bff9fd98709",
        ),
        StaticFile(
            file=STATIC_FOLDER / "js/hyperscript.min.js",
            url="https://unpkg.com/hyperscript.org@0.9.12",
            md5sum="0330c9319d47c891652ce3e9ba4ed987",
        ),
        StaticFile(
            file=STATIC_FOLDER / "js/idiomorph-ext.min.js",
            url="https://unpkg.com/idiomorph@0.3.0/dist/idiomorph-ext.min.js",
            md5sum="a4453fde77250e2655e5e2488e9fe98a",
        ),
    ]

    for static_file in js_files:
        if not static_file.file.exists() or md5_from_file(static_file.file) != static_file.md5sum:
            try:
                logger.info(f"Downloading {static_file.file} from {static_file.url}")
                response = request.urlopen(static_file.url)
                with static_file.file.open("wb") as f:
                    f.write(response.read())
            except Exception:
                logger.error(f"Error downloading {static_file.file}")
                raise


def maestro_post_install() -> None:
    download_static_files()
    compile_tailwind()
