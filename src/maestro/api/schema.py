import re
from datetime import datetime
from typing import Any

from pydantic import UUID4, BaseModel, computed_field, field_validator

from maestro.models import (
    BaseArtefactModel,
    Job,
    JobPriorityEnum,
    JobStatus,
    MaestroBase,
    WorkflowStepModel,  # noqa: F401
    WorkflowStepModelWithArtefacts,  # noqa: F401
)


class JobWithWorkflowStep(Job):
    workflow_step_maestro_id: UUID4 | None = None
    workflow_step_invalidated_at: datetime | None = None


class JobWithInputs(Job):
    workflow_name: str
    created: list[BaseArtefactModel] | None = None
    formatted_pending: list[dict[str, Any]] | None = None

    def __init__(self, **data: Any) -> None:
        super().__init__(**data)
        self.created = self.created or []
        self.formatted_pending = self.formatted_pending or []

    def format_pending(
        self, input_class_name: str, input_filter: dict | None = None
    ) -> dict[str, Any]:
        """Format pending input details into a standardized dictionary format.

        Args:
            input_class_name: Name of the input class that is pending
            input_filter: Filter criteria for the pending input

        Returns:
            Dictionary containing the input type and identifier criteria
        """
        formatted = {
            "type": input_class_name,
            "identifier": {k: v for k, v in (input_filter or {}).items()},
        }
        return formatted


class WorkflowModelWithDetails(MaestroBase):
    workflow_name: str
    jobs: set[JobWithWorkflowStep]
    search_metadata: list


class WorkflowCounts(MaestroBase):
    pending: int = 0
    queued: int = 0
    running: int = 0
    completed: int = 0
    cancelled: int = 0
    failed: int = 0
    halted: int = 0

    @computed_field  # type: ignore[prop-decorator]
    @property
    def all(self) -> int:
        return sum(
            getattr(self, field)
            for field in self.model_fields
            if isinstance(getattr(self, field), int)
        )


SEARCH_REGEX = re.compile(
    r"((?<=(^))|(?<=\s))(?!status)(?!priority)(?!-)(?P<search>[^\s]+)"  # Search terms
    r"|((?<=(^))|(?<=\s))(?!status)(?!priority)-(?P<negate>[^\s]+)"  # Negated terms
    r"|(?<=status:)(?P<status>[^\s]+)"  # Status terms
    r"|(?<=priority:)(?P<priority>[^\s]+)",  # Priority terms
    re.IGNORECASE,
)


class Search(BaseModel):
    statuses: set[JobStatus] = set()
    priorities: set[JobPriorityEnum] = set()
    search_terms: list[str] = []
    negate_terms: list[str] = []

    @field_validator("priorities", mode="before")
    @classmethod
    def parse_priorities(cls, v: dict[str, str]) -> set[JobPriorityEnum]:
        priorities_query = set()
        for priority in v:
            priorities_query.add(getattr(JobPriorityEnum, priority.upper()))
        return priorities_query

    @field_validator("statuses", mode="before")
    @classmethod
    def parse_statuses(cls, v: dict[str, str]) -> set[JobStatus]:
        statuses_query = set()
        for status in v:
            statuses_query.add(getattr(JobStatus, status.upper()))
        return statuses_query

    @classmethod
    def from_string(cls, search: str = "") -> "Search":
        "Build a Search object from a string, by using the SEARCH_REGEX"
        kwargs: dict[str, list] = {
            "search_terms": [],
            "negate_terms": [],
            "statuses": [],
            "priorities": [],
        }
        for m in SEARCH_REGEX.finditer(search):
            match m.lastgroup:
                case "priority":
                    kwargs["priorities"].extend(m.group("priority").lower().split(","))
                case "search":
                    kwargs["search_terms"].append(m.group("search"))
                case "negate":
                    kwargs["negate_terms"].append(m.group("negate"))
                case "status":
                    kwargs["statuses"].extend(m.group("status").lower().split(","))

        return Search(**kwargs)
