from typing import Annotated

from ansi2html import Ansi2HTMLConverter
from fastapi import APIRouter, Depends, Request
from fastapi.responses import HTMLResponse, PlainTextResponse
from pydantic import UUID4

from maestro.api import __version__, queries
from maestro.api.api import templates
from maestro.api.schema import Search
from maestro.neo4j.neo4j import Neo4jTransaction, provide_neo4j_transaction

router = APIRouter()
router.default_response_class = HTMLResponse


# Define the ansi2html filter function
# Create a single converter instance
_ansi_converter = Ansi2HTMLConverter()


def ansi2html_filter(value: str) -> str:
    """Convert ANSI escape sequences to HTML.

    Args:
        value: String containing ANSI escape sequences

    Returns:
        HTML formatted string
    """
    try:
        return str(_ansi_converter.convert(value, full=False))
    except Exception:
        return str(value)  # Fallback to original string on conversion error


# Add the filter to the Jinja2 environment
templates.env.filters["ansi2html"] = ansi2html_filter


@router.get("/")
async def get_index(
    request: Request,
) -> HTMLResponse:
    return templates.TemplateResponse(request, "index.html")


@router.get("/api/version", response_class=PlainTextResponse)
async def get_version() -> PlainTextResponse:
    return PlainTextResponse(content=__version__)


@router.get("/content/workflow-count")
async def get_workflow_job_count(
    request: Request,
    neo4j_tx: Annotated[Neo4jTransaction, Depends(provide_neo4j_transaction, use_cache=False)],
    search: Annotated[Search, Depends(Search.from_string)],
) -> HTMLResponse:
    workflow_count = await queries.workflows.get_workflow_counts(neo4j_tx, search)
    # TODO: return updated template when available
    return templates.TemplateResponse(
        request, "content_job_count.html", {"jobs_count": workflow_count}
    )


@router.get("/content/workflows")
async def get_workflows(
    request: Request,
    neo4j_tx: Annotated[Neo4jTransaction, Depends(provide_neo4j_transaction, use_cache=False)],
    search: Annotated[Search, Depends(Search.from_string)],
    withArtefacts: bool = False,
    page: int = 0,
    per_page: int = 100,
) -> HTMLResponse:
    workflows = await queries.workflows.get_workflows(
        neo4j_tx, search=search, page=page, per_page=per_page
    )
    has_more = (
        len(workflows) == per_page
    )  # Assuming if workflows length equals per_page, there might be more
    return templates.TemplateResponse(
        request,
        "workflows.html",
        {"workflows": workflows, "page": page, "per_page": per_page, "has_more": has_more},
    )


@router.get("/content/workflow_step/{workflow_step_maestro_id}/artefacts")
async def get_workflow_step_artefacts(
    request: Request,
    neo4j_tx: Annotated[Neo4jTransaction, Depends(provide_neo4j_transaction, use_cache=False)],
    workflow_step_maestro_id: UUID4,
    job_name: str,
    job_status: Annotated[str, "JobStatus.FAILED"],
    job_maestro_id: UUID4,
) -> HTMLResponse:
    workflow_step = await queries.workflows.get_workflow_step(neo4j_tx, workflow_step_maestro_id)
    workflow_name = await queries.workflows.get_workflow_name(neo4j_tx, workflow_step_maestro_id)
    workflow_step_artefacts = await queries.workflows.get_workflow_step_artefacts(
        neo4j_tx, workflow_step_maestro_id=workflow_step_maestro_id
    )
    return templates.TemplateResponse(
        request,
        "workflow_step_details.html",
        {
            "workflow_name": workflow_name,
            "workflow_step": workflow_step,
            "workflow_step_artefacts": workflow_step_artefacts,
            "job_name": job_name,
            "job_status": job_status,
            "job_maestro_id": job_maestro_id,
        },
    )


@router.get("/content/job/{job_maestro_id}")
async def get_job(
    request: Request,
    neo4j_tx: Annotated[Neo4jTransaction, Depends(provide_neo4j_transaction, use_cache=False)],
    job_maestro_id: UUID4,
) -> HTMLResponse:
    """Get details for a specific job including its input artefacts.

    Args:
        request: FastAPI request object
        neo4j_tx: Neo4j transaction dependency
        job_maestro_id: maestro_id (UUID) of the job to fetch

    Returns:
        HTML response with job details template
    """
    try:
        job_with_inputs = await queries.jobs.get_job_with_inputs(neo4j_tx, job_maestro_id)
        return templates.TemplateResponse(
            "job_details.html",
            {
                "request": request,
                "job": job_with_inputs,
                "workflow_name": job_with_inputs.workflow_name,
            },
        )
    except AssertionError as e:
        # Handle case where job is not found or not in pending, cancelled or failed status
        return templates.TemplateResponse(
            "error.html", {"request": request, "error": str(e)}, status_code=404
        )
