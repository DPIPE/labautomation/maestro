import importlib.util
import logging
import sys
import time
from collections.abc import AsyncGenerator, Awaitable, Callable
from contextlib import asynccontextmanager
from pathlib import Path

from fastapi import FastAPI, Request, Response
from fastapi.staticfiles import StaticFiles

from maestro.api import index
from maestro.api.api import templates
from maestro.api.api.config import Settings
from maestro.api.api.v1.api import api_v1_router
from maestro.config import Config
from maestro.log.config import LOGGING_CONFIG

MAESTRO_ROOT = Path(__file__).parent.parent
STATIC_FOLDER = MAESTRO_ROOT / "static"

logger = logging.getLogger("maestro-api")
MWCNFunc = Callable[[Request], Awaitable[Response]]


def pre_load_models(files: list[Path]) -> None:
    """
    Load BaseArtefactModel subclasses from files

    This is required to register any models that are not imported in the API code, and can be used to
    register arbitrary models to show custom HTML for them in the UI.

    Note: This runs the code in the supplied files, so they should not contain any code that has side effects
    """

    def _load_model_from_file(file: Path) -> None:
        "Utilize the importlib machinery to load a module from a file"
        module_name = f"maestro_pre_loaded.{file.stem}"
        spec = importlib.util.spec_from_file_location(module_name, file)
        assert spec and spec.loader, f"Unable to load module from file {file}"
        module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module)
        sys.modules[module_name] = module

    for file in files:
        logger.info(f"Loading models from file: {file}")
        try:
            _load_model_from_file(file)
        except Exception as e:
            logger.error(f"Error loading models from file {file}: {e}")


@asynccontextmanager
async def hot_reload_lifespan(app: FastAPI) -> AsyncGenerator[None, None]:
    import arel

    hotreload = arel.HotReload(
        paths=[arel.Path("/app/src")],
    )
    templates.env.globals["hotreload"] = hotreload
    app.add_websocket_route("/hot-reload", hotreload, name="hot-reload")  # type: ignore[arg-type]
    await hotreload.startup()
    yield
    await hotreload.shutdown()


def create_api(settings: Settings | None = None) -> FastAPI:
    # Explicitly set IS_RUNNING_IN_API to True - this is used to loosen validation not required for running in API
    Config.IS_RUNNING_IN_API = True

    # Load models from specified files
    # This is required to register any models that are not imported in the API code, and can be used to
    # register arbitrary models to show custom HTML for them in the UI
    # Note 1: This is not required for the API to function, only for the UI to display custom HTML for models
    # Note 2: The supplied files should not contain any code that has side effects, as they will be executed as part of import
    pre_load_models(Config.LOAD_MODELS_FROM)

    if settings is None:
        settings = Settings()

    if Config.ENV == "development":
        print("Running in development mode")
        lifespan = hot_reload_lifespan
    else:
        lifespan = None

    app = FastAPI(
        title=settings.PROJECT_NAME,
        openapi_url=f"{settings.API_STR}/openapi.json",
        lifespan=lifespan,
    )

    app.include_router(index.router)
    app.include_router(api_v1_router, prefix=settings.API_STR)

    if Config.ENV == "development":

        @app.middleware("http")
        async def no_cache(request: Request, call_next: MWCNFunc) -> Response:
            response = await call_next(request)
            response.headers["Cache-Control"] = "no-cache"
            return response

    app.mount("/static", StaticFiles(directory=STATIC_FOLDER), name="static")

    @app.middleware("http")
    async def logging_middleware(request: Request, call_next: MWCNFunc) -> Response:
        start = time.perf_counter()
        response = await call_next(request)
        duration = (time.perf_counter() - start) * 1000
        if not request.client:
            source = "unknown"
        else:
            source = f"{request.client.host}:{request.client.port}"
        resource = f"{request.method} {request.url.path}"
        result = f"{response.status_code} [{duration:.1f}ms]"
        message = f"{source} => {resource} => {result}"
        logger.info(
            message,
            extra={"duration": duration, "method": request.method, "path": request.url.path},
        )
        return response

    return app


def start() -> None:
    import uvicorn

    uvicorn.run(
        "maestro.api.main:create_api",
        host="0.0.0.0",
        factory=True,
        reload=Config.ENV == "development",
        port=Config.API_PORT,
        log_config=LOGGING_CONFIG,
        access_log=False,
    )


if __name__ == "__main__":
    from maestro.api.post_install import maestro_post_install

    maestro_post_install()
    start()
