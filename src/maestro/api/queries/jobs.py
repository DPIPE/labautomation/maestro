from pydantic import UUID4

from maestro.api.schema import JobWithInputs
from maestro.exceptions import InputNotFound
from maestro.models import Job, JobStatus, WorkflowModel
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import fetch_input, fetch_node_by_rel, read_node


async def get_job_with_inputs(neo4j_tx: Neo4jTransaction, job_maestro_id: UUID4) -> JobWithInputs:
    """Get a job and its input artefacts, categorizing them as required, created or pending.

    Args:
        neo4j_tx: Neo4j transaction to use for queries
        job_maestro_id: maestro_id (UUID) of the job to fetch

    Returns:
        JobWithInputs containing the job details and categorized input artefacts

    Raises:
        AssertionError: If job not found or not in pending, cancelled or failed status
    """
    # Fetch the job
    # Fetch and validate job exists and is in pending, cancelled or failed status
    db_job = await read_node(neo4j_tx, Job, {"maestro_id": job_maestro_id})
    if db_job is None:
        raise AssertionError("Job not found")
    if (
        db_job.status != JobStatus.PENDING
        and db_job.status != JobStatus.CANCELLED
        and db_job.status != JobStatus.FAILED
    ):
        raise AssertionError(f"Job is not pending, cancelled or failed (status: {db_job.status})")

    # Fetch associated workflow to get workflow name
    db_workflow = await fetch_node_by_rel(
        neo4j_tx, Job, {"maestro_id": job_maestro_id}, WorkflowModel
    )
    workflow_name = db_workflow.workflow_name if db_workflow else None

    # Initialize JobWithInputs with job data and workflow name
    job_with_inputs = JobWithInputs(**db_job.model_dump(), workflow_name=workflow_name)

    # Process each input, categorizing as either created or pending
    for _input_name, (input_class, input_filter) in db_job.inputs.items():
        try:
            # Check if input artefact already exists
            input_artefact = await fetch_input(neo4j_tx, input_class, input_filter)
            job_with_inputs.created.append(input_artefact)  # type: ignore[union-attr]
        except InputNotFound:
            # Format and track pending inputs
            formatted_pending = job_with_inputs.format_pending(input_class.__name__, input_filter)
            job_with_inputs.formatted_pending.append(formatted_pending)  # type: ignore[union-attr]

    return job_with_inputs
