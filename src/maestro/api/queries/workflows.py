from typing import Any

from pydantic import UUID4

from maestro.api.schema import (
    JobWithWorkflowStep,
    Search,
    WorkflowCounts,
    WorkflowModelWithDetails,
)
from maestro.models import WorkflowStepModel, WorkflowStepModelWithArtefacts
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import read_node

WORKFLOW_STATUS_CTE = """
    WITH w
    MATCH (w)--(all_jobs:Job)
    WITH w, w.maestro_id as maestro_id, collect(DISTINCT all_jobs.status) as job_statuses
    WITH w, maestro_id, CASE
        WHEN 'failed' in job_statuses THEN 'failed'
        WHEN 'cancelled' in job_statuses THEN 'cancelled'
        WHEN 'halted' in job_statuses THEN 'halted'
        WHEN 'running' in job_statuses THEN 'running'
        WHEN 'queued' in job_statuses THEN 'queued'
        WHEN 'pending' in job_statuses THEN 'pending'
        ELSE 'completed'
    END as workflow_status"""


def build_search_query(search: Search) -> tuple[str, dict[str, Any]]:
    query = """
        MATCH (w:WorkflowModel)--(j:Job)
        WHERE substring(w.workflow_name, 0, 1) <> "_"
    """
    parameters: dict[str, Any] = {}
    for term_index, search_pattern in enumerate(search.search_terms):
        query += (
            f" AND (w.workflow_name =~ '(?i).*' + $search{term_index} + '.*' "
            f"OR ANY(p IN j.search_metadata WHERE p =~ '(?i).*' + $search{term_index} + '.*') "
            f"OR ANY(p IN j.display_name WHERE p =~ '(?i).*' + $search{term_index} + '.*')) "
        )
        parameters[f"search{term_index}"] = search_pattern

    for term_index, negate_pattern in enumerate(search.negate_terms):
        query += (
            f" AND NOT (w.workflow_name =~ '(?i).*' + $negate{term_index} + '.*' "
            f"OR ANY(p IN j.search_metadata WHERE p =~ '(?i).*' + $negate{term_index} + '.*') "
            f"OR ANY(p IN j.display_name WHERE p =~ '(?i).*' + $negate{term_index} + '.*')) "
        )
        parameters[f"negate{term_index}"] = negate_pattern

    if search.priorities:
        query += " AND ANY(priority IN j.priority WHERE priority IN $priorities) "
        parameters["priorities"] = search.priorities

    if search.statuses:
        query += WORKFLOW_STATUS_CTE
        query += """
            WHERE workflow_status IN $statuses
            MATCH (w)--(j:Job)
        """
        parameters["statuses"] = search.statuses

    return query, parameters


async def get_workflows(
    neo4j_tx: Neo4jTransaction, search: Search, page: int = 0, per_page: int = 100
) -> list[WorkflowModelWithDetails]:
    query, parameters = build_search_query(search)
    parameters.update({"skip": page * per_page, "per_page": per_page})
    query += """
        WITH w, MAX(j.updated_at) as max_updated_at
        MATCH (w)--(j:Job)
        OPTIONAL MATCH (j)--(workflowstep:WorkflowStepModel)
        OPTIONAL MATCH (workflowstep)-[*2]-(filewatcherstep:FileWatcher)--(filewatcherjob:Job)
        WITH w, j, workflowstep, filewatcherstep, filewatcherjob, max_updated_at
        RETURN w.maestro_id as maestro_id,
            w.workflow_name as workflow_name,
            w.invalidated_at as invalidated_at,
            COLLECT([j, workflowstep.maestro_id, workflowstep.invalidated_at]) as workflows,
            COLLECT([filewatcherjob, filewatcherstep.maestro_id, filewatcherstep.invalidated_at]) as filestep,
            max_updated_at
        ORDER BY max_updated_at DESC
        SKIP $skip LIMIT $per_page
    """
    db_workflow_steps = await neo4j_tx.query(query, parameters=parameters)
    workflows = [
        WorkflowModelWithDetails(
            **record,
            jobs=[
                JobWithWorkflowStep(
                    **job,
                    workflow_step_maestro_id=ws_id,
                    workflow_step_invalidated_at=ws_invalidated_at,
                )
                for job, ws_id, ws_invalidated_at in record["workflows"] + record["filestep"]
                if job is not None
            ],
            search_metadata=[],
        )
        for record in db_workflow_steps
    ]
    return workflows


async def get_workflow_counts(neo4j_tx: Neo4jTransaction, search: Search) -> WorkflowCounts:
    """Count how many workflows are in each state, based on the following precedence:
    - If any job is failed, the workflow is failed
    - If any job is cancelled, the workflow is cancelled
    - If any job is halted, the workflow is halted
    - If any job is running, the workflow is running
    - If any job is queued, the workflow is queued for running
    - If any job is pending, the workflow is pending
    - Otherwise, this means that all jobs are completed, hence the workflow is successful
    """
    query, parameters = build_search_query(search)
    query += WORKFLOW_STATUS_CTE
    query += " RETURN workflow_status, COUNT(DISTINCT maestro_id) as count"

    counts = dict()
    db_workflow_counts = await neo4j_tx.query(query, parameters=parameters)
    for record in db_workflow_counts:
        counts[record["workflow_status"]] = record["count"]
    return WorkflowCounts(**counts)


async def get_workflow_step(
    neo4j_tx: Neo4jTransaction, workflow_step_maestro_id: UUID4
) -> WorkflowStepModel:
    workflow_step = await read_node(
        neo4j_tx, WorkflowStepModel, {"maestro_id": workflow_step_maestro_id}
    )
    assert workflow_step is not None, "Workflow step not found"
    return workflow_step


async def get_workflow_name(neo4j_tx: Neo4jTransaction, workflow_step_maestro_id: UUID4) -> str:
    query = """
        MATCH (w:WorkflowModel)-[*2]->(ws:WorkflowStepModel {maestro_id: $workflow_step_maestro_id})
        RETURN w.workflow_name as workflow_name
    """
    db_workflow_name = await neo4j_tx.query(
        query, parameters={"workflow_step_maestro_id": workflow_step_maestro_id}
    )

    return db_workflow_name[0]["workflow_name"]  # type: ignore[no-any-return]


async def get_workflow_step_artefacts(
    neo4j_tx: Neo4jTransaction, workflow_step_maestro_id: UUID4
) -> WorkflowStepModelWithArtefacts:
    """
    Given internal (maestro) worrkflow step ID, find and return all connected artefacts
    - we expect maximum four types of artefacts, each type will have list of artefacts
    - expected response types: INPUT, OUTPUT, STDOUT, STDERR
    """
    workflow_step = await read_node(
        neo4j_tx, WorkflowStepModelWithArtefacts, {"maestro_id": workflow_step_maestro_id}
    )
    assert workflow_step is not None, "Workflow step not found"
    return workflow_step
