import asyncio
import inspect
import logging
import multiprocessing
import os
import signal
import sys
from collections.abc import Iterator
from contextlib import contextmanager
from typing import TYPE_CHECKING

from setproctitle import setproctitle, setthreadtitle

from maestro.config import Config
from maestro.log.config import reconfigure_logging_for_child_process
from maestro.models import (
    BaseArtefactModel,
    Job,
    JobStatus,
    StdErrModel,
    StdOutModel,
    WorkflowStepModelWithArtefacts,
    WorkflowStepModelWithInput,
    WorkflowStepModelWithOutput,
    WorkflowStepModelWithStdOutAndStdErr,
)
from maestro.neo4j.invalidate import invalidate_workflow_steps
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import (
    create_node,
    create_relationship,
    merge_job_node,
    read_node,
)
from maestro.pipe import MaestroPipe
from maestro.prefixing_stream import PrefixingStream
from maestro.subreaper import Subreaper
from maestro.utils import multishield

if TYPE_CHECKING:
    from collections.abc import Sequence

logger = logging.getLogger("maestro")


class Worker:
    """
    Handle running of a workflow step, including child worker process creation and management.

    Note that some methods run in the context of the main process while others run in the
    child worker process.
    """

    @classmethod
    async def create(
        cls, neo4j_tx: Neo4jTransaction, job: Job, input_artefacts: dict[str, BaseArtefactModel]
    ) -> "Worker":
        worker = cls(job, input_artefacts)
        worker.workflow_step_model = WorkflowStepModelWithInput(
            additional_labels=worker.instance.labels(),
            search_metadata=worker.job.search_metadata,
            INPUT=worker.input_artefacts.values(),
            **worker.job.params,
        )
        await create_node(neo4j_tx, worker.workflow_step_model)
        await create_relationship(
            neo4j_tx, worker.job.maestro_id, worker.workflow_step_model.maestro_id, "WORKFLOW_STEP"
        )
        return worker

    def __init__(self, job: Job, input_artefacts: dict[str, BaseArtefactModel]):
        self.workflow_step = job.workflow_step_class
        self.job = job
        self.instance = self.workflow_step()
        self.instance._job = job
        self.input_artefacts = input_artefacts
        self.workflow_step_model: WorkflowStepModelWithInput | None = None

    async def run(self, sigchld_notifier: asyncio.Condition, subreaper: Subreaper) -> None:
        """
        Main entry point for running a worker process.

        This method runs in the context of the main process.

        Set job status to "running" and create the process and pipes for stdout, stderr and logging.

        Then wait for the worker process to finish and perform necessary cleanup.

        The "finally" block ensures that the worker process is terminated in case the
        coroutine is cancelled. First, it tries to terminate the child process and
        waits for it to finish. If the child process is still alive after the grace
        period, it is killed.
        """
        with self._create_worker_process_and_pipes(subreaper) as (
            process,
            stdout_pipe,
            stderr_pipe,
            logger_pipe,
        ):
            cancelled = False
            try:
                await _process_termination(process, sigchld_notifier)
            except asyncio.CancelledError:
                cancelled = True
                raise
            finally:
                # Finalization activity has to be protected from asyncio cancellations which
                # could be triggered by the user via the web-API.
                await multishield(
                    self._finalization(
                        sigchld_notifier,
                        process,
                        cancelled,
                        stdout_pipe,
                        stderr_pipe,
                        logger_pipe,
                    )
                )

    @contextmanager
    def _create_worker_process_and_pipes(
        self, subreaper: Subreaper
    ) -> Iterator[
        tuple[multiprocessing.context.ForkProcess, MaestroPipe, MaestroPipe, MaestroPipe]
    ]:
        """
        Create a worker process and pipes for stdout, stderr and logging.

        It is crucial that this function is synchronous so that no await happens between the creation
        of the pipes and the closing of the write ends of those pipes after child process has been forked.

        Otherwise, another child process could get forked during await and incorrectly inherit a copy
        of the write ends, causing EOF detection to fail on the read end when child process has terminated.

        This method runs in the context of the main process.
        """
        with (
            MaestroPipe(
                stream=PrefixingStream(sys.stdout.buffer, f"[{self.job.name_and_id}|OUT] ")
            ) as stdout_pipe,
            MaestroPipe(
                stream=PrefixingStream(sys.stderr.buffer, f"[{self.job.name_and_id}|ERR] ")
            ) as stderr_pipe,
            MaestroPipe(
                callback=lambda record: logging.getLogger(record.name).handle(record)
            ) as logger_pipe,
        ):
            process = multiprocessing.get_context("fork").Process(
                target=self._child_process_entry_point,
                kwargs=dict(
                    stdout_pipe=stdout_pipe, stderr_pipe=stderr_pipe, logger_pipe=logger_pipe
                ),
                name=self.job.name_and_id,
            )
            process.start()
            assert process.pid is not None
            subreaper.register_child(process.pid)
            try:
                # fork() has now taken place and both parent and child process have file descriptors
                # to both ends of both pipes. This is the reader (parent process), so close the
                # write end of the pipes.
                stdout_pipe.close_write_end()
                stderr_pipe.close_write_end()
                logger_pipe.close_write_end()

                yield process, stdout_pipe, stderr_pipe, logger_pipe

            finally:
                if process.is_alive():
                    logger.error(f"Sending SIGKILL to {process.name}")
                    process.kill()
                process.join()
                subreaper.unregister_child(process.pid)
                process.close()

    # Barrier preventing the function from being changed to async.
    # See function's docstring for more information.
    assert not inspect.iscoroutinefunction(_create_worker_process_and_pipes)

    async def _finalization(
        self,
        sigchld_notifier: asyncio.Condition,
        process: multiprocessing.context.ForkProcess,
        cancelled: bool,
        stdout_pipe: MaestroPipe,
        stderr_pipe: MaestroPipe,
        logger_pipe: MaestroPipe,
    ) -> None:
        """
        This method runs in the context of the main process.
        """
        assert self.workflow_step_model is not None
        # We may arrive here due to cancellation, in which case we need
        # to terminate the worker process.
        if process.is_alive():
            failed = True
            logger.info(f"Sending SIGTERM to {process.name}")
            process.terminate()
            try:
                async with asyncio.timeout(Config.GRACE_PERIOD):
                    await _process_termination(process, sigchld_notifier)
            except TimeoutError:
                logger.info(f"Sending SIGKILL to {process.name}")
                process.kill()
        else:
            failed = process.exitcode != 0
        await stdout_pipe.eof
        await stderr_pipe.eof
        await logger_pipe.eof
        stdout_data = stdout_pipe.data.decode(errors="backslashreplace")
        stderr_data = stderr_pipe.data.decode(errors="backslashreplace")
        async with Neo4jTransaction() as neo4j_tx:
            db_job = await read_node(neo4j_tx, Job, {"maestro_id": self.job.maestro_id})
            assert db_job is not None
            db_job.status = (
                JobStatus.CANCELLED
                if cancelled
                else JobStatus.FAILED
                if failed
                else JobStatus.COMPLETED
            )
            await merge_job_node(neo4j_tx, db_job)
            db_workflow_step = await read_node(
                neo4j_tx,
                WorkflowStepModelWithStdOutAndStdErr,
                {"maestro_id": self.workflow_step_model.maestro_id},
            )
            assert db_workflow_step is not None
            db_workflow_step.STDOUT = StdOutModel(output=stdout_data)
            db_workflow_step.STDERR = StdErrModel(output=stderr_data)
            await create_node(neo4j_tx, db_workflow_step)
            # If the WorkflowStep node was invalidated, the related STDOUT and STDERR should be invalidated too.
            if db_workflow_step.invalidated_at is not None:
                await invalidate_workflow_steps(neo4j_tx, [self.workflow_step_model.maestro_id])

        # Open a new transaction when publishing the workflow step.
        async with Neo4jTransaction() as neo4j_tx:
            db_workflow_step = await read_node(
                neo4j_tx,
                WorkflowStepModelWithArtefacts,
                {"maestro_id": self.workflow_step_model.maestro_id},
            )
            assert db_workflow_step is not None
            await self.instance.publish(db_workflow_step)
        assert db_job is not None
        logger.info(f"{process.name} terminated with status '{db_job.status}'")

    def _child_process_entry_point(
        self, stdout_pipe: MaestroPipe, stderr_pipe: MaestroPipe, logger_pipe: MaestroPipe
    ) -> None:
        """
        This method runs in the context of the child worker process.
        """
        setproctitle(f"[{self.job.name_and_id}]")
        setthreadtitle(f"[{str(self.job.maestro_id)[:13]}]")
        sys.exit(asyncio.run(self.child_process_main(stdout_pipe, stderr_pipe, logger_pipe)))

    async def child_process_main(
        self, stdout_pipe: MaestroPipe, stderr_pipe: MaestroPipe, logger_pipe: MaestroPipe
    ) -> int:
        """
        This method runs in the context of the child worker process.
        """
        assert self.workflow_step_model is not None
        # This is the writer (child process), so close the read end of the pipes.
        stdout_pipe.close_read_end()
        stderr_pipe.close_read_end()
        logger_pipe.close_read_end()
        # Promote write ends of the pipes to become the new "stdout" and "stderr" of this child proces.
        os.dup2(stdout_pipe.write_end, sys.stdout.fileno())
        os.dup2(stderr_pipe.write_end, sys.stderr.fileno())
        # Close the original pipe write end file descriptors, as they are now duplicates of "stdout" and "stderr".
        stdout_pipe.close_write_end()
        stderr_pipe.close_write_end()

        reconfigure_logging_for_child_process(logger_pipe)

        # maestro-worker logs will be included in StdErrModel.output
        self.logger = logging.getLogger("maestro-worker")
        self.logger.info("Starting to process job")

        current_task = asyncio.current_task()
        assert current_task is not None
        asyncio.get_running_loop().add_signal_handler(signal.SIGTERM, current_task.cancel)
        try:
            async with Neo4jTransaction() as neo4j_tx:
                self.job.status = JobStatus.RUNNING
                await merge_job_node(neo4j_tx, self.job)
            output: Sequence[BaseArtefactModel] = await asyncio.wait_for(
                self.instance.process(**self.input_artefacts, **self.job.params),
                timeout=self.job.running_timeout,
            )
            async with Neo4jTransaction() as neo4j_tx:
                db_workflow_step = await read_node(
                    neo4j_tx,
                    WorkflowStepModelWithOutput,
                    {"maestro_id": self.workflow_step_model.maestro_id},
                )
                assert db_workflow_step is not None
                assert db_workflow_step.OUTPUT == []
                db_workflow_step.OUTPUT = list(output)
                await create_node(neo4j_tx, db_workflow_step)
            self.logger.info("Done processing job")
            return 0
        except asyncio.CancelledError:
            self.logger.exception("Cancellation of job")
            return 1
        except:
            self.logger.error("Error processing job")
            raise
        finally:
            asyncio.get_running_loop().remove_signal_handler(signal.SIGTERM)


async def _process_termination(
    process: multiprocessing.context.ForkProcess, sigchld_notifier: asyncio.Condition
) -> None:
    """
    Wait for the provided process to terminate.

    It is significant to hold the "sigchld_notifier" lock BEFORE checking process.is_alive().
    If not, SIGCHLD may arrive and "sigchld_notifier" be notified AFTER last process.is_alive()
    check, but BEFORE locking "sigchld_notifier", causing deadlock.
    """
    async with sigchld_notifier:
        while process.is_alive():
            await sigchld_notifier.wait()
