import asyncio
import ctypes
import ctypes.util
import logging
import os
import pathlib
import signal
from collections.abc import Generator
from contextlib import contextmanager
from typing import TextIO

from maestro.config import Config

logger = logging.getLogger("maestro")


def _enable_child_subreaping() -> None:
    """
    Enable child subreaping for the current process.

    This is necessary to ensure that orphaned descendant processes are adopted by the current
    process, rather than being adopted by the "init" process, which is the default.

    This process can then kill and reap such adopted child processes, which would otherwise
    potentially remain running and consume system resources for no purpose.

    This function is called when a Subreaper object is created and uses the prctl() system call
    with the "PR_SET_CHILD_SUBREAPER" option.

    Ref: https://man7.org/linux/man-pages/man2/PR_SET_CHILD_SUBREAPER.2const.html
    """
    PR_SET_CHILD_SUBREAPER = 36
    libc = ctypes.CDLL(ctypes.util.find_library("c"))
    libc.prctl.argtypes = (ctypes.c_int,) + 4 * (ctypes.c_ulong,)
    libc.prctl.restype = ctypes.c_int
    ret = libc.prctl(PR_SET_CHILD_SUBREAPER, 1, 0, 0, 0)
    if ret != 0:
        raise OSError(f"prctl(PR_SET_CHILD_SUBREAPER) failed with return code {ret}")


class Subreaper:
    """
    This class implements child process subreaping.

    The class is used to reap orphaned child processes that were not reaped by their
    original parent processes, but were adopted by this process being a subreaper.
    """

    def __init__(self, sigchld_notifier: asyncio.Condition) -> None:
        """
        Initialize the Subreaper object and enable child subreaping on OS level.

        Register existing child processes to avoid reaping them.
        """
        self.sigchld_notifier = sigchld_notifier
        self.registered_children: set[int] = set()
        with self.open_proc_children() as children_file:
            for pid in self.read_children_pids(children_file):
                self.register_child(pid)
        _enable_child_subreaping()

    def register_child(self, pid: int) -> None:
        """
        Register a child process.

        This is necessary to ensure that the subreaper will not kill and reap a legitimate child process.

        The function is called to register newly created worker processes.
        """
        self.registered_children.add(pid)

    def unregister_child(self, pid: int) -> None:
        """
        Unregister a child process.

        This is necessary to ensure that the subreaper will kill and reap a future orphaned child process
        that is reusing the PID.

        The function is called to unregister worker processes that have terminated.
        """
        self.registered_children.discard(pid)

    async def loop(self) -> None:
        """
        This function is used to reap orphaned child processes that were not reaped by their
        original parent processes, but were adopted by this process being a subreaper.

        The function is started as a task and runs until cancellation, reaping orphaned child
        processes as they appear in the "/proc/self/task/<pid>/children" file.

        Each reaping pass is triggered by the reception of signal SIGCHLD.
        """
        with self.open_proc_children() as children_file:
            async with self.sigchld_notifier:
                async with asyncio.TaskGroup() as tg:
                    while True:
                        await self.sigchld_notifier.wait()
                        all_children = self.read_children_pids(children_file)
                        adopted_children = all_children - self.registered_children
                        for adopted_child in adopted_children:
                            tg.create_task(self.reap_child(adopted_child))  # type: ignore[unused-awaitable]
                        self.registered_children |= adopted_children
                        self.registered_children &= all_children

    @staticmethod
    @contextmanager
    def open_proc_children() -> Generator[TextIO]:
        "Open the 'procfs' 'children' file for reading and yield the file object."
        with pathlib.Path(f"/proc/self/task/{os.getpid()}/children").open("r") as file:
            yield file

    @staticmethod
    def read_children_pids(file: TextIO) -> set[int]:
        "Read the PIDs of child processes from the file."
        file.seek(0)
        return set(map(int, file.read().split()))

    async def reap_child(self, adopted_child: int) -> None:
        """
        Take care of orphaned child process termination.

        If the process does not terminate within the grace period of time,
        signal SIGTERM is sent.

        If the process still does not terminate within a second grace period
        of time, signal SIGKILL is sent.
        """

        def child_terminated(adopted_child: int) -> bool:
            pid, _ = os.waitpid(adopted_child, os.WNOHANG)
            return pid == adopted_child

        async def wait_for_child_termination(adopted_child: int) -> bool:
            try:
                async with asyncio.timeout(Config.GRACE_PERIOD):
                    while not child_terminated(adopted_child):
                        await self.sigchld_notifier.wait()
            except TimeoutError:
                return False
            else:
                return True

        terminated = False
        async with self.sigchld_notifier:
            try:
                if await wait_for_child_termination(adopted_child):
                    logger.warning(f"Orphan process with PID {adopted_child} terminated on its own")
                    terminated = True
                else:
                    os.kill(adopted_child, signal.SIGTERM)
                    if await wait_for_child_termination(adopted_child):
                        logger.warning(
                            f"Orphan process with PID {adopted_child} terminated after using SIGTERM"
                        )
                        terminated = True
            finally:
                if not terminated:
                    os.kill(adopted_child, signal.SIGKILL)
                    os.waitpid(adopted_child, 0)
                    logger.warning(
                        f"Orphan process with PID {adopted_child} terminated after using SIGKILL"
                    )
