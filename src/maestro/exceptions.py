from typing import override


class MaestroError(Exception):
    """Base class for all Maestro exceptions."""

    _description = "Generic Maestro error"

    def __init__(self, description: str | None = None):
        super().__init__(description)
        self._description = description or self._description

    @override
    def __str__(self) -> str:
        return f"{self._description}"


class ArtefactError(MaestroError):
    """Base class for all Artefact exceptions."""

    _description = "Generic Artefact error"


class WorkflowStepError(MaestroError):
    """Base class for all WorkflowStep exceptions."""

    _description = "Generic WorkflowStep error"


class ConfigError(MaestroError):
    """Base class for all Config exceptions."""

    _description = "Generic Config error"


class JobError(MaestroError):
    """Base class for all Config exceptions."""

    _description = "Generic Job error"


class NatsError(MaestroError):
    """Base class for all Nats exceptions."""

    _description = "Generic Nats error"


class Neo4jError(MaestroError):
    """Base class for all Neo4j exceptions."""

    _description = "Generic Neo4j error"


class SetupError(MaestroError):
    """Base class for all Setup exceptions."""

    _description = "Generic Setup error"


class InputNotFound(Exception):
    """
    Raised when an input artefact is not found.

    This is not an error, but indicates that a job is not ready to run at this time.
    """

    pass
