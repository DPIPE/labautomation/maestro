
// Used like: hx-swap="morph:{morphStyle:'innerHTML',callbacks:{beforeAttributeUpdated:keepOpen,beforeNodeMorphed:beforeNodeMorphed}}"
// See https://github.com/bigskysoftware/idiomorph?tab=readme-ov-file#htmx
const beforeNodeMorphed = (oldNode, newNode) => {
    // Keep details open on refresh, but only if the id is the same
    // (the id changes when new workflows are fetched, so we don't want to keep the details open in that case)
    if (oldNode.tagName === "DETAILS" && oldNode.open && oldNode.id === newNode.id ) {
        newNode.open = true;
    };
    // Keep invalidate or cancel dialogue open on refresh, but only if the id is the same
    if (oldNode.tagName === "DIV" && (oldNode.id.startsWith("invalidate-confirm_") || oldNode.id.startsWith("abort-confirm_")) && oldNode.id === newNode.id) {
        return false;
    };
    // Keep non-selected button hidden until confirm/cancel, but only if the id is the same
    if (oldNode.tagName === "BUTTON" && (oldNode.id.startsWith("invalidate-button_") || oldNode.id.startsWith("abort-button_")) && oldNode.id === newNode.id) {
        return false;
    };
    // Keep hidden class on Details button across refreshes
    if (oldNode.tagName === "BUTTON" && oldNode.id.startsWith("details-button_") && oldNode.classList.contains('hidden') && oldNode.id === newNode.id) {
        newNode.classList.add('hidden');
    };
}

const keepChecked = (oldNode, newNode) => {
    // Keep checkboxes checked on refresh
    if (oldNode.tagName === "INPUT" && oldNode.type === "checkbox" && oldNode.checked) {
        newNode.checked = true;
    }
}

var keepOpen = (attributeName, node, mutationType) => {
    // Avoid modal to be closed on refresh
    if (attributeName === "style" && node.id && node.id.startsWith("modal-")) {
        return false;
    }
};

// Copy text in element to clipboard
var toClipboard = (textToCopy) => {
    const title = "Copied to clipboard:";
    navigator.clipboard.writeText(textToCopy).then(() => {
            showNotification(title, textToCopy);
            console.log('Copied to clipboard: ', textToCopy);
        }).catch(err => {
            console.error('Failed to copy: ', err);
        });
    };

// Notification for clipboard in modal footer
let notificationTimeout;

var showNotification = (title, textToCopy) => {
    const notification = document.getElementById('notification');
    notification.innerHTML = `<span class="pr-1 font-semibold">${title}</span> ${textToCopy}`;
    notification.classList.remove('invisible');

    if (notificationTimeout) {
        clearTimeout(notificationTimeout);
    }

    notificationTimeout = setTimeout(() => {
        notification.classList.add('invisible');
    }, 5000);
};


var prepareSearch = () => {
    const searchField = document.getElementById("search")
    const isToggled = (id) => document.getElementById(id)?.checked

    const status = [
        isToggled("toggle-status-completed") ? "status:completed" : "",
        isToggled("toggle-status-running") ? "status:running" : "",
        isToggled("toggle-status-queued") ? "status:queued" : "",
        isToggled("toggle-status-pending") ? "status:pending" : "",
        isToggled("toggle-status-halted") ? "status:halted" : "",
        isToggled("toggle-status-cancelled") ? "status:cancelled" : "",
        isToggled("toggle-status-failed") ? "status:failed" : ""
    ]

    const priority = [
        isToggled("toggle-priority-urgent") ? "priority:urgent" : "",
        isToggled("toggle-priority-high") ? "priority:high" : "",
        isToggled("toggle-priority-normal") ? "priority:normal" : "",
    ]


    const query = [searchField.value, ...status, ...priority].filter(Boolean).join(" ")
    return query
}

// Keyboard shortcuts for search and toggle status and priority filters
// Keys for status and priority are disabled when search field is focused
// Esc clears and exits the search field (Tab can be used to only exit)
document.addEventListener('keydown', function(event) {
    // Only trigger shortcuts when not in an input field or no modifier/common special keys are pressed
    if (document.activeElement.id !== 'search' && !event.ctrlKey && !event.altKey && !event.shiftKey && !event.metaKey && event.key !== 'Enter' && event.key !== 'Tab' && !/^F\d+$/.test(event.key)) {
        event.preventDefault(); // Overrule browser-specific shortcuts, e.g. Firefox "Search for text when you start typing"
        switch (event.key) {
            case 'c':
                document.getElementById('toggle-status-completed').click();
                break;
            case 'r':
                document.getElementById('toggle-status-running').click();
                break;
            case 'q':
                document.getElementById('toggle-status-queued').click();
                break;
            case 'p':
                document.getElementById('toggle-status-pending').click();
                break;
            case 'a':
                document.getElementById('toggle-status-halted').click();
                break;
            case 'x':
                document.getElementById('toggle-status-cancelled').click();
                break;
            case 'f':
                document.getElementById('toggle-status-failed').click();
                break;
            case 'n':
                document.getElementById('toggle-priority-normal').click();
                break;
            case 'h':
                document.getElementById('toggle-priority-high').click();
                break;
            case 'u':
                document.getElementById('toggle-priority-urgent').click();
                break;
            case 't':
                document.getElementById('toggle-status-all').click();
                break;
            case 's':
                document.getElementById('search').focus();
                break;
            case 'Escape':
                document.getElementById('search').blur(); // Exit the search field
                document.getElementById('search').value = ''; // Clear the search field
                document.getElementById('search').dispatchEvent(new Event('keyup')); // Trigger the keyup event to refresh
                break;
        }
    } else if (event.key === 'Escape') {
        document.getElementById('search').blur(); // Exit the search field
        document.getElementById('search').value = ''; // Clear the search field
        document.getElementById('search').dispatchEvent(new Event('keyup')); // Trigger the keyup event to refresh
    }
});