import logging
from uuid import UUID

from maestro.nats.nats import NATSSession

logger = logging.getLogger("maestro")


async def publish_cancel(maestro_id: UUID) -> None:
    "Publish request to cancel a job on NATS."
    logger.debug(f"Publishing cancel request for job {maestro_id}")
    async with NATSSession() as nats_session:
        await nats_session.publish("cancel", str(maestro_id).encode())
