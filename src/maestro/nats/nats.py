import asyncio
import logging
from collections.abc import AsyncGenerator, Callable
from contextlib import asynccontextmanager
from types import TracebackType
from typing import Any, Never

import nats
from nats.js.api import ConsumerConfig, PubAck, StreamInfo
from nats.js.client import JetStreamContext
from nats.js.errors import KeyWrongLastSequenceError, NotFoundError

from maestro.config import Config

logger = logging.getLogger("maestro")


class NATSSession:
    def __init__(self) -> None:
        self._done: asyncio.Future[bool] = asyncio.Future()
        self.connection_options = Config.nats_connect_options()
        self.subscriptions: list[JetStreamContext.PullSubscription] = []
        self.subscription_tasks: set[asyncio.Task] = set()
        self.exceptions: list[BaseException] = []

    async def __aenter__(self) -> "NATSSession":
        await self.start()
        return self

    async def start(self) -> None:
        self._nc = nats.NATS()  # type: ignore[attr-defined]
        await self._nc.connect(**self.connection_options)
        self._js: JetStreamContext = self._nc.jetstream()

    async def __aexit__(
        self,
        exc_type: type[BaseException] | None,
        exc: BaseException | None,
        tb: TracebackType | None,
    ) -> None:
        if exc is not None:
            self.exceptions.append(exc)
        await self.stop()
        if self._done.done():
            # Extract any pending exception to avoid "Future exception was never retrieved" error.
            # The exception is already in self.exceptions, so deal with it from there.
            self._done.exception()
        if len(self.exceptions) == 1 and exc is None:
            # Exactly one callback raised exception, so just propagate it directly.
            raise self.exceptions[0]
        if len(self.exceptions) > 1:
            # Multiple exceptions are pending, so raise them all as a group.
            raise BaseExceptionGroup(
                "multiple exceptions during NATSSession", self.exceptions
            ) from None

    async def run_forever(self) -> None:
        try:
            await self._done
        finally:
            await self.stop()

    async def stop(self) -> None:
        for subscription in self.subscriptions:
            await subscription.unsubscribe()
        for subscription_task in self.subscription_tasks:
            if not subscription_task.done():
                subscription_task.cancel()
        await asyncio.gather(*self.subscription_tasks, return_exceptions=True)
        # drain will process remaining messages in the buffer,
        # before unsubscribing all subscribers
        await self._nc.drain()
        await self._nc.close()
        if not self._done.done():
            self._done.set_result(True)

    async def add_or_update_stream(self, name: str, subjects: list[str]) -> StreamInfo:
        logger.info(f"Adding/updating stream {name} with subjects {subjects}")
        async with self.lock():
            try:
                stream = await self._js._jsm.stream_info(name)
                existing_subjects = stream.config.subjects or []
                ret = await self._js.update_stream(
                    name=name, subjects=list(set(subjects + existing_subjects))
                )
            except NotFoundError:
                ret = await self._js.add_stream(name=name, subjects=subjects)
        logger.debug(f"Stream {name} updated with subjects {ret.config.subjects}")
        return ret

    async def delete_stream(self, name: str) -> bool:
        logger.debug(f"Deleting stream {name}")
        try:
            return await self._js.delete_stream(name=name)
        except NotFoundError:
            return False

    async def subscribe(
        self,
        stream: str,
        subject: str,
        cb: Callable[..., Any] | None,
        *,
        durable: str | None = None,
        config: ConsumerConfig | dict | None = None,
    ) -> JetStreamContext.PullSubscription:
        logger.debug(f"Subscribing to stream {stream} with subject {subject}: {cb}")
        if isinstance(config, dict):
            config = ConsumerConfig(**config)

        subscription = await self._js.pull_subscribe(
            subject, stream=stream, durable=durable, config=config
        )
        self.subscriptions.append(subscription)
        if cb is not None:

            async def subscriber_task(
                subscription: JetStreamContext.PullSubscription, cb: Callable[..., Any]
            ) -> Never:
                try:
                    while True:
                        [msg] = await subscription.fetch(timeout=None)
                        cb_task = asyncio.create_task(cb(msg))
                        try:
                            await asyncio.shield(cb_task)
                        except asyncio.CancelledError:
                            await cb_task
                            raise
                except asyncio.CancelledError:
                    raise
                except BaseException as e:
                    self.exceptions.append(e)
                    if not self._done.done():
                        self._done.set_exception(e)
                    raise

            self.subscription_tasks.add(asyncio.create_task(subscriber_task(subscription, cb)))
        return subscription

    async def publish(self, subject: str, data: bytes, stream: str | None = None) -> PubAck:
        stream = stream or Config.STREAM
        # logger.info(await self._js._jsm.stream_info(stream))
        logger.debug(f"Publishing to subject {subject} on {stream}: {data.decode()}")
        try:
            return await self._js.publish(subject, data, stream=stream)
        except Exception as e:
            raise RuntimeError(f"{subject}: {data.decode()}") from e

    async def delete_bucket(self, name: str) -> None:
        logger.debug(f"Deleting bucket '{name}'")
        try:
            await self._js.delete_key_value(name)
        except NotFoundError:
            pass

    @asynccontextmanager
    async def lock(self) -> AsyncGenerator[None, None]:
        """
        This is a context manager that creates a lock in a NATS Key/Value Store bucket
        and deletes it when the context is exited.
        """
        bucket = Config.BUCKET
        lock = Config.BUCKET_LOCK
        kv = await self._js.create_key_value(bucket=bucket)
        # Create a watcher to watch for changes to the lock's state. This is used to
        # wait for the lock to change state before trying to acquire it.
        watcher = await kv.watch(lock)
        try:
            while True:
                # There will always be one immediate result from watcher.updates() after
                # 'watcher' was created, so for the first loop iteration, we will proceed
                # immediately. On subsequent iterations, watcher.updates() will block
                # until there is a change to the lock's state.
                # The particular lock state's value is ignored, but the change indicates that
                # the lock may now be available. If not, a new iteration will be started.
                await watcher.updates(timeout=None)  # type: ignore[no-untyped-call]
                try:
                    # Attempt to acquire the lock by creating it in the key/value store.
                    await kv.create(lock, b"")
                except KeyWrongLastSequenceError:
                    logger.debug(f"Failed to create '{lock}' in '{bucket}'. Retrying...")
                    continue
                break
            try:
                logger.debug(f"Created '{lock}' in '{bucket}'")
                yield
            finally:
                await kv.delete(lock)
                logger.debug(f"Deleted '{lock}' in '{bucket}'")
        finally:
            await watcher.stop()  # type: ignore[no-untyped-call]
