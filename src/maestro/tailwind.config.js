/** @type {import('tailwindcss').Config} */
module.exports = {
    content: {
        relative: true,
        files: ["./templates/**.html"],
    },
    theme: {
        borderWidth: {
            DEFAULT: "1px",
            0: "0",
            2: "2px",
            4: "4px",
            6: "6px",
            8: "8px",
        },
        extend: {
            colors: {
                ellablue: {
                    superlight: "#f4f7f9",
                    lightest: "#eaeff2",
                    lighter: "#cbd7df",
                    DEFAULT: "#9caebf",
                    darker: "#859baf",
                    darkest: "#567490",
                },
                ellapurple: {
                    lightest: "#f5f5f9",
                    lighter: "#d6d8e6",
                    DEFAULT: "#c3c6db",
                    dark: "#9597bc",
                },
                ellagreen: {
                    light: "#f6f9f8",
                    DEFAULT: "#c9dbd9",
                    dark: "#72979d",
                },
                ellared: {
                    light: "#f8f5f5",
                    DEFAULT: "#d8c8c5",
                    darker: "#b58f89",
                    darkest: "#9c6a62",
                },
                ellayellow: {
                    light: "#fcf8f2",
                    DEFAULT: "#f1ddc0",
                    darker: "#d9b17f",
                    darkest: "#d59844",
                },
            },
        },
    },
    plugins: [require("@tailwindcss/forms")],
};
