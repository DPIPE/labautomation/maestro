import asyncio
import logging
import signal
from collections.abc import Sequence

from maestro.background_tasks import cancel_pending_jobs
from maestro.config import Config
from maestro.exceptions import InputNotFound, SetupError
from maestro.models import BaseArtefactModel, Job, JobStatus
from maestro.nats.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import fetch_inputs, merge_job_node, read_nodes
from maestro.subscribers import on_cancel_received, on_input_received, on_order_received
from maestro.utils import get_recursive_subclasses
from maestro.worker import Worker
from maestro.worker_queue import WorkerQueue
from maestro.workflow_step.file_watcher import FileWatcher
from maestro.workflow_step.message_watcher import MessageWatcher
from maestro.workflow_step.workflow_step import WorkflowStepBase

_main_task = None

logger = logging.getLogger("maestro")


async def setup(maestro_bases: Sequence[type[WorkflowStepBase]]) -> None:
    async with NATSSession() as nats_session:
        subjects = {"cancel"}
        for base_cls in maestro_bases:
            subjects.update(base_cls.subjects())
        await nats_session.add_or_update_stream(Config.STREAM, list(subjects))
        await nats_session.add_or_update_stream(
            Config.NEO4J_SYNC_STREAM, [Config.NEO4J_SYNC_SUBJECT]
        )


def find_duplicates(lst: list) -> set:
    return set([x for x in lst if lst.count(x) > 1])


async def wait_for_all(worker_queue_task: asyncio.Task, all_tasks: set[asyncio.Task]) -> None:
    """
    Keep track of all tasks and wait for them all to complete, provided that no tasks raise
    any exceptions other than possibly CancelledError.

    Propagate incoming cancellation to all tasks. Ignore CancelledError from tasks after they
    have been cancelled from here. If not cancelled from here, CancelledError will be considered
    an error and reraised.
    """
    # Used to keep track of whether or not asyncio.wait() was cancelled.
    pending_cancel_exception = None
    # Used to keep track of whether or not tasks have been cancelled. This is used to avoid
    # sending multiple cancellation requests to the tasks.
    cancel_sent = False
    # Keep running until all tasks are done.
    while all_tasks:
        try:
            done_tasks, all_tasks = await asyncio.wait(
                all_tasks, return_when=asyncio.FIRST_COMPLETED
            )
        except asyncio.CancelledError as e:
            # If asyncio.wait() was cancelled it means that the task running this function
            # received an external cancellation request, so propagate the cancellation
            # request to all tasks that are still running.
            # Note that CancelledError raised by any of the managed tasks does not end up here
            # since the tasks are awaited on and handled in the except block below.
            pending_cancel_exception = e
            if not cancel_sent:
                for task in all_tasks:
                    task.cancel()
                cancel_sent = True
            # Avoid falling thorugh because "done_tasks" is not set.
            continue

        # Await each task that has completed.
        for done_task in done_tasks:
            if done_task is worker_queue_task and not cancel_sent:
                # When the worker queue task is done, cancel all other tasks to
                # fully shut down Maestro.
                for task in all_tasks:
                    task.cancel()
                cancel_sent = True
            try:
                # This will return or raise immediately since the task is already done.
                await done_task
            except asyncio.CancelledError as e:
                if not cancel_sent:
                    raise RuntimeError(f"Unprovoked CancelledError from task {done_task}") from e
            except:
                # For any other exceptions, cancel and wait for all other tasks to finish.
                if not cancel_sent:
                    for task in all_tasks:
                        task.cancel()
                for task in all_tasks:
                    try:
                        await task
                    except asyncio.CancelledError:
                        pass
                raise

    # Reraise CancelledError if the task running this function was cancelled.
    if pending_cancel_exception is not None:
        raise pending_cancel_exception


def sigusr1_handler() -> None:
    all_tasks = asyncio.all_tasks()
    logger.info("Received SIGUSR1. Logging task information...")
    logger.info(f"Number of tasks: {len(all_tasks)}")
    for i, task in enumerate(all_tasks):
        logger.info(f"  Task {i + 1:2d}: {task}")


async def run_workflow_steps(
    message_watcher_classes: list[type[MessageWatcher]] | None = None,
    file_watcher_classes: list[type[FileWatcher]] | None = None,
    artefact_classes: list[type[BaseArtefactModel]] | None = None,
) -> asyncio.Task:
    """
    Run all message watchers and file watchers, and set up streams, subjects and subscriptions.

    If message_watcher_classes is None, all subclasses of MessageWatcher are used.
    If file_watcher_classes is None, all subclasses of FileWatcher are used.
    If artefact_classes is None, all subclasses of BaseArtefact are used.

    NATS connection is kept open for the duration of the program.
    """

    # logger.debug(f"all subclasses are: {get_all_subclasses(WorkflowStep)}")
    if message_watcher_classes is None:
        message_watcher_classes = get_recursive_subclasses(MessageWatcher)  # type: ignore[type-abstract]
    if artefact_classes is None:
        artefact_classes = get_recursive_subclasses(BaseArtefactModel)
    if file_watcher_classes is None:
        file_watcher_classes = get_recursive_subclasses(FileWatcher)  # type: ignore[type-abstract]

    if len(message_watcher_classes) + len(file_watcher_classes) == 0:
        logger.warning("No MessageWatcher or FileWatcher subclasses defined.")

    if len(artefact_classes) == 0:
        logger.warning("No Artefact subclasses defined")

    logger.info(
        f"Starting Maestro with {len(message_watcher_classes)} message watchers, "
        f"{len(file_watcher_classes)} file watchers "
        f"and {len(artefact_classes)} artefacts"
    )

    if find_duplicates([cls.__name__ for cls in message_watcher_classes]):
        raise SetupError(
            "Duplicate MessageWatcher types: "
            f"{find_duplicates([cls.__name__ for cls in message_watcher_classes])}"
        )

    if find_duplicates([cls.__name__ for cls in artefact_classes]):
        raise SetupError(
            "Duplicate Artefact names: "
            f"{find_duplicates([cls.__name__ for cls in artefact_classes])}"
        )

    watcher_classes = message_watcher_classes + file_watcher_classes
    await setup(watcher_classes + artefact_classes)  # type: ignore[operator]

    WorkerQueue.setup()

    # Set up signal handlers to trigger graceful shutdown.
    for sig in (signal.SIGTERM, signal.SIGINT):
        asyncio.get_running_loop().add_signal_handler(sig, WorkerQueue.shutdown_gracefully)

    asyncio.get_running_loop().add_signal_handler(signal.SIGUSR1, sigusr1_handler)

    ready_workers = []
    # Re-queue jobs that were ready to run when the system was last shut down.
    async with Neo4jTransaction() as neo4j_tx:
        ready_jobs = await read_nodes(
            neo4j_tx,
            Job,
            {"instance_id": Config.INSTANCE_ID},
            ["n.status IN ['queued', 'running', 'halted']"],
        )
        for job in ready_jobs:
            try:
                input_artefacts = await fetch_inputs(neo4j_tx, job.inputs)
            except InputNotFound:
                job.status = JobStatus.PENDING
            else:
                job.status = JobStatus.QUEUED
                ready_workers.append(await Worker.create(neo4j_tx, job, input_artefacts))
            logger.info(f"Changing status of interrupted job {job.maestro_id} to {job.status}")
            await merge_job_node(neo4j_tx, job)

    for worker in ready_workers:
        logger.info(f"Re-queueing job: {worker.job}")
        WorkerQueue.add_to_queue(worker)

    mgr = NATSSession()
    await mgr.start()

    # Set up subscribers for incoming orders and created artefacts
    # Note: Do not use queue here, as we may want to process messages on multiple instances
    await mgr.subscribe(
        Config.STREAM,
        "order.workflow_step.*",
        on_order_received,
        durable=f"{Config.INSTANCE_ID}_order",
    )
    await mgr.subscribe(
        Config.STREAM, "artefact.*", on_input_received, durable=f"{Config.INSTANCE_ID}_artefact"
    )

    await mgr.subscribe(
        Config.STREAM, "cancel", cb=on_cancel_received, durable=f"{Config.INSTANCE_ID}_cancel"
    )

    file_watcher_tasks = {asyncio.create_task(cls.run()) for cls in file_watcher_classes}
    msg_watcher_tasks = {asyncio.create_task(mgr.run_forever())}
    # TODO:
    # 1. when there are more than one background tasks we should have a wrapper
    #   or more generalized version of this
    # 2. cancel_pending_jobs accepts timeout in seconds, this interval should
    #   be configurable
    worker_queue_task = WorkerQueue.run()
    background_tasks = {asyncio.create_task(cancel_pending_jobs()), worker_queue_task}
    all_tasks = file_watcher_tasks | msg_watcher_tasks | background_tasks
    global _main_task
    _main_task = asyncio.create_task(wait_for_all(worker_queue_task, all_tasks))
    return _main_task


async def shutdown_workflow_steps() -> None:
    global _main_task
    if _main_task is None:
        return
    try:
        if _main_task.cancel():
            await _main_task
            raise RuntimeError(f"Main task {_main_task} did not raise CancelledError")
        else:
            await _main_task
    finally:
        _main_task = None
