import asyncio
from datetime import UTC, datetime
import logging

from neo4j import Record

from maestro.neo4j.neo4j import Neo4jTransaction

logger = logging.getLogger("maestro")


async def cancel_pending_jobs(interval: float = 3600) -> None:
    """Cancel pending job older than timeout
    default interval for checking these jobs is 1 hour
    """

    async def cancel_jobs() -> list[Record]:
        async with Neo4jTransaction() as neo4j_tx:
            cancelled_jobs = await neo4j_tx.query(
                """
                MATCH (j:Job)
                WHERE j.status = "pending"
                AND datetime(j.pending_timeout_at) < datetime($datetime)
                SET j.status = "failed"
                RETURN j
                """,
                {"datetime": datetime.now(UTC).isoformat()},
            )
        return cancelled_jobs

    while True:
        logger.info("cancelling pending jobs older than timeout")
        task = asyncio.create_task(cancel_jobs())
        try:
            cancelled_jobs = await asyncio.shield(task)
        except asyncio.CancelledError:
            if not task.done():
                await task
            logger.info("Cancelled background task: cancel_pending_jobs")
            raise
        except Exception as e:
            logger.exception(f"Error while cancelling timed out pending jobs: {repr(e)}")
        else:
            if cancelled_jobs:
                logger.info("Cancelled following timed out pending jobs")
                for job in cancelled_jobs:
                    logger.info(
                        f"""Job ID: {job['j']['maestro_id']},
                        Workflow: {job['j']['workflow_name']},
                        created at: {job['j']['created_at']}"""
                    )
        await asyncio.sleep(interval)
