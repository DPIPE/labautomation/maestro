import logging
import logging.config
import pickle
import sys
from logging.handlers import RotatingFileHandler
from typing import Any, override

import structlog

from maestro.config import Config
from maestro.pipe import MaestroPipe

humanized_timestamper = structlog.processors.TimeStamper(fmt="%Y-%m-%d %H:%M:%S")
iso_timestamper = structlog.processors.TimeStamper(fmt="iso")

pre_chain = [
    # Add extra attributes of LogRecord objects to the event dictionary
    # so that values passed in the extra parameter of log methods pass
    # through to log output.
    structlog.stdlib.ExtraAdder(),
]


# Filter out asyncio's warning regarding eof_received() having no effect when using ssl.
# Necessary until https://github.com/nats-io/nats.py/issues/574 gets resolved.
def ssl_eof_warning_filter(record: logging.LogRecord) -> bool:
    if "returning true from eof_received() has no effect when using ssl" in record.getMessage():
        return False
    return True


logging.getLogger("asyncio").addFilter(ssl_eof_warning_filter)


class MainStreamHandler(logging.StreamHandler):
    """
    Stream handler logging either to "stderr" or to a pipe if provided.

    In the parent process, the handler will log to "stderr" as usual.
    During initialization of the child process, the handler will be
    reconfigured to log to a pipe using the set_logger_pipe() method.
    """

    @override
    def emit(self, record: logging.LogRecord) -> None:
        if hasattr(self, "logger_pipe"):
            # Temporary remove exception traceback from the record to avoid pickling issues.
            original_exc_info = record.exc_info
            if record.exc_info is not None and record.exc_info[0] is not None:
                record.exc_info = (record.exc_info[0], record.exc_info[1], None)
            self.logger_pipe.write(pickle.dumps(record))
            record.exc_info = original_exc_info
        else:
            super().emit(record)

    def set_logger_pipe(self, logger_pipe: int) -> None:
        self.logger_pipe = open(logger_pipe, "wb", buffering=0)


def extract_from_record(
    _: Any, __: Any, event_dict: structlog.types.EventDict
) -> structlog.types.EventDict:
    """
    Extract process name and add them to the event dict.
    """
    record: logging.LogRecord = event_dict["_record"]
    event_dict["process_name"] = record.processName
    return event_dict


LOGGING_CONFIG = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "simple": {
            "()": structlog.stdlib.ProcessorFormatter,
            "processors": [
                humanized_timestamper,
                structlog.stdlib.add_log_level,
                structlog.stdlib.ProcessorFormatter.remove_processors_meta,
                structlog.processors.format_exc_info,
                structlog.dev.ConsoleRenderer(
                    colors=False,
                ),
            ],
        },
        "console": {
            "()": structlog.stdlib.ProcessorFormatter,
            "processors": [
                humanized_timestamper,
                structlog.stdlib.add_log_level,
                # extract_from_record,
                structlog.processors.CallsiteParameterAdder(
                    {
                        structlog.processors.CallsiteParameter.FILENAME,
                        structlog.processors.CallsiteParameter.FUNC_NAME,
                        structlog.processors.CallsiteParameter.LINENO,
                        # structlog.processors.CallsiteParameter.PATHNAME,
                        # structlog.processors.CallsiteParameter.PROCESS_NAME,
                        # structlog.processors.CallsiteParameter.THREAD,
                        # structlog.processors.CallsiteParameter.THREAD_NAME,
                    }
                ),
                structlog.stdlib.PositionalArgumentsFormatter(),
                structlog.stdlib.ProcessorFormatter.remove_processors_meta,
                structlog.dev.ConsoleRenderer(
                    colors=sys.stderr.isatty(),
                ),
            ],
            "foreign_pre_chain": pre_chain,
        },
        # File logger to JSON format, which is what machines want. Also makes it easy to
        # "prettyrint" it in terminal via jq.
        "json": {
            "()": structlog.stdlib.ProcessorFormatter,
            "processors": [
                iso_timestamper,
                structlog.stdlib.add_log_level,
                structlog.stdlib.add_logger_name,
                extract_from_record,
                structlog.stdlib.ProcessorFormatter.remove_processors_meta,
                structlog.processors.dict_tracebacks,
                structlog.processors.JSONRenderer(),
            ],
            "foreign_pre_chain": pre_chain,
        },
    },
    "handlers": {
        "default": {
            "level": Config.LOG_LEVEL,
            "class": MainStreamHandler,
            "formatter": "console",
        },
        "file": {
            "level": "DEBUG",
            # RotatingFileHandler uses append mode, so it should be safe to use in a forked process (on POSIX systems)
            # Ref @knutroy
            "class": RotatingFileHandler,
            "filename": Config.LOG_FILE,
            "maxBytes": Config.LOG_MAX_BYTES,
            "backupCount": Config.LOG_BACKUPS,
            "formatter": "json",
        },
        "worker": {
            "level": "INFO",
            "class": logging.StreamHandler,
            "formatter": "simple",
        },
    },
    "loggers": {
        "": {
            "handlers": [],
            "level": Config.LOG_LEVEL,
            "propagate": True,
        },
        "maestro": {
            "handlers": ["default", "file"],
            "level": Config.LOG_LEVEL,
            "propagate": False,
        },
        "maestro-api": {
            "handlers": ["default", "file"],
            "level": Config.LOG_LEVEL,
            "propagate": False,
        },
        "maestro-worker": {
            "handlers": ["worker", "file"],
            "level": Config.LOG_LEVEL,
            "propagate": False,
        },
        "nats-bridge": {
            "handlers": ["default", "file"],
            "level": Config.LOG_LEVEL,
            "propagate": False,
        },
        "uvicorn": {
            "handlers": [],
            "propagate": False,
        },
        "uvicorn.access": {
            "handlers": [],
            "propagate": False,
        },
        "uvicorn.error": {
            "handlers": [],
            "propagate": False,
        },
    },
}

logging.config.dictConfig(LOGGING_CONFIG)

structlog.configure(
    logger_factory=structlog.stdlib.LoggerFactory(),
    wrapper_class=structlog.stdlib.BoundLogger,
    cache_logger_on_first_use=True,
)


# Filter out Neo4j notifications that are not relevant
def neo4j_notifications_filter(record: logging.LogRecord) -> bool:
    from neo4j._debug._notification_printer import NotificationPrinter

    if isinstance(record.args, tuple) and isinstance(record.args[0], NotificationPrinter):
        notification = record.args[0].notification
        if notification.code == "Neo.ClientNotification.Statement.UnknownPropertyKeyWarning":
            # We know that invalidated_at is not necessarily a valid property - we're checking for it's existence in the
            # query - the way it's supposed to be done.
            if "(the missing property name is: invalidated_at)" in notification.description:
                return False
        elif notification.code == "Neo.ClientNotification.Statement.UnknownLabelWarning":
            # We will trigger this warning when we're looking for artefacts that are not yet created
            return False
    return True


logger = logging.getLogger("neo4j.notifications")
logger.addFilter(neo4j_notifications_filter)


def reconfigure_logging_for_child_process(logger_pipe: MaestroPipe) -> None:
    # For "default" handler, send log records to parent process via logger pipe.
    default_handler = logging.getHandlerByName("default")
    assert isinstance(default_handler, MainStreamHandler)
    default_handler.set_logger_pipe(logger_pipe.write_end)
    # Fully disable the "file" handler. Parent process will take care of forwarding
    # log records to file after receiving them via the logger pipe.
    file_handler = logging.getHandlerByName("file")
    assert isinstance(file_handler, logging.FileHandler)
    for logger in logging.getLogger().getChildren():
        logger.removeHandler(file_handler)
    file_handler.close()
    # Reattach "worker" handler to "sys.stderr". This fixes the issue where the "worker"
    # handler, when running in the child worker process, will otherwise log to the wrong
    # stream in the case where Pytest capturing is enabled and current working directory
    # is "/app" instead of "/app/src", which affects the ordering of "worker" handler
    # creation and Pytest's "sys.stderr" manipulation.
    # Otherwise, the "worker" handler stream is already "sys.stderr", making this a no-op.
    worker_handler = logging.getHandlerByName("worker")
    assert isinstance(worker_handler, logging.StreamHandler)
    worker_handler.setStream(sys.stderr)
