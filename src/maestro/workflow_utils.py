import logging
from uuid import UUID

from maestro.config import Config
from maestro.models import Job, JobStatus, WorkflowModel, WorkflowStepModel
from maestro.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import (
    create_node,
    fetch_node_by_rel,
    filter_duplicate_jobs,
    merge_job_node,
    read_node,
)

logger = logging.getLogger("maestro")


async def order_workflow(workflow_order: WorkflowModel) -> None:
    """
    Place a workflow order.

    This method will publish the job on NATS, which will eventually be picked up and added to
    the worker queue.

    Example usage:

    ```
    from maestro.workflow_utils import order_workflow
    from maestro.workflow_step.models import Job, WorkflowModel
    job = Job(
        inputs={"param": (ArtefactType, {"attr": "some_required_attr", ...}), ...},
        params={"foo": "bar"},
        search_metadata=["sample_order_id: 1234"],
        workflow_step=MessageWatcherClassA
    )
    workflow = WorkflowModel(
        workflow_name="some_name",
        jobs=[job],
        search_metadata=["project_id: wf_1234"],
    )
    await order_workflow(workflow)
    ```

    The Job.inputs will be resolved (when available in Neo4j), and passed as first argument to `process`-method.
    The Job.params will be passed as keyword arguments.
    """
    for job in workflow_order.jobs:
        job.search_metadata.extend(workflow_order.search_metadata)

    async with Neo4jTransaction() as neo4j_tx:
        jobs = workflow_order.jobs.copy()
        for job in jobs:
            # Remove existing jobs
            existing_job = await read_node(neo4j_tx, Job, {"maestro_id": job.maestro_id})
            if existing_job:
                logger.info(f"Job {job.maestro_id} already exists. Removing from workflow...")
                workflow_order.jobs.remove(job)

        # Push to Neo4j
        if workflow_order.jobs:
            await create_node(neo4j_tx, workflow_order)
        else:
            logger.info(f"Workflow {workflow_order.maestro_id} has no jobs, workflow not persisted")
            return

    async with NATSSession() as nats_session:
        await nats_session.publish(
            "order.workflow_step.jobs",
            workflow_order.model_dump_json().encode(),
            stream=Config.STREAM,
        )


async def order_workflow_with_job_filtering(workflow: WorkflowModel) -> None:
    """Filter out jobs that have already been run successfully and order the workflow."""
    async with Neo4jTransaction() as neo4j_tx:
        non_existing_jobs, _ = await filter_duplicate_jobs(neo4j_tx, workflow.jobs)

    if non_existing_jobs:
        workflow.jobs = non_existing_jobs
        await order_workflow(workflow)
    else:
        logger.info(
            f"Skipping workflow {workflow.maestro_id}: all jobs are duplicates of existing and valid jobs."
        )


async def reorder_job(maestro_id: UUID) -> None:
    """
    Reorder a workflow step by its maestro_id

    Example usage:

    ```
    from maestro.workflow_utils import reorder_job
    await reorder_job(maestro_id)
    ```

    This method will publish the job on NATS, which will eventually be picked up and added to the queue
    """
    async with Neo4jTransaction() as neo4j_tx:
        # fetch existing workflow and create new job to reorder
        # note that newly created job will have exact same user provided data as old job
        # fetch exisiting workflow
        db_job = await read_node(neo4j_tx, Job, {"maestro_id": maestro_id})
        if db_job is None:
            raise ValueError("Failed to run, job not found")

        db_workflow_step = await fetch_node_by_rel(
            neo4j_tx, Job, {"maestro_id": db_job.maestro_id}, WorkflowStepModel
        )

        if db_job.status not in [JobStatus.CANCELLED, JobStatus.FAILED] and not (
            db_workflow_step and db_workflow_step.invalidated_at
        ):
            raise ValueError(
                "To reorder a job either job must have status cancelled or failed or connected workflow step must be invalidated"
            )

        db_workflow = await fetch_node_by_rel(
            neo4j_tx, Job, {"maestro_id": db_job.maestro_id}, WorkflowModel
        )
        if db_workflow is None:
            raise ValueError("Workflow not found")

    # fields we do not want to copy from old job
    fields_to_exclude = {
        "invalidated_at",
        "maestro_id",
        "status",
        "created_at",
        "updated_at",
        "revision",
    }
    assert db_job is not None
    assert db_workflow is not None
    new_job = Job(**db_job.model_dump(exclude=fields_to_exclude))

    db_workflow.jobs.add(new_job)
    # following will append new job to the existing workflow
    await order_workflow(db_workflow)


async def cancel_pending_job(maestro_id: UUID) -> None:
    """
    Change status of a job from pending to cancelled on user request, use maestro_id to fetch the job.

    Example usage:

    ```
    from maestro.utils import cancel_pending_jobs
    await cancel_pending_jobs(maestro_id)
    ```
    """
    async with Neo4jTransaction() as neo4j_tx:
        db_job = await read_node(neo4j_tx, Job, {"maestro_id": maestro_id})
        if db_job is None:
            raise ValueError("Failed to cancel pending job, job not found")

        if db_job.status != JobStatus.PENDING:
            raise ValueError("Only job with status pending can be cancelled")

        await neo4j_tx.query(
            """
            MERGE (n:Job:MaestroBase {maestro_id: $maestro_id})
            SET n.status = $status
            RETURN n
            """,
            {"maestro_id": maestro_id, "status": JobStatus.CANCELLED},
        )


async def resume_halted_job(maestro_id: UUID) -> None:
    """Change status of a job from halted to running on user request."""
    async with Neo4jTransaction() as neo4j_tx:
        db_job = await read_node(neo4j_tx, Job, {"maestro_id": maestro_id})
        if db_job is None:
            raise ValueError(f"Failed to resume job, job {maestro_id} not found")

        if db_job.status != JobStatus.HALTED:
            raise ValueError(
                f"Only job with status halted can be resumed, job {maestro_id} has status {db_job.status}"
            )

        db_job.status = JobStatus.RUNNING
        await merge_job_node(neo4j_tx, db_job)
