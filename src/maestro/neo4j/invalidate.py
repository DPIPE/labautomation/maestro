import logging
from datetime import UTC, datetime

from pydantic import UUID4

from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.publishers import publish_cancel

logger = logging.getLogger("maestro")


async def invalidate_workflows(
    neo4j_tx: Neo4jTransaction, maestro_ids: list[UUID4], propagate: bool = False
) -> None:
    """"""
    if not maestro_ids:
        return
    logger.info(f"Invalidating workflows {maestro_ids} (propagate={propagate})")
    await neo4j_tx.query(
        """
        MATCH (wf:WorkflowModel)-->(j:Job)
        WHERE wf.maestro_id IN $maestro_ids AND j.status IN ['pending']
        SET j.status = 'cancelled'
        """,
        {
            "maestro_ids": maestro_ids,
        },
    )
    workflow_step_ids_result = await neo4j_tx.query(
        """
        MATCH (wf:WorkflowModel)
        WHERE wf.maestro_id IN $maestro_ids
        OPTIONAL MATCH (wf)--(:Job)-->(ws:WorkflowStepModel)
        SET wf.invalidated_at = datetime($datetime)
        RETURN ws.maestro_id as id_ws
        """,
        {
            "maestro_ids": maestro_ids,
            "datetime": datetime.now(UTC).isoformat(),
        },
    )
    # Filter out None values
    workflow_step_ids = list(
        filter(lambda x: x is not None, (record["id_ws"] for record in workflow_step_ids_result))
    )
    return await invalidate_workflow_steps(neo4j_tx, workflow_step_ids, propagate=propagate)


async def invalidate_workflow_steps(
    neo4j_tx: Neo4jTransaction, maestro_ids: list[UUID4], propagate: bool = False
) -> None:
    """Invalidating a workflow step involves invalidatings its outputs and stdout/stderr nodes."""
    if not maestro_ids:
        return
    logger.info(f"Invalidating workflow steps {maestro_ids} (propagate={propagate})")

    job_id_records = await neo4j_tx.query(
        """
        MATCH (n:WorkflowStepModel)<-[r]-(j:Job)
        WHERE n.maestro_id IN $maestro_ids AND j.status IN ['queued', 'running', 'halted']
        RETURN j.maestro_id
        """,
        {
            "maestro_ids": maestro_ids,
        },
    )
    for job_id_record in job_id_records:
        await publish_cancel(UUID4(job_id_record["j.maestro_id"]))

    # Set invalidated at for the WorkflowStep-node, and get connected artefact ids
    artefact_ids_result = await neo4j_tx.query(
        """
        MATCH (n:WorkflowStepModel)
        WHERE n.maestro_id IN $maestro_ids
        OPTIONAL MATCH (n)-[r]->(a:BaseArtefactModel)
        SET n.invalidated_at = datetime($datetime)
        RETURN a.maestro_id as id_a
        """,
        {
            "maestro_ids": maestro_ids,
            "datetime": datetime.now(UTC).isoformat(),
        },
    )

    # Invalidate all artefacts connected to the workflow step
    artefact_ids = list(
        filter(lambda x: x is not None, (record["id_a"] for record in artefact_ids_result))
    )
    return await invalidate_artefacts(neo4j_tx, artefact_ids, propagate=propagate)


async def invalidate_artefacts(
    neo4j_tx: Neo4jTransaction, maestro_ids: list[UUID4], propagate: bool = False
) -> None:
    """
    Invalidate artefacts and optionally propagate invalidation to dependent workflow steps.

    Args:
        neo4j_tx: Neo4j transaction
        maestro_ids: List of artefact maestro IDs to invalidate
        propagate: If True, invalidate workflow steps using these artefacts as input

    This will:
    1. Invalidate the specified artefacts and their directly related nested artefacts
    2. Invalidate workflow steps if all their (workflow step) outputs are invalidated
    3. Optionally propagate invalidation to dependent workflow steps
    """
    if not maestro_ids:
        return

    logger.info(f"Invalidating artefacts: {maestro_ids} (propagate={propagate})")
    current_time = datetime.now(UTC).isoformat()

    # Single query to:
    # 1. Invalidate artefacts and their nested artefacts
    # 2. Invalidate workflow steps with all outputs invalidated
    await neo4j_tx.query(
        """
        // Invalidate artefacts and collect nested ones
        MATCH (a:BaseArtefactModel)
        WHERE a.maestro_id in $maestro_ids
        OPTIONAL MATCH (a)-->(n:BaseArtefactModel)
        SET a.invalidated_at = datetime($datetime)
        SET n.invalidated_at = datetime($datetime)
        WITH COLLECT(DISTINCT a.maestro_id) + COLLECT(DISTINCT n.maestro_id) as all_artefact_ids

        // Find and invalidate workflow steps with all outputs invalidated
        OPTIONAL MATCH (a:BaseArtefactModel)<-[:OUTPUT]-(ws:WorkflowStepModel)-[:OUTPUT]->(out:BaseArtefactModel)
        WHERE a.maestro_id IN all_artefact_ids
        WITH ws, all_artefact_ids, COUNT(out) as output_count, COUNT(out.invalidated_at) as invalidated_count
        WHERE output_count = invalidated_count
        SET ws.invalidated_at = datetime($datetime)
        """,
        {
            "maestro_ids": maestro_ids,
            "datetime": current_time,
        },
    )

    if not propagate:
        return

    # Collect nested workflow step maestro IDs for propagation
    db_dependent_workflow_step_maestro_ids = await neo4j_tx.query(
        """
        OPTIONAL MATCH (art:BaseArtefactModel)-[:INPUT]->(dep_ws:WorkflowStepModel)
        WHERE art.maestro_id IN $maestro_ids
        RETURN dep_ws.maestro_id as dependent_ws_id
        """,
        {
            "maestro_ids": maestro_ids,
        },
    )
    # Filter and propagate to dependent workflow steps
    workflow_step_maestro_ids = [
        record["dependent_ws_id"]
        for record in db_dependent_workflow_step_maestro_ids
        if record["dependent_ws_id"] is not None
    ]

    if workflow_step_maestro_ids:
        await invalidate_workflow_steps(neo4j_tx, workflow_step_maestro_ids, propagate=True)
