from __future__ import annotations

import asyncio
import logging
import socket
from enum import Enum
from typing import TYPE_CHECKING, Any, Never
from uuid import UUID

from neo4j import AsyncGraphDatabase, Record
from pydantic import BaseModel, Field

from maestro.config import Config
from maestro.nats.nats import NATSSession
from maestro.neo4j.neo4j_migrations import run_migrations

if TYPE_CHECKING:
    from collections.abc import AsyncGenerator


logger = logging.getLogger("maestro")


async def provide_neo4j_transaction() -> AsyncGenerator[Neo4jTransaction, None]:
    async with Neo4jTransaction() as tx:
        yield tx


class Query(BaseModel):
    query: str
    parameters: dict = Field(default_factory=dict)


class TransactionWriteQueries(BaseModel):
    host: str
    server_address: str
    database_id: str
    queries: list[Query] = Field(default_factory=list)


def literal_params_map(m: dict) -> str:
    neo_params = {}
    for k, v in m.items():
        if isinstance(v, str | UUID):
            neo_params[k] = f'"{v}"'
        elif isinstance(v, int | float | bool):
            neo_params[k] = f"{v}"
        elif isinstance(v, Enum):
            neo_params[k] = f'"{v.value}"'
        elif v is None:
            # null in neo4j is the same as "does not exist",
            # so neo_params[k] = "null" won't work
            pass
        else:
            raise NotImplementedError(f"Type {type(v)} not supported")
    neo_params_str = ", ".join([f"{k}: {v}" for k, v in neo_params.items()])
    return f"{{{neo_params_str}}}"


def serialize_params(obj: BaseModel | dict | None) -> dict[str, Any]:
    if obj is None:
        return {}

    if isinstance(obj, BaseModel):
        return obj.model_dump(mode="json")

    class Serializeable(BaseModel):
        model_config = {"arbitrary_types_allowed": True, "extra": "allow"}

    return Serializeable(**obj).model_dump(mode="json")


class Neo4jTransaction:
    def __init__(
        self,
        uri: str | None = None,
        db_name: str | None = None,
        user: str | None = None,
        pwd: str | None = None,
        autocommit: bool = True,
    ):
        self._uri = uri or Config.NEO4J_URI
        self._db_name = db_name or Config.NEO4J_DATABASE
        self._user = user or Config.NEO4J_USER
        self._pwd = pwd or Config.NEO4J_PASSWORD
        self.autocommit = autocommit

    async def acquire_lock(self) -> None:
        """
        Acquire a distributed lock using Neo4j's built-in locking mechanism.

        The n_locked property value itself is not important - the MERGE operation
        is what provides the distributed locking by acquiring an exclusive lock on
        the __LockNode. Setting n_locked is just a side effect of needing to do
        a write operation to get the lock.
        """

        async def waiting_logger() -> Never:
            start_time = asyncio.get_running_loop().time()
            while True:
                await asyncio.sleep(5)
                logger.warning(
                    f"Waited {asyncio.get_running_loop().time() - start_time:.0f} seconds for Neo4j lock..."
                )

        async with asyncio.TaskGroup() as tg:
            waiting_logger_task = tg.create_task(waiting_logger())
            await self._tx.run(
                """
                MERGE (n:__LockNode)
                ON CREATE SET n.n_locked = true
                ON MATCH SET n.n_locked = true
                """
            )
            waiting_logger_task.cancel()
        logger.debug("Acquired Neo4j lock")

    async def __aenter__(self) -> Neo4jTransaction:  # noqa: PYI034
        """Open a connection to Neo4j - this is called when entering a context manager

        Slight overhead of 5-10 ms to fetch database ID and server address
        """
        # Run migrations if not already run (cached)
        run_migrations(self._uri, self._user, self._pwd)
        self._driver = AsyncGraphDatabase.driver(self._uri, auth=(self._user, self._pwd))
        self._session = self._driver.session()

        try:
            dbid = (
                await (await self._session.run("SHOW DATABASE maestro YIELD databaseID")).single()
            ).value()  # type: ignore[union-attr]
            # Acquire lock before starting transaction
            self._tx = await self._session.begin_transaction()
            await self.acquire_lock()
            try:
                self._tx_queries = TransactionWriteQueries(
                    server_address=str((await self._driver.get_server_info()).address),
                    host=socket.gethostname(),
                    database_id=dbid,
                )
                return self
            except:
                await self._tx.rollback()
                raise
        except:
            await self._session.close()
            await self._driver.close()
            raise

    async def __aexit__(self, exc_type: type[BaseException] | None, *args: object) -> bool:
        if exc_type is not None:
            logger.debug("Exception occured in context - rolling back transaction")
            await self.rollback()
            return False

        if not self._tx.closed():
            if self.autocommit:
                await self.commit()
            else:
                await self.rollback()
        return True

    async def commit(self) -> None:
        # Publish all queries that are not read queries to NATS
        if self._tx_queries.queries:
            try:
                async with NATSSession() as nats_session:
                    await nats_session.publish(
                        Config.NEO4J_SYNC_SUBJECT,
                        self._tx_queries.model_dump_json().encode(),
                        stream=Config.NEO4J_SYNC_STREAM,
                    )
            except BaseException:
                # If we can't publish to NATS, we rollback the transaction
                # to ensure that the data in Neo4j and NATS are in sync
                await self.rollback()
                raise

        await self._tx.commit()
        await self._session.close()
        await self._driver.close()
        del self._tx_queries

    async def rollback(self) -> None:
        await self._tx.rollback()
        await self._session.close()
        await self._driver.close()
        del self._tx_queries

    async def query(self, query: str, parameters: BaseModel | dict | None = None) -> list[Record]:
        assert (
            hasattr(self, "_tx") and not self._tx.closed()
        ), "No transaction - make sure you run this in a context manager (async with Neo4jTransaction as neo4j_tx: ...)"

        # Strip unnecessary whitespace and line shifts from query, and remove comments
        query = " ".join(line.split("//")[0].strip() for line in query.split("\n")).strip()

        parameters = serialize_params(parameters)
        records, summary, _ = await (
            await self._tx.run(query, parameters, database_=self._db_name)
        ).to_eager_result()

        # Keep a record of all queries that are not read queries,
        # to publish to NATS upon commit
        if summary.query_type != "r":
            self._tx_queries.queries.append(Query(query=query, parameters=parameters))

        return records
