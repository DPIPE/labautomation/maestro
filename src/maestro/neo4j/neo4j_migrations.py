import logging
from functools import cache
from pathlib import Path

from neo4j import GraphDatabase
from neo4j_python_migrations.executor import Executor
from neo4j_python_migrations.migration import Migration

logger = logging.getLogger("maestro")


# Run migrations, only once
@cache
def run_migrations(neo4j_uri: str, neo4j_user: str, neo4j_password: str) -> None:
    logger.info("Running migrations")

    def log_migration(migration: Migration) -> None:
        logger.info(
            f"Applied migration {migration.version} - {migration.description} (source: {migration.source})"
        )

    with GraphDatabase.driver(neo4j_uri, auth=(neo4j_user, neo4j_password)) as driver:
        executor = Executor(driver, migrations_path=Path(__file__).parent / "migrations")
        executor.migrate(on_apply=log_migration)

    logger.info("Migrations done")
