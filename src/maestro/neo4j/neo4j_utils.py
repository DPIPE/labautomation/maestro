import logging
from collections import defaultdict
from datetime import UTC, datetime
from typing import TypeVar, get_origin
from uuid import UUID

from neo4j import Record

from maestro.config import Config
from maestro.exceptions import InputNotFound
from maestro.models import (
    BaseArtefactModel,
    Job,
    MaestroBase,
    RelationshipEnum,
    SerializableBaseArtefactModelType,
    WorkflowStepModelWithArtefacts,
)
from maestro.neo4j.neo4j import Neo4jTransaction, literal_params_map

logger = logging.getLogger(__name__)

MaestroBaseType = TypeVar("MaestroBaseType", bound=MaestroBase)


class MultipleNodesFoundError(Exception):
    pass


async def fetch_node_by_rel(
    neo4j_tx: Neo4jTransaction,
    start_node: type[MaestroBase],
    params: dict,
    end_node: type[MaestroBaseType],
) -> MaestroBaseType | None:
    db_end_node = await neo4j_tx.query(
        f"MATCH (n:{':'.join(start_node.class_labels())} {literal_params_map(params)})--(m:{':'.join(end_node.class_labels())}) RETURN m.maestro_id as maestro_id"
    )
    if len(db_end_node) > 0:
        return await read_node(neo4j_tx, end_node, {"maestro_id": db_end_node[0]["maestro_id"]})
    else:
        return None


async def create_node(neo4j_tx: Neo4jTransaction, maestro_object: MaestroBase) -> UUID:
    """Create node(s) in Neo4j from a MaestroBase object. Will create nested nodes and relationships, if any."""

    logger.debug(f"Creating node in Neo4j: {maestro_object}")

    # Job object is a special case, where we need to handle revisions and params/inputs as collections
    if type(maestro_object) is Job:
        await merge_job_node(neo4j_tx, maestro_object)
        return maestro_object.maestro_id

    match_param = f'{{maestro_id: "{maestro_object.maestro_id}"}}'

    db_maestro_id = await neo4j_tx.query(
        f"""
        MERGE (n:{":".join(maestro_object.labels)} {match_param})
        ON CREATE SET n=$params
        RETURN n.maestro_id AS maestro_id
        """,
        {"params": maestro_object.model_dump_primitive()},
    )

    assert (
        str(maestro_object.maestro_id) == db_maestro_id[0]["maestro_id"]
    ), f"Maestro ID mismatch: {maestro_object.maestro_id} != {db_maestro_id[0]['maestro_id']} - how did this happen?"

    for iterable_field, iterable_value in maestro_object.get_nested_iterables().items():
        is_output = True
        iterable_field_info = maestro_object.model_fields.get(iterable_field)
        if iterable_field_info and RelationshipEnum.START in iterable_field_info.metadata:
            is_output = False
        for i, value in enumerate(iterable_value):
            # TODO: Move to MaestroBase model_validate
            assert isinstance(value, MaestroBase), f"Value {value} is not a MaestroBase object"
            await create_node(neo4j_tx, value)
            # By default, create_relationship` will create an outward directional relationship from the `maestro_object`.
            # This may be incorrect in some cases (for example, linking an INPUT artefact to a WorkflowStep artefact).
            # The direction of the relationship is set based on the metadata passed with fields that do not have
            #  default outward directional behavior.
            relationship_start_node, relationship_end_node = (
                (maestro_object.maestro_id, value.maestro_id)
                if is_output
                else (value.maestro_id, maestro_object.maestro_id)
            )

            # There could already be relationships added to this node, so we need to find the index offset
            # For sets, we don't want to keep track of the index (they are unordered). Adding an index offset
            # to this, would duplicate the relationships.
            if isinstance(iterable_value, set):
                attributes = None
            else:
                index_offset = (
                    await neo4j_tx.query(
                        f"MATCH (n) - [r:{iterable_field}] - (m:MaestroBase) WHERE n.maestro_id = $maestro_id RETURN count(r) as index_offset",
                        {"maestro_id": maestro_object.maestro_id},
                    )
                )[0]["index_offset"]

                attributes = {"relationship_index": i + index_offset}

            await create_relationship(
                neo4j_tx,
                relationship_start_node,
                relationship_end_node,
                iterable_field,
                attributes=attributes,
            )

    for nested_field, nested_value in maestro_object.get_nested().items():
        await create_node(neo4j_tx, nested_value)
        await create_relationship(
            neo4j_tx, maestro_object.maestro_id, nested_value.maestro_id, nested_field
        )

    return maestro_object.maestro_id


async def read_node(
    neo4j_tx: Neo4jTransaction,
    model: type[MaestroBaseType],
    params: dict,
    where_clauses: list[str] | None = None,
) -> MaestroBaseType | None:
    "Read node as MaestroBase object from Neo4j database - supports nested nodes"
    nodes = await read_nodes(neo4j_tx, model, params, where_clauses)
    if len(nodes) > 1:
        raise MultipleNodesFoundError(
            f"Multiple nodes found for {model.__name__} with params {params}"
        )
    return nodes[0] if nodes else None


async def read_nodes(
    neo4j_tx: Neo4jTransaction,
    model: type[MaestroBaseType],
    params: dict,
    where_clauses: list[str] | None = None,
) -> list[MaestroBaseType]:
    "Read nodes as MaestroBase object from Neo4j database - supports nested nodes"

    nested_fields: dict[str, list[type[MaestroBase]]] = defaultdict(list)
    for field, field_type in model.nested_fields():
        nested_fields[field].append(field_type)

    # collect all nested artefacts
    collect_query = """COLLECT{
        MATCH (n)-[r]-(m)
        WHERE type(r) in $nested_fields
        AND n.maestro_id <> m.maestro_id
        RETURN [
            r,
            m.maestro_id,
            apoc.coll.subtract(labels(m), apoc.convert.fromJsonList(m.additional_labels)) // class labels
        ]
    } AS nested_relationship_nodes"""

    where_clause = " AND ".join(where_clauses) if where_clauses else "True"

    db_nodes = [
        d
        for d in await neo4j_tx.query(
            f"""
            MATCH (n:{':'.join(model.class_labels())} {literal_params_map(params)})
            WHERE {where_clause}
            RETURN n as node, {collect_query}
            """,
            parameters={
                "nested_fields": list(nested_fields.keys()),
            },
        )
    ]

    nodes = []
    for db_node in db_nodes:
        db_node_data = dict(db_node["node"])

        nested_relationship_nodes = sorted(  # sort to preserve order of nested nodes
            db_node["nested_relationship_nodes"],
            key=lambda x: x[0].get("relationship_index", 0),
        )

        for (
            relationship,
            nested_node_maestro_id,
            labels,
        ) in nested_relationship_nodes:
            field = relationship.type

            # Get the correct subclass of the nested field - that is, the subclass that matches the
            # class labels obtained from the node
            nested_model = None
            for m in nested_fields[field]:
                subclass_matching_labels = m.get_subclass_matching_labels(labels)
                if subclass_matching_labels:
                    nested_model = subclass_matching_labels
                    break

            # We might not have access to all subclasses used to create the node (e.g. if called from the API)
            # In that case, we only support a single subclass for each nested field
            if not nested_model:
                if len(nested_fields[field]) > 1:
                    logger.info(
                        f"Unable to determine exact subclass to use for nested model. "
                        f"No registered subclass matches the labels of the node ({labels})."
                        "Using the first subclass that has a subset of the labels"
                    )

                try:
                    nested_model = next(
                        m for m in nested_fields[field] if set(m.class_labels()).issubset(labels)
                    )
                except StopIteration:
                    raise ValueError(
                        f"Unable to determine subclass to use for nested model for field '{field}' of '{model.__name__}'. "
                        f"No registered subclass matches the labels of the node ({labels})."
                    )

            nested_node = await read_node(
                neo4j_tx, nested_model, {"maestro_id": nested_node_maestro_id}
            )

            field_name = next(
                (key for key, fi in model.model_fields.items() if fi.alias == field), field
            )

            if get_origin(model.model_fields[field_name].annotation) in [list, tuple, set]:
                db_node_data.setdefault(field, []).append(
                    nested_node
                )  # We use a list here - if it is a tuple or set, pydantic will convert it
            else:
                db_node_data[field] = nested_node
        nodes.append(model(**db_node_data))

    return nodes


async def filter_duplicate_jobs(
    neo4j_tx: Neo4jTransaction, jobs: set[Job]
) -> tuple[set[Job], set[Job]]:
    """Filter jobs and return those that have no match in the database as well the matching
    ones from the database.
    """
    non_existing_jobs: set[Job] = set()
    existing_db_jobs: set[Job] = set()

    for job in jobs:
        existing_job = await get_valid_existing_job(neo4j_tx, job)
        if existing_job is None:
            non_existing_jobs.add(job)
        else:
            existing_db_jobs.add(existing_job)

    return non_existing_jobs, existing_db_jobs


async def get_valid_existing_job(neo4j_tx: Neo4jTransaction, job: Job) -> Job | None:
    """
    Check for, and return, valid existing job matching input job, where "valid" means

    1. The job has the same hash (i.e. same params, inputs, and workflow step)
    2. The job is in a valid state (not cancelled or failed)
    3. The job is not tied to an invalidated workflow_step
    """
    existing_job = await read_node(
        neo4j_tx,
        Job,
        {
            "hash": job.hash,
        },
        ["n.status IN ['queued', 'running', 'pending', 'halted', 'completed']"],
    )

    if existing_job:
        workflow_step = await fetch_node_by_rel(
            neo4j_tx, Job, {"maestro_id": existing_job.maestro_id}, WorkflowStepModelWithArtefacts
        )
        if workflow_step and workflow_step.invalidated_at:
            return None

    return existing_job


async def merge_job_node(neo4j_tx: Neo4jTransaction, job: Job) -> None:
    logger.debug(f"Create/Update job node in Neo4j: {job}")

    primitive_params = job.model_dump_primitive()

    # Jobs might be pushed to the queue multiple times, so we need to keep track of the number of times
    # the node has been updated due to the unknown order that NATS messages are being processed.
    # A job can be processed multiuple time when e.g. the same message is being published by
    # two different subscribers at the same time (or close to the same time),
    # causing the two identical messages to be processed by this function.
    # We keep track of the number of times the node has been updated, so that we do not
    # update the node if it has already been updated in the DB
    # (the DB node has a different revision than the incoming job object)
    # We don't want to create duplicate nodes, so we use MERGE instead of CREATE
    # The constraint on maestro_id ensures that we don't create duplicate nodes,
    # and this will raise an error if we try to create a duplicate node

    res = await neo4j_tx.query(
        """
        MERGE (n:Job:MaestroBase {maestro_id: $maestro_id, revision: $revision})
        ON CREATE SET n = $params, n.updated_at = datetime($datetime), n.created_at = datetime($datetime), n.revision = $increment,
        n.pending_timeout_at = datetime($datetime)+Duration({seconds: $pending_timeout})
        ON MATCH SET n.status = $status, n.updated_at = datetime($datetime), n.revision = n.revision + $increment
        RETURN n.maestro_id AS maestro_id, n.updated_at AS updated_at, n.created_at AS created_at, n.revision AS revision
        """,
        {
            "maestro_id": primitive_params["maestro_id"],
            "params": primitive_params,
            "status": primitive_params["status"],
            "revision": primitive_params["revision"],
            "pending_timeout": primitive_params["pending_timeout"],
            "datetime": datetime.now(UTC).isoformat(),
            # Only increment revision if the job is for this instance
            # If the job is for another instance, the other instance will increment the revision
            "increment": int(Config.INSTANCE_ID == job.instance_id),
        },
    )

    job.created_at = res[0]["created_at"].to_native()
    job.updated_at = res[0]["updated_at"].to_native()
    job.revision = res[0]["revision"]

    return None


async def create_relationship(
    neo4j_tx: Neo4jTransaction,
    start_maestro_id: UUID,
    end_maestro_id: UUID,
    relationship_type: str,
    attributes: dict | None = None,
) -> list[Record]:
    """Create relationship between two nodes in Neo4j. If attributes are provided, they will be added to the relationship."""
    logger.debug(f"Creating relationship in Neo4j: {start_maestro_id} -> {end_maestro_id}")
    if attributes is not None:
        items = []
        for k, v in attributes.items():
            items.append(f"{k}: {v}")
        attributes_str = "{" + ", ".join(items) + "}"
    else:
        attributes_str = ""
    return await neo4j_tx.query(
        f"""
        MATCH (a:MaestroBase)
        WHERE a.maestro_id = $start_maestro_id
        WITH a
        MATCH (b:MaestroBase)
        WHERE b.maestro_id = $end_maestro_id
        MERGE (a)-[:{relationship_type}{attributes_str}]->(b)
        """,
        {
            "start_maestro_id": start_maestro_id,
            "end_maestro_id": end_maestro_id,
        },
    )


async def fetch_input(
    neo4j_tx: Neo4jTransaction, input_class: type[BaseArtefactModel], input_filter: dict
) -> BaseArtefactModel:
    if "invalidated_at" in input_filter:
        raise ValueError("invalidated_at should not be passed to fetch_input")

    # Always filter by invalidated_at - only return nodes that are not invalidated
    where_clause = ["n.invalidated_at IS NULL"]
    input = await read_node(neo4j_tx, input_class, input_filter, where_clause)

    if input is None:
        raise InputNotFound(f"Input not found: {input_class}, {input_filter}")

    return input


async def fetch_inputs(
    neo4j_tx: Neo4jTransaction,
    inputs: dict[str, tuple[SerializableBaseArtefactModelType, dict]],
) -> dict[str, BaseArtefactModel]:
    return {
        arg: await fetch_input(neo4j_tx, input_class, input_filter)
        for arg, (input_class, input_filter) in inputs.items()
    }
