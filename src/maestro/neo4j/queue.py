from __future__ import annotations

from typing import TYPE_CHECKING

from maestro.models import Job, JobStatus
from maestro.neo4j.neo4j_utils import read_nodes

if TYPE_CHECKING:
    from maestro.neo4j.neo4j import Neo4jTransaction


async def get_pending_jobs(neo4j_tx: Neo4jTransaction) -> list[Job]:
    return await read_nodes(neo4j_tx, Job, {"status": JobStatus.PENDING})


async def get_all_jobs(neo4j_tx: Neo4jTransaction) -> list[Job]:
    return await read_nodes(neo4j_tx, Job, {})
