from typing import BinaryIO


class PrefixingStream:
    def __init__(self, stream: BinaryIO, prefix: str):
        self.stream: BinaryIO = stream
        self.prefix: bytes = prefix.encode()
        self.at_beginning_of_line: bool = True

    def write(self, chunk: bytes) -> int:
        for line in chunk.splitlines(True):
            self.stream.write(self.prefix + line if self.at_beginning_of_line else line)
            self.at_beginning_of_line = line.endswith(b"\n")
        return len(chunk)
