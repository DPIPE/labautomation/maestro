import asyncio
import inspect
import logging
import signal
from collections.abc import ValuesView
from uuid import UUID

from maestro.config import Config
from maestro.models import Job, JobStatus
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import merge_job_node
from maestro.subreaper import Subreaper
from maestro.worker import Worker

logger = logging.getLogger("maestro")


def _worker_can_run_now(job: Job, running_workers: ValuesView[Worker]) -> bool:
    """
    This function is called from the worker queue's loop to determine whether a new worker
    process can be started.
    """
    if len(running_workers) >= Config.MAX_WORKERS:
        return False
    if job.workflow_step_class.concurrency_limit is not None:
        return (
            len([w for w in running_workers if w.workflow_step == job.workflow_step_class])
            < job.workflow_step_class.concurrency_limit
        )
    return True


async def _sigchld_waiter(sigchld_notifier: asyncio.Condition) -> None:
    """
    This function is used to notify the active _process_worker() coroutines that a child
    process has exited. The use of asyncio.Event as incoming event mechanism enables it
    to be used in a signal handler which is not allowed to use await.
    The use of asyncio.Condition as outgoing event mechanism ensures that multiple active
    _process_worker() coroutines are all notified, which would not be guaranteed with
    asyncio.Event.
    """
    sigchld_event = asyncio.Event()
    # Register a signal handler for SIGCHLD to notify active _process_worker()
    # couroutines that a child process has exited.
    asyncio.get_running_loop().add_signal_handler(signal.SIGCHLD, sigchld_event.set)
    try:
        # We need to loop forever to handle multiple SIGCHLD from multiple child
        # worker processes.
        while True:
            await sigchld_event.wait()
            sigchld_event.clear()
            async with sigchld_notifier:
                sigchld_notifier.notify_all()
    finally:
        asyncio.get_running_loop().remove_signal_handler(signal.SIGCHLD)


class _WorkerQueue:
    def __new__(cls) -> "_WorkerQueue":
        if not hasattr(cls, "instance"):
            cls.instance = super().__new__(cls)
        return cls.instance

    def setup(self) -> None:
        "Reset the worker queue."
        # Priority-sorted list of jobs waiting to be started.
        self.queue: list[Worker] = []
        # Dictionary of currently running workers.
        self.running_workers: dict[asyncio.Task, Worker] = {}
        # Event triggered by add_to_queue() to wake up _loop().
        self.queue_event = asyncio.Event()
        # Flag to indicate that the worker queue is shutting down.
        self.shutting_down = False

    def run(self) -> asyncio.Task:
        "Run worker queue by starting the main loop in a task."
        self.loop_task = asyncio.create_task(self._loop_wrapper())
        return self.loop_task

    def add_to_queue(self, worker: Worker) -> None:
        """
        Add a worker to the worker queue.

        This uses asyncio.Event "queue_event" to wake up the loop running
        inside coroutine _loop().
        """
        if self.shutting_down:
            logger.info("Shutdown in progress, so not adding job to queue")
            return

        self.queue.append(worker)
        self.queue.sort(key=lambda worker: worker.job.priority, reverse=True)
        # Wake up _loop().
        self.queue_event.set()

    def shutdown_gracefully(self) -> None:
        """
        Schedule graceful shutdown of the worker queue.

        This will prevent new workers from starting.
        """
        logger.info("Graceful shutdown request received")
        self.shutting_down = True
        self.queue.clear()
        self.queue_event.set()

    async def cancel(self, maestro_id: UUID) -> None:
        """
        Cancel a worker.

        If job is still in queue, remove it.

        If job has been assigned to a running worker, cancel the worker's task.
        """
        for worker in self.queue:
            if worker.job.maestro_id == maestro_id:
                logger.info(f"Received request to cancel queued job {maestro_id}")
                async with Neo4jTransaction() as neo4j_tx:
                    worker.job.status = JobStatus.CANCELLED
                    await merge_job_node(neo4j_tx, worker.job)
                self.queue.remove(worker)
                return
        for task, worker in self.running_workers.items():
            if worker.job.maestro_id == maestro_id:
                logger.info(f"Received request to cancel running job {maestro_id}")
                task.cancel()
                try:
                    await task
                except asyncio.CancelledError:
                    pass
                return
        logger.warning(
            f"Received request to cancel job {maestro_id}, but not found in queue or running"
        )

    async def _loop(self, sigchld_notifier: asyncio.Condition, subreaper: Subreaper) -> None:
        """
        Main loop for the worker queue.

        Should not be called directly, but via _loop_wrapper().

        The loop is waiting for two types of events:
            1) a new job has been added to the wait queue,
            2) a worker has finished.

        In each case, a decision is made whether new workers can be started or not,
        based on jobs waiting in the wait queue.
        """
        async with asyncio.TaskGroup() as inner_task_group:
            queue_waiter = inner_task_group.create_task(self.queue_event.wait())
            while True:
                # Wait for either new jobs to be queued or running workers to finish.
                done_tasks, _ = await asyncio.wait(
                    {queue_waiter} | self.running_workers.keys(),
                    return_when=asyncio.FIRST_COMPLETED,
                )
                for task in done_tasks:
                    if task is queue_waiter:
                        # Something has been added to the queue, so reset the event
                        # and create a new waiter, like the one that was just done.
                        self.queue_event.clear()
                        queue_waiter = inner_task_group.create_task(self.queue_event.wait())
                    else:
                        # A worker has finished, so remove it from the running workers
                        # dictionary.
                        del self.running_workers[task]
                if not self.shutting_down:
                    self._start_queued_workers_that_can_run_now(
                        inner_task_group, sigchld_notifier, subreaper
                    )
                else:
                    if len(self.running_workers) > 0:
                        logger.info(f"Number of workers still running: {len(self.running_workers)}")
                    else:
                        logger.info("No running workers, so proceed with shutdown")
                        # Need to cancel "queue_waiter" so that "inner_task_group" context can exit.
                        queue_waiter.cancel()
                        try:
                            await queue_waiter
                        except asyncio.CancelledError:
                            pass
                        return

    def _start_queued_workers_that_can_run_now(
        self,
        inner_task_group: asyncio.TaskGroup,
        sigchld_notifier: asyncio.Condition,
        subreaper: Subreaper,
    ) -> None:
        """
        Go through the queued workers and start those that can run now.

        This function modifies "self.queue" and should not run concurrently with self.add_to_queue().
        This is ensured by having this function be synchronous.

        Ref: https://gitlab.com/DPIPE/labautomation/maestro/-/merge_requests/256
        """
        i = 0
        while i < len(self.queue):
            worker = self.queue[i]
            if _worker_can_run_now(worker.job, self.running_workers.values()):
                # Start worker.
                task = inner_task_group.create_task(worker.run(sigchld_notifier, subreaper))
                self.running_workers[task] = worker
                # Leave "i" unchanged, as the next job now moves up.
                self.queue.pop(i)
            else:
                i += 1

    # Barrier preventing the function from being changed to async.
    # See function's docstring for more information.
    assert not inspect.iscoroutinefunction(_start_queued_workers_that_can_run_now)

    async def _loop_wrapper(self) -> None:
        """
        Wrapper around the main loop for the worker queue.

        Sets up SIGCHLD signal handling before starting the main loop.
        """
        try:
            # Condition to be used to notify running _process_worker() tasks upon reception
            # of signal SIGCHLD.
            sigchld_notifier = asyncio.Condition()
            async with asyncio.TaskGroup() as outer_task_group:
                # SIGCHLD waiter task has to be kept in an outer TaskGroup to ensure that it
                # is cancelled AFTER all worker tasks in the inner TaskGroup have terminated,
                # because they depend on the SIGCHLD notification.
                sigchld_waiter = outer_task_group.create_task(_sigchld_waiter(sigchld_notifier))
                # Subreaper used to reap orphaned child processes.
                subreaper = Subreaper(sigchld_notifier)
                # Start a task to reap adopted child processes.
                child_subreaper = outer_task_group.create_task(subreaper.loop())
                # Run the loop.
                await self._loop(sigchld_notifier, subreaper)
                # When the loop terminates without raising, a graceful shutdown is ongoing. Cancel
                # "child_subreaper" and "sigchld_waiter" so that "outer_task_group" context can exit.
                child_subreaper.cancel()
                try:
                    await child_subreaper
                except asyncio.CancelledError:
                    pass
                sigchld_waiter.cancel()
                try:
                    await sigchld_waiter
                except asyncio.CancelledError:
                    pass
        finally:
            # Clean up in case of exceptions. The inner TaskGroup will have taken care of
            # canceling and waiting for any running workers to complete.
            self.running_workers.clear()


WorkerQueue = _WorkerQueue()
