import asyncio
import io
import os
import pickle
from collections.abc import Callable

from maestro.prefixing_stream import PrefixingStream


class MaestroPipe:
    """
    Context manager for creating a pipe, reading from it and closing it when done.
    """

    def __init__(self, stream: PrefixingStream | None = None, callback: Callable | None = None):
        """
        Initialize the pipe with either a stream and or a callback or both.

        When a stream is provided, the data received via the pipe will be written
        to the stream.

        When a callback is provided, the data received via the pipe will be unpickled
        and the callback will be called once for each unpickled object received.
        """
        self.stream = stream
        self.callback = callback

    def __enter__(self) -> "MaestroPipe":
        """
        Create a pipe consisting of two file descriptors for reading and writing.

        The read end file descriptor is added to the event loop for reading.

        A future is created to indicate end-of-file (EOF) when the write end of the pipe
        is closed.
        """
        self.data = bytearray()
        self.loop = asyncio.get_running_loop()
        self.eof = self.loop.create_future()
        self.read_end, self.write_end = os.pipe()
        self.loop.add_reader(self.read_end, self._on_readable)
        return self

    def _on_readable(self) -> None:
        """
        Callback for when the pipe is readable.

        Read a chunk of data and store it in "data" buffer.

        If "stream" is provided, write a copy of the chunk to the stream.

        If "callback" is provided, unpickle objects from the "data" buffer and
        call the callback once for each unpickled object. In this case, the data
        remaining in the "data" buffer contains only incomplete data which is
        not yet unpicklable.

        Set the EOF future when the read() returns an empty byte string.
        """
        keep_reading = False
        try:
            # Use a buffer size of 64 kB which is the typical capacity of a pipe.
            # Ref: https://man7.org/linux/man-pages/man7/pipe.7.html
            chunk = os.read(self.read_end, 1 << 16)
            if len(chunk) > 0:
                if self.stream is not None:
                    self.stream.write(chunk)
                self.data.extend(chunk)
                if self.callback is not None:
                    while True:
                        bio = io.BytesIO(self.data)
                        try:
                            obj = pickle.load(bio)
                        except Exception:
                            break
                        else:
                            del self.data[: bio.tell()]
                            self.callback(obj)
                keep_reading = True
            else:
                self.eof.set_result(None)
        except BaseException as e:
            self.eof.set_exception(e)
        finally:
            if not keep_reading:
                self.loop.remove_reader(self.read_end)

    def _close(self, attr: str) -> None:
        "Close the pipe file descriptor specified by the the provided name."
        os.close(getattr(self, attr))
        delattr(self, attr)

    def close_read_end(self) -> None:
        "Close the read end of the pipe."
        self._close("read_end")

    def close_write_end(self) -> None:
        "Close the write end of the pipe."
        self._close("write_end")

    def __exit__(self, *args: object) -> None:
        """
        Close pipe file descriptors still open.

        Remove the reader end file descritpor from the event loop before closing it.
        """
        if hasattr(self, "write_end"):
            self.close_write_end()
        if hasattr(self, "read_end"):
            self.loop.remove_reader(self.read_end)
            self.close_read_end()
        del self.eof
        del self.loop
        del self.data
