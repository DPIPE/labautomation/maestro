import builtins
import inspect
import json
from collections.abc import Generator
from datetime import datetime
from enum import Enum, IntEnum
from pathlib import Path
from types import UnionType
from typing import TYPE_CHECKING, Annotated, Any, get_origin, override
from uuid import uuid4

import typing_utils
from jinja2 import Template
from neo4j.time import DateTime
from pydantic import (
    UUID4,
    UUID5,
    BaseModel,
    ConfigDict,
    Field,
    NonNegativeInt,
    PlainSerializer,
    PlainValidator,
    TypeAdapter,
    ValidationError,
    ValidationInfo,
    ValidatorFunctionWrapHandler,
    WithJsonSchema,
    computed_field,
    field_validator,
    model_validator,
)

from maestro.config import Config

if TYPE_CHECKING:
    from maestro.workflow_step.workflow_step import WorkflowStepBase


class MaestroBase(BaseModel):
    """Base model for any Maestro model that is to be persisted in Neo4j"""

    invalidated_at: datetime | None = None
    maestro_id: UUID4 = Field(default_factory=lambda: uuid4(), description="Unique identifier")
    additional_labels: list[str] = Field(
        default=[], description="Additional labels to be used in Neo4j"
    )

    model_config = ConfigDict(populate_by_name=True)

    @computed_field  # type: ignore[prop-decorator]
    @property
    def type(self) -> str:
        "Returns the class name of the object - or last additional label if supplied"
        if self.model_extra and self.model_extra.get("type"):
            assert isinstance(self.model_extra["type"], str)
            return self.model_extra["type"]

        return self.__class__.__name__ if not self.additional_labels else self.additional_labels[-1]

    @computed_field  # type: ignore[prop-decorator]
    @property
    def labels(self) -> list[str]:
        """List of labels to be used in Neo4j. Includes the class name, superclasses class names, and additional labels if supplied."""
        return self.__class__.class_labels() + self.additional_labels

    @classmethod
    def class_labels(cls) -> list[str]:
        """List of labels to be used in Neo4j. Includes the class name, superclasses class names."""
        return [
            getattr(cls, "__node_label__", cls.__name__)
            for cls in cls.mro()
            if cls not in [BaseModel, object]
        ]

    @classmethod
    def get_subclass_matching_labels(cls, labels: list[str]) -> builtins.type["MaestroBase"] | None:
        """Returns the subclass of cls that has the same labels as the list of labels"""
        if set(cls.class_labels()) == set(labels):
            return cls
        for subclass in cls.__subclasses__():
            if set(subclass.class_labels()) == set(labels):
                return subclass

        # Recursively check subclasses of subclasses
        for subclass in cls.__subclasses__():
            result = subclass.get_subclass_matching_labels(labels)
            if result:
                return result
        return None

    @field_validator("*", mode="wrap")
    @classmethod
    def deserialize_collections(
        cls, value: Any, handler: ValidatorFunctionWrapHandler, info: ValidationInfo
    ) -> Any:
        """
        Collections coming from the database are stored as JSON strings, serialized in `model_dump_primitive`.

        This function attempts to run normal pydantic validation on all values, and if it fails, it tries to deserialize the value from a JSON string.
        """
        if not isinstance(value, str):
            return handler(value)

        try:
            return handler(value)
        except ValidationError:
            return handler(json.loads(value.replace("'", '"')))

    @field_validator("*", mode="before")
    @classmethod
    def neo4j_datetime_to_native(cls, v: Any) -> Any:
        """Converts Neo4j DateTime objects to native Python datetime objects."""
        if isinstance(v, DateTime):
            return v.to_native()
        return v

    def get_nested_iterables(
        self,
    ) -> dict[str, list["MaestroBase"] | tuple["MaestroBase"] | set["MaestroBase"]]:
        """Returns a dictionary {field_name: value} of lists or tuples consisting of MaestroBase object in the model.

        These are represented in the database as relationships, e.g.

        class Nested(MaestroBase):
            pass

        class Root(MaestroBase):
            HAS: list[Nested]

        nested1 = Nested()
        nested2 = Nested()

        obj = Root(nested=[nested1, nested2])
        obj.get_nested_iterables() -> {"nested": [nested1, nested2]}

        This will be represented in the database as a relationship of type `HAS` between obj and nested1, and between obj and nested2.

        Field alias takes precedence over field name
        """
        nested_iterables = {}
        for field, field_info in self.model_fields.items():
            v = getattr(self, field)
            if isinstance(v, list | tuple | set) and any(isinstance(i, MaestroBase) for i in v):
                # TODO: Move to model_validator
                assert all(isinstance(i, MaestroBase) for i in v)
                nested_iterables[field_info.alias or field] = v
        return nested_iterables

    def get_nested(self) -> dict[str, "MaestroBase"]:
        """Returns a dictionary {field_name: value} of MaestroBase objects in the model.

        These are represented in the database as relationships, e.g.

        class Nested(MaestroBase):
            pass

        class Root(MaestroBase):
            HAS: Nested

        nested = Nested()
        obj = Root(nested=nested)
        obj.get_nested() -> {"nested": nested}

        This will be represented in the database as a relationship of type `HAS` between obj and nested.

        Field alias takes precedence over field name
        """
        return {
            field_info.alias or field: getattr(self, field)
            for field, field_info in self.model_fields.items()
            if isinstance(getattr(self, field), MaestroBase)
        }

    def model_dump_primitive(self) -> dict[str, str]:
        """Dump all 'primitive' fields - meaning fields not containing MaestroBase objects or iterables of MaestroBase objects.

        Used for dumping the model to the database.

        Field alias takes precedence over field name
        """

        # Exclude fields that are nested iterables or nested objects ("non-primitive" fields)
        # We also exclude labels, since they are stored as Neo4j labels
        #
        # For example, a class like this (where Bar, Baz, Qux are MaestroBase subclasses):
        #
        # class Foo(MaestroBase):
        #     bar: Bar
        #     baz: list[Baz]
        #     qux: tuple[Qux] = Field(alias="quux")
        #     primitive: int
        #
        # would result in the following exclude set:
        #
        # exclude == {"labels", "bar", "baz", "quux"}
        #
        exclude = (
            set(["labels"])
            | set(self.get_nested_iterables().keys())
            | set(self.get_nested().keys())
            | set(dict(self.__class__.nested_fields()).keys())
        )
        # We need to exclude on field name, not alias. Extract field names from aliases, and modify exclude set
        alias_mapping = {v.alias: k for k, v in self.model_fields.items() if v.alias in exclude}
        exclude -= set(alias_mapping.keys())
        exclude |= set(alias_mapping.values())

        dump = {k: v for k, v in self.model_dump(by_alias=True, exclude=exclude).items()}

        # We need to convert collections (lists, tuples and dicts) to JSON strings to store them
        # as attributes on the node in the database
        for k, v in dump.items():
            if isinstance(v, tuple):
                v = list(v)
            if isinstance(v, list | dict):
                # Convert to JSON string - use TypeAdapter.dump_json instead of json.dumps to handle custom types
                dump[k] = TypeAdapter(type(v)).dump_json(v).decode().replace('"', "'")

        return dump

    @classmethod
    def nested_fields(cls) -> Generator[tuple[str, builtins.type["MaestroBase"]], None, None]:
        """
        Best effort to get which fields contain MaestroBase objects.

        Supports at least the following constructions:

        class Test(MaestroBase):
            pass

        class TestNested(MaestroBase):
            test: Test
            optional_test1: Test | None
            optional_test2: Optional[Test]
            tuple_test1: tuple[Test, Test]
            tuple_test2: tuple[Test, ...]
            list_test1: list[Test]
            optional_tuple_test1: Optional[tuple[Test, Test]]
            optional_tuple_test2: Optional[tuple[Test, ...]]
            alias_test: Test = Field(alias="alias_field") -> "alias_field", Test

        Yields tuples of (alias or field_name, Test)
        """
        for field, field_info in cls.model_fields.items():

            def recursive_get_classes(annotation: type[Any]) -> Generator[type[Any], None, None]:
                "Try to get all classes from an annotation"
                if get_origin(annotation) is type:
                    pass
                elif hasattr(annotation, "__args__"):
                    for arg in annotation.__args__:
                        yield from recursive_get_classes(arg)
                else:
                    if not inspect.isclass(annotation):
                        pass
                    elif issubclass(annotation, MaestroBase):
                        yield annotation

            assert field_info.annotation is not None
            maestro_base_subclasses = set(recursive_get_classes(field_info.annotation))

            for maeste_base_subclass in maestro_base_subclasses:
                yield field_info.alias or field, maeste_base_subclass

    @override
    def __hash__(self) -> int:
        return hash(self.maestro_id)


class RelationshipEnum(str, Enum):
    START = "start"
    END = "end"


class JobPriorityEnum(IntEnum):
    NORMAL = 1
    HIGH = 2
    URGENT = 3


class BaseArtefactModel(MaestroBase):
    @classmethod
    def subjects(cls) -> list[str]:
        """Subjects for NATS messages"""
        return [f"artefact.{cls.__name__}"]

    @property
    def properties(self) -> dict[str, Any]:
        "Utility method to get all properties of the model"
        return self.model_dump()

    # allow extra values to pass additional values for artefacts
    model_config = ConfigDict(
        extra="allow",
    )

    @staticmethod
    def class_serializer(x: type["BaseArtefactModel"]) -> str:
        """Return class name as string"""
        return x.__name__

    @staticmethod
    def class_validator(value: str | type["BaseArtefactModel"]) -> type["BaseArtefactModel"]:
        """Return class corresponding to input value - either class name or class itself"""
        from maestro.utils import get_recursive_subclasses

        if not isinstance(value, str):
            return value

        try:
            return next(
                cls for cls in get_recursive_subclasses(BaseArtefactModel) if cls.__name__ == value
            )
        except StopIteration:
            # Fallback to BaseArtefactModel if running in API
            if Config.IS_RUNNING_IN_API:
                return BaseArtefactModel
            raise ValueError(f"Artefact class '{value}' not found")

    @property
    def html(self) -> str:
        # Load default template from file
        with open(Path(__file__).parent / "templates" / "default_artefact.html") as f:
            template = Template(f.read())

        footer = (
            '<p style="text-align: center;font-size:xx-small;color:rgba(0,0,0,0.5);margin-top:0.5rem;">'
            "Template defined in BaseArtefactModel python model"
            "</p>"
        )

        return template.render(obj=self) + footer


# Annotated type to be used in place of type[BaseArtefactModel] in Pydantic models, with:
# - a serializer that serializes the class name
# - a validator that transforms the class name to the class itself
SerializableBaseArtefactModelType = Annotated[
    type[BaseArtefactModel],
    PlainSerializer(BaseArtefactModel.class_serializer),
    PlainValidator(BaseArtefactModel.class_validator),
    WithJsonSchema(BaseArtefactModel.model_json_schema()),
]


class FileArtefactModel(BaseArtefactModel):
    """
    Artefact for recording files

    Records absolute path, size

    Does _not_ record the file contents
    """

    path: Path

    @field_validator("path", mode="after")
    @classmethod
    def path_is_absolute(cls, v: Path) -> Path:
        return v.absolute()

    @computed_field  # type: ignore[prop-decorator]
    @property
    def size(self) -> int:
        try:
            return self.path.stat().st_size
        except FileNotFoundError:
            return -1

    @computed_field  # type: ignore[prop-decorator]
    @property
    def updated_at(self) -> datetime:
        if not self.path.exists():
            return datetime.fromtimestamp(0)

        return datetime.fromtimestamp(self.path.stat().st_mtime)


class StdErrModel(BaseArtefactModel):
    """Internal model for recording stderr output of a workflow step."""

    output: str = ""


class StdOutModel(BaseArtefactModel):
    """Internal model for recording stdout output of a workflow step."""

    output: str = ""


class WorkflowStepModel(MaestroBase):
    "Represents the workflow step with details resulting from a Job"

    creation_date: datetime = Field(default_factory=datetime.now)
    search_metadata: list = Field(default_factory=list)

    # allow extra values to pass additional params to workflow step process, etc.
    model_config = ConfigDict(
        extra="allow",
    )


class WorkflowStepModelWithStdOutAndStdErr(WorkflowStepModel):
    "Model for creating/reading a workflow step with stdout and stderr nodes"

    __node_label__ = "WorkflowStep"
    STDOUT: Annotated[StdOutModel, RelationshipEnum.END] | None = None
    STDERR: Annotated[StdErrModel, RelationshipEnum.END] | None = None


class WorkflowStepModelWithInput(WorkflowStepModel):
    "Model for creating/reading a workflow step with input artefact nodes"

    __node_label__ = "WorkflowStep"
    INPUT: Annotated[list[BaseArtefactModel], RelationshipEnum.START] = []


class WorkflowStepModelWithOutput(WorkflowStepModel):
    "Model for creating/reading a workflow step with output artefact nodes"

    __node_label__ = "WorkflowStep"
    OUTPUT: Annotated[list[BaseArtefactModel], RelationshipEnum.END] = []


class WorkflowStepModelWithArtefacts(
    WorkflowStepModelWithStdOutAndStdErr, WorkflowStepModelWithInput, WorkflowStepModelWithOutput
):
    "Model for creating/reading a workflow step with all artefact nodes"


class JobStatus(Enum):
    PENDING = "pending"
    QUEUED = "queued"
    RUNNING = "running"
    COMPLETED = "completed"
    FAILED = "failed"
    CANCELLED = "cancelled"
    HALTED = "halted"

    def is_processing(self) -> bool:
        """Check whether the job is currently being processed in the queue - pending jobs are not processing"""
        return self in (
            JobStatus.QUEUED,
            JobStatus.RUNNING,
            JobStatus.HALTED,
        )


class Job(MaestroBase):
    "Represents a job - in the queue or completed/failed/cancelled"

    workflow_step: (
        str  # TODO: Change to type[WorkflowStepBase] when possible (circular import problems)
    )
    instance_id: str = Config.INSTANCE_ID
    display_name: str | None = None
    status: JobStatus = JobStatus.PENDING
    priority: JobPriorityEnum = Field(
        default=JobPriorityEnum.NORMAL, description="priority level for job in int"
    )
    inputs: dict[str, tuple[SerializableBaseArtefactModelType, dict]] = Field(default_factory=dict)
    params: dict = Field(default_factory=dict)
    search_metadata: list = Field(default_factory=list)
    pending_timeout: int = Field(
        default=86400,
        description="max amount of time in seconds for which job can be in pending state",
    )
    running_timeout: int = Field(
        default=86400,
        description="max amount of time in seconds for which job can be in running state",
    )
    created_at: datetime | None = None  # This should only be set when the job is created in Neo4j
    updated_at: datetime | None = None  # This should only be set when the job is updated in Neo4j
    revision: NonNegativeInt = 0  # Used to keep track of the number of times a job has been updated

    @property
    def name(self) -> str:
        return self.display_name or self.workflow_step

    @property
    def name_and_id(self) -> str:
        return f"{self.name}|{self.maestro_id}"

    @computed_field  # type: ignore[prop-decorator]
    @property
    def hash(self) -> int:
        """Used when filtering out existing and valid jobs from workflow orders.

        Computed from the attributes inputs, params, and workflow_step.
        """

        def make_hashable(obj: object) -> object:
            """Recursively convert nested dictionaries to tuples of sorted items."""
            if isinstance(obj, dict):
                return tuple(sorted((k, make_hashable(v)) for k, v in obj.items()))
            elif isinstance(obj, list | set | tuple):
                return tuple(make_hashable(e) for e in obj)
            elif isinstance(obj, BaseModel):
                return tuple(sorted((k, make_hashable(v)) for k, v in obj.model_dump().items()))
            return obj

        inputs_hashable = tuple(sorted((k, make_hashable(v)) for k, v in self.inputs.items()))
        params_hashable = tuple(sorted((k, make_hashable(v)) for k, v in self.params.items()))
        return hash((inputs_hashable, params_hashable, self.workflow_step))

    @property
    def workflow_step_class(self) -> type["WorkflowStepBase"]:
        """Utility method to get the workflow step class - since Job.workflow_step is a string"""
        from maestro.workflow_step.workflow_step import WorkflowStepBase

        return WorkflowStepBase._registered_subclasses[self.workflow_step]

    @field_validator("workflow_step", mode="before")
    @classmethod
    def validate_workflow_step(cls, v: str | type["WorkflowStepBase"]) -> str:
        """
        Check that the workflow step is a valid subclass of WorkflowStepBase

        TODO: This should no longer be necessary if Job.workflow_step can be annotated as type[WorkflowStepBase] (similar to SerializableBaseArtefactModelType)
        """
        from maestro.workflow_step.workflow_step import WorkflowStepBase

        if isinstance(v, str) and v in WorkflowStepBase._registered_subclasses.keys():
            return v
        elif isinstance(v, type) and v in WorkflowStepBase._registered_subclasses.values():
            return v.__name__

        # Ignore validation if running in API
        if not Config.IS_RUNNING_IN_API:
            raise ValueError(f"Workflow step '{v}' not found")
        else:
            return v if isinstance(v, str) else v.__name__

    @field_validator("inputs", mode="after")
    @classmethod
    def validate_input_properties(
        cls, v: dict[str, tuple[SerializableBaseArtefactModelType, dict]]
    ) -> dict[str, tuple[SerializableBaseArtefactModelType, dict]]:
        """Check that properties are valid for the artefact class"""
        # Ignore validation if running in API
        if Config.IS_RUNNING_IN_API:
            return v

        for artefact_class, artefact_props in v.values():
            # Check that properties are valid
            for prop in artefact_props:
                if prop not in {
                    **artefact_class.model_fields,
                    **artefact_class.model_computed_fields,
                }:
                    raise ValueError(
                        f"Property '{prop}' not part of artefact class {artefact_class}"
                    )
                elif prop in dict(artefact_class.nested_fields()):
                    raise ValueError(
                        f"Property '{prop}' is a nested field for artefact class {artefact_class} - can not be used to filter inputs"
                    )

                # TODO: Check correct type of prop?

        return v

    @model_validator(mode="after")
    def validate_workflow_step_inputs(self) -> "Job":
        """
        Check that all requested inputs are valid for the workflow step's process-method
        """
        # Ignore validation if running in API
        if Config.IS_RUNNING_IN_API:
            return self

        if duplicated := set(self.inputs.keys()) & set(self.params.keys()):
            raise ValueError(
                f"Keys duplicated in both inputs and params: {sorted(list(duplicated))}"
            )
        model_args = set(self.inputs.keys()) | set(self.params.keys())
        required_inputs = dict(self.workflow_step_class._process_method_required_args())
        optional_inputs = dict(self.workflow_step_class._process_method_optional_args())
        required_args = set(required_inputs.keys())
        optional_args = set(optional_inputs.keys())

        if not model_args.issuperset(required_args):
            raise ValueError(
                f"Missing required arguments: {sorted(list(required_args - model_args))} for {self.workflow_step}.process"
            )
        if (
            not model_args.issubset(required_args | optional_args)
            and not self.workflow_step_class._process_method_has_kwargs()
        ):
            raise ValueError(
                f"Unknown arguments: {sorted(list(model_args - (required_args | optional_args)))} for {self.workflow_step}.process"
            )

        def issubtype(a: Any, b: Any) -> bool:
            if get_origin(b) is UnionType:
                return any(issubtype(a, x) for x in typing_utils.get_args(b))
            return bool(typing_utils.issubtype(a, b))

        for arg, (artefact_class, _) in self.inputs.items():
            if (
                arg not in required_args | optional_args
                and self.workflow_step_class._process_method_has_kwargs()
            ):
                # Ignore inputs going into **kwargs
                continue

            annotation = required_inputs.get(arg, optional_inputs.get(arg))

            if not issubtype(artefact_class, annotation):
                raise ValueError(
                    f"Artefact class '{artefact_class}' is not valid argument for {arg} ({annotation})"
                )
        for arg, value in self.params.items():
            if arg not in optional_inputs and arg not in required_inputs:
                # Ignore params going into **kwargs
                continue
            annotation = required_inputs.get(arg, optional_inputs.get(arg))
            # Use TypeAdapter to validate the value
            if inspect.isclass(annotation) and issubclass(annotation, BaseModel):
                coerced_value = annotation.model_validate(value)
            else:
                type_adapter = TypeAdapter(annotation)
                coerced_value = type_adapter.validate_python(value)
            try:
                self.params[arg] = coerced_value
            except ValidationError:
                raise ValueError(f"Value '{value}' is not valid argument for {arg} ({annotation})")

        return self


class WorkflowModel(MaestroBase):
    maestro_id: UUID4 | UUID5 = Field(
        default_factory=lambda: uuid4(), description="Unique identifier"
    )
    workflow_name: str
    jobs: set[Job] = Field(default_factory=set, alias="JOB")
    search_metadata: list = Field(default_factory=list)
