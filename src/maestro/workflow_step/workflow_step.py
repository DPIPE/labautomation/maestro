import asyncio
import inspect
import logging
from collections.abc import Generator
from typing import TYPE_CHECKING, Any, ClassVar, override
from weakref import WeakValueDictionary

from maestro.config import Config
from maestro.exceptions import SetupError
from maestro.models import Job, JobStatus
from maestro.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import merge_job_node, read_node
from maestro.utils import get_optional_args, get_required_args, has_kwargs
from maestro.workflow_step.abstract_workflow_step import AbstractWorkflowStep

logger = logging.getLogger("maestro")


if TYPE_CHECKING:
    from maestro.models import WorkflowStepModelWithArtefacts


class WorkflowStepBase(AbstractWorkflowStep):
    # Limit maximum number of concurrent jobs for this type of step. None means unlimited.
    concurrency_limit: ClassVar[int | None] = None

    # Collection of all WorkflowStep subclasses (not including direct descendants)
    # Use WeakValueDictionary to allow garbage collection of subclasses
    _registered_subclasses: WeakValueDictionary[str, type["WorkflowStepBase"]] = (
        WeakValueDictionary()
    )
    # Collection of all direct descendants of WorkflowStep (classes that can be subclassed to create an actual WorkflowStep)
    _direct_descendant_subclasses: dict[str, type[AbstractWorkflowStep]] = {}

    # Job instance associated with this workflow step
    _job: Job | None = None

    @override
    def __init_subclass__(cls) -> None:
        """Check that subclass is valid and register it"""
        # Do not run checks on direct descendants - these are abstract classes
        # as these are considered abstract classes
        if WorkflowStepBase in cls.__bases__:
            # No duplicate types (default: class name) allowed
            if cls.__name__ in WorkflowStepBase._direct_descendant_subclasses:
                raise SetupError(
                    f"WorkflowStep subclass with type '{cls.__name__}' already exists. Try renaming the class."
                )
            WorkflowStepBase._direct_descendant_subclasses[cls.__name__] = cls
            return

        # Check that subclass inherits WorkflowStep through only one ascendant
        # (i.e. not subclassing both FileWatcher and MessageWatcher)
        superclasses = [
            supercls
            for supercls in WorkflowStepBase._direct_descendant_subclasses.values()
            if issubclass(cls, supercls)
        ]
        if len(superclasses) != 1:
            raise SetupError(
                f"{cls.__name__} should inherit WorkflowStep through only one ascendant. Got: {superclasses}"
            )

        # Check that process method is implemented in subclass
        superclass = superclasses[0]
        if cls.process is superclass.process:
            raise SetupError(f"{superclass.__name__} subclasses must implement a process method")

        # Check that process function is async
        if not inspect.iscoroutinefunction(cls.process) or not inspect.isfunction(cls.process):
            raise SetupError(
                f"A {superclass.__name__}'s process method must be defined with `async def process(self, ...)`"
            )

        # Check signature of process method
        try:
            cls._check_process_signature()
        except SetupError:
            raise SetupError(f"Invalid process method signature for {cls.__name__}")

        # No duplicate types (default: class name) allowed
        if cls.__name__ in WorkflowStepBase._registered_subclasses:
            raise SetupError(
                f"WorkflowStep subclass with type '{cls.__name__}' already exists. Try renaming the class."
            )

        WorkflowStepBase._registered_subclasses[cls.__name__] = cls

    @override
    @classmethod
    def labels(cls) -> list[str]:
        return ["WorkflowStep"]

    @override
    @classmethod
    def subjects(cls) -> set[str]:
        return {"order.workflow_step.jobs", f"workflow_step.{cls.__name__}.done"}

    @classmethod
    def _process_method_required_args(cls) -> Generator[tuple[str, Any], None, None]:
        """Get required arguments for process method"""
        iter_params = iter(get_required_args(cls.process))
        next(iter_params)  # Skip self
        yield from iter_params

    @classmethod
    def _process_method_optional_args(cls) -> Generator[tuple[str, Any], None, None]:
        """Get optional arguments for process method"""
        yield from get_optional_args(cls.process)

    @classmethod
    def _process_method_has_kwargs(cls) -> bool:
        """Check if process method has **kwargs"""
        return has_kwargs(cls.process)

    @classmethod
    def _check_process_signature(cls) -> None:
        """Check that process method signature is valid"""
        sig = inspect.signature(cls.process)
        iter_params = iter(sig.parameters.items())
        next(iter_params)  # Skip self

        for arg, param in iter_params:
            if param.kind == param.VAR_POSITIONAL or param.kind == param.POSITIONAL_ONLY:
                # Signatures including *args or (arg1, arg2, /) are not allowed
                raise SetupError(f"Purely positional arguments are not allowed ({param})")
            elif param.kind == param.VAR_KEYWORD:
                # Ignore **kwargs
                continue
            elif param.annotation == param.empty:  # No type hint
                raise SetupError(f"Missing type hint for required argument '{arg}'")
            else:  # Optional
                if param.annotation == param.empty:
                    raise SetupError(f"Missing type hint for optional argument '{arg}'")

    async def publish(self, workflow_step: "WorkflowStepModelWithArtefacts") -> None:
        logger.debug(f"parsed payload from nats to neo {workflow_step}")
        async with NATSSession() as nats_session:
            for output_artefact in workflow_step.OUTPUT:
                await nats_session.publish(
                    f"artefact.{output_artefact.type}", str(output_artefact.maestro_id).encode()
                )
            await nats_session.publish(
                f"workflow_step.{type(self).__name__}.done",
                workflow_step.model_dump_json().encode(),
                stream=Config.STREAM,
            )

    async def halt(self) -> None:
        """Temporarily halt workflow step.

        Returns when job is no longer halted, i.e. either resumed or cancelled via the UI."""
        if self._job is None:
            raise ValueError(f"Failed to halt, job not set for {self}")

        job = self._job

        if job.status != JobStatus.RUNNING:
            raise ValueError(f"Failed to halt, job with maestro_id {job.maestro_id} is not running")

        logger.info(f"Halting job with maestro_id {job.maestro_id}")

        async with Neo4jTransaction() as neo4j_tx:
            job.status = JobStatus.HALTED
            await merge_job_node(neo4j_tx, job)

        # Periodically check if job is still halted
        while job.status == JobStatus.HALTED:
            await asyncio.sleep(2)

            async with Neo4jTransaction() as neo4j_tx:
                db_job = await read_node(neo4j_tx, Job, {"maestro_id": job.maestro_id})

            assert db_job is not None, f"Job with maestro_id {job.maestro_id} not found"
            job = db_job

        # Match job status and revision for queued job instance
        self._job.status = job.status
        self._job.revision = job.revision
