import inspect
import logging
import re
from pathlib import Path
from typing import ClassVar, override
from uuid import uuid4

from pydantic import UUID4
from watchfiles import BaseFilter, Change, DefaultFilter, awatch

from maestro.exceptions import SetupError
from maestro.models import FileArtefactModel, Job, JobPriorityEnum, WorkflowModel
from maestro.workflow_step.workflow_step import WorkflowStepBase
from maestro.workflow_utils import order_workflow

logger = logging.getLogger("maestro")


class AddedFilesFilter(DefaultFilter):
    "watchfiles filter for only added files"

    @override
    def __call__(self, change: "Change", path: str) -> bool:
        if change != Change.added:
            return False
        return super().__call__(change, path)


class FileWatcher(WorkflowStepBase):
    """
    Monitor specific paths for changes.

    Subclass this file with a path and a process method to create a watcher.

    Class variables:
        path: Path to watch
        pattern: Regex pattern to match absolute path of filenames
        filter: Filter to apply to watchfiles (default: AddedFilesFilter - only watch for added files)

    Wrapper around watchfiles.awatch. See documentation for more details: https://watchfiles.helpmanual.io/
    """

    filter: ClassVar[BaseFilter] = AddedFilesFilter()
    path: ClassVar[str | Path]
    pattern: ClassVar[str | None] = None
    _pattern: ClassVar[re.Pattern[str] | None] = None
    priority: ClassVar[JobPriorityEnum | None] = JobPriorityEnum.NORMAL

    @override
    def __init_subclass__(cls) -> None:
        if not hasattr(cls, "path"):
            raise SetupError("FileWatcher subclass must define path")
        if not isinstance(cls.path, str | Path):
            raise SetupError("FileWatcher path must be str or Path")
        if not Path(cls.path).exists():
            raise SetupError(f"FileWatcher path does not exist: {cls.path}")
        if cls.pattern is not None:
            try:
                cls._pattern = re.compile(cls.pattern)
            except Exception:
                raise SetupError(f"Invalid regex pattern: {cls.pattern}")
        if not isinstance(cls.priority, JobPriorityEnum):
            raise SetupError(f"{cls.__name__} priority must be 1, 2 or 3, not {cls.priority}")
        super().__init_subclass__()

    @override
    @classmethod
    def _check_process_signature(cls) -> None:
        """Check that process method signature is valid"""
        sig = inspect.signature(cls.process)
        if len(sig.parameters) != 2:
            raise SetupError(
                f"{cls.__name__}'s process method must have exactly 1 parameter (in addition to self)"
            )

    @override
    @classmethod
    def labels(cls) -> list[str]:
        return super().labels() + ["FileWatcher", cls.__name__]

    @classmethod
    def workflow_id_and_name(cls, path: Path) -> tuple[UUID4, str]:
        """
        This can be overridden to return a specific workflow_id and workflow_name,
        possibly derived from the path or file contents.
        """
        return uuid4(), f"{cls.__name__}_{path.stem}"

    @classmethod
    async def run(cls) -> None:
        if cls.path is None:
            raise SetupError("Path is not defined")
        # safely check and create path dir
        Path(cls.path).mkdir(parents=True, exist_ok=True)
        logger.info(f"Observing {cls}: {cls.path}")

        # Merge given filter and pattern filter
        def file_filter(change: Change, path: str) -> bool:
            return cls.filter(change, path) and (
                cls._pattern is None or bool(cls._pattern.match(path))
            )

        # Watch for changes in path - use force_polling to not rely on file system notifications
        # Consider other options for watching files: https://watchfiles.helpmanual.io/
        process_arg_name = next(cls._process_method_required_args())[0]
        async for changes in awatch(cls.path, watch_filter=file_filter, force_polling=True):
            for change, path in changes:
                # This is a *file watcher*, so we do not process directories
                if not Path(path).is_file():
                    continue
                logger.info(
                    f"{cls.__name__} processing change: {Change(change).name}, path: {path}"
                )

                artefact = FileArtefactModel(path=path)
                job = Job(
                    params={process_arg_name: artefact},
                    workflow_step=cls,
                    priority=cls.priority,
                )
                try:
                    workflow_maestro_id, workflow_name = cls.workflow_id_and_name(Path(path))
                    workflow = WorkflowModel(
                        maestro_id=workflow_maestro_id,
                        workflow_name=workflow_name,
                        jobs=[job],
                    )
                except Exception:
                    logger.exception(f"Unable to create workflow from {cls.__name__}")
                    continue

                await order_workflow(workflow)
