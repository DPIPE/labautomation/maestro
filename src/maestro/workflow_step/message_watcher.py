import logging
from typing import override

from maestro.workflow_step.workflow_step import WorkflowStepBase

logger = logging.getLogger("maestro")


class MessageWatcher(WorkflowStepBase):
    """
    Base class for watching NATS messages on "order.workflow_step.<type>"
    and "artefact.<artefact type>.*" (where artefact type is the input types)

    When an order is ready to process, the process method is called with the
    inputs as arguments by a worker.
    """

    @override
    @classmethod
    def labels(cls) -> list[str]:
        return super().labels() + ["MessageWatcher", cls.__name__]
