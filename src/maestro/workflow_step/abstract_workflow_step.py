from abc import ABC, abstractmethod
from collections.abc import Sequence
from typing import TYPE_CHECKING, Any

if TYPE_CHECKING:
    from maestro.models import BaseArtefactModel


class AbstractWorkflowStep(ABC):
    @classmethod
    @abstractmethod
    def labels(cls) -> list[str]: ...

    @classmethod
    @abstractmethod
    def subjects(cls) -> set[str]: ...

    @abstractmethod
    async def process(self, *args: Any, **kwargs: Any) -> Sequence["BaseArtefactModel"]: ...
