import os
import ssl
from pathlib import Path
from typing import Annotated, Any

from dotenv import load_dotenv
from pydantic import (
    BaseModel,
    DirectoryPath,
    FilePath,
    PlainValidator,
    PositiveFloat,
    PositiveInt,
    field_validator,
    model_validator,
)

load_dotenv()

AbsoluteFilePath = Annotated[FilePath, PlainValidator(lambda v: Path(v).absolute())]
AbsoluteDirectoryPath = Annotated[DirectoryPath, PlainValidator(lambda v: Path(v).absolute())]


class AppConfigError(Exception):
    pass


# AppConfig class with required fields, default values, type checking, and typecasting for int and bool values


class AppConfig(BaseModel):
    ENV: str = "production"
    INSTANCE_ID: str = "default"
    NATS_URL: list[str] = ["nats://localhost:4222"]
    NATS_TLS_CA_CERT: Path | None = None
    NATS_NKEYS_SEED: Path | None = None
    STREAM: str = "maestro"
    BUCKET: str = "maestro"
    BUCKET_LOCK: str = "lock"
    NEO4J_SYNC_STREAM: str = "neo4j-sync"
    NEO4J_SYNC_SUBJECT: str = "transaction-write"
    API_PORT: PositiveInt = 8000
    NEO4J_URI: str = "bolt://localhost:7687"
    NEO4J_DATABASE: str = "maestro"
    NEO4J_USER: str = "neo4j"
    NEO4J_PASSWORD: str = ""
    MAX_WORKERS: PositiveInt = (os.cpu_count() or 1) // 2
    GRACE_PERIOD: PositiveFloat = 1.0
    LOG_LEVEL: str = "INFO"
    LOG_DIR: AbsoluteDirectoryPath = Path("/tmp/")
    LOG_BACKUPS: PositiveInt = 5
    LOG_MAX_BYTES: int = 1024 * 1024 * 128
    LOAD_MODELS_FROM: list[AbsoluteFilePath] = []
    _IS_RUNNING_IN_API: bool = False

    @model_validator(mode="before")
    @classmethod
    def extract_prefixed_variables(cls, values: dict[str, Any]) -> dict[str, Any]:
        return {
            key.replace("MAESTRO_", ""): value
            for key, value in values.items()
            if key.startswith("MAESTRO_")
        }

    @property
    def IS_RUNNING_IN_API(self) -> bool:
        return self._IS_RUNNING_IN_API

    @IS_RUNNING_IN_API.setter
    def IS_RUNNING_IN_API(self, value: bool) -> None:
        self._IS_RUNNING_IN_API = value

    @property
    def LOG_FILE(self) -> Path:
        if self.IS_RUNNING_IN_API:
            return self.LOG_DIR / "maestro-api.log"
        else:
            return self.LOG_DIR / "maestro.log"

    """
    Map environment variables to class fields according to these rules:
      - Field won't be parsed unless it has a type annotation
      - Field will be skipped if not in all caps
      - Class field and environment variable name are the same
    """

    @field_validator("NATS_URL", "LOAD_MODELS_FROM", mode="before")
    @classmethod
    def split_url_string(cls, v: str | None) -> list[str] | None:
        if v is not None:
            return v.split(",")
        return v

    @property
    def nats_ssl_context(self) -> None | ssl.SSLContext:
        if not hasattr(self, "_nats_ssl_context"):
            if not self.NATS_TLS_CA_CERT:
                self._nats_ssl_context = None
            else:
                self._nats_ssl_context = ssl.create_default_context(ssl.Purpose.SERVER_AUTH)
                self._nats_ssl_context.load_verify_locations(self.NATS_TLS_CA_CERT)

        return self._nats_ssl_context

    def nats_connect_options(self) -> dict[str, Any]:
        return {
            "servers": self.NATS_URL,
            "tls": self.nats_ssl_context,
            "nkeys_seed_str": open(self.NATS_NKEYS_SEED).readline().strip()
            if self.NATS_NKEYS_SEED
            else None,
        }


# Expose Config object for app to import
Config = AppConfig.model_validate(os.environ)
