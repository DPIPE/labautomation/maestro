import asyncio
import inspect
from collections.abc import Callable, Coroutine, Generator
from typing import Any, TypeVar

from nats.aio.msg import Msg
from nats.js.client import JetStreamContext
from pydantic import UUID4

from maestro.config import Config
from maestro.models import JobStatus
from maestro.nats.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction, TransactionWriteQueries

T = TypeVar("T", bound=type)


def get_recursive_subclasses(arg: T) -> list[T]:
    """
    Get all subclasses of a class recursively

    Like `cls.__subclasses__()` but also includes subclasses of subclasses
    """

    def _recursive_subclasses(cls: T) -> Generator[T, None, None]:
        for subclass in cls.__subclasses__():
            yield subclass
            yield from _recursive_subclasses(subclass)

    return list(_recursive_subclasses(arg))


def get_required_args(func: Callable) -> Generator[tuple[str, Any], None, None]:
    """
    Get required arguments for a function
    """
    yield from (
        (arg, param.annotation if param.annotation != param.empty else Any)
        for arg, param in inspect.signature(func).parameters.items()
        if param.default == param.empty and param.kind != param.VAR_KEYWORD
    )


def get_optional_args(func: Callable) -> Generator[tuple[str, Any], None, None]:
    """
    Get optional arguments for a function
    """
    yield from (
        (arg, param.annotation if param.annotation != param.empty else Any)
        for arg, param in inspect.signature(func).parameters.items()
        if param.default != param.empty and param.kind != param.VAR_KEYWORD
    )


def has_kwargs(func: Callable) -> bool:
    """
    Check if function has **kwargs
    """
    return any(
        param.kind == param.VAR_KEYWORD for _, param in inspect.signature(func).parameters.items()
    )


async def multishield(aw: Coroutine) -> Any:
    """
    Protect provided awaitable "aw" from any number of cancellation attempts.

    asyncio.shield() protects a task from being cancelled, but it only works for one cancellation attempt.

    In case of cancellation, the last "CancelledError" is re-raised after the task has completed.
    """
    exception = None
    task = asyncio.create_task(aw)
    try:
        while True:
            try:
                return await asyncio.shield(task)
            except asyncio.CancelledError as e:
                exception = e
    finally:
        if exception:
            raise exception


async def subscribe_to_job_update(
    nats_session: NATSSession, maestro_id: UUID4, status: JobStatus, future: asyncio.Future
) -> JetStreamContext.PullSubscription:
    """
    Utility function to set a future when a job gets updated in Neo4j

    Subscribes to the NEO4J_SYNC_STREAM and sets the future when a job with the given maestro_id and status is updated in the database
    """

    async def callback(msg: Msg) -> None:
        tx = TransactionWriteQueries.model_validate_json(msg.data)
        for query in tx.queries:
            if (
                query.query.startswith("MERGE (n:Job:MaestroBase {maestro_id: $maestro_id")
                and query.parameters.get("maestro_id") == str(maestro_id)
                and query.parameters.get("status") == status.value
            ):
                # Set the future when the job is updated
                # However, we need to make sure that the job is actually updated in the database,
                # since this could be triggered before the Neo4j transaction is committed
                async with asyncio.timeout(5):
                    while True:
                        async with Neo4jTransaction() as neo4j_tx:
                            job = await neo4j_tx.query(
                                "MATCH (n:Job:MaestroBase {maestro_id: $maestro_id, status: $status}) RETURN n LIMIT 1",
                                {"maestro_id": str(maestro_id), "status": status.value},
                            )
                            if job:
                                future.set_result(True)
                                break
                            await asyncio.sleep(0.01)
        await msg.ack()

    await nats_session.add_or_update_stream(Config.NEO4J_SYNC_STREAM, [Config.NEO4J_SYNC_SUBJECT])
    sub = await nats_session.subscribe(
        Config.NEO4J_SYNC_STREAM,
        Config.NEO4J_SYNC_SUBJECT,
        callback,
        config={"deliver_policy": "new"},
    )
    return sub
