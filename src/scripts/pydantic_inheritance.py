import ast
from pathlib import Path

from pydantic import BaseModel


def collect_class_definitions(project_dir: Path) -> dict[str, ast.ClassDef]:
    """Find all class definitions in the project."""
    class_defs: dict[str, ast.ClassDef] = {}
    for file_path in project_dir.rglob("*.py"):
        with file_path.open("r") as f:
            tree = ast.parse(f.read(), filename=str(file_path))
            for node in ast.walk(tree):
                if isinstance(node, ast.ClassDef):
                    class_defs[node.name] = node
    return class_defs


def inherits_from_base_model(node: ast.ClassDef, class_defs: dict[str, ast.ClassDef]) -> bool:
    for base in node.bases:
        if isinstance(base, ast.Name):
            base_name = base.id
        elif isinstance(base, ast.Attribute):  # if superclass is specified with module prefix
            base_name = base.attr
        else:
            continue

        if base_name == BaseModel.__name__:
            return True

        base_class_node = class_defs.get(base_name)
        if base_class_node and inherits_from_base_model(base_class_node, class_defs):
            return True

    return False


def find_matching_rst_and_module(model: str, rst_files: list[Path]) -> tuple[Path, str]:
    """Find matching reST file for package which defines Pydantic model and the model's module."""
    model_parts = model.split(".")
    best_match = None
    best_match_length = 0

    for rst_file in rst_files:
        rst_name = rst_file.stem
        rst_parts = rst_name.split(".")

        match_length = 0
        for model_part, rst_part in zip(model_parts, rst_parts):
            if model_part == rst_part:
                match_length += 1
            else:
                break

        # Ensure the match is from the start and is the longest
        if match_length > best_match_length and match_length == len(rst_parts):
            best_match = rst_file
            best_match_length = match_length

    if best_match is None:
        raise ValueError(f"Could not find matching reST file for {model}")

    module = ".".join(model_parts[:-1])
    return best_match, module


def insert_directives(models: list[str], source_path: Path) -> None:
    """Insert autopydantic directives into appropriate reST file."""
    rst_files = list(source_path.glob("*.rst"))
    grouped_models: dict[tuple[Path, str], list[str]] = {}
    for model in models:
        rst_file, module = find_matching_rst_and_module(model, rst_files)
        key = (rst_file, module)
        grouped_models[key] = grouped_models.get(key, []) + [model]

    for (rst_file, module), models in grouped_models.items():
        with rst_file.open("r") as f:
            rst_lines = f.readlines()

        # Find line to insert directives
        pattern = ".. automodule:: " + ".".join(models[0].split(".")[:-1])
        automodule_match = False
        for index, line in enumerate(rst_lines):
            if automodule_match and line.strip() == "":
                break
            else:
                if pattern in line:
                    automodule_match = True

        if not automodule_match:
            raise ValueError(f"Could not find automodule directive for {models} in {rst_file}")

        # Avoid repeated class definitions
        str_insert = f"   :exclude-members: {', '.join([m.split(".")[-1] for m in models])}\n"

        # Show inherited fields and validators up until BaseModel
        for model in models:
            str_insert += (
                f"\n.. autopydantic_model:: {model}\n   :inherited-members: {BaseModel.__name__}\n"
            )

        rst_lines.insert(index, str_insert)
        with rst_file.open("w") as f:
            f.writelines(rst_lines)


def add_directives(project_dir: Path, source_path: Path) -> None:
    """Traverse project_dir and insert autopydantic directives for all Pydantic models in
    the reST file corresponding to the package where the models are defined.

    This will display all inherited model fields and validators in the reference
    documentation up until, but not including, Pydantic's BaseModel.

    See https://autodoc-pydantic.readthedocs.io/en/stable/users/faq.html#show-inherited-fields-validators
    """
    models: list[str] = []

    # Need all classes upfront to not miss any inheritance relationships
    class_defs = collect_class_definitions(project_dir)

    for file_path in project_dir.rglob("*.py"):
        traversed_nodes: set[ast.AST] = set()
        with file_path.open("r") as f:
            tree = ast.parse(f.read(), filename=str(file_path))
            for node in ast.walk(tree):
                if node in traversed_nodes:
                    continue

                # Skip class definitions within function scope
                if isinstance(node, ast.FunctionDef | ast.AsyncFunctionDef):
                    for child_node in ast.walk(node):
                        traversed_nodes.add(child_node)  # adds any class defs

                if isinstance(node, ast.ClassDef):
                    if inherits_from_base_model(node, class_defs):
                        module_path = (
                            file_path.relative_to(project_dir)
                            .with_suffix("")
                            .as_posix()
                            .replace("/", ".")
                        )
                        class_name = node.name
                        models.append(f"maestro.{module_path}.{class_name}")

    insert_directives(models, source_path)


if __name__ == "__main__":
    project_dir = Path(__file__).parents[1] / "maestro"
    source_path = Path(__file__).parents[2] / "docs/source/"
    add_directives(project_dir, source_path)
