#!/usr/bin/env python3

import asyncio
import hashlib
import inspect
import logging
import os
import time
from collections import namedtuple
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path
from random import randint
from typing import Type, override
from uuid import UUID

from maestro import run_workflow_steps
from maestro.config import Config
from maestro.models import BaseArtefactModel, FileArtefactModel, Job, WorkflowModel
from maestro.nats.nats import NATSSession
from maestro.neo4j.neo4j import Neo4jTransaction
from maestro.neo4j.neo4j_utils import create_node, read_nodes
from maestro.run import shutdown_workflow_steps
from maestro.worker_queue import WorkerQueue
from maestro.workflow_step.file_watcher import FileWatcher
from maestro.workflow_step.message_watcher import MessageWatcher
from maestro.workflow_utils import order_workflow
from nats.js.errors import NotFoundError
from pydantic import UUID4
from watchfiles import DefaultFilter

"""

Small menu driven program to demonstrate the use of Maestro

Workflow steps and inputs/outputs are defined as follows:
(FileWatcher) -> [InputA] -> (StepA) -> [OutputA] -> (StepB) -> [OutputB] -> (StepC) -> [OutputC]
                                                        |                            -> [OutputD]
                                                    [InputB]
"""

Path("/tmp/maestro-demo").mkdir(exist_ok=True)


def identifier_to_uuid(identifier: str) -> UUID4:
    """
    Convert a string to a UUID4

    Utility function to use the same uuid for all steps in a workflows, based on identifier
    """

    # Convert to md5 hash - 16 bytes
    b = hashlib.md5(identifier.encode()).digest()

    return UUID(bytes=b, version=4)


class ArtefactWithId(BaseArtefactModel):
    identifier: str


class FileWatcherSetup(FileWatcher):
    path = "/tmp/maestro-demo"
    filter = DefaultFilter()

    @classmethod
    def workflow_id_and_name(cls, path: Path) -> tuple[UUID4, str]:
        return identifier_to_uuid(path.stem), "workflow_" + path.stem

    @override
    async def process(self, input: FileArtefactModel):
        identifier = input.path.stem
        artefact = InputA(identifier=identifier)
        print(f"Created artefact: {artefact}")
        return [artefact]


class InputA(ArtefactWithId):
    @property
    def html(self):
        return f"<h1>Custom html for {self.__class__.__name__}:</h1><pre>{self.model_dump_json(indent=2)}</pre>"


class OutputA(ArtefactWithId): ...


class InputB(ArtefactWithId): ...


class OutputB(ArtefactWithId): ...


class OutputC(ArtefactWithId): ...


class OutputD(ArtefactWithId): ...


class StepA(MessageWatcher):
    @override
    async def process(self, input: InputA, **kwargs):
        # Sleep for a random amount of time to simulate a long running process
        t = randint(1, 5)
        time.sleep(t)

        # Run some code here to create the output
        # In this case, we just create a dummy output
        output = OutputA(identifier=input.identifier)
        return [output]


class StepB(MessageWatcher):
    @override
    async def process(self, inputB: InputB, outputA: OutputA, **kwargs):
        # Sleep for a random amount of time to simulate a long running process
        t = randint(1, 5)
        time.sleep(t)

        # Run some process here to create the output
        output = OutputB(identifier=inputB.identifier)
        return [output]


class StepC(MessageWatcher):
    @override
    async def process(self, input: OutputB, **kwargs):
        # Sleep for a random amount of time to simulate a long running process
        t = randint(1, 5)
        time.sleep(t)

        # Run some process here to create the output
        output1 = OutputC(identifier=input.identifier)
        output2 = OutputD(identifier=input.identifier)
        return [output1, output2]


class HaltedDemoStep(MessageWatcher):
    @override
    async def process(self, input: OutputB, **kwargs):
        # Sleep for a random amount of time to simulate a long running process
        t = randint(1, 5)
        time.sleep(t)

        # Run some process here to create the output
        output1 = OutputC(identifier=input.identifier)
        output2 = OutputD(identifier=input.identifier)

        # Halt the job
        await self.halt()

        return [output1, output2]


class EternalStep(MessageWatcher):
    async def process(self, **_):
        try:
            while True:
                await asyncio.sleep(1)
        except asyncio.CancelledError:
            logging.getLogger("maestro").info("Cancelled")
        os._exit(1)


async def order_steps(message_watchers: list[Type[MessageWatcher]], identifier: str):
    "Order workflow steps with given identifier"
    jobs = []
    for message_watcher in message_watchers:
        jobs.append(
            Job(
                inputs={
                    arg: (input_type.__name__, {"identifier": identifier})
                    for arg, input_type in message_watcher._process_method_required_args()
                },
                params={"foo": "bar"},
                workflow_step=message_watcher,
                search_metadata=[identifier, "some_special_keyword"],
            )
        )
    jobs_order = WorkflowModel(
        maestro_id=identifier_to_uuid(identifier),
        workflow_name=f"workflow_{identifier}",
        jobs=jobs,
        search_metadata=["workflow_key"],
    )
    await order_workflow(jobs_order)


async def create_artefact(ArtefactClass: Type[ArtefactWithId], identifier: str):
    "Create artefact with given identifier. Add to database, and publish to NATS"
    async with NATSSession() as nats_session:
        artefact = ArtefactClass(identifier=identifier)
        async with Neo4jTransaction() as neo4j_tx:
            await create_node(neo4j_tx, artefact)

        await nats_session.publish(
            f"artefact.{artefact.type}",
            str(artefact.maestro_id).encode(),
            stream=Config.STREAM,
        )


async def print_queue():
    "Print queue for each process"
    for workflow_step in MessageWatcher.__subclasses__():
        async with Neo4jTransaction() as neo4j_tx:
            jobs = await read_nodes(
                neo4j_tx, Job, {"workflow_step": workflow_step.__name__}
            )
        print(f"Jobs for {workflow_step.__name__}: {jobs}")


async def reset_all():
    "Clear all entries from Neo4j and delete streams and bucket from NATS"
    async with Neo4jTransaction() as neo4j_tx:
        await neo4j_tx.query(
            "MATCH (n) WHERE NOT n:`__Neo4jMigration` AND NOT n:`__NATSBridge` DETACH DELETE n"
        )
    async with NATSSession() as nats_session:
        await nats_session.delete_stream(Config.STREAM)


async def exit_gracefully():
    WorkerQueue.shutdown_gracefully()
    await WorkerQueue.loop_task
    await exit_immediately()


async def exit_immediately():
    try:
        await shutdown_workflow_steps()
    except asyncio.CancelledError:
        pass
    exit()


async def ainput(prompt: str = "") -> str:
    """
    Async input, to avoid blocking the event loop
    From https://gist.github.com/delivrance/675a4295ce7dc70f0ce0b164fcdbd798)
    """
    with ThreadPoolExecutor(1, "AsyncInput") as executor:
        return await asyncio.get_event_loop().run_in_executor(executor, input, prompt)


identifier = "initial"


async def set_identifier():
    global identifier
    identifier = await ainput("Enter identifier: ")
    return identifier


async def request_input():
    Input = namedtuple("Input", ["label", "callable"])

    print(f"Identifier: {identifier}")
    options = {
        "1": Input("Set identifier", set_identifier),
        "2": Input(
            "Touch input A-file",
            lambda: Path(f"/tmp/maestro-demo/{identifier}").touch(),
        ),
        "3": Input("Create input B", lambda: create_artefact(InputB, identifier)),
        "4": Input(
            "Order all workflow steps",
            lambda: order_steps([StepA, StepB, StepC], identifier),
        ),
        "5": Input(
            "Order workflow that never completes",
            lambda: order_steps([EternalStep], identifier),
        ),
        "6": Input(
            "Order workflow that halts",
            lambda: order_steps([StepA, StepB, HaltedDemoStep], identifier),
        ),
        "p": Input("Print queue", lambda: print_queue()),
        "r": Input("Reset", lambda: reset_all()),
        "q": Input("Exit gracefully", exit_gracefully),
        "x": Input("Exit immediately", exit_immediately),
    }

    print("Select an option")
    for i, option in options.items():
        print(f"{i}: {option.label}")

    choice = await ainput("Enter choice: ")
    if choice not in options:
        print(f"Invalid choice: '{choice}'")
        return await request_input()
    return options[choice]


async def main():
    try:
        await reset_all()
    except NotFoundError:
        pass
    await run_workflow_steps()

    while True:
        option = await request_input()
        x = option.callable()
        if inspect.isawaitable(x):
            await x


async def populate(saturate=False, N=100):
    try:
        await reset_all()
    except NotFoundError:
        pass
    task = await run_workflow_steps()
    global identifier

    async def run_workflow():
        await create_artefact(InputB, identifier)
        await order_steps([StepA, StepB, StepC], identifier)
        Path(f"/tmp/maestro-demo/{identifier}").touch()

    def sleep_for():
        if saturate:
            return 0.01
        return randint(1, 5)

    for i in range(1, N + 1):
        await asyncio.sleep(sleep_for())
        identifier = f"identifier_{i}"
        await run_workflow()
    # uncomment the following if you want to keep creating workflows
    # while True:
    #     await asyncio.sleep(randint(20, 150))
    #     i += 1
    #     identifier = f"identifier_{i}"
    #     await run_workflow()

    await task


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Maestro demo", formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument(
        "action",
        choices=["run", "populate", "saturate"],
        help="\n".join(
            [
                "Action to perform:",
                "- run: run the demo with a terminal menu",
                "- populate: populate the demo with N workflows",
                "- saturate: populate the demo with N workflows, with almost no time between workflows",
            ]
        ),
    )
    parser.add_argument(
        "-N",
        type=int,
        nargs="?",
        default=100,
        help="Number of workflows to populate/saturate with (default: 100)",
    )
    args = parser.parse_args()

    if args.action == "run":
        asyncio.run(main())
    elif args.action == "populate":
        asyncio.run(populate(N=args.N))
    elif args.action == "saturate":
        asyncio.run(populate(saturate=True, N=args.N))

    import sys

    if len(sys.argv) > 1:
        try:
            N = int(sys.argv[2])
        except IndexError:
            N = 100
        if sys.argv[1] == "populate":
            asyncio.run(populate(N=N))
        elif sys.argv[1] == "saturate":
            asyncio.run(populate(saturate=True, N=N))
        else:
            raise ValueError(f"Invalid argument: {sys.argv[1]}")
    else:
        asyncio.run(main())
