import asyncio
import time
from pathlib import Path
from random import randint

from maestro import run_workflow_steps
from maestro import BaseArtefactModel, FileArtefactModel, Job, WorkflowModel
from maestro import order_workflow
from maestro import FileWatcher, MessageWatcher
from watchfiles import DefaultFilter

"""
TODO: Rewrite to minimal example
"""

"""

Small menu driven program to demonstrate the use of Maestro

Workflow steps and inputs/outputs are defined as follows:
[InputA] -> (StepA) -> [OutputA] -> (StepB) -> [OutputB] -> (StepC) -> [OutputC]
                                          |
                                       [InputB]

"""

Path("/tmp/maestro-demo").mkdir(exist_ok=True)


class BaseExampleArtefact(BaseArtefactModel):
    identifier: str


class FileWatcherSetup(FileWatcher):
    path = "/tmp/maestro-demo"
    filter = DefaultFilter()

    async def process(self, input: FileArtefactModel):
        identifier = input.path.stem
        artefact = InputA(identifier=identifier)
        print(f"Created artefact: {artefact}")
        return [artefact]


class InputA(BaseExampleArtefact): ...


class OutputA(BaseExampleArtefact): ...


class InputB(BaseExampleArtefact): ...


class OutputB(BaseExampleArtefact): ...


class OutputC(BaseExampleArtefact): ...


class StepA(MessageWatcher):
    async def process(self, input: InputA, **kwargs):  # type: ignore[override]
        # Sleep for a random amount of time to simulate a long running process
        t = randint(1, 5)
        time.sleep(t)

        # Run some code here to create the output
        # In this case, we just create a dummy output
        output = OutputA(identifier=input.identifier)
        return [output]


class StepB(MessageWatcher):
    async def process(self, outputA: OutputA, **kwargs):  # type: ignore[override]
        # Sleep for a random amount of time to simulate a long running process
        t = randint(1, 5)
        time.sleep(t)

        # Run some process here to create the output
        output = OutputB(identifier=OutputA.identifier)
        return [output]


class StepC(MessageWatcher):
    async def process(self, input: OutputB, **kwargs):  # type: ignore[override]
        # Sleep for a random amount of time to simulate a long running process
        t = randint(1, 5)
        time.sleep(t)
        # Run some process here to create the output
        output = OutputC(identifier=input.identifier)
        return [output]


async def main():
    await run_workflow_steps()
    "Order workflow steps with given identifier"
    jobs = []
    identifier = "example_workflow"
    for message_watcher in (StepA, StepB, StepC):
        jobs.append(
            Job(
                inputs={
                    arg: (input_type.__name__, {"identifier": identifier})
                    for arg, input_type in message_watcher._process_method_required_args()
                },
                params={"foo": "bar"},
                workflow_step=message_watcher,
                search_metadata=[identifier, "some_special_keyword"],
            )
        )
    jobs_order = WorkflowModel(
        workflow_name=f"workflow_{identifier}",
        jobs=jobs,
        search_metadata=["workflow_key"],
    )
    await order_workflow(jobs_order)


if __name__ == "__main__":
    asyncio.run(main())
