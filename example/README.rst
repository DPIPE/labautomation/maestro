Maestro example
===============

Please copy the entire ``example/`` directory and follow the instructions in :doc:`getting_started` if you would like to quickly set up Maestro.

To become familiar with the library, we recommend reading through this README or the :doc:`concepts`.

The ``example/`` directory contains the bare minimum to get started:

.. code-block:: text

    example/
    |--config/neo4j.conf
    |--docker-compose.yml
    |--Dockerfile
    |--example.py
    |--pyproject.toml
    |--README.md

``config/neo4j.conf``
---------------------

The ``config`` directory is used to set the project's configurations. In this case, it contains the `Neo4j <https://neo4j.com>`_ configuration settings, as Maestro utilizes `Neo4j <https://neo4j.com>`_ as its backend database.

``pyproject.toml``
------------------

We will use `Poetry <https://python-poetry.org>`_ to manage all our Python dependencies, you can add more using :code:`poetry add PackageName`. Please note the Python version and the branch that Maestro is configured to use, which will remain set to ``dev`` until the first release.

``Dockerfile``
--------------

This file defines the image that will be built using Docker, along with the necessary components to set up the project.

``docker-compose.yml``
----------------------

We define all the services we need for our project in ``docker-compose.yml``

1. NATS - Maestro uses `NATS <https://nats.io>`_ for messaging, which is a critical component for Maestro to function properly.
2. Neo4j - As mentioned previously, we need the Neo4j service for the backend database.
3. example - This is the main container where we will place all our code. This will also be responsible for the API and front end.

``example.py``
--------------

In this Python file we use Maestro components to define and order workflows. It is recommended that this file be read thoroughly to understand how to use Maestro.

``README.rst``
--------------

It's this file!